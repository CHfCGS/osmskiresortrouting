﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElevationServer {
    public class List2DArrayWrapper : IReadOnlyList<byte> {
        private byte[][] data;
        private int xOffset;
        private int yOffset;
        private int size;
        private int step;

        public byte this[int index] {
            get {
                int valIndex = index / 2;
                int subIndex = index % 2;
                int xIndex = valIndex / size;
                int yIndex = valIndex % size;
                return data[xIndex * step + xOffset][yIndex * step * 2 + yOffset + subIndex];
            }
        }

        public int Count => size * size * 2;

        public List2DArrayWrapper(byte[][] data, int xOffset, int yOffset, int size, int step) {
            this.data = data;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            this.size = size;
            this.step = step;
        }

        public IEnumerator<byte> GetEnumerator() {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator() {
            throw new NotImplementedException();
        }
    }
}
