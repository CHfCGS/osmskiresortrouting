﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ElevationServer {
    public class WebEndpoint {
        public const int MIN_ZOOM = 8;
        public const int MAX_ZOOM = 13;
        public const int MIN_RES = 15;
        public const int MAX_RES = 20;
        private static readonly byte[] zeroPayload = { 0, 0 };

        public static async Task HandleRequest(HttpContext context) {
            var request = context.Request;
            var rqBody = request.Body;
            var response = context.Response;
            var reBody = response.Body;

            var bodyData = new byte[10];

            if (await rqBody.ReadAsync(bodyData, 0, bodyData.Length) != bodyData.Length) { //not enough request data: reject request
                response.StatusCode = 400;
                return;
            }

            var zoom = bodyData[0]; //retrieve parameters from request, see api-format.txt
            var x = (bodyData[1] << 24) | (bodyData[2] << 16) | (bodyData[3] << 8) | bodyData[4];
            var y = (bodyData[5] << 24) | (bodyData[6] << 16) | (bodyData[7] << 8) | bodyData[8];
            var res = bodyData[9];

            //check for invalid request parameters
            if (zoom < MIN_ZOOM || zoom > MAX_ZOOM || x < 0 || x >= (1 << zoom) || y < 0 || y >= (1 << zoom) || res < MIN_RES || res > MAX_RES) {
                response.StatusCode = 400;
                return;
            }

            response.StatusCode = 200; //request is OK: retrieve data

            var tileX = x >> (zoom - MIN_ZOOM); //X coordinate of MIN_ZOOM tile
            var tileY = y >> (zoom - MIN_ZOOM); //Y coordinate of MIN_ZOOM tile
            var xIndex = x % (1 << (zoom - MIN_ZOOM)); //X sub-coordinate of requested tile within MIN_ZOOM tile
            var yIndex = y % (1 << (zoom - MIN_ZOOM)); //Y sub-coordinate of requested tile within MIN_ZOOM tile
            var rawSize = 1 << (MAX_RES - zoom); //Edge length of requested tile (samples) in the raw full resolution
            var edgeWidth = 1 << (res - MIN_RES); //Requested resolution sample width of the edge of additional samples included for gradient preservation
            var size = (1 << (res - zoom)) + 1 + 2 * edgeWidth; //Edge length of requested tile (samples) in the requested resolution, including redundant tile edge and gradient preservation edges
            var step = 1 << (MAX_RES - res); //Iteration step size for requested resolution

            var tile = await Task.Run(() => TileCache.GetData(tileX, tileY)); //Get MIN_ZOOM tile
            if (tile == null) { //If null: Data doesn't exist, send zero response
                await reBody.WriteAsync(zeroPayload, 0, zeroPayload.Length);
                return;
            }

            var xOffset = xIndex * rawSize; //X Offset of requested data in MIN_ZOOM, MAX_RES array
            var yOffset = yIndex * rawSize * 2; //Y Offset of requested data in MIN_ZOOM, MAX_RES array (*2 because each value is two bytes)
            var sizeArr = new byte[] { (byte)(size >> 8), (byte)size }; //Encode and send edge length of requested tile (samples)
            await reBody.WriteAsync(sizeArr, 0, sizeArr.Length);
            /* toggle for compression
            if (step == 1) for (int i = xOffset; i <= xOffset + rawSize; i++) await reBody.WriteAsync(tile[i], yOffset, 2 * rawSize + 2); //if MAX_RES: quickly write array section to response
            else { //if lower resolution: step through array and write values one at a time
                for (int i = xOffset; i <= xOffset + rawSize; i += step) {
                    for (int j = yOffset; j < yOffset + rawSize * 2 + 2; j += 2 * step) {
                        await reBody.WriteAsync(tile[i], j, 2);
                    }
                }
            }/*/
            var arrayWrapper = new List2DArrayWrapper(tile, xOffset, yOffset, size, step); //create wrapper to extract subtile at correct resolution
            await ElevationDataCompression.CompressHeightData(arrayWrapper, reBody); //compress and send data
            //*/
        }
    }
}
