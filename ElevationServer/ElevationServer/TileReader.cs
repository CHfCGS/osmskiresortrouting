﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ElevationServer {
    public class TileReader {
        private const string FILENAME_FORMAT = "{0}-{1}-{2}.bin";
        private const string FOLDER_PATH = "data";

        public static byte[][] LoadData(int x, int y) {
            var path = Path.Combine(FOLDER_PATH, string.Format(FILENAME_FORMAT, WebEndpoint.MIN_ZOOM, x, y));
            if (!File.Exists(path)) return null; //file not present: no data for this tile

            var edgeLength = (1 << (WebEndpoint.MAX_RES - WebEndpoint.MIN_ZOOM)) + 1 + 2 * (1 << (WebEndpoint.MAX_RES - WebEndpoint.MIN_RES)); //edge length of data in samples
            var doubleEdgeLength = edgeLength * 2;

            byte[][] data = new byte[edgeLength][];
            using (var f = File.OpenRead(path)) { //read data into array
                for (int i = 0; i < edgeLength; i++) {
                    data[i] = new byte[doubleEdgeLength];
                    f.Read(data[i], 0, doubleEdgeLength);
                }
            }
            return data;
        }
    }
}
