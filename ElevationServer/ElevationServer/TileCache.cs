﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;

namespace ElevationServer {
    public class TileCache {
        private const int MAX_CACHE_SIZE = 20; //tiles ~32MB each

        private static readonly object _lock = new object();

        private static uint requestCounter = 0;

        //(lat, long) -> (points, lastUsed)
        private static readonly Dictionary<(int, int), (byte[][], uint)> cache = new Dictionary<(int, int), (byte[][], uint)>();

        /// <summary>
        /// Gets a MIN_ZOOM tile of elevation data points.
        /// </summary>
        /// <param name="x">OSM tile X coordinate</param>
        /// <param name="y">OSM tile Y coordinate</param>
        /// <returns></returns>
        public static byte[][] GetData(int x, int y) {
            var key = (x, y);
            byte[][] data;
            lock (_lock) {
                if (cache.ContainsKey(key)) { //tile already present in cache:
                    var pair = cache[key]; //get from cache
                    pair.Item2 = requestCounter++; //last used: now
                    return pair.Item1;
                }
                data = TileReader.LoadData(x, y); //otherwise: load from disk
                cache[key] = (data, requestCounter++); //add to cache
                if (cache.Count > MAX_CACHE_SIZE) RemoveOldestFromCache(); //remove oldest data from cache if required
            }
            return data;
        }

        private static void RemoveOldestFromCache() {
            (int, int)? oldestKey = null;
            uint oldest = uint.MaxValue;
            foreach (var (key, val) in cache) { //find cached tile that has not been used for the longest time
                if (val.Item2 < oldest) {
                    oldest = val.Item2;
                    oldestKey = key;
                }
            }
            if (oldestKey.HasValue) cache.Remove(oldestKey.Value); //remove tile from cache
        }
    }
}
