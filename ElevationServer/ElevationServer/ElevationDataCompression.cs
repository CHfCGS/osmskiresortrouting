﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ElevationServer {
    public class ElevationDataCompression {

        /**
         * The left 5 bits are used to encode the total difference.
         * 00001 = 1, ..., 11111 = 31
         */
        private const byte MAJOR_ADD = 0b00000_101;

        /**
         * The left 5 bits are used to encode the total difference.
         * 00001 = 1, ..., 11111 = 31
         */
        private const byte MAJOR_SUB = 0b00000_001;

        /**
         * The left 6 bits are used to encode the amount of data points.
         * 000001 = 1, ..., 111111 = 63
         */
        private const byte MINOR_SET = 0b000000_11;

        /**
         * The left 6 bits are used to encode amount how often the data point occurs
         * in the uncompressed array
         * 000001 = 1, ..., 111111 = 63
         */
        private const byte MINOR_SET_MULTIPLE = 0b000000_10;

        /**
         * Compresses an uncompressed height data array
         */
        public static async Task CompressHeightData(IReadOnlyList<byte> data, Stream outStream) {
            int length = data.Count;

            int writeIndex = 8; //4 + 4 bytes to store uncompressed & compressed byte array size
            byte[] compressedData = new byte[length * 2 + 100];

            byte lastMajorByte = 0;

            int setMinorStartIndex = -1;
            byte valuesCounter = 0;

            bool modeMultiple = false;
            byte modeMultipleMinorByte = 0;

            for (int i = 0; i < length; i += 2) {
                byte currentMajorByte = data[i];
                byte currentMinorByte = data[i + 1];

                // Major Byte has changed -> Handle
                if (currentMajorByte != lastMajorByte) {

                    // Finish MINOR_SET / MINOR_SET_MULTIPLE
                    if (setMinorStartIndex != -1) {
                        compressedData[setMinorStartIndex] |= (byte)(valuesCounter << 2);

                        setMinorStartIndex = -1;
                        modeMultiple = false;
                        valuesCounter = 0;
                    }

                    // Add 31 until difference is smaller than 31
                    while (currentMajorByte - lastMajorByte > 31) {
                        compressedData[writeIndex++] = MAJOR_ADD | (31 << 3);
                        lastMajorByte += 31;
                    }

                    // Subtract 31 until difference is smaller than 31
                    while (lastMajorByte - currentMajorByte > 31) {
                        compressedData[writeIndex++] = MAJOR_SUB | (31 << 3);
                        lastMajorByte -= 31;
                    }

                    int diff = lastMajorByte - currentMajorByte;

                    if (diff < 0) {
                        compressedData[writeIndex++] = (byte)(MAJOR_ADD | ((-diff) << 3));
                    }
                    if (diff > 0) {
                        compressedData[writeIndex++] = (byte)(MAJOR_SUB | ((diff) << 3));
                    }

                    lastMajorByte = currentMajorByte;
                }

                // Finish MINOR_SET / MINOR_SET_MULTIPLE
                if ((valuesCounter >= 63 || (modeMultiple && modeMultipleMinorByte != currentMinorByte))
                        && setMinorStartIndex != -1) {

                    compressedData[setMinorStartIndex] |= (byte)(valuesCounter << 2);

                    setMinorStartIndex = -1;
                    modeMultiple = false;
                    valuesCounter = 0;
                }

                // Check if MINOR_SET_MULTIPLE should be used
                if (!modeMultiple && i + 5 < length && currentMinorByte == data[i + 1 + 2] && currentMinorByte == data[i + 1 + 4]
                        && currentMajorByte == data[i + 2] && currentMajorByte == data[i + 4]) {

                    // Finish MINOR_SET
                    if (setMinorStartIndex != -1) {
                        compressedData[setMinorStartIndex] |= (byte)(valuesCounter << 2);
                    }

                    setMinorStartIndex = -1;
                    modeMultiple = true;
                    modeMultipleMinorByte = currentMinorByte;
                }

                if (setMinorStartIndex == -1) {
                    valuesCounter = 0;

                    setMinorStartIndex = writeIndex;

                    if (modeMultiple) {
                        compressedData[writeIndex++] = MINOR_SET_MULTIPLE;
                        compressedData[writeIndex++] = modeMultipleMinorByte;
                    } else {
                        compressedData[writeIndex++] = MINOR_SET;
                    }
                }

                valuesCounter++;

                if (!modeMultiple) {
                    compressedData[writeIndex++] = currentMinorByte;
                }
            }

            // Finish MINOR_SET / MINOR_SET_MULTIPLE
            if (setMinorStartIndex != -1) {
                compressedData[setMinorStartIndex] |= (byte)(valuesCounter << 2);
            }

            // Write uncompressed byte array size
            compressedData[0] = (byte)(length >> 24);
            compressedData[1] = (byte)(length >> 16);
            compressedData[2] = (byte)(length >> 8);
            compressedData[3] = (byte)length;

            // Write compressed byte array size
            compressedData[4] = (byte)(writeIndex >> 24);
            compressedData[5] = (byte)(writeIndex >> 16);
            compressedData[6] = (byte)(writeIndex >> 8);
            compressedData[7] = (byte)writeIndex;

            await outStream.WriteAsync(compressedData, 0, writeIndex);
        }
    }
}
