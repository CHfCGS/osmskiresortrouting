# OSM Ski Resort Routing

In this project we use OpenStreetMap data to implement routing on ski slopes.

## Setup and run project
- Open Android Studio and load the project via "Open an existing Android Studio project".
- After project has been loaded, you can choose between running the program as desktop app or android app.

### Desktop:
- To start the DesktopLauncher, you have to right-click on DesktopLauncher in desktop/java/de.uni.stuttgart.skipisten.desktop of the project structure.
- Press DesktopLauncher.main() in the dialog box.

### Android:
- To run the android app, you have to choose "android" in the "Edit Run/Debug configurations" dialog box.
- Choose your virtual device or the connected device in the next dialog box.
- Run the app via clicking the "Run" button.

## Backend components:

### Overpass API

We use the Overpass API to query OSM data.\
Currently this Overpass instance is used:
https://lz4.overpass-api.de/api/interpreter

(This can be changed in 
`de.uni.stuttgart.skipisten.data.overpass.OverpassRequest`)

### OSM Tiles

We use an OSM tile provider for our visualisation.\
Currently this tile provider instance is used:
https://tiles.fmi.uni-stuttgart.de

(This can be changed in 
`de.uni.stuttgart.skipisten.Main`)

### Elevation Server

The elevation server is implemented and hosted by us.

**Attention**:
We only host a limited set of data.
Our server only provides height data for the alps.
Therefore only ski regions in the alps will load
with the current provided height data. 

(The server can be changed in 
`de.uni.stuttgart.skipisten.data.elevation.ElevationTileLoader`)

#### Elevation Server Implementation
The code of our elevation server is included as a C# project
in the folder `ElevationServer/`

The server contains a `data/` folder which needs to be filled with converted height data files.

These files can be generated using the ElevationTranscoder, included in the folder `ElevationTranscoder/`.
The configuration can be changed in the source code.
As input data raw SRTM elevation data must be provided.
