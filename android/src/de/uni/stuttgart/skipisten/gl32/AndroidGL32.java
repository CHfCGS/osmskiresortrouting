package de.uni.stuttgart.skipisten.gl32;

import android.annotation.TargetApi;
import android.opengl.GLES30;
import android.opengl.GLES31;
import android.os.Build;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.utils.GdxRuntimeException;
import lib.gui.util.gl32.GL32;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class AndroidGL32 implements GL32 {
    @Override
    public void glTexImage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        GLES31.glTexStorage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations);
    }

    @Override
    public int pixmapFormatToSizedGLFormat(Pixmap.Format format) {
        if (format == Pixmap.Format.RGB565) return GLES30.GL_RGB565;
        if (format == Pixmap.Format.RGBA4444) return GLES30.GL_RGBA4;
        if (format == Pixmap.Format.RGB888) return GLES30.GL_RGB8;
        if (format == Pixmap.Format.RGBA8888) return GLES30.GL_RGBA8;
        throw new GdxRuntimeException("Unknown Format: " + format);
    }
}
