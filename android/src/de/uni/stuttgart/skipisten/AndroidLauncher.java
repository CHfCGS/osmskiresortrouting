package de.uni.stuttgart.skipisten;

import android.os.Build;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import lib.gui.util.gl32.GL32Manager;

import de.uni.stuttgart.skipisten.gl32.AndroidGL32;

public class AndroidLauncher extends AndroidApplication {

    private static final boolean useGL30 = true;

    private Main main;
    private Runnable permissionCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String language = getResources().getString(R.string.language);
        main = new Main(language, new AndroidGPSPositionHandler(this));

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.numSamples = 2;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && useGL30) {
            config.useGL30 = true;
            GL32Manager.setGL32(new AndroidGL32());
        }

        initialize(main, config);
    }

    @Override
    public void onBackPressed() {
        main.onBackPressed();
    }

    public void requestPermissions(String[] permissions, Runnable permissionCallback) {
        this.permissionCallback = permissionCallback;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, 0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        permissionCallback.run();
    }

}
