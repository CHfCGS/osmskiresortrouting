package de.uni.stuttgart.skipisten;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;

public class AndroidGPSPositionHandler implements GPSPositionHandler, LocationListener {

    private AndroidLauncher androidLauncher;
    private LocationManager locationManager;

    private Location currentLocation;

    private long lastCheckTimeForLocation;

    public AndroidGPSPositionHandler(AndroidLauncher androidLauncher) {
        this.androidLauncher = androidLauncher;
        locationManager = (LocationManager) androidLauncher.getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public boolean isGPSSupported() {
        return true;
    }

    @Override
    public boolean hasGPSPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return androidLauncher.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    androidLauncher.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    @Override
    public void askForGPSPermission(Runnable callback) {
        if (hasGPSPermission()) {
            callback.run();
        }

        androidLauncher.requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, callback);
    }

    @Override
    public boolean isGPSEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void start() {
        if (!hasGPSPermission())
            throw new RuntimeException("Accessing GPS location is not granted");

        androidLauncher.runOnUiThread(this::startInUIThread);
    }

    @SuppressLint("MissingPermission")
    private void startInUIThread() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, this);
        useLastKnownLocation();
    }

    @SuppressLint("MissingPermission")
    private void useLastKnownLocation() {
        if (!hasGPSPermission())
            return;

        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (currentLocation == null) {
            currentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
    }

    @Override
    public void stop() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public boolean hasGPSLocation() {
        if(currentLocation == null && System.currentTimeMillis() - lastCheckTimeForLocation > 1000 && isGPSEnabled()) {
            lastCheckTimeForLocation = System.currentTimeMillis();
            useLastKnownLocation();
        }

        return currentLocation != null;
    }

    @Override
    public boolean hasGPSLocation(long maxAgeInSeconds) {
        return hasGPSLocation() && getAgeOfLocationInSeconds() <= maxAgeInSeconds;
    }

    @Override
    public long getAgeOfLocationInSeconds() {
        if (!hasGPSLocation()) {
            return Integer.MAX_VALUE;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return (SystemClock.elapsedRealtimeNanos() - currentLocation.getElapsedRealtimeNanos()) / 1_000_000_000L;
        }

        return (System.currentTimeMillis() - currentLocation.getTime()) / 1000L;
    }

    @Override
    public double getLatitude() {
        return currentLocation.getLatitude();
    }

    @Override
    public double getLongitude() {
        return currentLocation.getLongitude();
    }

    @Override
    public double getAltitude() {
        return currentLocation.getAltitude();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.currentLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
