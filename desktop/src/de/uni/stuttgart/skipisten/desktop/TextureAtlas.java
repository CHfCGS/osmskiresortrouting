package de.uni.stuttgart.skipisten.desktop;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.tools.bmfont.BitmapFontWriter;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import lib.gui.GUIConfiguration;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class TextureAtlas {

    /**
     * If this is true, the default characters are added to the texture atlas,
     * otherwise characters from the String fontCharacters.
     */
    public static final boolean useDefaultFontCharacters = true;
    /**
     * Only characters which are contained in this string can be used when rendering texts
     */
    public static final String fontCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static final String fontFilePath = "resources/library/font/font.ttf";
    public static final String drawableDirPath = "resources/drawable";

    public static final String generatedFilesDir = "resources/generated";

    public static final String textureAltasLibraryImages = "resources/library/drawable";
    public static final String textureAltasInputDirPath = "resources/generated/drawable";
    public static final String textureAltasOutputDirPath = "android/assets/drawable";
    public static final String textureAltasOutputFileName = "atlas";


    public static final String createdFontFntFilePath = "android/assets/" + GUIConfiguration.fontFntFilePath;
    public static final String createdFontPngFileDir = "resources/generated/drawable";
    public static final String createdFontPngFileName = GUIConfiguration.fontTextureRegionName;

    public static void main(String[] args) {
        new LwjglApplication(new ApplicationAdapter() {
            public void create() {
                runTextureAtlas();
            }
        });
    }

    private static void runTextureAtlas() {
        if (Gdx.files.absolute(generatedFilesDir).exists()) {
            System.out.println("There is already a generated directory. Please delete it.");
            System.exit(-1);
        }

        int padding = 2;

        // from https://github.com/libgdx/libgdx/wiki/Hiero#bitmapfontwriter
        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();

        if (!useDefaultFontCharacters) {
            param.characters = fontCharacters;
        }

        param.size = GUIConfiguration.fontResolution;
        param.packer = new PixmapPacker(2048, 1024,
                Pixmap.Format.RGBA8888, padding, false,
                new PixmapPacker.SkylineStrategy());

        System.out.println("Loading font.");

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.absolute(fontFilePath));
        FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(param);


        System.out.println("Creating font.fnt and font.png files");

        BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
        info.padding = new BitmapFontWriter.Padding(padding, padding, padding, padding);
        BitmapFontWriter.writeFont(data, new String[]{"font.png"}, Gdx.files.absolute(createdFontFntFilePath), info, 512, 512);
        BitmapFontWriter.writePixmaps(param.packer.getPages(), Gdx.files.absolute(createdFontPngFileDir), createdFontPngFileName);

        System.out.println("Creating " + GUIConfiguration.whitePixelsTextureRegionName);

        createWhitePixelsPng(new File(textureAltasInputDirPath + "/" + GUIConfiguration.whitePixelsTextureRegionName + ".png"));

        System.out.println("Creating Texture Atlas");

        copyPNGs(new File(textureAltasLibraryImages), new File(textureAltasInputDirPath));
        copyPNGs(new File(drawableDirPath), new File(textureAltasInputDirPath));

        TexturePacker.Settings s = new TexturePacker.Settings();
        s.edgePadding = true;
        s.paddingX = 2;
        s.paddingY = 2;
        s.maxHeight = 1024 * 4;
        s.maxWidth = 1024 * 4;
        s.combineSubdirectories = true;

        TexturePacker.process(s, textureAltasInputDirPath, textureAltasOutputDirPath, textureAltasOutputFileName);

        // Clean up
        FileHandle generatedDir = Gdx.files.absolute(generatedFilesDir);

        if (generatedDir.exists() && generatedDir.isDirectory()) {
            generatedDir.deleteDirectory();
            System.out.println("Delete old generated directory");
        }

        System.exit(0);
    }

    private static void createWhitePixelsPng(File filePath) {

        BufferedImage image = new BufferedImage(5, 5, BufferedImage.TYPE_INT_RGB);

        int color = Color.WHITE.getRGB();

        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getWidth(); y++) {
                image.setRGB(x, y, color);
            }
        }

        try {
            ImageIO.write(image, "png", filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyPNGs(File dirFrom, File dirTo) {

        File[] list = dirFrom.listFiles();

        for (int i = 0; i < list.length; i++) {
            if (list[i].isFile()) {
                try {
                    if (list[i].getAbsolutePath().endsWith(".png")) {

                        File output = new File(dirTo.getAbsolutePath() + "/" + list[i].getName());

                        System.out.println("Creating " + output.getAbsolutePath());

                        BufferedImage source = ImageIO.read(list[i]);

                        ImageIO.write(source, "png", output);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        for (String name : dirFrom.list()) {

            File fileInner = new File(dirFrom.getAbsolutePath() + "/" + name);

            if (fileInner.isDirectory()) {
                File innerDirTo = new File(dirTo.getAbsolutePath() + "/" + name);

                if (!innerDirTo.exists()) {
                    innerDirTo.mkdir();
                }

                copyPNGs(fileInner, innerDirTo);
            }
        }

    }
}
