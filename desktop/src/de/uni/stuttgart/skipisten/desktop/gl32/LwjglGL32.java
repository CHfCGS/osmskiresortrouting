package de.uni.stuttgart.skipisten.desktop.gl32;

import com.badlogic.gdx.graphics.Pixmap;
import lib.gui.util.gl32.GL32;

public class LwjglGL32 implements GL32 {
    @Override
    public void glTexImage2DMultisample(int target, int samples, int internalformat, int width, int height, boolean fixedsamplelocations) {
        org.lwjgl.opengl.GL32.glTexImage2DMultisample(target, samples, internalformat, width, height, fixedsamplelocations);
    }

    @Override
    public int pixmapFormatToSizedGLFormat(Pixmap.Format format) {
        return Pixmap.Format.toGlFormat(format);
    }
}
