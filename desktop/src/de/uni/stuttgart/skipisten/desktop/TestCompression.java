package de.uni.stuttgart.skipisten.desktop;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import lib.gui.util.Stopwatch;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import de.uni.stuttgart.skipisten.data.elevation.compression.ElevationDataCompression;

public class TestCompression {

    public static void main(String[] args) {
        new LwjglApplication(new ApplicationAdapter() {
            public void create() {
                testCompression();
            }
        });
    }

    public static void testCompression() {
        Stopwatch swRead = new Stopwatch();

        String file;

        // file = "ElevationServer/ElevationServer/data/9-254-174.bin";
        file = "ElevationServer/ElevationServer/data/9-272-179.bin";
        // file = "ElevationServer/ElevationServer/data/9-272-180.bin";

        byte[] contentRaw = Gdx.files.local(file).readBytes();
        int contentPtr = 0;
        short[][] content = new short[2049][2049];
        for(int i = 0; i <= 2048; i++) {
            for (int j = 0; j <= 2048; j++) {
                content[i][j] = (short)((contentRaw[contentPtr++] << 8) | contentRaw[contentPtr++]);
            }
        }

        swRead.stop();

        System.out.println("UNCOMPRESSED: " + (contentRaw.length / 1000) + " k");

        ByteArrayOutputStream os = new ByteArrayOutputStream(contentRaw.length * 2 + 100);

        Stopwatch swCompress = new Stopwatch();
        ElevationDataCompression.compressHeightData(content, os);
        swCompress.stop();

        System.out.println("COMPRESSED: " + (os.size() / 1000) + " k");

        Stopwatch swUncompress = new Stopwatch();
        short[][] uncompressed = null;
        try {
            uncompressed = ElevationDataCompression.uncompressHeightData(new ByteArrayInputStream(os.toByteArray()), 2049);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            swUncompress.stop();
        }

        //System.out.println("UNCOMPRESSED2: " + (uncompressed.length / 1000) + " k");

        /*if(content.length != uncompressed.length) {
            throw new RuntimeException("DATA IS NOT EQUAL!");
        }*/

        for (int i = 0; i < 2049; i++) {
            for (int j = 0; j < 2049; j++) {
                if (content[i][j] != uncompressed[i][j]) {
                    throw new RuntimeException("DATA IS NOT EQUAL! " + i + ", " + j);
                }
            }
        }

        System.out.println("Success!");
        System.out.println("File Read Time: " + swRead.elapsedTimeInMs() + " ms");
        System.out.println("Compress Time: " + swCompress.elapsedTimeInMs() + " ms");
        System.out.println("Uncompress Time: " + swUncompress.elapsedTimeInMs() + " ms");

        System.exit(0);
    }
}
