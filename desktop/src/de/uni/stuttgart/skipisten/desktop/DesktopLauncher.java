package de.uni.stuttgart.skipisten.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import lib.gui.util.gl32.GL32Manager;

import de.uni.stuttgart.skipisten.GPSPositionHandler;
import de.uni.stuttgart.skipisten.Main;
import de.uni.stuttgart.skipisten.desktop.gl32.LwjglGL32;

public class DesktopLauncher {

    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.y = 50;
        config.width = 400;
        config.height = config.width * 16 / 9;
        config.samples = 3;
        config.useGL30 = true;

        GL32Manager.setGL32(new LwjglGL32());

        new LwjglApplication(new Main("EN", new GPSPositionHandler() {}), config);
    }
}
