package de.uni.stuttgart.skipisten;

import com.badlogic.gdx.ApplicationAdapter;
import lib.gui.GUIConfiguration;
import lib.gui.GUIManager;

import de.uni.stuttgart.skipisten.gui.osm.OSMConfiguration;
import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;

import de.uni.stuttgart.skipisten.gui.chooselocation.ChooseSkiAreaScreen;
import de.uni.stuttgart.skipisten.gui.mainscreen.MainScreen;

public class Main extends ApplicationAdapter {

    private String language;
    public static GPSPositionHandler gps;

    public Main(String language, GPSPositionHandler gps) {
        this.language = language;
        Main.gps = gps;
    }

    @Override
    public void create() {
        OSMConfiguration osmConfig = new OSMConfiguration();
        osmConfig.userAgent = "de.uni.stuttgart.skipisten";
        osmConfig.osmTileProviderURL = "https://tiles.fmi.uni-stuttgart.de";

        OSMPanel.setConfig(osmConfig);

        GUIConfiguration config = new GUIConfiguration();
        config.appLanguage = language;


        GUIManager.setUp(config);

        new MainScreen();
        new ChooseSkiAreaScreen();
    }

    @Override
    public void render() {
        GUIManager.instance().render();
    }

    @Override
    public void resize(int width, int height) {
        GUIManager.instance().resize(width, height);
    }

    public boolean onBackPressed() {
        return GUIManager.instance().onBackPressed();
    }
}
