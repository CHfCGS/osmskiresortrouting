package de.uni.stuttgart.skipisten.gui.osm.util;

public class TileMath {
    public static int longitudeToTileX(double longitude, int zoom) {
        int tileXPos = (int) Math.floor((longitude + 180) / 360 * (1 << zoom));

        if (tileXPos < 0)
            tileXPos = 0;

        if (tileXPos >= (1 << zoom))
            tileXPos = ((1 << zoom) - 1);

        return tileXPos;
    }

    public static int latitudeToTileY(double latitude, int zoom) {
        int tileYPos = (int)
                Math.floor(
                (1 - Math.log(Math.tan(Math.toRadians(latitude)) + 1
                        / Math.cos(Math.toRadians(latitude)))
                        / Math.PI) / 2 * (1 << zoom));

        if (tileYPos < 0)
            tileYPos = 0;

        if (tileYPos >= (1 << zoom))
            tileYPos = ((1 << zoom) - 1);

        return tileYPos;
    }

    public static double tileXtoLongitude(int x, int zoom) {
        return x / Math.pow(2.0, zoom) * 360.0 - 180;
    }

    public static double tileYtoLatitude(int y, int zoom) {
        double n = Math.PI - (2.0 * Math.PI * y) / Math.pow(2.0, zoom);
        return Math.toDegrees(Math.atan(Math.sinh(n)));
    }

    public static double latitudeToMapY(double latitude) {
        return (1 - Math.log(Math.tan(Math.toRadians(latitude)) + 1 / Math.cos(Math.toRadians(latitude))) / Math.PI) / 2;
    }

    public static double longitudeToMapX(double longitude) {
        return (longitude + 180) / 360.0;
    }

    public static double mapYToLatitude(double mapY) {
        return Math.toDegrees(Math.atan(Math.sinh(Math.PI - (2.0 * Math.PI * mapY))));
    }

    public static double mapXToLongitude(double mapX) {
        return 360.0 * mapX - 180.0;
    }
}
