package de.uni.stuttgart.skipisten.gui.osm.util;

import com.badlogic.gdx.Gdx;

import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;
import lib.gui.GUIManager;

import java.util.ArrayList;
import java.util.List;

public class MapInputProcessor {
    private OSMPanel map;
    private MapPosition mapPosition;

    public MapInputProcessor(OSMPanel map, MapPosition mapPosition) {
        this.map = map;
        this.mapPosition = mapPosition;
    }

    public boolean touchDown(int screenX, int screenY, int pointer) {
        if (!isInMapBounds(screenX, screenY)) return false;
        return true;
    }

    public boolean touchUp(int screenX, int screenY, int pointer) {
        distanceOld = 0;
        return true;
    }

    private int mouseX, mouseY;
    private double distanceOld = 0;

    private float dragSpeedX, dragSpeedY, zoomSpeed = 1f;

    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (!isInMapBounds(screenX, screenY)) return false;
        if (mapPosition.blockTouchInput()) return false;

        int deltaX = -Gdx.input.getDeltaX(pointer);
        int deltaY = -Gdx.input.getDeltaY(pointer);

        GUIManager m = GUIManager.instance();

        if (m.getPointerCount() == 1) {
            map.moveMap(deltaX, deltaY);

            dragSpeedX += deltaX / 2f;
            dragSpeedY += deltaY / 2f;

        } else if (m.getPointerCount() == 2) {
            int x1 = Gdx.input.getX(m.getTouchPointer(0));
            int x2 = Gdx.input.getX(m.getTouchPointer(1));
            int y1 = Gdx.input.getY(m.getTouchPointer(0));
            int y2 = Gdx.input.getY(m.getTouchPointer(1));

            double distanceNew = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));

            if (distanceOld == 0) distanceOld = distanceNew;

            double zoomFactor = distanceNew / distanceOld;

            if (distanceNew != 0 && distanceOld != 0) {
                mapPosition.setVisibleMapW(mapPosition.getVisibleMapW() / zoomFactor);

                zoomSpeed /= zoomFactor;
            }

            distanceOld = distanceNew;
        }

        return true;
    }

    public boolean mouseMoved(int screenX, int screenY) {
        this.mouseX = screenX;
        this.mouseY = screenY;
        return false;
    }

    public boolean scrolled(int amount) {
        if (!isInMapBounds(mouseX, mouseY)) return false;
        map.zoom((amount < 0) ? 1.2f : 0.8f);
        return false;
    }

    public void update() {
        dragSpeedX /= 1.2f;
        dragSpeedY /= 1.2f;

        if (zoomSpeed > 1f) {
            zoomSpeed = 1 + (zoomSpeed - 1) / 1.5f;
            if (zoomSpeed < 1.001f) zoomSpeed = 1f;
        } else {
            zoomSpeed = zoomSpeed + (1 - zoomSpeed) / 4f;
            if (zoomSpeed > 0.999f) zoomSpeed = 1f;
        }

        if (GUIManager.instance().getPointerCount() == 0 && !mapPosition.blockTouchInput()) {

            if (dragSpeedX != 0.0 || dragSpeedY != 0.0) {
                map.moveMap(dragSpeedX, dragSpeedY);
            }

            if (zoomSpeed != 1f && mapPosition.getMinVisibleMapW() != 0) {
                mapPosition.setVisibleMapW(mapPosition.getVisibleMapW() * zoomSpeed);
            }
        }
    }

    public boolean isInMapBounds(int x, int y) {
        return x >= 0 && y >= 0 && x <= map.w.value() && y <= map.h.value();
    }
}
