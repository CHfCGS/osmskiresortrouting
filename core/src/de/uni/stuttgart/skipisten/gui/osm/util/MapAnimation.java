package de.uni.stuttgart.skipisten.gui.osm.util;

public class MapAnimation {

    private MapPosition mapPosition;

    private double targetMapX;
    private double targetMapY;
    private double targetMapW;

    private double currentMapX;
    private double currentMapY;
    private double currentMapW;

    private boolean isActive = false;
    private int tickCounter;

    public MapAnimation(MapPosition mapPosition) {
        this.mapPosition = mapPosition;
    }

    public void startAnimation(double targetMapX, double targetMapY, double targetMapW) {
        this.targetMapX = targetMapX;
        this.targetMapY = targetMapY;
        this.targetMapW = targetMapW;

        this.currentMapX = mapPosition.getMapX();
        this.currentMapY = mapPosition.getMapY();
        this.currentMapW = mapPosition.getVisibleMapW();

        isActive = true;
        tickCounter = 0;

    }

    public boolean isActive() {
        return isActive;
    }

    public boolean blockTouchInput() {
        return isActive() && tickCounter < 16 * 10;
    }

    public void tick() {
        double deltaX = targetMapX - currentMapX;
        double deltaY = targetMapY - currentMapY;
        double deltaW = targetMapW - currentMapW;

        currentMapX += deltaX / 50.0;
        currentMapY += deltaY / 50.0;
        currentMapW += deltaW / 50.0;

        mapPosition.moveMapX(deltaX / 50.0);
        mapPosition.moveMapY(deltaY / 50.0);
        mapPosition.setVisibleMapW(mapPosition.getVisibleMapW() + deltaW / 50.0);

        tickCounter++;

        if(tickCounter > 16 * 120) {
            isActive = false;
        }
    }
}
