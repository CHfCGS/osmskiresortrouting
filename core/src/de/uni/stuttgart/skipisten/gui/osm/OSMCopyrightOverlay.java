package de.uni.stuttgart.skipisten.gui.osm;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import lib.gui.Draw;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.components.basic.ButtonAnimation;
import lib.gui.components.basic.TextButton;
import lib.gui.components.panel.Panel;

public class OSMCopyrightOverlay extends Panel {

    @PosX(align = Alignment.RIGHT)
    @PosY(align = Alignment.BOTTOM)
    @Width(h = 0.2)
    @Height(h = 0.025)
    private final TextButton copyrightNotice;

    private final Color copyrightNoticeBackground = new Color(0.4f, 0.5f, 0.5f, 0.5f);

    public OSMCopyrightOverlay() {
        if (OSMPanel.getConfig() == null) {
            throw new RuntimeException("You must call OSMPanel.setConfig first!");
        }

        copyrightNotice = new TextButton();
        copyrightNotice.setText(OSMPanel.getConfig().osmAttributionText);
        copyrightNotice.setEvent(() -> Gdx.net.openURI(OSMPanel.getConfig().osmAttributionLink));
        copyrightNotice.setAnimation(ButtonAnimation.NOTHING_ON_CLICK);
        copyrightNotice.underlineText(true);
    }

    @Override
    protected void render() {
        Draw.setColor(copyrightNoticeBackground);
        Draw.rect(copyrightNotice.x.value(), copyrightNotice.y.value(), copyrightNotice.w.value(), copyrightNotice.h.value());
        Draw.resetColor();
    }
}
