package de.uni.stuttgart.skipisten.gui.osm.tiles;

import de.uni.stuttgart.skipisten.gui.osm.texture.SharedTextureCache;
import de.uni.stuttgart.skipisten.gui.osm.tiles.texture.TextureSourcePosition;
import de.uni.stuttgart.skipisten.gui.osm.tiles.texture.TileTexture;

public class Tile {

    private TileManager tileManager;
    private TilePosition position;
    private TileTexture tileTexture;

    private boolean textureNotLoaded = true;

    Tile(TilePosition position, TileManager tileManager) {
        this.position = position;
        this.tileManager = tileManager;
        tileTexture = new TileTexture(position);
    }

    private void loadTexture() {
        SharedTextureCache.getInstance().loadTexture2d(position, () -> {
            boolean isVisible = tileManager.isVisible(this);

            if (!isVisible)
                textureNotLoaded = true;

            return isVisible;
        });
    }

    public TileTexture getTexture() {
        return tileTexture;
    }

    public void render(float x, float y, float w, float h) {

        if (textureNotLoaded) {
            textureNotLoaded = false;
            loadTexture();
        }

        if (!tileTexture.finishedLoading()) {

            // Tile texture is not loaded yet
            // Search for other textures which can be rendered.
            tryRenderParentTexture(x, y, w, h);
            tryRenderChildTexture(x, y, w, h);

        } else {
            if (tileTexture.isAnimationActive()) {
                tryRenderParentTexture(x, y, w, h);
                tryRenderChildTexture(x, y, w, h);
            }

            tileTexture.draw(x, y, w, h, TextureSourcePosition.DEFAULT);
        }
    }

    private final TilePosition child = new TilePosition();

    private void tryRenderChildTexture(float x, float y, float w, float h) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.position.zoomIn(child, i, j);

                if (tileManager.isTileTextureLoaded(child)) {
                    TileTexture childTexture = tileManager.getTile(child).getTexture();
                    childTexture.draw(x + i * w / 2, y + j * h / 2, w / 2, h / 2, TextureSourcePosition.DEFAULT, false);
                }
            }
        }
    }

    private final TextureSourcePosition parentSrc = new TextureSourcePosition();
    private final TilePosition parent = new TilePosition();

    private void tryRenderParentTexture(float x, float y, float w, float h) {

        int amountOfZoomOuts = 0;

        do {
            parentSrc.resetToDefault();
            this.position.zoomOut(parent, ++amountOfZoomOuts, parentSrc);

            // We haven't found a parent texture
            if (!parent.isValid()) break;

            // We have found a parent texture
            if (tileManager.isTileTextureLoaded(parent)) break;

        } while (!tileManager.isTileTextureLoaded(parent));

        if (tileManager.isTileTextureLoaded(parent)) {
            TileTexture parentTexture = tileManager.getTile(parent).getTexture();
            parentTexture.draw(x, y, w, h, parentSrc, false);
        }
    }

    public TilePosition getPosition() {
        return position;
    }

    boolean canBeDisposed(long maxLastUsedDuration) {
        return System.currentTimeMillis() - tileTexture.getLastTimeTextureUsed() > maxLastUsedDuration
                && tileTexture.finishedLoading();
    }
}
