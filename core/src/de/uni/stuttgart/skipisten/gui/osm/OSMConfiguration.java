package de.uni.stuttgart.skipisten.gui.osm;

import com.badlogic.gdx.graphics.Color;
import de.uni.stuttgart.skipisten.gui.osm.util.UrlBuilder;

import lib.gui.resources.Text;

/**
 * A configuration could be created like this:
 * <p>
 * OSMConfiguration config = new OSMConfiguration();
 * config.userAgent = "<insert something like your package name of your app>";
 * config.osmTileProviderURL = "http://tile.openstreetmap.org";
 */
public class OSMConfiguration {

    public String userAgent;

    public String osmTileProviderURL;

    public Color backgroundColor = Color.LIGHT_GRAY;

    /**
     * When this option is enabled, the current map position (x, y and zoom)
     * will be saved and restored after an app restart
     */
    public boolean saveMapPosition = true;

    /**
     * Creates a url for a given TilePosition
     * which is used to download the tile texture
     */
    public UrlBuilder urlBuilder = p -> osmTileProviderURL + "/" + p.getZoom() + "/" + p.getX() + "/" + p.getY() + ".png";

    public Text osmAttributionText = new Text("\u00A9 OpenStreetMap contributors", false);

    public String osmAttributionLink = "https://www.openstreetmap.org/copyright";
}
