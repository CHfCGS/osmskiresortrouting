package de.uni.stuttgart.skipisten.gui.osm.texture;

@FunctionalInterface
public interface TextureStillNeeded {
    boolean textureStillNeeded();
}
