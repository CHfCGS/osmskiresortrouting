package de.uni.stuttgart.skipisten.gui.osm.tiles;

import de.uni.stuttgart.skipisten.gui.osm.tiles.texture.TextureSourcePosition;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;

public class TilePosition implements Comparable<TilePosition> {

    public final static int MIN_ZOOM = 0;
    public final static int MAX_ZOOM = 18;

    private int x;
    private int y;
    private int zoom;

    public TilePosition() {
    }

    public TilePosition(int x, int y, int zoom) {
        this.x = x;
        this.y = y;
        this.zoom = zoom;
    }

    public TilePosition(double lat, double lon, int zoom) {
        this.x = TileMath.longitudeToTileX(lon, zoom);
        this.y = TileMath.latitudeToTileY(lat, zoom);
        this.zoom = zoom;
    }

    public int getX() {
        return x;
    }

    public double getMapX() {
        return x / (double)(1 << zoom);
    }

    public int getY() {
        return y;
    }

    public double getMapY() {
        return y / (double)(1 << zoom);
    }

    public double getMapSize() {
        return 1d / (1 << zoom);
    }

    public int getZoom() {
        return zoom;
    }

    @Override
    public int hashCode() {
        return x + y * (1 << zoom) + zoom * (1 << (zoom * 2));
    }

    @Override
    public boolean equals(Object other) {
        return hashCode() == other.hashCode();
    }

    @Override
    public String toString() {
        return zoom + "/" + x + "/" + y;
    }

    boolean isValid() {
        // Check if x and y positions are out of bound
        // [(1 << zoom) == Math.pow(2, zoom)]
        if (x < 0 || y < 0 || x >= (1 << zoom) || y >= (1 << zoom))
            return false;

        if (zoom < MIN_ZOOM || zoom > MAX_ZOOM)
            return false;

        return true;
    }

    public void zoomOut(TilePosition result, int amount, TextureSourcePosition src) {
        int srcStartW = src.w;
        int srcStartH = src.h;

        for (int i = 0; i < amount; i++) {
            src.x += (srcStartW >> (amount - i)) * ((x >> i) % 2);
            src.y += (srcStartH >> (amount - i)) * ((y >> i) % 2);
        }

        src.w = src.w >> amount;
        src.h = src.h >> amount;

        if (src.w == 0)
            src.w = 1;

        if (src.h == 0)
            src.h = 1;

        result.x = x >> amount;
        result.y = y >> amount;
        result.zoom = zoom - amount;
    }

    public TilePosition zoomOut(int amount) {
        return new TilePosition(x >> amount, y >> amount, zoom - amount);
    }

    public TilePosition zoomIn(int dX, int dY) {
        return new TilePosition(x * 2 + dX, y * 2 + dY, zoom + 1);
    }

    public void zoomIn(TilePosition result, int dX, int dY) {
        result.x = x * 2 + dX;
        result.y = y * 2 + dY;
        result.zoom = zoom + 1;
    }

    public TilePosition getNeighborPosition(int deltaX, int deltaY) {
        return new TilePosition(x + deltaX, y + deltaY, zoom);
    }

    @Override
    public int compareTo(TilePosition tilePosition) {
        if (this.getZoom() != tilePosition.getZoom()) {
            return this.getZoom() - tilePosition.getZoom();
        }

        if (this.getX() != tilePosition.getX()) {
            return this.getX() - tilePosition.getX();
        }

        if (this.getY() != tilePosition.getY()) {
            return this.getY() - tilePosition.getY();
        }

        return 0;
    }


    public static class FractionalTilePosition {
        private TilePosition pos;
        private float xFract;
        private float yFract;

        public FractionalTilePosition(TilePosition pos, float xFract, float yFract) {
            this.pos = pos;
            this.xFract = xFract;
            this.yFract = yFract;
        }

        public TilePosition getPosition() {
            return pos;
        }

        public float getXFraction() {
            return xFract;
        }

        public float getYFraction() {
            return yFract;
        }
    }
}
