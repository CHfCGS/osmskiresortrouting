package de.uni.stuttgart.skipisten.gui.osm.util;

import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;

@FunctionalInterface
public interface UrlBuilder {
    String getUrl(TilePosition pos);
}