package de.uni.stuttgart.skipisten.gui.osm.tiles;

import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;
import de.uni.stuttgart.skipisten.gui.osm.texture.SharedTextureCache;
import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TileManager {

    private OSMPanel osmPanel;
    private MapPosition mapPosition;

    private HashMap<TilePosition, Tile> allTiles;
    private long lastTreeCleanUpTime;

    private ArrayList<Tile> visibleTiles;

    private final boolean lowPerformanceMode = false;

    public TileManager(OSMPanel osmPanel, MapPosition mapPosition) {
        this.osmPanel = osmPanel;
        this.mapPosition = mapPosition;
        allTiles = new HashMap<>();
        visibleTiles = new ArrayList<>();
    }

    private int zoomLevel = 1;
    private int x, y;
    private int tileCountLeft, tileCountTop, tileCountRight, tileCountBottom;

    public ArrayList<Tile> getVisibleTiles() {
        visibleTiles.clear();
        cleanUpIfNeeded();

        int visibleTilesW = mapPosition.getVisibleTilesW();

        if (lowPerformanceMode && visibleTilesW * mapPosition.getVisibleMapW() >= 4) {
            visibleTilesW = (int) (3 / mapPosition.getVisibleMapW());
        }

        zoomLevel = mapPosition.getTileTextureZoomLevel(visibleTilesW);

        x = TileMath.longitudeToTileX(mapPosition.getLongitude(), zoomLevel);
        y = TileMath.latitudeToTileY(mapPosition.getLatitude(), zoomLevel);

        double tileSize = (1.0) / (Math.pow(2, zoomLevel));

        Tile center = getTile(new TilePosition(x, y, zoomLevel));

        double mapX0, mapY0, mapX1, mapY1;

        mapX0 = mapPosition.getMapX() - mapPosition.getVisibleMapW() / 2; //MapX at screenX = 0
        mapY0 = mapPosition.getMapY() - mapPosition.getVisibleMapH() / 2; //MapY at screenY = 0
        mapX1 = mapPosition.getMapX() + mapPosition.getVisibleMapW() / 2; //MapX at screenX = screenW
        mapY1 = mapPosition.getMapY() + mapPosition.getVisibleMapH() / 2; //MapY at screenY = screenH

        tileCountLeft = (int) Math.ceil((center.getPosition().getMapX() - mapX0) / tileSize);
        tileCountTop = (int) Math.ceil((center.getPosition().getMapY() - mapY0) / tileSize);
        tileCountRight = (int) Math.ceil((mapX1 - center.getPosition().getMapX()) / tileSize);
        tileCountBottom = (int) Math.ceil((mapY1 - center.getPosition().getMapY()) / tileSize);

        for (int j = y - tileCountTop; j < y + tileCountBottom; j++) {
            for (int i = x - tileCountLeft; i < x + tileCountRight; i++) {
                TilePosition tilePos = new TilePosition(i, j, zoomLevel);

                if (tilePos.isValid()) {
                    visibleTiles.add(getTile(tilePos));
                }
            }
        }


        return visibleTiles;
    }

    public boolean isTileTextureLoaded(TilePosition tilePos) {
        if (!tilePos.isValid()) {
            return false;
        }

        if (!allTiles.containsKey(tilePos)) {
            return false;
        }

        return allTiles.get(tilePos).getTexture().finishedLoading();
    }

    public Tile getTile(TilePosition tilePos) {
        if (allTiles.containsKey(tilePos)) {
            return allTiles.get(tilePos);
        }

        Tile tile = new Tile(tilePos, this);

        allTiles.put(tilePos, tile);
        return tile;
    }

    private void cleanUpIfNeeded() {
        //Cleans up the HashMap every 15 seconds
        if (System.currentTimeMillis() - lastTreeCleanUpTime > 15_000) {
            lastTreeCleanUpTime = System.currentTimeMillis();
            cleanUp();
        }
    }

    /**
     * Disposes old textures, which
     * have not been rendered for a while.
     */
    private void cleanUp() {
        Iterator<Map.Entry<TilePosition, Tile>> iterator = allTiles.entrySet().iterator();

        long timeUntilUnload = 100_000 * 600 / Math.max(allTiles.size(), 100);

        while (iterator.hasNext()) {
            Map.Entry<TilePosition, Tile> entry = iterator.next();
            Tile tile = entry.getValue();

            if (tile.getPosition().getZoom() > 4
                    && tile.canBeDisposed(timeUntilUnload)) {
                SharedTextureCache.getInstance().unloadTexture2d(tile.getPosition());
                iterator.remove();
            }
        }
    }

    public void dispose() {
        SharedTextureCache.dispose();

        allTiles.clear();
    }

    public boolean isVisible(Tile tile) {
        if (tile.getPosition().getZoom() != zoomLevel) return false;

        for (int j = y - tileCountTop; j < y + tileCountBottom; j++) {
            for (int i = x - tileCountLeft; i < x + tileCountRight; i++) {
                if (tile.getPosition().getX() == i && tile.getPosition().getY() == j) return true;
            }
        }

        return false;
    }
}
