package de.uni.stuttgart.skipisten.gui.osm.util;

import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm.tiles.texture.TileTexture;
import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;

public class MapPosition {
    private double mapY;
    private double mapX;
    private double visibleMapW;

    private OSMPanel map;

    private MapAnimation mapAnimation;

    public MapPosition(OSMPanel map) {
        this(map, 0, 0, 0);
    }

    public MapPosition(OSMPanel map, double latitude, double longitude, double visibleDegreesLon) {
        this.map = map;
        setLatitude(latitude);
        setLongitude(longitude);
        setVisibleDegreesLon(visibleDegreesLon);

        mapAnimation = new MapAnimation(this);
    }

    public void setLatitude(double latitude) {
        setMapY(TileMath.latitudeToMapY(latitude));
    }

    public void setLongitude(double longitude) {
        setMapX(TileMath.longitudeToMapX(longitude));
    }

    public double getLatitude() {
        return TileMath.mapYToLatitude(mapY);
    }

    public double getLongitude() {
        return TileMath.mapXToLongitude(mapX);
    }

    public void setMapX(double mapX) {
        this.mapX = mapX;
        testForBorder();
    }

    public void setMapY(double mapY) {
        this.mapY = mapY;
        testForBorder();
    }

    public void moveMapX(double deltaX) {
        setMapX(getMapX() + deltaX);
    }

    public void moveMapY(double deltaY) {
        setMapY(getMapY() + deltaY);
    }

    public double getMapX() {
        return mapX;
    }

    public double getMapY() {
        return mapY;
    }

    public double getVisibleDegreesLon() {
        return visibleMapW * 360.0;
    }

    public double getVisibleMapW() {
        return visibleMapW;
    }

    public double getVisibleMapH() {
        return visibleMapW * map.h.value() / map.w.value();
    }

    private void testForBorder() {
        if(getVisibleMapW() == 0) return;

        if (this.mapX < 0 + getVisibleMapW() / 2) this.mapX = 0 + getVisibleMapW() / 2;
        if (this.mapX > 1 - getVisibleMapW() / 2) this.mapX = 1 - getVisibleMapW() / 2;

        if (this.mapY < 0 + getVisibleMapH() / 2) this.mapY = 0 + getVisibleMapH() / 2;
        if (this.mapY > 1 - getVisibleMapH() / 2) this.mapY = 1 - getVisibleMapH() / 2;

        if (this.visibleMapW > getMaxVisibleMapW()) this.visibleMapW = getMaxVisibleMapW();
    }

    public void setVisibleDegreesLon(double visibleDegreesLon) {
        setVisibleMapW(visibleDegreesLon / 360.0);
    }

    public void setVisibleMapW(double visibleMapW) {
        this.visibleMapW = visibleMapW;
        if (this.visibleMapW > getMaxVisibleMapW()) this.visibleMapW = getMaxVisibleMapW();
        if (this.visibleMapW < getMinVisibleMapW()) this.visibleMapW = getMinVisibleMapW();

        testForBorder();
    }

    public void setVisibleMapH(double visibleMapH) {
        setVisibleMapW(visibleMapH * map.w.value() / map.h.value());
    }

    private double getMaxVisibleMapW() {
        return Math.min(1.0, 1.0 * map.w.value() / (double) map.h.value());
    }

    public double getMinVisibleMapW() {
        return 1.0 / Math.pow(2, 18);
    }

    public int getVisibleTilesW() {
        return (int) Math.ceil(map.w.value() / (TileTexture.TEXTURE_SIZE * getVisibleMapW()));
    }

    public int getTileTextureZoomLevel(int visibleTilesW) {
        return (int) getZoomLevel(visibleTilesW);
    }

    public float getZoomLevel(int visibleTilesW) {
        float zoomLevel = (float) (Math.log(visibleTilesW) / Math.log(2));

        if (zoomLevel > TilePosition.MAX_ZOOM) zoomLevel = TilePosition.MAX_ZOOM;
        if (zoomLevel < 0) zoomLevel = 0;

        return zoomLevel;
    }

    @Override
    public String toString() {
        return getLongitude() + "/" + getLatitude();
    }

    public void update(long deltaTimeInMs) {
        if (mapAnimation.isActive()) {
            for (int i = 0; i < deltaTimeInMs; i++) {
                mapAnimation.tick();
            }
        }
    }

    public void zoomMapTo(double targetMapX, double targetMapY, double targetMapW, boolean animate) {
        if (animate) {
            mapAnimation.startAnimation(targetMapX, targetMapY, targetMapW);
        } else {
            setVisibleMapW(targetMapW);
            setMapX(targetMapX);
            setMapY(targetMapY);
        }
    }

    public boolean blockTouchInput() {
        return mapAnimation.blockTouchInput();
    }

    public boolean isAnimationActive() {
        return mapAnimation.isActive();
    }
}
