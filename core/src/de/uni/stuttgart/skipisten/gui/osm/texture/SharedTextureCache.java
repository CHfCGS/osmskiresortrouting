package de.uni.stuttgart.skipisten.gui.osm.texture;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;

public class SharedTextureCache implements Runnable {

    private static SharedTextureCache instance;

    private boolean isRunning = true;

    // Shared texture hash map
    private HashMap<TilePosition, Texture> textures;

    private HashSet<TilePosition> textures2d;
    private HashSet<TilePosition> textures3d;

    private TreeMap<TilePosition, TextureStillNeeded> textureRequests2d;
    private TreeMap<TilePosition, TextureStillNeeded> textureRequests3d;

    private SharedTextureCache() {
        textures = new HashMap<>();
        textures2d = new HashSet<>();
        textures3d = new HashSet<>();
        textureRequests2d = new TreeMap<>();
        textureRequests3d = new TreeMap<>();

        // Run in two threads
        new Thread(this).start();
        new Thread(this).start();
    }

    public static SharedTextureCache getInstance() {
        if (instance == null) init();
        return instance;
    }

    public static void init() {
        instance = new SharedTextureCache();
    }

    public static void dispose() {
        if (instance != null) {
            instance.isRunning = false;

            synchronized (instance) {
                for (Map.Entry<TilePosition, Texture> tilePositionTextureEntry : instance.textures.entrySet()) {
                    tilePositionTextureEntry.getValue().dispose();
                }
            }

            instance = null;
        }
    }

    public boolean isTextureLoaded(TilePosition tilePosition) {
        return textures.containsKey(tilePosition);
    }

    public Texture getTexture(TilePosition tilePosition) {
        if (isTextureLoaded(tilePosition)) {
            return textures.get(tilePosition);
        }

        return null;
    }

    public void loadTexture2d(TilePosition tilePosition, TextureStillNeeded textureStillNeeded) {
        if (!textures2d.contains(tilePosition)) {
            textures2d.add(tilePosition);
        }

        if (isTextureLoaded(tilePosition)) return;

        synchronized (this) {
            if (!textureRequests2d.containsKey(tilePosition)) {
                textureRequests2d.put(tilePosition, textureStillNeeded);
            }

            notifyAll();
        }
    }

    public void loadTexture3d(TilePosition tilePosition, TextureStillNeeded textureStillNeeded) {
        if (!textures3d.contains(tilePosition)) {
            textures3d.add(tilePosition);
        }

        if (isTextureLoaded(tilePosition)) return;

        synchronized (this) {
            if (!textureRequests3d.containsKey(tilePosition)) {
                textureRequests3d.put(tilePosition, textureStillNeeded);
            }

            notifyAll();
        }
    }

    public void unloadTexture2d(TilePosition tilePosition) {
        synchronized (this) {
            if (!isRunning) return;

            textures2d.remove(tilePosition);

            if (textures.containsKey(tilePosition) && !textures3d.contains(tilePosition)) {
                Texture texture = textures.get(tilePosition);
                texture.dispose();
                textures.remove(tilePosition);
            } else if (textureRequests2d.containsKey(tilePosition)) {
                textureRequests2d.remove(tilePosition);
            }
        }
    }

    public void unloadTexture3d(TilePosition tilePosition) {
        synchronized (this) {
            if (!isRunning) return;

            textures3d.remove(tilePosition);

            if (textures.containsKey(tilePosition) && !textures2d.contains(tilePosition)) {
                Texture texture = textures.get(tilePosition);
                texture.dispose();
                textures.remove(tilePosition);
            } else if (textureRequests3d.containsKey(tilePosition)) {
                textureRequests3d.remove(tilePosition);
            }
        }
    }

    public int getQueueSize() {
        return textureRequests2d.size() + textureRequests3d.size();
    }

    public int getCacheSize() {
        return textures.size();
    }

    private FileHandle getCacheFile(TilePosition pos) {
        return Gdx.files.local(getFilePath(pos));
    }

    private String getFilePath(TilePosition pos) {
        return "cache/tile/" + pos.getZoom() + "/" + pos.getX() + "-" + pos.getY() + ".png";
    }

    @Override
    public void run() {
        TextureLoader textureLoader = new TextureLoader();

        while (isRunning) {
            TilePosition tilePosition;

            synchronized (this) {
                try {
                    while (getQueueSize() == 0) {
                        wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(textureRequests2d.size() > 0) {
                    Map.Entry<TilePosition, TextureStillNeeded> entry = textureRequests2d.pollFirstEntry();
                    tilePosition = entry.getKey();

                    if (!entry.getValue().textureStillNeeded()) {
                        textures2d.remove(tilePosition);
                        continue;
                    }
                } else {
                    Map.Entry<TilePosition, TextureStillNeeded> entry = textureRequests3d.pollFirstEntry();
                    tilePosition = entry.getKey();

                    if (!entry.getValue().textureStillNeeded()) {
                        textures3d.remove(tilePosition);
                        continue;
                    }
                }
            }

            Texture texture = textureLoader.load(tilePosition, getCacheFile(tilePosition));

            if (texture == null) {
                // Could not load texture. TODO handle
            } else {
                if (isRunning) {
                    synchronized (this) {
                        textures.put(tilePosition, texture);
                    }
                }
            }
        }
    }
}
