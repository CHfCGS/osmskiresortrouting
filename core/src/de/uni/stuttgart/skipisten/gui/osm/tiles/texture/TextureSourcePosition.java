package de.uni.stuttgart.skipisten.gui.osm.tiles.texture;

public class TextureSourcePosition {

    public static final TextureSourcePosition DEFAULT = new TextureSourcePosition(0, 0, TileTexture.TEXTURE_SIZE, TileTexture.TEXTURE_SIZE);

    public int x;
    public int y;
    public int w;
    public int h;

    public TextureSourcePosition() {
        this(DEFAULT);
    }

    public TextureSourcePosition(TextureSourcePosition src) {
        this.x = src.x;
        this.y = src.y;
        this.w = src.w;
        this.h = src.h;
    }

    public TextureSourcePosition(int srcX, int srcY, int srcW, int srcH) {
        this.x = srcX;
        this.y = srcY;
        this.w = srcW;
        this.h = srcH;
    }

    public void resetToDefault() {
        this.x = DEFAULT.x;
        this.y = DEFAULT.y;
        this.w = DEFAULT.w;
        this.h = DEFAULT.h;
    }
}
