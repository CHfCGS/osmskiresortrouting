package de.uni.stuttgart.skipisten.gui.osm.util;

import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;

import java.util.Arrays;
import java.util.Collection;

import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class MapBoundingBox {
    public double minX = Float.POSITIVE_INFINITY;
    public double maxX = Float.NEGATIVE_INFINITY;
    public double minY = Float.POSITIVE_INFINITY;
    public double maxY = Float.NEGATIVE_INFINITY;

    public MapBoundingBox() {}

    public MapBoundingBox(Collection<MapBoundingBox> boxes) {
        for (MapBoundingBox b : boxes) addBox(b);
    }

    public MapBoundingBox(MapBoundingBox... boxes) {
        this(Arrays.asList(boxes));
    }

    public boolean crossesTile(TilePosition pos) {
        double mapX = pos.getMapX();
        double mapY = pos.getMapY();
        double mapSize = pos.getMapSize();
        double mapXEnd = mapX + mapSize;
        double mapYEnd = mapY + mapSize;
        return (maxX >= mapX && mapXEnd >= minX && maxY >= mapY && mapYEnd >= minY);
    }

    public void addPoint(double x, double y) {
        if (x < minX) minX = x;
        if (x > maxX) maxX = x;
        if (y < minY) minY = y;
        if (y > maxY) maxY = y;
    }

    public void addBox(MapBoundingBox box) {
        if (box.minX < minX) minX = box.minX;
        if (box.maxX > maxX) maxX = box.maxX;
        if (box.minY < minY) minY = box.minY;
        if (box.maxY > maxY) maxY = box.maxY;
    }

    public void addWay(OSMGraph graph, OSMElement way) {
        for(long nodeId : way.nodes) {
            OSMElement node = graph.getNode(nodeId);
            addPoint(TileMath.longitudeToMapX(node.lon), TileMath.latitudeToMapY(node.lat));
        }
    }
}
