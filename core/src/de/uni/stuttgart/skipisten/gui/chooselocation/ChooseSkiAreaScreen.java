package de.uni.stuttgart.skipisten.gui.chooselocation;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.annotations.StartScreen;
import lib.gui.annotations.Visible;
import lib.gui.annotations.ZLayer;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.basic.Label;
import lib.gui.components.basic.TextButton;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screen;
import lib.gui.components.screen.Screens;
import de.uni.stuttgart.skipisten.gui.osm.OSMCopyrightOverlay;
import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;
import lib.gui.layout.values.DynamicValue;

import de.uni.stuttgart.skipisten.Main;
import de.uni.stuttgart.skipisten.data.graph.GraphManager;
import de.uni.stuttgart.skipisten.data.overpass.SkiGraphLoader;
import de.uni.stuttgart.skipisten.gui.mainscreen.MainScreen;
import de.uni.stuttgart.skipisten.gui.mainscreen.OSMView;

@StartScreen
public class ChooseSkiAreaScreen extends Screen {

    @ZLayer(-2)
    @Width(w = 1)
    @Height(h = 1)
    public final OSMView osmView;

    @ZLayer(1)
    @Width(w = 1)
    @Height(h = 1)
    private final OSMCopyrightOverlay osmCopyright = new OSMCopyrightOverlay();

    @ZLayer(1)
    @Width(w = 1)
    @Height(h = 0.06)
    @TextValue("Choose a ski area:")
    private final Label chooseSkiAreaText;

    @ZLayer(-1)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.25)
    @Height(h = 0.06)
    @TextValue("Choose")
    private final TextButton chooseLocation;

    @ZLayer(-1)
    @PosX(h = 0.05, align = Alignment.RIGHT)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.06)
    @Height(h = 0.06)
    private final TextButton gpsPosition;

    @Visible(false)
    @ZLayer(-1)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.51)
    @Width(h = 0.6)
    @Height(h = 0.037)
    private final Label statusLabel;

    private TextureRegion placeIcon;
    private TextureRegion locationIcon;

    private boolean useNextGpsLocation;

    public ChooseSkiAreaScreen() {
        osmView = new OSMView();

        chooseSkiAreaText = new Label();

        chooseLocation = new TextButton(TextButton.COLOR_BLUE);
        chooseLocation.setEvent(this::chooseLocationPressed);

        gpsPosition = new TextButton(TextButton.COLOR_BLUE);
        gpsPosition.setEvent(this::gpsButtonPressed);

        statusLabel = new Label();
        statusLabel.setTextColor(Color.BLACK);

        placeIcon = Renderer2D.getTextureManager().getTextureRegion("place");
        locationIcon = Renderer2D.getTextureManager().getTextureRegion("buttons/location");
    }

    @Override
    protected void update(long deltaTimeInMs) {
        if (useNextGpsLocation) {
            if (Main.gps.hasGPSLocation(3600)) {
                osmView.zoomMapTo(Main.gps.getLatitude(), Main.gps.getLongitude(), true);
                useNextGpsLocation = false;
                statusLabel.setVisible(false);
                Main.gps.stop();
            }
        }
    }

    @Override
    protected void render() {
        Draw.setColor(Color.DARK_GRAY);
        Draw.rect(chooseSkiAreaText);

        Draw.setColor(Color.DARK_GRAY);
        float iconS = h.value() * 0.06f;
        Draw.image(placeIcon, DynamicValue.SCREEN_W.value() / 2 - iconS / 2, DynamicValue.SCREEN_H.value() / 2 - iconS, iconS, iconS);
        Draw.resetColor();

        Draw.image(locationIcon, gpsPosition.x.value(), gpsPosition.y.value(), gpsPosition.w.value(), gpsPosition.h.value(), 0.7f);
    }

    private void chooseLocationPressed() {
        useNextGpsLocation = false;

        chooseLocation.setActive(false);
        gpsPosition.setActive(false);

        MapPosition pos = osmView.getMapPositionAt(DynamicValue.SCREEN_W.value() / 2, DynamicValue.SCREEN_H.value() / 2);

        SkiGraphLoader loader = new SkiGraphLoader();
        loader.setCallbackNoSkiRegionFound(() -> {
            skiLoaderFinished();
            System.out.println("No ski region found");
            Dialogs.showInfoDialog("Info", "No ski region found. Please try again at a different position");
        });
        loader.setCallbackSkiRegionFound(() -> {
            statusLabel.setText("Loading Ski Region...");
        });
        loader.setCallbackGraphLoadedButNoElevationYet(() -> {
            //Origin needs to be set before elevation data is loaded
            Screens.get(MainScreen.class).osmView.get3DPanel().setOrigin(pos.getLatitude(), pos.getLongitude());

            statusLabel.setText("Loading Elevation Data...");
        });
        loader.setCallbackSuccess(() -> {
            skiLoaderFinished();
            Main.gps.stop();
            GraphManager.setGraph(loader.getSkiGraph());

            Screens.get(MainScreen.class).openAndInit();
        });
        loader.setCallbackError(() -> {
            skiLoaderFinished();
            Dialogs.showInfoDialog("Error", "Unable to load data. Please check your internet connection!");
        });

        loader.load(pos);

        statusLabel.setVisible(true);
        statusLabel.setText("Searching for Ski Region...");
    }

    /**
     * This method is called when the loading finished
     * successfully or if an error occurred
     */
    private void skiLoaderFinished() {
        statusLabel.setVisible(false);

        chooseLocation.setActive(true);
        gpsPosition.setActive(true);
    }

    private void gpsButtonPressed() {
        if (!Main.gps.isGPSSupported()) {
            Dialogs.showInfoDialog("Info", "GPS is not supported on this device!");
        } else if (!Main.gps.isGPSEnabled()) {
            Dialogs.showInfoDialog("Info", "Please enable GPS on your device");
        } else if(!Main.gps.hasGPSPermission()){
            Main.gps.askForGPSPermission(() -> {
                if (Main.gps.hasGPSPermission())
                    requestGPSLocation();
            });
        } else {
            requestGPSLocation();
        }
    }

    private void requestGPSLocation() {
        Main.gps.start();

        useNextGpsLocation = true;

        statusLabel.setVisible(true);
        statusLabel.setText("Waiting for GPS Signal...");
    }
}
