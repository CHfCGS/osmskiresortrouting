package de.uni.stuttgart.skipisten.gui.chooselocation;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.annotations.ZLayer;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.basic.Label;
import lib.gui.components.basic.TextButton;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screen;
import lib.gui.components.screen.Screens;
import de.uni.stuttgart.skipisten.gui.osm.OSMCopyrightOverlay;
import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;

import lib.gui.layout.values.DynamicValue;

import de.uni.stuttgart.skipisten.Main;
import de.uni.stuttgart.skipisten.data.graph.GraphManager;
import de.uni.stuttgart.skipisten.data.graph.GraphUtils;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;
import de.uni.stuttgart.skipisten.data.search.SearchResult;
import de.uni.stuttgart.skipisten.gui.mainscreen.OSMView;

public class ChoosePositionScreen extends Screen {

    @ZLayer(-2)
    @Width(w = 1)
    @Height(h = 1)
    public final OSMView osmView;

    @ZLayer(1)
    @Width(w = 1)
    @Height(h = 1)
    private final OSMCopyrightOverlay osmCopyright = new OSMCopyrightOverlay();

    @ZLayer(1)
    @Width(w = 1)
    @Height(h = 0.06)
    private final Label choosePositionText;

    @ZLayer(1)
    @PosX(h = 0.05, align = Alignment.LEFT)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.06)
    @Height(h = 0.06)
    @TextValue("X")
    private final TextButton cancelButton;

    @ZLayer(1)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.25)
    @Height(h = 0.06)
    @TextValue("Choose")
    private final TextButton chooseButton;

    @ZLayer(-1)
    @PosX(h = 0.05, align = Alignment.RIGHT)
    @PosY(h = 0.05, align = Alignment.BOTTOM)
    @Width(h = 0.06)
    @Height(h = 0.06)
    private final TextButton gpsPosition;

    @ZLayer(1)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.51)
    @Width(h = 0.6)
    @Height(h = 0.037)
    private final Label statusLabel;

    private TextureRegion placeIcon;
    private TextureRegion locationIcon;

    private boolean useNextGpsLocation;

    private OSMElement currentSelectedNode;
    private OSMElement currentSelectedWay;
    private Runnable positionSelectedCallback;
    private MapPosition selectedPosition;

    public ChoosePositionScreen() {
        osmView = new OSMView();

        choosePositionText = new Label();

        cancelButton = new TextButton(TextButton.COLOR_RED);
        cancelButton.setEvent(this::cancelPressed);

        chooseButton = new TextButton(TextButton.COLOR_BLUE);
        chooseButton.setEvent(this::choosePressed);

        gpsPosition = new TextButton(TextButton.COLOR_BLUE);
        gpsPosition.setEvent(this::gpsButtonPressed);

        statusLabel = new Label();
        statusLabel.setTextColor(Color.BLACK);

        placeIcon = Renderer2D.getTextureManager().getTextureRegion("place");
        locationIcon = Renderer2D.getTextureManager().getTextureRegion("buttons/location");
    }

    public void open(String text, Runnable positionSelectedCallback) {
        choosePositionText.setText(text);
        this.positionSelectedCallback = positionSelectedCallback;
        currentSelectedNode = null;
        if (currentSelectedWay != null) currentSelectedWay.renderHighlight = false;
        currentSelectedWay = null;
        chooseButton.setActive(true);

        Screens.open(this);
    }

    @Override
    protected void update(long deltaTimeInMs) {
        if (useNextGpsLocation) {
            if (Main.gps.hasGPSLocation(3600)) {
                osmView.zoomMapTo(Main.gps.getLatitude(), Main.gps.getLongitude(), 0.0001, true);
                useNextGpsLocation = false;
                statusLabel.setVisible(false);
                Main.gps.stop();
            }
        }
    }

    @Override
    protected void render() {
        //Get and render nearest node
        MapPosition mapPos = osmView.getMapPositionAt(DynamicValue.SCREEN_W.value() / 2f, DynamicValue.SCREEN_H.value() / 2f);

        OSMElement nearestNode = null;
        OSMElement parentWay = null;

        if (mapPos != null) {
            nearestNode = GraphUtils.getNearestPoint(mapPos.getLatitude(), mapPos.getLongitude(), false);
            parentWay = GraphManager.getGraph().getWay(nearestNode.parentWayId);
        }

        if (mapPos == null || GraphUtils.getDistanceInMeters(nearestNode.lat, nearestNode.lon, mapPos.getLatitude(), mapPos.getLongitude()) > 500) {
            if (currentSelectedWay != null) currentSelectedWay.renderHighlight = false;
            currentSelectedWay = null;
            currentSelectedNode = null;
            chooseButton.setActive(false);
            statusLabel.setText("");
        } else {
            currentSelectedNode = nearestNode;
            if (currentSelectedWay != null) currentSelectedWay.renderHighlight = false;
            currentSelectedWay = parentWay;
            currentSelectedWay.renderHighlight = true;
            chooseButton.setActive(true);
            statusLabel.setText(SearchResult.generatedDisplayedName(parentWay));
        }

        if (osmView.is3DMode()) {
            if (currentSelectedNode == null) {
                osmView.get3DPanel().hideMarkerSphere();
            } else {
                osmView.get3DPanel().showMarkerSphere(currentSelectedNode.lat, currentSelectedNode.lon);
            }
        }

        //Draw selected node
        if (currentSelectedNode != null && osmView.is2DMode()) {
            Vector2 point = osmView.get2DPanel().getPixelPosition(currentSelectedNode.lat, currentSelectedNode.lon);

            float s = (float) (w.value() * 1e-6 / mapPos.getVisibleMapW()) * 0.9f;

            Draw.setColor(0.8f, 0.8f, 0.8f, 1f);
            Draw.roundedRect(point.x - s / 2f, point.y - s / 2f, s, s, s * 0.5f, 1f);
            s *= 0.8f;
            Draw.setColor(Color.WHITE);
            Draw.roundedRect(point.x - s / 2f, point.y - s / 2f, s, s, s * 0.5f, 1f);
        }

        //Label background and place icon
        Draw.setColor(Color.DARK_GRAY);
        Draw.rect(choosePositionText);

        Draw.setColor(Color.DARK_GRAY);
        float iconS = h.value() * 0.06f;
        Draw.image(placeIcon, DynamicValue.SCREEN_W.value() / 2 - iconS / 2, DynamicValue.SCREEN_H.value() / 2 - iconS, iconS, iconS);
        Draw.resetColor();

        Draw.addComponentTranslation(gpsPosition.x.value(), gpsPosition.y.value());
        gpsPosition.render();
        Draw.addComponentTranslation(-gpsPosition.x.value(), -gpsPosition.y.value());
        Draw.image(locationIcon, gpsPosition.x.value(), gpsPosition.y.value(), gpsPosition.w.value(), gpsPosition.h.value(), 0.7f);
    }

    private void cancelPressed() {
        Screens.closeCurrentScreen();
    }

    private void choosePressed() {
        Screens.closeCurrentScreen();
        useNextGpsLocation = false;
        selectedPosition = osmView.get2DPanel().getMapPosition(DynamicValue.SCREEN_W.value() / 2, DynamicValue.SCREEN_H.value() / 2);
        positionSelectedCallback.run();
    }

    public OSMElement getChosenNode() {
        return currentSelectedNode;
    }

    public MapPosition getSelectedPosition() {
        return selectedPosition;
    }

    private void gpsButtonPressed() {
        if (!Main.gps.isGPSSupported()) {
            Dialogs.showInfoDialog("Info", "GPS is not supported on this device!");
        } else if (!Main.gps.isGPSEnabled()) {
            Dialogs.showInfoDialog("Info", "Please enable GPS on your device");
        } else if (!Main.gps.hasGPSPermission()) {
            Main.gps.askForGPSPermission(() -> {
                if (Main.gps.hasGPSPermission())
                    requestGPSLocation();
            });
        } else {
            requestGPSLocation();
        }
    }

    @Override
    public void screenClosed() {
        super.screenClosed();
        if (currentSelectedWay != null) currentSelectedWay.renderHighlight = false;
        osmView.get3DPanel().hideMarkerSphere();
    }

    private void requestGPSLocation() {
        Main.gps.start();

        useNextGpsLocation = true;

        statusLabel.setVisible(true);
        statusLabel.setText("Waiting for GPS Signal...");
    }
}
