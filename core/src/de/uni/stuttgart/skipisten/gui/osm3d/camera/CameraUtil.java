package de.uni.stuttgart.skipisten.gui.osm3d.camera;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;

public class CameraUtil {

    private Camera camera;

    private final int poolSize = 10;
    private int poolIndex = 0;
    private Vector3[] vectorPool;

    public CameraUtil(Camera camera) {
        this.camera = camera;

        vectorPool = new Vector3[poolSize];
        for (int i = 0; i < poolSize; i++) {
            vectorPool[i] = new Vector3();
        }
    }

    public Vector3 getPlanePoint(Vector2 screenPoint) {
        return getPlanePoint(screenPoint.x, screenPoint.y);
    }

    public Vector3 getPlanePoint(float screenX, float screenY) {
        Ray ray = camera.getPickRay(screenX, screenY);

        // Calculate intersection point with plane (x, y, 0)
        // (ray.origin.x)           (ray.direction.x)   (x)
        // (ray.origin.y) + delta * (ray.direction.y) = (y)
        // (ray.origin.z)           (ray.direction.z)   (0)
        // =>
        // delta = -ray.origin.z / ray.direction.z

        float delta = -ray.origin.z / ray.direction.z;

        poolIndex = (poolIndex + 1) % poolSize;

        return vectorPool[poolIndex].set(ray.origin.add(ray.direction.scl(delta)));
    }
}
