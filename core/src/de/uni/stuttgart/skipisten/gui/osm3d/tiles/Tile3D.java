package de.uni.stuttgart.skipisten.gui.osm3d.tiles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonBatch;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.math.Matrix4;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.Renderer3D;
import de.uni.stuttgart.skipisten.gui.osm.texture.SharedTextureCache;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;
import lib.gui.util.gl32.MultisampledFramebuffer;

import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RunnableFuture;

import de.uni.stuttgart.skipisten.data.elevation.ElevationProvider;
import de.uni.stuttgart.skipisten.data.elevation.ElevationTile;
import de.uni.stuttgart.skipisten.data.graph.GraphManager;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;
import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;

public class Tile3D {
    private TileManager3D manager;
    private TilePosition tilePosition;

    private float tilePosX;
    private float tilePosY;
    private float tileSize;
    private ElevationTile elevation;

    // 0 1  |  x = index % 2
    // 2 3  |  y = index / 2
    private Tile3D[] childTiles;
    private volatile boolean loadingChildren = false;

    private static final ExecutorService runner = Executors.newFixedThreadPool(16);
    private RunnableFuture<TileFactory.MeshData> meshFuture = null;
    private TileFactory.MeshData mesh = null;

    private int framebufferVersion;
    private Model model;
    private ModelInstance modelInstance;
    private MultisampledFramebuffer frameBuffer;
    private Matrix4 framebufferMatrix;

    private boolean needsTextureLoad;

    private long lastRenderTime = System.currentTimeMillis();
    private long lastRenderFrameCounter;

    public Tile3D(TileManager3D manager, TilePosition tilePosition, float tilePosX, float tilePosY, float tileSize) {
        this.manager = manager;
        this.tilePosition = tilePosition;
        this.tilePosX = tilePosX;
        this.tilePosY = tilePosY;
        this.tileSize = tileSize;

        needsTextureLoad = true;
        ElevationProvider.loadTile(tilePosition, getResolution(), false);
    }

    public void render2DBefore() {
        if (!TileManager3D.frustum.sphereInFrustum(tilePosX + tileSize / 2f, tilePosY + tileSize / 2f, 0,
                tileSize * 2 + 100 + Renderer3D.getCamera().position.z)) return;

        if (getDistanceToCamera() < TileManager3D.DISTANCE_FOR_ZOOM_LEVEL[tilePosition.getZoom() + 1]) {
            if (childTiles != null) {
                for (int i = 0; i < 4; i++) {
                    childTiles[i].render2DBefore();
                }
            }
        }

        if ((frameBuffer != null && TileManager3D.currentFramebufferVersion == framebufferVersion) || !GraphManager.isGraphLoaded())
            return;

        framebufferVersion = TileManager3D.currentFramebufferVersion;

        if (!GraphManager.getGraph().getBoundingBox().crossesTile(tilePosition)) return;

        Texture baseTexture = SharedTextureCache.getInstance().getTexture(tilePosition); //get OSM map texture
        if (baseTexture == null) return;
        PolygonBatch batch = Renderer2D.getBatch();
        ModelBatch modelBatch = Renderer2D.getModelBatch();

        final int textureSizeExponent = 8;
        final int textureSize = 1 << textureSizeExponent; //= 256;

        if (frameBuffer == null) frameBuffer = new MultisampledFramebuffer(Pixmap.Format.RGB888, textureSize, textureSize, 4); //create framebuffer for tile
        batch.flush(); //flush previous batch data

        if (framebufferMatrix == null) {
            framebufferMatrix = new Matrix4(); //create orthographic projection for framebuffer
            framebufferMatrix.setToOrtho2D(0, 0, textureSize, textureSize);
        }
        batch.setProjectionMatrix(framebufferMatrix);

        frameBuffer.begin(); //initialize framebuffer for rendering
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        Draw.image(baseTexture, 0, 0, textureSize, textureSize, 1); //draw OSM map texture

        Collection<OSMElement> ways = GraphManager.getGraph().getWays(); //get OSMGraph ways for overlay

        batch.end(); //flush batch data

        OrthographicCamera cam = new OrthographicCamera();
        int subTileMask = 1 << (tilePosition.getZoom() - ElevationProvider.MIN_ZOOM);
        float subTileSize = 1f / subTileMask;
        float subTileX = (tilePosition.getX() % subTileMask) * subTileSize;
        float subTileY = (tilePosition.getY() % subTileMask) * subTileSize;

        cam.setToOrtho(false, subTileSize, subTileSize);
        cam.translate(subTileX, subTileY);
        cam.update();

        TilePosition modelPos = tilePosition.zoomOut(tilePosition.getZoom() - ElevationProvider.MIN_ZOOM);

        modelBatch.begin(cam);

        for (OSMElement way : ways) { //draw outer polylines
            if (way.outerLine != null && way.innerLine != null && way.outerLine.getBoundingBox().crossesTile(modelPos)) {
                modelBatch.render(way.outerLine.getModel(modelPos));
                modelBatch.render(way.innerLine.getModel(modelPos));
                //if (way.renderHighlight && way.highlightLine != null) modelBatch.render(way.highlightLine.getModel(modelPos));
            }
        }

        if (NavigationPath.currentPath != null && NavigationPath.currentPath.line != null
                && NavigationPath.currentPath.line.getBoundingBox().crossesTile(modelPos)) {
            modelBatch.render(NavigationPath.currentPath.line.getModel(modelPos));
        }

        modelBatch.end();

        frameBuffer.end(); //end rendering to framebuffer and reset batch camera
        batch.setProjectionMatrix(Renderer2D.instance().getCamera().combined);
        batch.begin();

        if (model != null)
            model.materials.get(0).set(TextureAttribute.createDiffuse(frameBuffer.getResolvedColorBufferTexture()));
    }

    public void render() {
        lastRenderTime = System.currentTimeMillis();
        lastRenderFrameCounter = TileManager3D.FRAME_COUNTER;

        // Request texture load of needed
        if (needsTextureLoad) {
            needsTextureLoad = false;

            SharedTextureCache.getInstance().loadTexture3d(tilePosition, () -> {
                        if (isCurrentlyRendered()) {
                            return true;
                        } else {
                            needsTextureLoad = true;
                            return false;
                        }
                    }
            );
        }

        if (modelInstance == null) {

            if (meshFuture == null && mesh == null) {
                if (!ElevationProvider.isTileLoaded(tilePosition, getResolution(), false)) return;
                elevation = ElevationProvider.getTile(tilePosition, getResolution(), false);
                meshFuture = TileFactory.createTileData(tilePosition, elevation);
                runner.submit(meshFuture);
            } else {
                Texture texture = null;
                if (GraphManager.getGraph().getBoundingBox().crossesTile(tilePosition)) {
                    if (frameBuffer == null) return;
                    texture = frameBuffer.getResolvedColorBufferTexture();
                } else {
                    texture = SharedTextureCache.getInstance().getTexture(tilePosition); //get OSM map texture
                    if (texture == null) return;
                }
                if (mesh == null) {
                    if (!meshFuture.isDone()) return;
                    try {
                        mesh = meshFuture.get();
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                    meshFuture = null;
                }
                model = TileFactory.createModel(mesh, texture);
                modelInstance = new ModelInstance(model);
                modelInstance.transform.setToTranslation(tilePosX, tilePosY, 0);
                modelInstance.transform.scale(tileSize, tileSize, 1f);
                meshFuture = null;
            }

            //System.out.println("TILE-3D LOADED!!!: " + tilePosition.toString());
        } else {
            if (getDistanceToCamera() < TileManager3D.DISTANCE_FOR_ZOOM_LEVEL[tilePosition.getZoom() + 1]) {

                if (childTiles == null && !loadingChildren) {
                    loadingChildren = true;
                    runner.submit(this::loadChildTiles);
                }

                if (!allChildTilesLoaded()) {
                    TileManager3D.RENDERED_TILES_AMOUNT++;
                    Renderer3D.render(modelInstance);
                }

                if (childTiles != null) {
                    for (int i = 0; i < 4; i++) {
                        childTiles[i].render();
                    }
                }
            } else {
                TileManager3D.RENDERED_TILES_AMOUNT++;
                Renderer3D.render(modelInstance);
            }
        }
    }

    private void loadChildTiles() {
        Tile3D[] children = new Tile3D[4];
        for (int i = 0; i < 4; i++) {

            int dX = i % 2;
            int dY = i / 2;
            float childTileSize = tileSize / 2;

            children[i] = new Tile3D(manager,
                    tilePosition.zoomIn(dX, dY),
                    tilePosX + childTileSize * dX, tilePosY + childTileSize * (1 - dY), childTileSize);

        }
        childTiles = children;
        loadingChildren = false;
    }

    /**
     * Check if textures have been disposed in 2d mode
     */
    public void viewSwitchedFrom2dTo3d() {
        if (childTiles != null) {
            for (int i = 0; i < 4; i++) childTiles[i].viewSwitchedFrom2dTo3d();
        }

        if(modelInstance != null && !SharedTextureCache.getInstance().isTextureLoaded(tilePosition)) {
            disposeTile();
        }
    }

    /**
     * Disposes old tiles, which have not been rendered for a while.
     */
    public void cleanUp() {
        if (childTiles != null) {
            for (int i = 0; i < 4; i++) childTiles[i].cleanUp();
        }

        long timeUntilUnload = 25_000 * 600 / Math.max(SharedTextureCache.getInstance().getCacheSize(), 100);

        if (modelInstance != null && getTimeSinceLastRender() > timeUntilUnload) {
            disposeTile();
        }
    }

    public void disposeTile() {
        if (frameBuffer != null) frameBuffer.dispose();
        frameBuffer = null;

        if (model != null) model.dispose();
        model = null;

        SharedTextureCache.getInstance().unloadTexture3d(tilePosition);
        needsTextureLoad = true;

        modelInstance = null;
    }

    public boolean tileIsLoaded() {
        return modelInstance != null;
    }

    private boolean isCurrentlyRendered() {
        return lastRenderFrameCounter == TileManager3D.FRAME_COUNTER;
    }

    private long getTimeSinceLastRender() {
        return System.currentTimeMillis() - lastRenderTime;
    }

    public boolean allChildTilesLoaded() {
        if (childTiles == null) return false;

        for (Tile3D childTile : childTiles) {
            if (childTile == null || !childTile.tileIsLoaded()) return false;
        }

        return true;
    }

    public float getDistanceToCamera() {
        Camera camera = Renderer3D.getCamera();
        float camX = camera.position.x;
        float camY = camera.position.y;
        float camZ = camera.position.z;
        float dx = 0;
        float dy = 0;
        float dz = 0;

        if (camX < tilePosX) {
            if (camY < tilePosY) { //x and y outside lower bounds
                dx = camX - tilePosX;
                dy = camY - tilePosY;
                dz = camZ - getZCoordinate(tilePosX, tilePosY);
            } else if (camY <= tilePosY + tileSize) { //x outside lower bound, y within bounds
                dx = camX - tilePosX;
                dz = camZ - getZCoordinate(tilePosX, camY);
            } else { //x outside lower bound, y outside higher bound
                dx = camX - tilePosX;
                dy = camY - (tilePosY + tileSize);
                dz = camZ - getZCoordinate(tilePosX, tilePosY + tileSize);
            }
        } else if (camX <= tilePosX + tileSize) {
            if (camY < tilePosY) { //x within bounds, y outside lower bound
                dy = camY - tilePosY;
                dz = camZ - getZCoordinate(camX, tilePosY);
            } else if (camY <= tilePosY + tileSize) { //x and y within bounds
                dz = camZ - getZCoordinate(camX, camY);
            } else { //x within bounds, y outside higher bound
                dy = camY - (tilePosY + tileSize);
                dz = camZ - getZCoordinate(camX, tilePosY + tileSize);
            }
        } else {
            if (camY < tilePosY) { //x outside higher bound, y outside lower bound
                dx = camX - (tilePosX + tileSize);
                dy = camY - tilePosY;
                dz = camZ - getZCoordinate(tilePosX + tileSize, tilePosY);
            } else if (camY <= tilePosY + tileSize) { //x outside higher bound, y within bounds
                dx = camX - (tilePosX + tileSize);
                dz = camZ - getZCoordinate(tilePosX + tileSize, camY);
            } else { //x and y outside higher bounds
                dx = camX - (tilePosX + tileSize);
                dy = camY - (tilePosY + tileSize);
                dz = camZ - getZCoordinate(tilePosX + tileSize, tilePosY + tileSize);
            }
        }

        return (float) Math.sqrt(dx * dx + dy * dy + dz * dz);
    }

    private int getResolution() {
        return Math.min(tilePosition.getZoom() + 5, 20);
    }

    /**
     * @return if the coordinate is within the tile bound,
     * returns false if the tile is not loaded yet.
     */
    public boolean isInTileBounds(float x, float y) {
        if (!tileIsLoaded()) return false;

        return x >= tilePosX && x <= tilePosX + tileSize
                && y >= tilePosY && y <= tilePosY + tileSize;
    }

    public float getZCoordinate(float x, float y) {
        if (childTiles != null) {
            for (Tile3D childTile : childTiles) {
                if (childTile.isInTileBounds(x, y)) {
                    return childTile.getZCoordinate(x, y);
                }
            }
        }

        if (elevation == null) return 0f;

        float xFraction = (x - tilePosX) / tileSize;
        float yFraction = 1 - ((y - tilePosY) / tileSize);

        // TODO refactor
        int edgeLengthVertices = elevation.getEdgeLength();
        int edgeLengthSpaces = edgeLengthVertices - 1;
        double tileSizeMapCoords = 1d / (1 << tilePosition.getZoom()); //edge length of tile in map coordinates
        double tileYMapCoords = tilePosition.getMapY();

        float unitInMeters = (float) (40_075_017d * Math.cos(Math.toRadians(TileMath.mapYToLatitude(tileYMapCoords + tileSizeMapCoords * yFraction / (double) edgeLengthSpaces))) / (double) (1 << TilePosition.MAX_ZOOM));

        return (float) elevation.getInterpolatedElevation(xFraction, yFraction) / unitInMeters;
    }
}
