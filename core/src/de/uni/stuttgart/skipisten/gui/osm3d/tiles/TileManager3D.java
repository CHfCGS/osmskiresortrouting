package de.uni.stuttgart.skipisten.gui.osm3d.tiles;

import com.badlogic.gdx.math.Frustum;
import com.badlogic.gdx.math.Vector3;
import lib.gui.Renderer3D;
import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;

import de.uni.stuttgart.skipisten.data.elevation.ElevationProvider;

public class TileManager3D {

    private TilePosition minZoomLevelOrigin;

    /*package-private*/ static int currentFramebufferVersion;

    // length * length = total size of array
    private final int length = 5;
    private Tile3D[][] tiles;

    private long lastTreeCleanUpTime;

    // --- class members ---
    public static int RENDERED_TILES_AMOUNT;
    public static long FRAME_COUNTER;

    public static int[] DISTANCE_FOR_ZOOM_LEVEL;
    private final static double ZOOM_DISTANCE_BASE = 0.51;
    private final static double ZOOM_DISTANCE_FACTOR = 3;

    public static Frustum frustum = new Frustum();

    public TileManager3D() {
        tiles = new Tile3D[length][length];
        initZoomLevelValues();
    }

    public void initZoomLevelValues() {
        // Specifies when to load / show higher zoom level tiles
        DISTANCE_FOR_ZOOM_LEVEL = new int[20];

        for (int i = 0; i < DISTANCE_FOR_ZOOM_LEVEL.length - 1; i++)
            DISTANCE_FOR_ZOOM_LEVEL[i] = (int) (Math.pow(ZOOM_DISTANCE_BASE, i - TilePosition.MAX_ZOOM) * ZOOM_DISTANCE_FACTOR);

        DISTANCE_FOR_ZOOM_LEVEL[19] = -1;

        /*for (int i = 0; i < DISTANCE_FOR_ZOOM_LEVEL.length; i++) {
            System.out.println(i + " -> " + DISTANCE_FOR_ZOOM_LEVEL[i]);
        }*/
    }

    public void setTileOrigin(TilePosition tileOrigin) {
        if (tileOrigin.getZoom() < ElevationProvider.MIN_ZOOM) {
            throw new IllegalArgumentException("tileOrigin.getZoom() must be " + ElevationProvider.MIN_ZOOM + "or larger.");
        }

        this.minZoomLevelOrigin = tileOrigin.zoomOut(tileOrigin.getZoom() - ElevationProvider.MIN_ZOOM);

        float minZoomLevelTileSize = (float) (1 << (TilePosition.MAX_ZOOM - ElevationProvider.MIN_ZOOM));

        for (int x = -length / 2; x <= length / 2; x++) {
            for (int y = -length / 2; y <= length / 2; y++) {
                tiles[x + length / 2][y + length / 2] = new Tile3D(this,
                        minZoomLevelOrigin.getNeighborPosition(x, y),
                        x * minZoomLevelTileSize, -y * minZoomLevelTileSize, minZoomLevelTileSize
                );
            }
        }
    }

    public void render2DBefore() {
        for (int x = 0; x < length; x++) {
            for (int y = 0; y < length; y++) {
                if (tiles[x][y] != null) tiles[x][y].render2DBefore();
            }
        }
    }

    public void render() {
        RENDERED_TILES_AMOUNT = 0;
        FRAME_COUNTER++;
        frustum.update(Renderer3D.getCamera().invProjectionView);

        for (int x = 0; x < length; x++) {
            for (int y = 0; y < length; y++) {
                if (tiles[x][y] != null) tiles[x][y].render();
            }
        }

        cleanUpIfNeeded();
    }

    private void cleanUpIfNeeded() {
        //Cleans up the HashMap every 15 seconds
        if (System.currentTimeMillis() - lastTreeCleanUpTime > 15_000) {
            lastTreeCleanUpTime = System.currentTimeMillis();

            for (int x = 0; x < length; x++) {
                for (int y = 0; y < length; y++) {
                    if (tiles[x][y] != null) tiles[x][y].cleanUp();
                }
            }
        }
    }

    /**
     * Signals that the tile framebuffers need to be updated in the next frame.
     */
    public static void setFramebufferUpdateRequired() {
        currentFramebufferVersion++;
    }

    /**
     * Units in scene coordinates
     */
    public float getZCoordinate(float x, float y) {
        Tile3D tile = getTileAtPosition(x, y);
        if (tile == null) return 0f;
        return tile.getZCoordinate(x, y);
    }

    private Tile3D getTileAtPosition(float x, float y) {
        float minZoomLevelTileSize = (float) (1 << TilePosition.MAX_ZOOM - ElevationProvider.MIN_ZOOM);
        int tileX = (int) Math.floor(x / minZoomLevelTileSize) + length / 2;
        int tileY = (int) Math.floor(-y / minZoomLevelTileSize) + length / 2 + 1;
        if (tileX < 0 || tileX >= length || tileY < 0 || tileY >= length) return null;
        return tiles[tileX][tileY];
    }

    public TilePosition getTilePositionAt(float x, float y) {
        return getFractTilePositionAt(x, y).getPosition();
    }

    public TilePosition.FractionalTilePosition getFractTilePositionAt(float x, float y) {
        float minZoomLevelTileSize = (float) (1 << TilePosition.MAX_ZOOM - ElevationProvider.MIN_ZOOM);
        float tileX = x / minZoomLevelTileSize;
        float tileY = -y / minZoomLevelTileSize;
        int wholeTileX = (int) Math.floor(tileX);
        int wholeTileY = (int) Math.floor(tileY);
        TilePosition pos = new TilePosition(wholeTileX + minZoomLevelOrigin.getX(), wholeTileY + minZoomLevelOrigin.getY() + 1, ElevationProvider.MIN_ZOOM);
        return new TilePosition.FractionalTilePosition(pos, tileX - wholeTileX, tileY - wholeTileY);
    }

    public Vector3 getWorldPosition(MapPosition mapPosition) {
        final double worldOriginX = minZoomLevelOrigin.getMapX();
        final double worldOriginY = minZoomLevelOrigin.getMapY() + 1 / ((double) (1 << minZoomLevelOrigin.getZoom()));

        double deltaX = mapPosition.getMapX() - worldOriginX;
        double deltaY = worldOriginY - mapPosition.getMapY();

        float worldX = (float) ((1 << TilePosition.MAX_ZOOM) * deltaX);
        float worldY = (float) ((1 << TilePosition.MAX_ZOOM) * deltaY);
        float worldZ = (float) ((1 << TilePosition.MAX_ZOOM) * mapPosition.getVisibleMapH());
        worldZ += getZCoordinate(worldX, worldY);

        return new Vector3(worldX, worldY, worldZ);
    }

    public Vector3 getWorldPosition(double latitude, double longitude) {
        final double worldOriginX = minZoomLevelOrigin.getMapX();
        final double worldOriginY = minZoomLevelOrigin.getMapY() + 1 / ((double) (1 << minZoomLevelOrigin.getZoom()));

        double deltaX = TileMath.longitudeToMapX(longitude) - worldOriginX;
        double deltaY = worldOriginY - TileMath.latitudeToMapY(latitude);

        float worldX = (float) ((1 << TilePosition.MAX_ZOOM) * deltaX);
        float worldY = (float) ((1 << TilePosition.MAX_ZOOM) * deltaY);
        float worldZ = getZCoordinate(worldX, worldY);

        return new Vector3(worldX, worldY, worldZ);
    }

    public void setMapPositionToWorldPosition(MapPosition mapPosition, float worldX, float worldY, float worldZ) {
        final double worldOriginX = minZoomLevelOrigin.getMapX();
        final double worldOriginY = minZoomLevelOrigin.getMapY() + 1 / ((double) (1 << minZoomLevelOrigin.getZoom()));

        float height = worldZ - getZCoordinate(worldX, worldY);

        mapPosition.setVisibleMapH(height / (double) ((1 << TilePosition.MAX_ZOOM)));

        double deltaX = worldX / (double) ((1 << TilePosition.MAX_ZOOM));
        double deltaY = worldY / (double) ((1 << TilePosition.MAX_ZOOM));

        mapPosition.setMapX(deltaX + worldOriginX);
        mapPosition.setMapY(worldOriginY - deltaY);
    }

    public void setMapPositionToWorldPosition(MapPosition mapPosition) {
        float worldX = Renderer3D.getCamera().position.x;
        float worldY = Renderer3D.getCamera().position.y;
        float worldZ = Renderer3D.getCamera().position.z;
        setMapPositionToWorldPosition(mapPosition, worldX, worldY, worldZ);
    }

    public MapPosition getMapPosition(OSMPanel panel, float x, float y, float z) {
        MapPosition pos = new MapPosition(panel);
        setMapPositionToWorldPosition(pos, x, y, z);
        return pos;
    }
}
