package de.uni.stuttgart.skipisten.gui.osm3d;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Align;
import lib.gui.Draw;
import lib.gui.Renderer3D;
import lib.gui.components.panel.Panel3D;
import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;
import de.uni.stuttgart.skipisten.gui.osm.texture.SharedTextureCache;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;
import de.uni.stuttgart.skipisten.gui.osm3d.camera.MapCameraController;
import de.uni.stuttgart.skipisten.gui.osm3d.tiles.TileManager3D;

import de.uni.stuttgart.skipisten.Main;
import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;

public class OSMPanel3D extends Panel3D {

    private final static boolean DRAW_DEBUG_INFO = false;

    private MapCameraController cameraController;
    private TileManager3D tileManager3D;

    private boolean isMarkerVisible = false;
    private Vector3 markerPosition = new Vector3();

    private ModelInstance sphereModel;
    private ModelInstance gpsPositionSphere;

    public OSMPanel3D() {
        super(true);

        cameraController = new MapCameraController();
        tileManager3D = new TileManager3D();

        Material sphereMatWhite = new Material(ColorAttribute.createDiffuse(Color.WHITE));
        Material sphereMatBlack = new Material(ColorAttribute.createDiffuse(new Color(0.3f, 0.3f, 0.3f, 1f)));

        sphereModel = new ModelInstance(new ModelBuilder().createSphere(0.3f, 0.3f, 0.3f, 10, 10, sphereMatWhite, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal));
        sphereModel.transform.setToTranslation(0, 0, 0);

        gpsPositionSphere = new ModelInstance(new ModelBuilder().createSphere(0.3f, 0.3f, 0.3f, 10, 10, sphereMatBlack, VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal));
        gpsPositionSphere.transform.setToTranslation(0, 0, 0);
    }

    @Override
    protected void componentInitialized() {
        setActive(isVisible());
    }

    public void setOrigin(double lat, double lon) {
        TilePosition tilePosition = new TilePosition(
                TileMath.longitudeToTileX(lon, 18),
                TileMath.latitudeToTileY(lat, 18),
                18);

        tileManager3D.setTileOrigin(tilePosition);
    }

    public void setCameraPositionFromMapPosition(MapPosition mapPosition) {
        //Set position based on MapPosition
        Vector3 worldPosition = tileManager3D.getWorldPosition(mapPosition);
        Renderer3D.getCamera().position.set(worldPosition);

        //Reset camera direction
        Renderer3D.getCamera().direction.set(0, 0, -1);
        Renderer3D.getCamera().up.set(0, 1, 0);
    }

    public void setMapPositionToWorldPosition(MapPosition mapPosition) {
        tileManager3D.setMapPositionToWorldPosition(mapPosition);

        // Increase 2d zoom for higher viewing angles in 3d mode
        if (cameraController.getCurrentCameraToMapAngle() > 145) {
            mapPosition.setVisibleMapW(mapPosition.getVisibleMapW() * 4f);
        } else if (cameraController.getCurrentCameraToMapAngle() > 110) {
            mapPosition.setVisibleMapW(mapPosition.getVisibleMapW() * 2f);
        }

        cameraController.resetCurrentCameraToMapAngle();
    }

    public void setMapPositionToWorldPosition(MapPosition mapPosition, float worldX, float worldY, float worldZ) {
        tileManager3D.setMapPositionToWorldPosition(mapPosition, worldX, worldY, worldZ);
    }

    public MapPosition getMapPosition(OSMPanel panel, float x, float y, float z) {
        return tileManager3D.getMapPosition(panel, x, y, z);
    }

    public void hideMarkerSphere() {
        isMarkerVisible = false;
    }

    public void showMarkerSphere(double latitude, double longitude) {
        isMarkerVisible = true;

        Vector3 worldPosition = tileManager3D.getWorldPosition(latitude, longitude);
        worldPosition.z -= 0.045f;

        markerPosition.set(worldPosition);
    }

    public float getZCoordinate(float x, float y) {
        return tileManager3D.getZCoordinate(x, y);
    }

    private Vector3 cameraTargetPosition;
    private Vector3 tempVector = new Vector3();

    public void zoomMapTo(OSMPanel panel, double latitude, double longitude, double mapW) {
        MapPosition position = new MapPosition(panel);
        position.setVisibleMapW(mapW);
        position.setLatitude(latitude);
        position.setLongitude(longitude);

        cameraTargetPosition = tileManager3D.getWorldPosition(position);
    }

    public void zoomMapToXY(OSMPanel panel, double mapX, double mapY, double mapW) {
        MapPosition position = new MapPosition(panel);
        position.setVisibleMapW(mapW);
        position.setMapX(mapX);
        position.setMapY(mapY);

        cameraTargetPosition = tileManager3D.getWorldPosition(position);
    }

    @Override
    protected void update(long deltaTimeInMs) {

        if(cameraTargetPosition != null) {
            Renderer3D.getCamera().position.lerp(cameraTargetPosition, 0.125f);
            Renderer3D.getCamera().direction.lerp(new Vector3(0,0,-1), 0.2f);
            Renderer3D.getCamera().up.lerp(new Vector3(0,1, 0), 0.2f);

            tempVector.set(Renderer3D.getCamera().position);
            tempVector.sub(cameraTargetPosition);
            if(tempVector.z < 2) tempVector.z = 0;

            // Animation done
            if(tempVector.len2() < 1) {
                cameraTargetPosition = null;
            }
        }

        cameraController.update(tileManager3D);
    }

    @Override
    protected void render2DBefore() {
        Draw.setColor(0.68f, 0.68f, 1f, 1f);
        Draw.rect(this);
        Draw.setColor(Color.WHITE);
        tileManager3D.render2DBefore();
    }

    @Override
    protected void render() {
        tileManager3D.render();

        if (isMarkerVisible) {
            sphereModel.transform.setToTranslation(markerPosition);
            Renderer3D.render(sphereModel);
        }

        //Highlight waypoints
        if(NavigationPath.currentPath != null) {
            for(NavigationPath.Waypoint waypoint : NavigationPath.currentPath.waypoints) {
                Vector3 worldPosition = tileManager3D.getWorldPosition(waypoint.getNode().lat, waypoint.getNode().lon);
                worldPosition.z -= 0.045f;

                sphereModel.transform.setToTranslation(worldPosition);
                Renderer3D.render(sphereModel);
            }
        }

        //Show gps position
        if(Main.gps.hasGPSLocation(300)) {
            Vector3 worldPosition = tileManager3D.getWorldPosition(Main.gps.getLatitude(), Main.gps.getLongitude());
            worldPosition.z -= 0.045f;

            gpsPositionSphere.transform.setToTranslation(worldPosition);
            Renderer3D.render(gpsPositionSphere);
        }
    }

    @Override
    protected void render2DAfter() {
        if (DRAW_DEBUG_INFO) {
            Draw.setColor(Color.BLACK);
            Draw.setTextAlignment(Align.left);

            float h = this.h.value();
            Draw.setTextSize(h * 0.015f);
            Draw.font(h * 0.01f, h * 0.01f, w.value(), "FPS: " + Gdx.graphics.getFramesPerSecond());
            Draw.font(h * 0.01f, h * 0.025f, w.value(), "Rendered Tiles: " + TileManager3D.RENDERED_TILES_AMOUNT);
            Draw.font(h * 0.01f, h * 0.04f, w.value(), "Camera Z: " + Renderer3D.getCamera().position.z);
            Draw.font(h * 0.01f, h * 0.055f, w.value(), "Texture Queue: " + SharedTextureCache.getInstance().getQueueSize());
            Draw.font(h * 0.01f, h * 0.07f, w.value(), "Texture Cache: " + SharedTextureCache.getInstance().getCacheSize());
        }
    }

    @Override
    protected boolean scrolled(int amount) {
        // if (!isInMapBounds(mouseX, mouseY)) return false;

        Vector3 screenCenterPoint = cameraController.getCameraUtil()
                .getPlanePoint(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);


        if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {

            cameraController.rotateMap(screenCenterPoint, ((amount > 0) ? 4f : -4f));

        } else if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT)) {

            cameraController.changeViewAngle(screenCenterPoint, (amount > 0) ? 4f : -4f);

        } else {
            cameraController.zoomMap((amount > 0) ? 1.2f : 0.8f);
        }

        return true;
    }

    public void setActive(boolean isActive) {
        setVisible(isActive);
        cameraController.setActive(isActive);
    }

    public TilePosition getTilePositionAt(float x, float y) {
        return tileManager3D.getTilePositionAt(x, y);
    }

    public TilePosition.FractionalTilePosition getFractTilePositionAt(float x, float y) {
        return tileManager3D.getFractTilePositionAt(x, y);
    }
}
