package de.uni.stuttgart.skipisten.gui.osm3d.camera;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screens;

import de.uni.stuttgart.skipisten.gui.mainscreen.MainScreen;
import de.uni.stuttgart.skipisten.gui.mainscreen.NavigationDirectionsOverlay;

public class CustomGestureDetector implements GestureDetector.GestureListener {

    private GestureDetector gestureDetector;
    private CustomGestureListener customGestureListener;

    private CameraUtil cameraUtil;

    private boolean isActive = true;

    // --- general ---
    private Vector2 initialPointer1_2d;
    private Vector2 initialPointer2_2d;
    private Vector3 initialPointer1_3d;
    private Vector3 initialPointer2_3d;

    private Vector2 lastPointer1_2d = new Vector2();
    private Vector2 lastPointer2_2d = new Vector2();

    private Vector2 lastTouchDownPoint = new Vector2();

    // --- rotation ---
    private boolean isPinchActive;
    private float initialPinchAngle;
    private float currentPinchAngle;
    private float oldTotalPinchRotation;

    // --- zoom ---
    private float initialPointerDistance2D;
    private float lastPointerDistance2D;
    private float pointerDistance2D;

    public CustomGestureDetector(CustomGestureListener customGestureListener, Camera camera) {
        this.customGestureListener = customGestureListener;

        gestureDetector = new GestureDetector(this);
        cameraUtil = new CameraUtil(camera);

        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(gestureDetector);
        multiplexer.addProcessor(Gdx.input.getInputProcessor());

        Gdx.input.setInputProcessor(multiplexer);
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    private boolean isActive() {
        return isActive && !Dialogs.isDialogOpen() &&
                (!NavigationDirectionsOverlay.isPressed || !Screens.isOpen(MainScreen.class));
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button) {
        lastTouchDownPoint.set(x, y);
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button) {
        return false;
    }

    @Override
    public boolean longPress(float x, float y) {
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button) {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {
        if (isActive()) {
            customGestureListener.pan(x, y, deltaX, deltaY);
        }

        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        if (isActive()) {
            customGestureListener.panStop(x, y, pointer, button);
        }

        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance) {
        return false;
    }

    @Override
    public boolean pinch(Vector2 lastPointer1_2d, Vector2 lastPointer2_2d, Vector2 pointer1_2d, Vector2 pointer2_2d) {
        if (!isActive()) return false;

        // --- rotation ---
        float deltaX2D = pointer2_2d.x - pointer1_2d.x;
        float deltaY2D = pointer2_2d.y - pointer1_2d.y;

        oldTotalPinchRotation = getTotalPinchRotation();

        currentPinchAngle = (float) Math.atan2(deltaY2D, deltaX2D) * MathUtils.radiansToDegrees;

        // --- zoom ---
        pointerDistance2D = (float) Math.sqrt(Math.pow(deltaX2D, 2) + Math.pow(deltaY2D, 2));

        // --- pinch start ---
        if (!isPinchActive) {
            isPinchActive = true;
            initialPinchAngle = currentPinchAngle;

            initialPointerDistance2D = pointerDistance2D;
            lastPointerDistance2D = pointerDistance2D;

            this.initialPointer1_2d = lastPointer1_2d;
            this.initialPointer2_2d = lastPointer2_2d;

            this.initialPointer1_3d = cameraUtil.getPlanePoint(initialPointer1_2d);
            this.initialPointer2_3d = cameraUtil.getPlanePoint(initialPointer2_2d);

            this.lastPointer1_2d.set(pointer1_2d);
            this.lastPointer2_2d.set(pointer2_2d);
        }

        customGestureListener.pinch(pointer1_2d, pointer2_2d, cameraUtil.getPlanePoint(pointer1_2d), cameraUtil.getPlanePoint(pointer2_2d));

        this.lastPointer1_2d.set(pointer1_2d);
        this.lastPointer2_2d.set(pointer2_2d);

        lastPointerDistance2D = pointerDistance2D;

        return false;
    }

    @Override
    public void pinchStop() {
        isPinchActive = false;
        oldTotalPinchRotation = 0;

        if (!isActive()) return;

        customGestureListener.pinchStop();
    }

    // --- general ---
    public Vector2 getInitialPointer1_2d() {
        return initialPointer1_2d;
    }

    public Vector2 getInitialPointer2_2d() {
        return initialPointer2_2d;
    }

    public Vector3 getInitialPointer1_3d() {
        return initialPointer1_3d;
    }

    public Vector3 getInitialPointer2_3d() {
        return initialPointer2_3d;
    }

    public Vector2 getLastPointer1_2d() {
        return lastPointer1_2d;
    }

    public Vector2 getLastPointer2_2d() {
        return lastPointer2_2d;
    }

    public Vector2 getLastTouchDownPoint() {
        return lastTouchDownPoint;
    }

    // --- rotation ---
    public float getInitialPinchAngle() {
        return initialPinchAngle;
    }

    public float getCurrentPinchAngle() {
        return currentPinchAngle;
    }

    public float getTotalPinchRotation() {
        return getCurrentPinchAngle() - getInitialPinchAngle();
    }

    public float getDeltaPinchRotation() {
        return getTotalPinchRotation() - oldTotalPinchRotation;
    }

    // --- zoom ---
    public float getTotalZoom() {
        return initialPointerDistance2D / pointerDistance2D;
    }

    public float getDeltaZoom() {
        return lastPointerDistance2D / pointerDistance2D;
    }

    public interface CustomGestureListener {
        boolean pan(float x, float y, float deltaX, float deltaY);

        boolean panStop(float x, float y, int pointer, int button);

        boolean pinch(Vector2 pointer1_2d, Vector2 pointer2_2d, Vector3 pointer1_3d, Vector3 pointer2_3d);

        void pinchStop();
    }
}
