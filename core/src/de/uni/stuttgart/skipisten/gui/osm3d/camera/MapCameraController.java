package de.uni.stuttgart.skipisten.gui.osm3d.camera;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import lib.gui.GUIManager;
import lib.gui.Renderer3D;
import de.uni.stuttgart.skipisten.gui.osm3d.tiles.TileManager3D;

public class MapCameraController implements CustomGestureDetector.CustomGestureListener {

    private Camera camera;
    private CameraUtil cameraUtil;
    private CustomGestureDetector gesture;

    private int currentPinchMode = MODE_NONE;

    private static final int MODE_NONE = 0;
    private static final int MODE_ZOOM = 1;
    private static final int MODE_ROTATE = 2;
    private static final int MODE_CHANGE_VIEW_ANGLE = 3;

    // Animation
    private float dragSpeedX, dragSpeedY, viewAngleSpeed, rotationSpeed, zoomSpeed = 1f;

    private Vector3 changeViewAngleCenter = new Vector3();
    private Vector3 rotationCenter = new Vector3();

    private float currentCameraToMapAngle = 90;

    public MapCameraController() {
        this.camera = Renderer3D.getCamera();
        gesture = new CustomGestureDetector(this, camera);
        cameraUtil = new CameraUtil(camera);
    }

    public void update(TileManager3D tileManager3D) {
        dragSpeedX /= 1.2f;
        dragSpeedY /= 1.2f;
        viewAngleSpeed /= 1.2f;
        rotationSpeed /= 1.2f;

        if (zoomSpeed > 1f) {
            zoomSpeed = 1 + (zoomSpeed - 1) / 1.5f;
            if (zoomSpeed < 1.001f) zoomSpeed = 1f;
        } else {
            zoomSpeed = zoomSpeed + (1 - zoomSpeed) / 4f;
            if (zoomSpeed > 0.999f) zoomSpeed = 1f;
        }

        if (!Gdx.input.isTouched()) {
            if (dragSpeedX != 0.0 || dragSpeedY != 0.0) {
                moveMap(dragSpeedX, dragSpeedY);
            }
            if (rotationSpeed != 0.0) {
                rotateMap(rotationCenter, rotationSpeed);
            }
            if (viewAngleSpeed != 0.0) {
                changeViewAngle(changeViewAngleCenter, viewAngleSpeed);
            }
            if (zoomSpeed != 1f) {
                zoomMap(zoomSpeed);
            }
        }

        //Prevent camera from being below terrain
        float terrainZHeight = tileManager3D.getZCoordinate(camera.position.x, camera.position.y);

        if (camera.position.z < terrainZHeight + 1f && !Float.isNaN(terrainZHeight))
            camera.position.z = terrainZHeight + 1f;

        if(camera.position.len() > (1 << 17))
            camera.position.lerp(Vector3.Zero, 0.1f);

        if (Float.isNaN(camera.position.z))
            camera.position.z = 1000;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY) {

        float fractionX = deltaX / Gdx.graphics.getWidth();
        float fractionY = deltaY / Gdx.graphics.getHeight();

        // --- Rotation and Camera Angle Control on Desktop ---
        if (Gdx.app.getType() == Application.ApplicationType.Desktop &&
                GUIManager.instance().getInputButton(0) == Input.Buttons.RIGHT) {

            Vector3 worldPoint = cameraUtil.getPlanePoint(gesture.getLastTouchDownPoint());
            Vector3 screenCenterPoint = cameraUtil.getPlanePoint(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);

            changeViewAngle(screenCenterPoint, fractionY * 180f);
            rotateMap(screenCenterPoint, fractionX * 360f);

            return true;
        }

        // -- Map Movement ---
        Vector3 panPos = cameraUtil.getPlanePoint(x, y);
        Vector3 panPosDelta = cameraUtil.getPlanePoint(x + deltaX, y + deltaY);

        float moveX = panPos.x - panPosDelta.x;
        float moveY = panPos.y - panPosDelta.y;

        if (moveX > camera.position.z / 5f) moveX = camera.position.z / 5f;
        if (moveY > camera.position.z / 5f) moveY = camera.position.z / 5f;

        if (moveX < -camera.position.z / 5f) moveX = -camera.position.z / 5f;
        if (moveY < -camera.position.z / 5f) moveY = -camera.position.z / 5f;

        dragSpeedX += moveX / 2;
        dragSpeedY += moveY / 2;

        moveMap(moveX, moveY);

        return true;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button) {
        return true;
    }

    @Override
    public void pinchStop() {
        currentPinchMode = MODE_NONE;
    }

    @Override
    public boolean pinch(Vector2 pointer1_2d, Vector2 pointer2_2d, Vector3 pointer1_3d, Vector3 pointer2_3d) {

        float pointerDistanceX2D = Math.abs(pointer2_2d.x - pointer1_2d.x);
        float pointerDistanceY2D = Math.abs(pointer2_2d.y - pointer1_2d.y);

        Vector3 fingerPos = pointer1_3d.add(pointer2_3d).scl(0.5f);

        // No mode detected yet, try to detect mode
        if (currentPinchMode == MODE_NONE) {
            float pinchDistanceX1 = gesture.getInitialPointer1_2d().x - pointer1_2d.x;
            float pinchDistanceX2 = gesture.getInitialPointer2_2d().x - pointer2_2d.x;
            float pinchDistanceY1 = gesture.getInitialPointer1_2d().y - pointer1_2d.y;
            float pinchDistanceY2 = gesture.getInitialPointer2_2d().y - pointer2_2d.y;

            if (Math.abs(pinchDistanceY1) > 0 && Math.abs(pinchDistanceY2) > 0) {

                if (Math.abs(pinchDistanceY1 + pinchDistanceY2) > Gdx.graphics.getHeight() / 20f &&
                        Math.abs(pinchDistanceX1) + Math.abs(pinchDistanceX2) < Gdx.graphics.getHeight() / 10f
                        && Math.abs(pointer1_2d.y - pointer2_2d.y) < Gdx.graphics.getHeight() / 10f) {

                    currentPinchMode = MODE_CHANGE_VIEW_ANGLE;
                }
            }

            if (Math.abs(gesture.getTotalPinchRotation()) > 10f && pointerDistanceY2D >= pointerDistanceX2D / 2) {
                currentPinchMode = MODE_ROTATE;
            }

        } else if (currentPinchMode == MODE_CHANGE_VIEW_ANGLE) {
            float pinchDistance = (pointer1_2d.y - gesture.getLastPointer1_2d().y) + (pointer2_2d.y - gesture.getLastPointer2_2d().y);

            float angle = (pinchDistance / Gdx.graphics.getHeight()) * 180f;

            // For animation
            viewAngleSpeed += angle / 2f;

            changeViewAngleCenter.set(fingerPos);
            changeViewAngle(changeViewAngleCenter, angle);

        } else if (currentPinchMode == MODE_ROTATE) {
            rotationCenter.set(fingerPos);

            // For animation
            rotationSpeed += gesture.getDeltaPinchRotation() / 2f;

            rotateMap(rotationCenter, gesture.getDeltaPinchRotation());
        }

        if (currentPinchMode != MODE_CHANGE_VIEW_ANGLE) {
            zoomSpeed *= gesture.getDeltaZoom();
            zoomMap(gesture.getDeltaZoom());

            if ((gesture.getTotalZoom() > 1.5f || gesture.getTotalZoom() < 0.75f) && currentPinchMode == MODE_NONE) {
                currentPinchMode = MODE_ZOOM;
            }
        }

        return true;
    }

    private void moveMap(float moveX, float moveY) {
        camera.position.x += moveX;
        camera.position.y += moveY;
    }

    public void rotateMap(Vector3 atPoint, float angle) {
        camera.rotateAround(atPoint, Vector3.Z, angle);
    }

    private Vector3 lastCameraPosition = new Vector3();
    private Vector3 lastCameraDirection = new Vector3();
    private Vector3 lastCameraUp = new Vector3();

    private Vector3 down = new Vector3(0, 0, -1);

    private Vector3 rotationAxis = new Vector3();

    public void changeViewAngle(Vector3 atPoint, float angle) {
        float oldZ = camera.position.z;

        lastCameraPosition.set(camera.position);
        lastCameraDirection.set(camera.direction);
        lastCameraUp.set(camera.up);

        rotationAxis.set(camera.up.x, camera.up.y, 0).rotate(Vector3.Z, 90f);

        camera.rotateAround(atPoint, rotationAxis, angle);

        float cameraToMapAngle = MathUtils.acos(down.dot(camera.up)) * MathUtils.radiansToDegrees;

        // Only angle 90 to 175 is allowed
        if (cameraToMapAngle < 90 || cameraToMapAngle > 155) {
            camera.position.set(lastCameraPosition);
            camera.direction.set(lastCameraDirection);
            camera.up.set(lastCameraUp);
        }

        this.currentCameraToMapAngle = MathUtils.acos(down.dot(camera.up)) * MathUtils.radiansToDegrees;

        if(Float.isNaN(camera.position.z)) {
            camera.position.z = oldZ;
        }
    }

    public void zoomMap(float amount) {
        float oldZ = camera.position.z;

        Vector3 screenCenterPoint = cameraUtil.getPlanePoint(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f);

        float distance = camera.position.dst(screenCenterPoint);

        Vector3 zoomVector = new Vector3(camera.direction).scl(distance * (1 - amount));

        camera.position.add(zoomVector);

        if(Float.isNaN(camera.position.z)) {
            camera.position.z = oldZ;
        }
    }

    public CameraUtil getCameraUtil() {
        return cameraUtil;
    }

    public void setActive(boolean isActive) {
        gesture.setActive(isActive);
    }

    public float getCurrentCameraToMapAngle() {
        return currentCameraToMapAngle;
    }

    public void resetCurrentCameraToMapAngle() {
        currentCameraToMapAngle = 90f;
    }
}
