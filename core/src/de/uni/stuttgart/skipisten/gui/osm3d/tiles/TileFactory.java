package de.uni.stuttgart.skipisten.gui.osm3d.tiles;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;

import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

import de.uni.stuttgart.skipisten.data.elevation.ElevationTile;

public class TileFactory {

    private static final int VERTEX_STRIDE = 8;

    // The edge vector directions used to calculate the normal vectors for the vertices
    private static final int[][] edgeVectorDirections = {
            {1, 1}, {0, 1},
            {1, 0}, {1, 1},
            {0, -1}, {1, 0},
            {-1, -1}, {0, -1},
            {-1, 0}, {-1, -1},
            {0, 1}, {-1, 0}
    };

    /**
     * Generates the tile mesh data for the given tile.
     * @param pos OSM tile position
     * @param elevation Tile elevation data
     * @return Future mesh data
     */
    public static RunnableFuture<MeshData> createTileData(TilePosition pos, ElevationTile elevation) {
        return new FutureTask<>(() -> {
            if (elevation.isNullTile()) { //create simple plane for null-tile
                float[] vertices = new float[] {
                //   position |  normal   |   uv
                    0f, 1f, 0f, 0f, 0f, 1f, 0f, 0f, //north-west
                    1f, 1f, 0f, 0f, 0f, 1f, 1f, 0f, //north-east
                    0f, 0f, 0f, 0f, 0f, 1f, 0f, 1f, //south-west
                    1f, 0f, 0f, 0f, 0f, 1f, 1f, 1f  //south-east
                };
                short[] indices = new short[] {
                    0, 2, 1, //north-west triangle
                    1, 2, 3  //south-east triangle
                };
                return new MeshData(vertices, indices);
            }

            int edgeLengthVertices = elevation.getEdgeLength();
            int edgeLengthSpaces = edgeLengthVertices - 1;

            float[] vertices = new float[edgeLengthVertices * edgeLengthVertices * VERTEX_STRIDE];
            int vertexArrayPos = 0;
            short[] indices = new short[edgeLengthSpaces * edgeLengthSpaces * 6]; //each space has two triangles with three indices each
            int indexArrayPos = 0;

            short currentVertexIndex = 0;
            short[][] vertexIndices = new short[edgeLengthVertices][edgeLengthVertices];

            // The size of a grid rectangle part
            float size = 1f / (float) edgeLengthSpaces;

            double tileSizeMapCoords = 1d / (1 << pos.getZoom()); //edge length of tile in map coordinates
            double tileYMapCoords = pos.getMapY();

            // --- create vertices (tile grid) ---
            for (int y = 0; y < edgeLengthVertices; y++) {
                float unitInMeters = (float)(40_075_017d * Math.cos(Math.toRadians(TileMath.mapYToLatitude(tileYMapCoords + tileSizeMapCoords * y / (double)edgeLengthSpaces))) / (double) (1 << TilePosition.MAX_ZOOM));
                float elevationScale = 1f / unitInMeters;
                for (int x = 0; x < edgeLengthVertices; x++) {
                    float offsetX = x / (float) edgeLengthSpaces;
                    float offsetY = y / (float) edgeLengthSpaces;

                    float vertexElevation = elevation.getElevation(x, y, elevationScale); //elevation of the current vertex

                    // --- calculate normal vector ---

                    int numEdgeVectorPairs = edgeVectorDirections.length / 2; //number of edge vector pairs used for normal calculation

                    float normalX = 0, normalY = 0, normalZ = 0; //components of normal vector

                    for (int i = 0; i < numEdgeVectorPairs; i++) {
                        int edge1XOffset = edgeVectorDirections[i * 2][0]; //offsets of first edge vector
                        int edge1YOffset = edgeVectorDirections[i * 2][1];
                        float edge1X = edge1XOffset * size; //components of first edge vector
                        float edge1Y = edge1YOffset * size;
                        float edge1Z = elevation.getElevation(x + edge1XOffset,y + edge1YOffset, elevationScale) - vertexElevation;

                        int edge2XOffset = edgeVectorDirections[i * 2 + 1][0]; //offsets of second edge vector
                        int edge2YOffset = edgeVectorDirections[i * 2 + 1][1];
                        float edge2X = edge2XOffset * size; //components of second edge vector
                        float edge2Y = edge2YOffset * size;
                        float edge2Z = elevation.getElevation(x + edge2XOffset, y + edge2YOffset, elevationScale) - vertexElevation;

                        float crossX = edge1Y * edge2Z - edge1Z * edge2Y; //cross product of edge vectors
                        float crossY = edge1Z * edge2X - edge1X * edge2Z;
                        float crossZ = edge1X * edge2Y - edge1Y * edge2X;
                        float crossLength = (float)Math.sqrt(crossX * crossX + crossY * crossY + crossZ * crossZ); //length of cross product

                        normalX += crossX / crossLength; //add normalized cross product to normal vector
                        normalY += crossY / crossLength;
                        normalZ += crossZ / crossLength;
                    }

                    normalX /= numEdgeVectorPairs; //normalize normal vector
                    normalY /= numEdgeVectorPairs;
                    normalZ /= numEdgeVectorPairs;

                    // --- add vertex to mesh data ---
                    vertices[vertexArrayPos++] = offsetX;           //pos x
                    vertices[vertexArrayPos++] = 1f - offsetY;      //pos y
                    vertices[vertexArrayPos++] = vertexElevation;   //pos z
                    vertices[vertexArrayPos++] = normalX;           //nor x
                    vertices[vertexArrayPos++] = normalY;           //nor y
                    vertices[vertexArrayPos++] = normalZ;           //nor z
                    vertices[vertexArrayPos++] = offsetX;           //tex u
                    vertices[vertexArrayPos++] = offsetY;           //tex v

                    vertexIndices[x][y] = currentVertexIndex++;
                }
            }

            // --- create triangles ---
            for (int x = 0; x < edgeLengthSpaces; x++) {
                for (int y = 0; y < edgeLengthSpaces; y++) {
                    //triangle 1
                    indices[indexArrayPos++] = vertexIndices[x][y];
                    indices[indexArrayPos++] = vertexIndices[x + 1][y + 1];
                    indices[indexArrayPos++] = vertexIndices[x + 1][y];
                    //triangle 2
                    indices[indexArrayPos++] = vertexIndices[x + 1][y + 1];
                    indices[indexArrayPos++] = vertexIndices[x][y];
                    indices[indexArrayPos++] = vertexIndices[x][y + 1];
                }
            }

            return new MeshData(vertices, indices);
        });
    }

    /**
     * Creates a tile model based on the given data
     * @param data      Mesh data for the model
     * @param texture   the texture of the tile
     * @return the created model
     */
    public static Model createModel(MeshData data, Texture texture) {
        Material material = new Material(TextureAttribute.createDiffuse(texture), ColorAttribute.createSpecular(new Color(0x302000ff)));

        final long attributes = VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal | VertexAttributes.Usage.TextureCoordinates;

        ModelBuilder modelBuilder = new ModelBuilder();
        modelBuilder.begin();

        MeshPartBuilder meshBuilder = modelBuilder.part("tile", GL20.GL_TRIANGLES, attributes, material);
        meshBuilder.ensureVertices(data.vertices.length / VERTEX_STRIDE);
        meshBuilder.ensureIndices(data.indices.length);

        meshBuilder.vertex(data.vertices);
        for (short i : data.indices) meshBuilder.index(i);

        return modelBuilder.end();
    }

    public static class MeshData {
        public float[] vertices;
        public short[] indices;

        public MeshData(float[] vertices, short[] indices) {
            this.vertices = vertices;
            this.indices = indices;
        }
    }
}
