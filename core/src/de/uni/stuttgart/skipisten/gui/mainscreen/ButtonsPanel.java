package de.uni.stuttgart.skipisten.gui.mainscreen;

import com.badlogic.gdx.graphics.Color;
import lib.gui.Draw;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.Size;
import lib.gui.components.basic.ImageButton;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.panel.Panel;
import lib.gui.components.screen.Screens;

import de.uni.stuttgart.skipisten.Main;

public class ButtonsPanel extends Panel {

    @PosX(w = -0.34, align = Alignment.CENTER)
    @Size(h = 1)
    private final ImageButton settings;

    @PosX(w = -0.12, align = Alignment.CENTER)
    @Size(h = 1)
    private final ImageButton location;

    @PosX(w = 0.12, align = Alignment.CENTER)
    @Size(h = 1)
    private final ImageButton directions;

    @PosX(w = 0.34, align = Alignment.CENTER)
    @Size(h = 1)
    private final ImageButton map;

    public ButtonsPanel() {
        settings = new ImageButton("settings", 0.7f);
        settings.setEvent(() -> Dialogs.open(SettingsDialog.class));
        map = new ImageButton("map", 0.7f);
        map.setEvent(() -> Screens.get(MainScreen.class).osmView.toggleMode());
        location = new ImageButton("location", 0.7f);
        location.setEvent(this::gpsButtonPressed);

        directions = new ImageButton("directions", 0.7f);
        directions.setEvent(() -> {
            Screens.get(MainScreen.class).toggleDirectionsOverlay();
        });

        settings.setButtonColor(Color.GRAY);
        map.setButtonColor(Color.GRAY);
        location.setButtonColor(Color.GRAY);
        directions.setButtonColor(Color.GRAY);
    }

    private void gpsButtonPressed() {
        if (!Main.gps.isGPSSupported()) {
            Dialogs.showInfoDialog("Info", "GPS is not supported on this device!");
        } else if (!Main.gps.isGPSEnabled()) {
            Dialogs.showInfoDialog("Info", "Please enable GPS on your device");
        } else if (!Main.gps.hasGPSPermission()) {
            Main.gps.askForGPSPermission(() -> {
                if (Main.gps.hasGPSPermission())
                    appHasAccessToGps();
            });
        } else {
            appHasAccessToGps();
        }
    }

    private void appHasAccessToGps() {
        if (Main.gps.hasGPSLocation(300)) {
            Screens.get(MainScreen.class).osmView.zoomMapTo(Main.gps.getLatitude(), Main.gps.getLongitude(), 0.000025, true);
        } else {
            Main.gps.start();
            Dialogs.showInfoDialog("Info", "GPS has been enabled, but there is currently no gps fix yet.");
        }
    }

    @Override
    protected void render() {
        Draw.rect(this, new Color(1, 1, 1, 0.75f));
        Draw.setAlpha(0.5f);
        Draw.shadow(0, 0, w.value(), -h.value() * 0.1f, false);
        Draw.setAlpha(1f);
    }
}
