package de.uni.stuttgart.skipisten.gui.mainscreen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.components.panel.ListPanel;
import lib.gui.components.screen.Screens;
import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;
import lib.gui.util.TextRenderer;

import de.uni.stuttgart.skipisten.data.overpass.OSMElement;
import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;
import de.uni.stuttgart.skipisten.gui.chooselocation.ChoosePositionScreen;
import de.uni.stuttgart.skipisten.gui.draw.Colors;

public class NavigationDirectionsOverlay extends ListPanel {

    private TextureRegion placeIcon;
    private TextRenderer textRenderer;

    /**
     * Needed to block map camera controller
     */
    public static boolean isPressed;

    protected NavigationDirectionsOverlay() {
        super(true, true);
        placeIcon = Renderer2D.getTextureManager().getTextureRegion("place");
        textRenderer = new TextRenderer();
    }

    private boolean isModeChooseStartPosition() {
        return NavigationPath.currentPath.waypoints.size() == 0;
    }

    private boolean isModeChooseEndPosition() {
        return NavigationPath.currentPath.waypoints.size() == 1;
    }

    private boolean isModeShowTotalPathData() {
        return NavigationPath.currentPath.waypoints.size() > 2;
    }

    @Override
    protected void update(long deltaTimeInMs) {
        if (!isVisible()) return;

        int listSize;

        //Just show "Choose Start Position"
        if (isModeChooseStartPosition()) {
            listSize = 1;
        }
        //Show "Choose Start Position" and "Choose End Position"
        else if (isModeChooseEndPosition()) {
            listSize = 2;
        }
        //One element for every waypoint + (waypoint - 1) Plus-Buttons
        else {
            int amountOfWaypoints = NavigationPath.currentPath.waypoints.size();
            listSize = amountOfWaypoints + amountOfWaypoints - 1;

            if (isModeShowTotalPathData()) listSize++;
        }

        updateListStatus(listSize, h.value() * 0.1954f, h.value() * 0.008f);
    }

    @Override
    protected void renderItem(int index, float iX, float iY, float iW, float iH, boolean isPressed) {
        boolean leftPartPressed = (getTouchPosX() < (w.value() - getSizePerElement())) && isPressed;
        boolean rightPartPressed = isPressed && !leftPartPressed;

        if (isModeShowTotalPathData() && index == getListSize() - 1) {
            Draw.roundedRect(iX + iH, iY, iW - iH * 2, iH, iH * 0.1f, isPressed ? Colors.COLOR_TOTAL_PATH_PRESSED : Colors.COLOR_TOTAL_PATH);
        } else if (hasPlusButton(index)) {
            Draw.roundedRect(iX, iY, iW - iH * 1.05f, iH, iH * 0.1f, leftPartPressed ? Color.GRAY : Color.DARK_GRAY);
            Draw.roundedRect(iX + iW - iH, iY, iH, iH, iH * 0.1f, rightPartPressed ? Colors.COLOR_PLUS_PRESSED : Colors.COLOR_PLUS);
        } else if (hasDeleteButton(index)) {
            Draw.roundedRect(iX, iY, iW - iH * 1.05f, iH, iH * 0.1f, leftPartPressed ? Color.GRAY : Color.DARK_GRAY);
            Draw.roundedRect(iX + iW - iH, iY, iH, iH, iH * 0.1f, rightPartPressed ? Colors.COLOR_MINUS_PRESSED : Colors.COLOR_MINUS);
        } else {
            Draw.roundedRect(iX, iY, iW, iH, iH * 0.1f, isPressed ? Color.GRAY : Color.DARK_GRAY);
        }

        textRenderer.setWidth(iW * 0.75f - iH * 0.5f);

        if (isModeShowTotalPathData() && index == getListSize() - 1) {

            Draw.setTextSize(iH * 0.4f);
            Draw.setTextAlignment(Align.left);
            Draw.font(iX + iH * 1.2f, iY + iH * 0.35f, iW, "Total:");

            Draw.setTextSize(iH * 0.3f);
            Draw.setTextAlignment(Align.center);

            int lastWP = NavigationPath.currentPath.waypoints.size() - 1;

            if (NavigationPath.currentPath.getDistanceFromTo(0, lastWP) == Double.POSITIVE_INFINITY) {
                Draw.font(iX, iY + iH * 0.2f, iW, "No path found. Please check");
                Draw.font(iX, iY + iH * 0.6f, iW, "your navigation settings");
            } else {
                Draw.font(iX, iY + iH * 0.2f, iW, "Time: " + NavigationPath.currentPath.getFormattedTimeFromTo(0, lastWP));
                Draw.font(iX, iY + iH * 0.6f, iW, "Length: " + NavigationPath.currentPath.getFormattedDistanceFromTo(0, lastWP));
            }

        } else if (hasPlusButton(index)) {
            textRenderer.setWidth(iH);
            textRenderer.setHeight(iH * 0.4f);
            textRenderer.setText("|");
            textRenderer.render(iX, iY, iH, iH * 0.9f);

            textRenderer.setWidth(iH);
            textRenderer.setHeight(iH * 0.5f);
            textRenderer.setText("+");
            textRenderer.render(iX + (iW - iH) / 2, iY, iW, iH);

            int wpIndex1 = convertIndexToWaypointIndex(index - 1);
            int wpIndex2 = convertIndexToWaypointIndex(index + 1);

            Draw.setTextSize(iH * 0.3f);
            Draw.setTextAlignment(Align.center);

            if (NavigationPath.currentPath.getDistanceFromTo(wpIndex1, wpIndex2) == Double.POSITIVE_INFINITY) {
                Draw.font(iX, iY + iH * 0.2f, iW, "No path found. Please check");
                Draw.font(iX, iY + iH * 0.6f, iW, "your navigation settings");
            } else {
                Draw.font(iX, iY + iH * 0.2f, iW, "Time: " + NavigationPath.currentPath.getFormattedTimeFromTo(wpIndex1, wpIndex2));
                Draw.font(iX, iY + iH * 0.6f, iW, "Length: " + NavigationPath.currentPath.getFormattedDistanceFromTo(wpIndex1, wpIndex2));
            }


        } else {
            Draw.image(placeIcon, iX, iY, iH, iH, 0.7f);

            textRenderer.setHeight(iH * 0.3f);
            textRenderer.setText(getText(index));
            textRenderer.render(iX, iY, iW, iH);

            if (hasDeleteButton(index)) {
                textRenderer.setWidth(iH);
                textRenderer.setHeight(iH * 0.5f);
                textRenderer.setText("-");
                textRenderer.render(iX + (iW - iH) / 2, iY, iW, iH * 0.9f);
            }
        }
    }

    private String getText(int index) {
        if (isModeChooseStartPosition() || (isModeChooseEndPosition() && index == 1)) {
            return getChooseText(index) + "...";
        } else {
            int waypointIndex = convertIndexToWaypointIndex(index);

            return NavigationPath.currentPath.waypoints.get(waypointIndex).getName();
        }
    }

    private String getChooseText(int index) {
        if (index == 0) {
            return "Choose Start Position";
        } else if ((isModeChooseEndPosition() && index == 1) || index == getListSize() - 1) {
            return "Choose End Position";
        } else {
            return "Choose Waypoint";
        }
    }

    private boolean hasDeleteButton(int index) {
        return index > 1
                && index != getListSize() - (isModeShowTotalPathData() ? 2 : 1)
                && !hasPlusButton(index);
    }

    private boolean hasPlusButton(int index) {
        if (isModeChooseStartPosition() || isModeChooseEndPosition()) return false;
        return index % 2 == 1;
    }

    private int convertIndexToWaypointIndex(int index) {
        if (isModeChooseStartPosition() || isModeChooseEndPosition()) return index;
        return index / 2;
    }

    @Override
    public boolean touchDown(int x, int y, int pointer) {
        NavigationDirectionsOverlay.isPressed = isWithinComponent(x, y);
        return false;
    }

    @Override
    public boolean touchUpPanel(int x, int y, int pointer) {
        if (x != Integer.MAX_VALUE && y != Integer.MAX_VALUE)
            NavigationDirectionsOverlay.isPressed = false;
        return super.touchUpPanel(x, y, pointer);
    }

    @Override
    protected void listItemSelected(int index) {
        boolean rightPartPressed = !(getTouchPosX() < (w.value() - getSizePerElement()));
        NavigationDirectionsOverlay.isPressed = false;

        if (isModeShowTotalPathData() && index == getListSize() - 1) {
            //Show complete navigation path on map
            MapBoundingBox bb = NavigationPath.currentPath.getBoundingBox(0, NavigationPath.currentPath.waypoints.size() - 1);
            Screens.get(MainScreen.class).osmView.zoomMapTo(bb, true);

            setVisible(false);

        } else if (hasPlusButton(index) && !rightPartPressed) {
            //Show navigation path part on map
            int wpIndex1 = convertIndexToWaypointIndex(index - 1);
            int wpIndex2 = convertIndexToWaypointIndex(index + 1);

            MapBoundingBox bb = NavigationPath.currentPath.getBoundingBox(wpIndex1, wpIndex2);
            Screens.get(MainScreen.class).osmView.zoomMapTo(bb, true);

            setVisible(false);

        } else if (hasDeleteButton(index) && rightPartPressed) {
            NavigationPath.currentPath.removeWaypoint(convertIndexToWaypointIndex(index));

        } else {
            //Zoom map to old node
            if (!isModeChooseStartPosition() && !(isModeChooseEndPosition() && index == 1) && (!rightPartPressed || !hasPlusButton(index))) {
                OSMElement currentNode = NavigationPath.currentPath.waypoints.get(convertIndexToWaypointIndex(index)).getNode();
                Screens.get(MainScreen.class).osmView.zoomMapTo(currentNode.lat, currentNode.lon, true);
            }

            Screens.get(ChoosePositionScreen.class).open(getChooseText(index) + ":", () -> {
                positionSelected(Screens.get(ChoosePositionScreen.class).getChosenNode(), index, rightPartPressed);
            });
        }
    }

    private void positionSelected(OSMElement node, int index, boolean rightPartPressed) {

        if (isModeChooseStartPosition()) {
            NavigationPath.currentPath.addWaypoint(0, node);
        } else if (isModeChooseEndPosition() && index == 1) {
            NavigationPath.currentPath.addWaypoint(1, node);
        } else if (hasPlusButton(index) && rightPartPressed) {
            NavigationPath.currentPath.addWaypoint(convertIndexToWaypointIndex(index) + 1, node);
        } else {
            NavigationPath.currentPath.changeWaypoint(convertIndexToWaypointIndex(index), node);
        }
    }
}
