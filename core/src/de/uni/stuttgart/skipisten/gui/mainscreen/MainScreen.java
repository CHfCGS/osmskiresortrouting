package de.uni.stuttgart.skipisten.gui.mainscreen;

import lib.gui.annotations.Visible;
import lib.gui.annotations.ZLayer;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.components.basic.TextBox;
import lib.gui.components.screen.Screen;
import lib.gui.components.screen.Screens;
import de.uni.stuttgart.skipisten.gui.osm.OSMCopyrightOverlay;
import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;

import java.util.List;

import de.uni.stuttgart.skipisten.data.elevation.ElevationTileLoader;
import de.uni.stuttgart.skipisten.data.graph.GraphManager;
import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;
import de.uni.stuttgart.skipisten.data.search.SearchResult;
import de.uni.stuttgart.skipisten.data.search.SkiGraphSearch;

public class MainScreen extends Screen {

    @ZLayer(-1)
    @Width(w = 1)
    @Height(h = 1)
    public final OSMView osmView;

    @ZLayer(1)
    @PosY(h = -0.07)
    @Width(w = 1)
    @Height(h = 1)
    private final OSMCopyrightOverlay osmCopyright = new OSMCopyrightOverlay();

    @ZLayer(1)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.025)
    @Width(w = 0.9)
    @Height(h = 0.05)
    private final TextBox searchBox;

    @ZLayer(1)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.1)
    @Width(w = 0.9)
    @Height(h = 0.8)
    private final SearchResultList searchResultList;

    @Visible(false)
    @ZLayer(2)
    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.595)
    @Width(term = "h * 0.35 + w * 0.3")
    @Height(h = 0.33)
    private final NavigationDirectionsOverlay navigationDirectionsOverlay;

    @PosY(align = Alignment.BOTTOM)
    @Width(w = 1)
    @Height(h = 0.07)
    private final ButtonsPanel buttonsPanel;

    private SkiGraphSearch skiGraphSearch;

    private SearchResult currentSearchResult;

    public MainScreen() {
        osmView = new OSMView();

        ElevationTileLoader.setPanel(osmView.get3DPanel());

        searchBox = new TextBox();
        searchResultList = new SearchResultList(searchBox);

        navigationDirectionsOverlay = new NavigationDirectionsOverlay();

        searchBox.setKeyTypedEvent(() -> {
            List<SearchResult> results = skiGraphSearch.search(searchBox.getInput());
            searchResultList.showSearchResults(results);
            navigationDirectionsOverlay.setVisible(false);
        });

        buttonsPanel = new ButtonsPanel();
    }

    /**
     * Opens the screen and sets the map position
     */
    public void openAndInit() {
        Screens.open(this);

        skiGraphSearch = new SkiGraphSearch(GraphManager.getGraph());

        MapBoundingBox box = GraphManager.getGraph().getBoundingBox();
        osmView.get2DPanel().zoomMapTo(box, true);

        navigationDirectionsOverlay.setVisible(false);
    }

    /**
     * This method is called,
     * when the user selects an element from the search bar
     *
     * @param searchResult the selected element
     */
    public void searchResultSelected(SearchResult searchResult) {
        if (currentSearchResult != null) currentSearchResult.setHighlight(false);
        currentSearchResult = searchResult;
        currentSearchResult.setHighlight(true);
        osmView.zoomMapTo(searchResult.getBoundingBox(), true);
    }

    public void toggleDirectionsOverlay() {
        navigationDirectionsOverlay.setVisible(!navigationDirectionsOverlay.isVisible());
    }

    @Override
    public boolean onBackPressed() {
        NavigationPath.currentPath.reset();
        return true;
    }

    @Override
    protected boolean longPress(int x, int y) {
        return false;
    }
}
