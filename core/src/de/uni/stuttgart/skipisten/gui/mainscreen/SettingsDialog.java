package de.uni.stuttgart.skipisten.gui.mainscreen;

import com.badlogic.gdx.graphics.Color;
import lib.gui.annotations.dialog.DialogRatio;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.UIComponent;
import lib.gui.components.basic.CheckBox;
import lib.gui.components.basic.Label;
import lib.gui.components.basic.TextButton;
import lib.gui.components.dialog.Dialog;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.layout.TableLayout;
import lib.gui.components.screen.Screens;

import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;
import de.uni.stuttgart.skipisten.data.pathfinding.PathfindingSettings;
import de.uni.stuttgart.skipisten.gui.chooselocation.ChooseSkiAreaScreen;
import de.uni.stuttgart.skipisten.gui.draw.Colors;

@DialogRatio(1.25)
@TextValue("Settings")
public class SettingsDialog extends Dialog {

    @PosX(w = 0.04)
    @PosY(w = 0.04)
    @Width(w = 0.92)
    @Height(h = 0.5)
    private final TableLayout checkBoxes;

    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.62)
    @Width(h = 0.5)
    @Height(h = 0.09)
    @TextValue("Reset settings")
    private final TextButton resetButton;

    @PosX(align = Alignment.CENTER)
    @PosY(h = 0.84)
    @Width(h = 0.5)
    @Height(h = 0.09)
    @TextValue("Choose ski area")
    private final TextButton changeAreaButton;

    private static final String[] SLOPE_DIFFICULTY_NAMES = {
            "Novice",
            "Easy",
            "Intermediate",
            "Advanced",
            "Expert",
            "Freeride",
            "Extreme",
    };

    private static final String[] AERIALWAY_TYPES_NAMES = {
            "Cable car",
            "Gondola",
            "Chair lift",
            "Drag lift",
            "Platter lift",
            "Rope tow",
            "Magic carpet",
    };

    public SettingsDialog() {
        UIComponent[] tableComponents = new UIComponent[2 + SLOPE_DIFFICULTY_NAMES.length + AERIALWAY_TYPES_NAMES.length];
        int index = 0;

        // Add Slope Label
        tableComponents[index++] = new Label("Slopes", Color.BLACK);

        // Add Slope Checkboxes
        for (int i = 0; i < SLOPE_DIFFICULTY_NAMES.length; i++) {
            CheckBox checkBox = new CheckBox(SLOPE_DIFFICULTY_NAMES[i], Colors.pisteOutlineColors[i].mul(0.95f, 0.95f, 0.95f, 1f));
            final int finalI = i;
            checkBox.setEvent(() -> PathfindingSettings.setDifficultyAllowed(finalI, checkBox.isChecked()));
            checkBox.setChecked(PathfindingSettings.getDifficultiesAllowed(finalI));

            tableComponents[index++] = checkBox;
        }

        // Add Lift Label
        tableComponents[index++] = new Label("Lifts", Color.BLACK);

        // Add Lift Checkboxes
        for (int i = 0; i < AERIALWAY_TYPES_NAMES.length; i++) {
            CheckBox checkBox = new CheckBox(AERIALWAY_TYPES_NAMES[i]);
            final int finalI = i;
            checkBox.setEvent(() -> PathfindingSettings.setAerialwayTypeAllowed(finalI, checkBox.isChecked()));
            checkBox.setChecked(PathfindingSettings.getAerialwayTypesAllowed(finalI));

            tableComponents[index++] = checkBox;
        }

        checkBoxes = new TableLayout(tableComponents);
        checkBoxes.setMode(8, 2, true);
        checkBoxes.setPadding(0.8f, 0.9f);

        // Reset
        resetButton = new TextButton(TextButton.COLOR_RED);
        resetButton.setEvent(() -> {
            for (int i = 1; i < SLOPE_DIFFICULTY_NAMES.length + 1; i++) {
                PathfindingSettings.setAerialwayTypeAllowed(i, true);
            }
            for (int i = 0; i < AERIALWAY_TYPES_NAMES.length; i++) {
                PathfindingSettings.setDifficultyAllowed(i, true);
            }
            for (int i = 0; i < tableComponents.length; i++) {
                if (i == 0 || i == SLOPE_DIFFICULTY_NAMES.length + 1) continue;
                CheckBox checkBox = (CheckBox) tableComponents[i];
                checkBox.setChecked(true);
            }
        });

        // Change area
        changeAreaButton = new TextButton(TextButton.COLOR_BLUE);
        changeAreaButton.setEvent(() -> {
            Dialogs.close(this);
            if(Screens.get(MainScreen.class).osmView.is3DMode()) {
                Screens.get(MainScreen.class).osmView.toggleMode();
            }
            Screens.open(ChooseSkiAreaScreen.class);
        });
    }

    @Override
    protected void dialogClosed() {
        if (NavigationPath.currentPath != null) {
            NavigationPath.currentPath.settingsChanged();
            NavigationPath.currentPath.calculatePath();
        }
    }
}
