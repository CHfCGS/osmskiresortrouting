package de.uni.stuttgart.skipisten.gui.mainscreen;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import lib.gui.Draw;
import lib.gui.Renderer3D;
import lib.gui.annotations.ZLayer;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.Width;
import lib.gui.components.panel.Panel;

import de.uni.stuttgart.skipisten.gui.osm.OSMPanel;
import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;
import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;
import de.uni.stuttgart.skipisten.gui.osm3d.OSMPanel3D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.uni.stuttgart.skipisten.Main;
import de.uni.stuttgart.skipisten.data.graph.GraphManager;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;
import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;

public class OSMView extends Panel {

    public static class Mode {
        public static final int MODE_2D = 0;
        public static final int MODE_3D = 1;
    }

    private static int currentMode = Mode.MODE_2D;

    @ZLayer(-1)
    @Width(w = 1)
    @Height(h = 1)
    private final static OSMPanel osmPanel = new OSMPanel();

    @ZLayer(-1)
    @Width(w = 1)
    @Height(h = 1)
    private final static OSMPanel3D osmPanel3d = new OSMPanel3D();
    private Color highlightColor = new Color(Color.WHITE);

    static {
        osmPanel3d.setVisible(false);
    }

    /**
     * Toggles between 2D and 3D Map Mode
     */
    public void toggleMode() {
        if (currentMode == Mode.MODE_2D) {
            currentMode = Mode.MODE_3D;
            osmPanel.setVisible(false);
            osmPanel3d.setActive(true);
            osmPanel3d.setCameraPositionFromMapPosition(osmPanel.getMapPosition());
        } else {
            currentMode = Mode.MODE_2D;
            osmPanel.setVisible(true);
            osmPanel3d.setActive(false);
            osmPanel3d.setMapPositionToWorldPosition(osmPanel.getMapPosition());
        }
    }

    public boolean is3DMode() {
        return currentMode == Mode.MODE_3D;
    }

    public boolean is2DMode() {
        return currentMode == Mode.MODE_2D;
    }

    public OSMPanel get2DPanel() {
        return osmPanel;
    }

    public OSMPanel3D get3DPanel() {
        return osmPanel3d;
    }

    public void zoomMapTo(MapBoundingBox boundingBox, boolean animate) {
        if(boundingBox == null) return;

        if(is2DMode()) {
            osmPanel.zoomMapTo(boundingBox, animate);
        } else {
            double targetMapX = (boundingBox.minX + boundingBox.maxX) / 2f;
            double targetMapY = (boundingBox.minY + boundingBox.maxY) / 2f;
            double targetMapW = boundingBox.maxX - boundingBox.minX;
            double targetMapH = boundingBox.maxY - boundingBox.minY;

            float panelRatio = h.value() / w.value();
            double targetRatio = targetMapH / targetMapW;

            boolean useTargetW = panelRatio > targetRatio;

            osmPanel3d.zoomMapToXY(osmPanel, targetMapX, targetMapY, useTargetW ? targetMapW : targetMapH / panelRatio);
        }
    }

    public void zoomMapTo(double latitude, double longitude, boolean animate) {
        zoomMapTo(latitude, longitude, 0.0001, animate);
    }

    public void zoomMapTo(double latitude, double longitude, double mapW, boolean animate) {

        if(is2DMode()) {
            osmPanel.zoomMapTo(latitude, longitude, mapW, animate);
        } else {
            osmPanel3d.zoomMapTo(osmPanel, latitude, longitude, mapW);
        }
    }

    /**
     * Performs an iterative raycast from the given screen position.
     * @param screenPosition Screen position of the point to be found (in GL View space, i.e. -1 to 1 in all directions)
     * @return World position at given screen position (approximate), or null if raycast failed
     */
    private Vector3 rayCast(Vector3 screenPosition) {
        Camera cam = Renderer3D.getCamera();
        Vector3 look = screenPosition.cpy(); //look direction in screen space
        look.z = 1f;
        look.mul(cam.invProjectionView).nor(); //transform look direction into world space
        Vector3 current = cam.position.cpy(); //current intersection approximation: begins at camera
        Vector3 step = look.cpy(); //iteration step along ray: initialized to look direction
        float terrainZ = osmPanel3d.getZCoordinate(current.x, current.y); //elevation directly below/above current approximation
        float diff = current.z - terrainZ; //vertical height of current approximation above terrain
        float absDiff = Math.abs(diff);
        Vector3 vertical = new Vector3(0, 0, -1);
        float maxStep = (float)(1d / Math.acos(look.dot(vertical))); //maximum step distance multiplier: inverse of the angle between look direction and vertical
        int counter = 0;
        while (counter++ < 1000 && absDiff > .1f) { //iterate while approximation not good enough
            step.set(look).scl(diff / -look.z).clamp(0, maxStep * absDiff); //calculate step to intersection of look direction with terrainZ plane, clamp to maximum step distance
            current.add(step); //add step to current approximation
            terrainZ = osmPanel3d.getZCoordinate(current.x, current.y); //update terrainZ and differences
            diff = current.z - terrainZ;
            absDiff = Math.abs(diff);
        }
        if (counter >= 1000) return null; //if counter reached maximum: no suitable approximation found
        return current;
    }

    public MapPosition getMapPositionAt(float screenX, float screenY) {
        if (is2DMode()) return osmPanel.getMapPosition(screenX, screenY);
        else {
            float screenCoordX = 2 * screenX / osmPanel3d.w.value() - 1;
            float screenCoordY = -2 * screenY / osmPanel3d.h.value() + 1;
            Vector3 look = new Vector3(screenCoordX, screenCoordY, 1);
            Vector3 rayHit = rayCast(look);
            if (rayHit == null) return null;
            return osmPanel3d.getMapPosition(osmPanel, rayHit.x, rayHit.y, rayHit.z);
        }
    }

    int x = 0;

    @Override
    protected void render() {
        if (!GraphManager.isGraphLoaded() || is3DMode()) return;

        //Draw ski graph
        List<OSMElement> ways = new ArrayList<>(GraphManager.getGraph().getWays());
        Collections.sort(ways, (e1, e2) -> Integer.compare(e2.difficulty, e1.difficulty));

        double[] tCoeff = osmPanel.getMapToPixelTransformCoefficients();

        highlightColor.set(1f, 1f, 1f, ((float) Math.cos(System.currentTimeMillis() / 300.0) + 1) * 0.25f + 0.5f);

        for (OSMElement way : ways) {
            if (way.outerLine != null) {
                way.outerLine.draw(tCoeff);
            }
        }

        for (OSMElement way : ways) {
            if (way.innerLine != null) {
                way.innerLine.draw(tCoeff);
            }
        }

        for (OSMElement way : ways) {
            if (way.highlightLine != null && way.renderHighlight) {
                way.highlightLine.draw(tCoeff, highlightColor);
            }
        }

        if (NavigationPath.currentPath != null) {
            if (NavigationPath.currentPath.pathExists() && NavigationPath.currentPath.line != null) {
                NavigationPath.currentPath.line.draw(tCoeff);
            }

            //Highlight waypoints
            for(NavigationPath.Waypoint waypoint : NavigationPath.currentPath.waypoints) {
                Vector2 point = get2DPanel().getPixelPosition(waypoint.getNode().lat, waypoint.getNode().lon);

                float s = (float) (w.value() * 1e-6 / get2DPanel().getMapPosition().getVisibleMapW()) * 0.9f;

                Draw.setColor(0.8f, 0.8f, 0.8f, 1f);
                Draw.roundedRect(point.x - s / 2f, point.y - s / 2f, s, s, s * 0.5f, 1f);
                s *= 0.8f;
                Draw.setColor(Color.WHITE);
                Draw.roundedRect(point.x - s / 2f, point.y - s / 2f, s, s, s * 0.5f, 1f);
            }


            //Show gps position
            if(Main.gps.hasGPSLocation(300)) {
                Vector2 point = get2DPanel().getPixelPosition(Main.gps.getLatitude(), Main.gps.getLongitude());

                float s = (float) (w.value() * 1e-6 / get2DPanel().getMapPosition().getVisibleMapW()) * 0.9f;

                Draw.setColor(Color.BLACK);
                Draw.roundedRect(point.x - s / 2f, point.y - s / 2f, s, s, s * 0.5f, 1f);
                s *= 0.8f;
                Draw.setColor(0.3f, 0.3f, 0.3f, 1f);
                Draw.roundedRect(point.x - s / 2f, point.y - s / 2f, s, s, s * 0.5f, 1f);
            }
        }
    }
}
