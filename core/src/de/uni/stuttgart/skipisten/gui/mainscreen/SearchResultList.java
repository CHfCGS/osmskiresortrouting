package de.uni.stuttgart.skipisten.gui.mainscreen;

import com.badlogic.gdx.graphics.Color;
import lib.gui.Draw;
import lib.gui.components.basic.TextBox;
import lib.gui.components.panel.ListPanel;
import lib.gui.components.screen.Screens;
import lib.gui.util.TextRenderer;

import java.util.List;

import de.uni.stuttgart.skipisten.data.search.SearchResult;
import de.uni.stuttgart.skipisten.gui.draw.Colors;

public class SearchResultList extends ListPanel {

    private List<SearchResult> searchResults;
    private TextRenderer textRenderer;
    private TextBox textBox;

    public SearchResultList(TextBox textBox) {
        super(true);
        setVisible(false);
        textRenderer = new TextRenderer();
        this.textBox = textBox;
    }

    public void showSearchResults(List<SearchResult> searchResults) {
        this.searchResults = searchResults;
        updateListStatus(searchResults.size(), h.value() * 0.08f, h.value() * 0.008f);
        setVisible(searchResults.size() > 0);
    }

    @Override
    protected void renderItem(int index, float iX, float iY, float iW, float iH, boolean isPressed) {
        SearchResult result = searchResults.get(index);

        Draw.roundedRect(iX, iY, iW, iH, iH * 0.15f,  isPressed ? Color.GRAY : Color.DARK_GRAY);
        textRenderer.setWidth(iW * 0.8f - iH * 0.5f);
        textRenderer.setHeight(iH * 0.3f);
        textRenderer.setText(result.getDisplayedName());
        textRenderer.render(iX, iY, iW, iH);

        if(result.isPiste()) {
            Draw.setColor(Colors.pisteOutlineColors[result.getWays().get(0).difficulty]);
            Draw.roundedRect(iX - iH * 0.1f, iY, iH, iH, iH * 0.5f, 0.4f);
            Draw.setColor(Colors.pisteInlineColors[result.getWays().get(0).difficulty]);
            Draw.roundedRect(iX - iH * 0.1f, iY, iH, iH, iH * 0.5f, 0.3f);
        }
    }

    @Override
    protected void listItemSelected(int index) {
        Screens.get(MainScreen.class).searchResultSelected(searchResults.get(index));
        textBox.setInput("");
        setVisible(false);
    }
}
