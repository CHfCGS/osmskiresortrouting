package de.uni.stuttgart.skipisten.gui.draw;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g2d.PolygonBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ShortArray;
import lib.gui.GUIConfiguration;
import lib.gui.Renderer2D;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;

import java.util.HashMap;
import java.util.Map;

import de.uni.stuttgart.skipisten.data.elevation.ElevationProvider;

public class Polyline {

    private static final double HALF_PI = Math.PI / 2d;
    private static final double PI2 = Math.PI * 2d;

    private static PolygonBatch batch;
    private static TextureRegion white_pixels;
    private static Material whiteMat;

    private DoubleArray vertices;
    private ShortArray triangles;

    private float[] floatVertices;

    private MapBoundingBox boundingBox;

    private float modelZ;
    private Map<TilePosition, ModelInstance> models = new HashMap<>();

    static {
        batch = Renderer2D.getBatch();
        white_pixels = Renderer2D.getTextureManager().getTextureRegion(GUIConfiguration.whitePixelsTextureRegionName);
        whiteMat = new Material(ColorAttribute.createDiffuse(Color.WHITE));
    }

    /**
     * Create polyline along the given points.
     * @param points Line points, at least 2 required
     * @param thickness Thickness of the line
     * @param capSegments Number of segments of the line end caps. 0 = flat end, higher numbers approach circular end
     */
    public Polyline(Vector2[] points, double thickness, int capSegments, Color color, float modelZ) {
        this(points, thickness, capSegments, thickness, color, modelZ);
    }

    /**
     * Create polyline along the given points.
     * @param x X coordinates of line points, at least 2 required
     * @param y Y coordinates of line points, length must match x
     * @param thickness Thickness of the line
     * @param capSegments Number of segments of the line end caps. 0 = flat end, higher numbers approach circular end
     */
    public Polyline(double[] x, double[] y, double thickness, int capSegments, Color color, float modelZ) {
        this(x, y, thickness, capSegments, thickness, color, modelZ);
    }

    /**
     * Create polyline along the given points.
     * @param points Line points, at least 2 required
     * @param thickness Thickness of the line
     * @param capSegments Number of segments of the line end caps. 0 = flat end, higher numbers approach circular end
     * @param cornerThickness Thickness to use for corner radius considerations. Useful when overlaying polylines to create an outline. For that purpose, always set this to the smaller line thickness.
     */
    public Polyline(Vector2[] points, double thickness, int capSegments, double cornerThickness, Color color, float modelZ) {
        double[] x = new double[points.length];
        double[] y = new double[points.length];
        for (int i = 0; i < points.length; i++) {
            x[i] = points[i].x;
            y[i] = points[i].y;
        }
        this.modelZ = modelZ;
        boundingBox = new MapBoundingBox();
        create(x, y, thickness, capSegments, cornerThickness, color);
    }

    /**
     * Create polyline along the given points.
     * @param x X coordinates of line points, at least 2 required
     * @param y Y coordinates of line points, length must match x
     * @param thickness Thickness of the line
     * @param capSegments Number of segments of the line end caps. 0 = flat end, higher numbers approach circular end
     * @param cornerThickness Thickness to use for corner radius considerations. Useful when overlaying polylines to create an outline. For that purpose, always set this to the smaller line thickness.
     */
    public Polyline(double[] x, double[] y, double thickness, int capSegments, double cornerThickness, Color color, float modelZ) {
        this.modelZ = modelZ;
        boundingBox = new MapBoundingBox();
        create(x, y, thickness, capSegments, cornerThickness, color);
    }

    private void create(double[] x, double[] y, double thickness, int capSegments, double cornerThickness, Color color) {
        if (x == null || y == null || x.length != y.length || x.length < 2)
            throw new IllegalArgumentException("Polyline: At least two x and y values must be given and there must be equally many x and y values");
        if (thickness <= 0f) return; //only positive width lines please
        if (capSegments < 0) capSegments = 0;

        double packedColor = color.toFloatBits();
        double u = (white_pixels.getRegionX() + 2) / (double)white_pixels.getTexture().getWidth();
        double v = (white_pixels.getRegionY() + 2) / (double)white_pixels.getTexture().getHeight();

        double currentX = x[0];
        double currentY = y[0];
        double nextX = x[1];
        double nextY = y[1];
        double deltaX = nextX - currentX;
        double deltaY = nextY - currentY;
        double angle = -Math.atan2(deltaY, deltaX);
        double radius = thickness / 2f;
        double cornerRadius = cornerThickness / 2f;
        int nextIndex = 2;
        int vertexCount = 0;

        vertices = new DoubleArray();
        triangles = new ShortArray();

        // --- create start cap ---
        if (capSegments > 0) {
            addVertex(vertices, currentX, currentY, packedColor, u, v); //central vertex, index 0
            vertexCount++;
            double sections = capSegments + 1;
            for (int i = 1; i <= capSegments; i++) { //intermediate vertices, indices 1...capSegments
                addOffsetVertex(vertices, currentX, currentY, packedColor, u, v, angle + HALF_PI + Math.PI * i / sections, radius);
                vertexCount++;
            }
            for (int i = 1; i < capSegments; i++) { //inner triangles (only using intermediate vertices)
                addTriangle(triangles, 0, i, i + 1);
            }
        }
        addOffsetVertex(vertices, currentX, currentY, packedColor, u, v, angle + HALF_PI, radius); //left vertex of cap
        addOffsetVertex(vertices, currentX, currentY, packedColor, u, v, angle - HALF_PI, radius); //right vertex of cap
        vertexCount += 2;
        if (capSegments > 0) {
            addTriangle(triangles, 0, capSegments + 1, 1); //leftmost cap triangle
            addTriangle(triangles, 0, capSegments, capSegments + 2); //rightmost cap triangle
        }

        // --- create intermediate line segments ---
        while (nextIndex < x.length) {
            double lastAngle = angle;
            currentX = nextX;
            currentY = nextY;
            nextX = x[nextIndex];
            nextY = y[nextIndex++];
            deltaX = nextX - currentX;
            deltaY = nextY - currentY;
            angle = -Math.atan2(deltaY, deltaX);

            double splitAngle = (lastAngle + angle) / 2f;
            double diffAngle = angle - lastAngle;
            double cornerDistance = radius / Math.cos(diffAngle / 2f);
            double adjCornerDistance = cornerRadius / Math.cos(diffAngle / 2f);
            if (diffAngle < -Math.PI) diffAngle += PI2;
            else if (diffAngle > Math.PI) diffAngle -= PI2;
            double absDiff = Math.abs(diffAngle);

            //check for [nearly] 180 degree angles
            if (Math.abs(absDiff - Math.PI) < 0.005) {
                //no cap here
            } else if (absDiff < .1f) { //tiny direction change: draw corner
                addOffsetVertex(vertices, currentX, currentY, packedColor, u, v, splitAngle + HALF_PI, cornerDistance); //left corner vertex
                addOffsetVertex(vertices, currentX, currentY, packedColor, u, v, splitAngle - HALF_PI, cornerDistance); //right corner vertex
                vertexCount += 2;

                addTriangle(triangles, vertexCount - 4, vertexCount - 3, vertexCount - 2);
                addTriangle(triangles, vertexCount - 1, vertexCount - 2, vertexCount - 3);
            } else if (diffAngle > 0f) { //left turn: point on left, curve on right
                double cornerX = currentX + cornerDistance * Math.cos(splitAngle + HALF_PI);
                double cornerY = currentY + cornerDistance * Math.sin(-splitAngle - HALF_PI);
                double centerX = currentX + adjCornerDistance * Math.cos(splitAngle + HALF_PI);
                double centerY = currentY + adjCornerDistance * Math.sin(-splitAngle - HALF_PI);

                addOffsetVertex(vertices, centerX, centerY, packedColor, u, v, lastAngle - HALF_PI, radius + cornerRadius); //first right vertex
                int curveStartIndex = vertexCount++;

                int numSegments = (int)Math.round(capSegments * absDiff / Math.PI);
                int sections = numSegments + 1;
                for (int i = 1; i <= numSegments; i++) { //intermediate vertices
                    addOffsetVertex(vertices, centerX, centerY, packedColor, u, v, lastAngle - HALF_PI + i * absDiff / sections, radius + cornerRadius);
                    vertexCount++;
                }

                addVertex(vertices, cornerX, cornerY, packedColor, u, v); //corner vertex
                addOffsetVertex(vertices, centerX, centerY, packedColor, u, v, angle - HALF_PI, radius + cornerRadius); //last right vertex
                vertexCount += 2;

                addTriangle(triangles, curveStartIndex - 2, curveStartIndex - 1, vertexCount - 2); //line segment triangle 1
                addTriangle(triangles, curveStartIndex, vertexCount - 2, curveStartIndex - 1); //line segment triangle 2
                for (int i = 0; i < numSegments; i++) { //intermediate curve triangles
                    addTriangle(triangles, vertexCount - 2, curveStartIndex + i, curveStartIndex + i + 1);
                }
                addTriangle(triangles,vertexCount - 2, vertexCount - 3, vertexCount - 1); //last curve triangle
            } else { //right turn: point on right, curve on left
                double cornerX = currentX + cornerDistance * Math.cos(splitAngle - HALF_PI);
                double cornerY = currentY + cornerDistance * Math.sin(-splitAngle + HALF_PI);
                double centerX = currentX + adjCornerDistance * Math.cos(splitAngle - HALF_PI);
                double centerY = currentY + adjCornerDistance * Math.sin(-splitAngle + HALF_PI);

                addOffsetVertex(vertices, centerX, centerY, packedColor, u, v, lastAngle + HALF_PI, radius + cornerRadius); //first left vertex
                int curveStartIndex = vertexCount++;

                int numSegments = (int)Math.round(capSegments * absDiff / Math.PI);
                int sections = numSegments + 1;
                for (int i = 1; i <= numSegments; i++) { //intermediate vertices
                    addOffsetVertex(vertices, centerX, centerY, packedColor, u, v, lastAngle + HALF_PI - i * absDiff / sections, radius + cornerRadius);
                    vertexCount++;
                }

                addOffsetVertex(vertices, centerX, centerY, packedColor, u, v, angle + HALF_PI, radius + cornerRadius); //last left vertex
                addVertex(vertices, cornerX, cornerY, packedColor, u, v); //corner vertex
                vertexCount += 2;

                addTriangle(triangles, curveStartIndex - 2, curveStartIndex - 1, curveStartIndex); //line segment triangle 1
                addTriangle(triangles, vertexCount - 1, curveStartIndex, curveStartIndex - 1); //line segment triangle 2
                for (int i = 0; i < numSegments; i++) { //intermediate curve triangles
                    addTriangle(triangles, vertexCount - 1, curveStartIndex + i + 1, curveStartIndex + i);
                }
                addTriangle(triangles,vertexCount - 1, vertexCount - 2, vertexCount - 3); //last curve triangle
            }
        }

        // --- create end cap ---
        int centerVertexIndex = vertexCount;
        if (capSegments > 0) {
            addVertex(vertices, nextX, nextY, packedColor, u, v); //central vertex
            vertexCount++;
            double sections = capSegments + 1;
            for (int i = 1; i <= capSegments; i++) { //intermediate vertices, indices 1...capSegments
                addOffsetVertex(vertices, nextX, nextY, packedColor, u, v, angle - HALF_PI + Math.PI * i / sections, radius);
                vertexCount++;
            }
            for (int i = 1; i < capSegments; i++) { //inner triangles (only using intermediate vertices)
                addTriangle(triangles, centerVertexIndex, centerVertexIndex + i, centerVertexIndex + i + 1);
            }
        }
        addOffsetVertex(vertices, nextX, nextY, packedColor, u, v, angle + HALF_PI, radius); //left vertex of cap
        addOffsetVertex(vertices, nextX, nextY, packedColor, u, v, angle - HALF_PI, radius); //right vertex of cap
        vertexCount += 2;
        if (capSegments > 0) {
            addTriangle(triangles, centerVertexIndex, vertexCount - 1, centerVertexIndex + 1); //rightmost cap triangle
            addTriangle(triangles, centerVertexIndex, centerVertexIndex + capSegments, vertexCount - 2); //leftmost cap triangle
        }
        addTriangle(triangles, centerVertexIndex - 2, centerVertexIndex - 1, vertexCount - 2);
        addTriangle(triangles, vertexCount - 1, vertexCount - 2, centerVertexIndex - 1);

        floatVertices = new float[vertices.size];
        for (int i = 0; i <= vertices.size - 5; i += 5) {
            floatVertices[i + 2] = (float)vertices.items[i + 2];
            floatVertices[i + 3] = (float)vertices.items[i + 3];
            floatVertices[i + 4] = (float)vertices.items[i + 4];
        }
    }

    private void addVertex(DoubleArray vertexList, double x, double y, double color, double u, double v) {
        boundingBox.addPoint(x, y);
        vertexList.add(x);
        vertexList.add(y);
        vertexList.add(color);
        vertexList.add(u);
        vertexList.add(v);
    }

    private void addOffsetVertex(DoubleArray vertexList, double x, double y, double color, double u, double v, double angle, double distance) {
        addVertex(vertexList, x + distance * Math.cos(angle), y + distance * Math.sin(-angle), color, u, v);
    }

    private void addTriangle(ShortArray triangleList, int v1, int v2, int v3) {
        triangleList.add((short)v1);
        triangleList.add((short)v2);
        triangleList.add((short)v3);
    }

    public MapBoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void draw() {
        draw(new double[] {1, 0, 1, 0});
    }

    public void draw(double[] transformCoefficients) {
        double[] va = vertices.items;
        for (int i = 0; i <= vertices.size - 5; i += 5) {
            floatVertices[i] = (float)(va[i] * transformCoefficients[0] + transformCoefficients[1]);
            floatVertices[i + 1] = (float)(va[i + 1] * transformCoefficients[2] + transformCoefficients[3]);
        }
        batch.draw(white_pixels.getTexture(), floatVertices, 0, vertices.size, triangles.toArray(), 0, triangles.size);
    }

    public void draw(double[] transformCoefficients, Color color) {
        float colorPacked = color.toFloatBits();
        float oldColorPacked = floatVertices[2];

        double[] va = vertices.items;
        for (int i = 0; i <= vertices.size - 5; i += 5) {
            floatVertices[i] = (float)(va[i] * transformCoefficients[0] + transformCoefficients[1]);
            floatVertices[i + 1] = (float)(va[i + 1] * transformCoefficients[2] + transformCoefficients[3]);
            floatVertices[i + 2] = colorPacked;
        }
        batch.draw(white_pixels.getTexture(), floatVertices, 0, vertices.size, triangles.toArray(), 0, triangles.size);

        //Reset color values
        for (int i = 0; i <= vertices.size - 5; i += 5) {
            floatVertices[i + 2] = oldColorPacked;
        }
    }

    public ModelInstance getModel(TilePosition pos) {
        if (pos.getZoom() != ElevationProvider.MIN_ZOOM || !boundingBox.crossesTile(pos)) return null;
        double modelZoomFactor = 1 << ElevationProvider.MIN_ZOOM;
        double modelXOffset = -pos.getMapX() * modelZoomFactor;
        double modelYOffset = -pos.getMapY() * modelZoomFactor;
        ModelInstance modelInstance;
        if (models.containsKey(pos)) modelInstance = models.get(pos);
        else {
            ModelBuilder b = new ModelBuilder();
            b.begin();
            VertexAttributes attributes = new VertexAttributes(new VertexAttribute(VertexAttributes.Usage.Position, 2, ShaderProgram.POSITION_ATTRIBUTE),
                                                               VertexAttribute.ColorPacked(), VertexAttribute.TexCoords(0));
            MeshPartBuilder mb = b.part("polyline", GL20.GL_TRIANGLES, attributes, new Material(whiteMat));
            mb.ensureVertices(vertices.size / 5);
            mb.ensureIndices(triangles.size);

            double[] va = vertices.items;
            for (int i = 0; i <= vertices.size - 5; i += 5) {
                floatVertices[i] = (float)(va[i] * modelZoomFactor + modelXOffset);
                floatVertices[i + 1] = (float)(va[i + 1] * modelZoomFactor + modelYOffset);
            }

            mb.vertex(floatVertices);
            for (int i = 0; i < triangles.size / 3; i++) {
                int iI = 3 * i;
                mb.index(triangles.get(iI));
                mb.index(triangles.get(iI + 2));
                mb.index(triangles.get(iI + 1));
            }

            Model model = b.end();
            modelInstance = new ModelInstance(model);
            modelInstance.transform.setToTranslation(0f, 0f, modelZ);
            models.put(pos, modelInstance);
        }
        return modelInstance;
    }

}
