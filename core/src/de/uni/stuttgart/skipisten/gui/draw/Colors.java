package de.uni.stuttgart.skipisten.gui.draw;

import com.badlogic.gdx.graphics.Color;
import lib.gui.components.basic.TextButton;

public class Colors {
    public static final Color LIGHT_BLUE = new Color(0xA0A0FFFF);
    public static final Color LIGHT_GREEN = new Color(0xA0FFA0FF);
    public static final Color LIGHT_RED = new Color(0xFFA0A0FF);
    public static final Color LIGHT_GREY = new Color(0xA0A0A0FF);
    public static final Color LIGHT_ORANGE = new Color(0xFFA749FF);
    public static final Color DARK_ORANGE = new Color(0xC16400FF);

    public static final Color[] pisteOutlineColors =
            { Color.GREEN, Color.BLUE, Color.RED, Color.BLACK, DARK_ORANGE, DARK_ORANGE, DARK_ORANGE };

    public static final Color[] pisteInlineColors =
            { LIGHT_GREEN, LIGHT_BLUE, LIGHT_RED, LIGHT_GREY, LIGHT_ORANGE, LIGHT_ORANGE, LIGHT_ORANGE };

    public static final Color LIGHT_AERIALWAY = new Color(0xFFE500FF);
    public static final Color AERIALWAY = Color.BLACK;//new Color(0xCCCC00FF);


    public static final Color COLOR_PLUS = new Color(0f, 0.62f, 0.173f, 1f);
    public static final Color COLOR_MINUS = new Color(0.749f, 0f, 0f, 1f);
    public static final Color COLOR_PLUS_PRESSED = new Color(0f, 0.62f, 0.173f, 1f).mul(1.1f);
    public static final Color COLOR_MINUS_PRESSED = new Color(0.749f, 0f, 0f, 1f).mul(1.1f);

    public static final Color COLOR_TOTAL_PATH = new Color(TextButton.COLOR_BLUE);
    public static final Color COLOR_TOTAL_PATH_PRESSED = new Color(COLOR_TOTAL_PATH).mul(1.1f);
}
