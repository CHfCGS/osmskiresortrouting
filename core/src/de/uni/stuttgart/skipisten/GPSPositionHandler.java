package de.uni.stuttgart.skipisten;

public interface GPSPositionHandler {

    /**
     * @return if the device supports gps
     */
    default boolean isGPSSupported() {
        return false;
    }

    /**
     * @return if the app has a permission to access the gps location
     */
    default boolean hasGPSPermission() {
        return false;
    }

    /**
     * If the app already has a permission for the gps location,
     * the callback will be called imideately.
     * <p>
     * If isGPSSupported() returns false, nothing happens
     * and the callback will not be called
     * <p>
     * Otherwise it will ask the user for permission to use gps.
     *
     * @param callback will be called after the user has
     *                 accepted or declined the permission.
     */
    default void askForGPSPermission(Runnable callback) {
    }

    /**
     * @return if gps is currently enabled on the users device
     */
    default boolean isGPSEnabled() {
        return false;
    }

    /**
     * Must be called in order to get location updates
     */
    default void start() {
    }

    /**
     * Should be called when location updates are no longer needed
     */
    default void stop() {
    }

    /**
     * @return if there is a known gps location
     * Use getAgeOfLocationInSeconds()
     * to know how old the location is
     */
    default boolean hasGPSLocation() {
        return false;
    }

    /**
     * @return if there is a known gps location
     *
     * If the known gps location is older than
     * @param maxAgeInSeconds
     * the method will return false
     */
    default boolean hasGPSLocation(long maxAgeInSeconds) {
        return false;
    }

    /**
     * @return the age of the location data in seconds
     */
    default long getAgeOfLocationInSeconds() {
        return Integer.MAX_VALUE;
    }

    default double getLatitude() {
        return 0;
    }

    default double getLongitude() {
        return 0;
    }

    default double getAltitude() {
        return 0;
    }
}
