package de.uni.stuttgart.skipisten.data.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class SkiGraphSearch {

    private OSMGraph graph;
    private List<SearchResult> allResults;

    public SkiGraphSearch(OSMGraph graph) {
        this.graph = graph;
        generatePossibleSearchResults();
    }

    private void generatePossibleSearchResults() {
        allResults = new ArrayList<>();

        outerLoop:
        for (long wayID : graph.getWayIDs()) {
            OSMElement way = graph.getWay(wayID);

            //Search results are either pistes or aerialways
            if (!way.isPiste && !way.isAerialway()) continue;

            //Only add ways which have searchable information
            if (!way.tags.containsKey("name") && !way.tags.containsKey("ref") && !way.tags.containsKey("piste:ref") && !way.tags.containsKey("piste:name"))
                continue;

            //Check if there is already a adjacent way with the same name / description
            for (SearchResult result : allResults) {
                if (result.tryCombine(graph, way)) continue outerLoop;
            }

            allResults.add(new SearchResult(graph, way));
        }

        Collections.sort(allResults, (a, b) -> a.getDisplayedName().compareTo(b.getDisplayedName()));
    }

    public List<SearchResult> search(String searchInput) {
        List<SearchResult> searchResults = new ArrayList<>();

        if (searchInput.length() == 0) return searchResults;

        searchInput = searchInput.trim().toLowerCase();

        for (SearchResult result : allResults) {
            if (result.matches(searchInput)) searchResults.add(result);
        }

        for (SearchResult result : allResults) {
            if (result.matchesWords(searchInput)) searchResults.add(result);
        }

        if (searchResults.size() == 0) {
            for (SearchResult result : allResults) {
                if (result.matchesPartly(searchInput)) searchResults.add(result);
            }
        }

        return searchResults;
    }
}
