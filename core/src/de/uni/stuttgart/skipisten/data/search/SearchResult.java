package de.uni.stuttgart.skipisten.data.search;

import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;

import java.util.ArrayList;
import java.util.List;

import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class SearchResult {

    private String displayedName;

    private List<OSMElement> ways;
    private MapBoundingBox boundingBox;

    /**
     * Search results are either pistes or aerialways
     */
    private boolean isPiste;

    public SearchResult(OSMGraph graph, OSMElement way) {
        ways = new ArrayList<>();
        ways.add(way);
        isPiste = way.isPiste;
        boundingBox = new MapBoundingBox();
        boundingBox.addWay(graph, way);
        displayedName = generatedDisplayedName(way);
    }

    public static String generatedDisplayedName(OSMElement way) {
        String name = "";
        String ref = "";
        String prefix = way.isPiste ? "Piste" : "Aerialway";

        if (way.tags.containsKey("name"))  name = way.getTag("name");
        else if (way.tags.containsKey("piste:name")) name = way.getTag("piste:name");

        if (way.tags.containsKey("ref")) ref = way.getTag("ref");
        else if (way.tags.containsKey("piste:ref")) ref = way.getTag("piste:ref");

        if (ref.length() > 0) {
            if (name.equals(ref) || name.length() == 0) {
                return prefix + " " + ref;
            } else {
                return name + " " + ref;
            }
        } else {
            //Check if name is a ref
            if(name.length() < 4) {
                try {
                    int refInteger = Integer.parseInt(name);
                    return prefix + " " + refInteger;
                } catch (NumberFormatException ignored) {
                }
            }

            return name;
        }
    }

    /**
     * @return true if the way was combined with the existing search result
     */
    public boolean tryCombine(OSMGraph graph, OSMElement way) {
        //Cancel if names are not equal
        if(!displayedName.equals(generatedDisplayedName(way))) return false;

        //Only checking for names is currently the better solution

        /*//Check if way is adjacent to existing ways
        for(OverpassResult.Element existingWay : ways) {
            if(existingWay.isAdjacentTo(way)) {
                ways.add(way);
                boundingBox.addWay(graph, way);

                return true;
            }
        }
        return false;*/

        ways.add(way);
        boundingBox.addWay(graph, way);
        return true;
    }

    public boolean matches(String searchInput) {
        //Name contains search input
        return displayedName.toLowerCase().contains(searchInput);
    }

    /**
     * @return false, if SearchResult#matches returns true
     */
    public boolean matchesWords(String searchInput) {
        if(matches(searchInput)) return false;

        //Name must contains every word from search input
        String[] words = searchInput.split(" \\s*");
        for(String word : words) {
            if(!displayedName.toLowerCase().contains(word)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return false, if SearchResult#matchesWords returns true
     */
    public boolean matchesPartly(String searchInput) {
        if(matchesWords(searchInput)) return false;

        //Name must contains at least one word from search input
        String[] words = searchInput.split(" \\s*");
        for(String word : words) {
            if(displayedName.toLowerCase().contains(word)) {
                return true;
            }
        }

        return false;
    }

    public void setHighlight(boolean highlight) {
        for (OSMElement way : ways) way.renderHighlight = highlight;
    }

    /**
     * Search results are either pistes or aerialways
     */
    public boolean isPiste() {
        return isPiste;
    }

    public String getDisplayedName() {
        return displayedName;
    }

    public List<OSMElement> getWays() {
        return ways;
    }

    public MapBoundingBox getBoundingBox() {
        return boundingBox;
    }
}
