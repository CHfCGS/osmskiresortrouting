package de.uni.stuttgart.skipisten.data.overpass;

import de.uni.stuttgart.skipisten.gui.osm.util.MapPosition;

import de.uni.stuttgart.skipisten.data.graph.OSMGraph;

/**
 * Tries to load a ski osm graph for a specific location.
 * <p>
 * To load a ski region call load(MapPosition position) or load(double lat, double lon).
 * <p>
 * The GraphLoader tries to find the suited ski region for that location.
 * <p>
 * If an error occurs (e.g no internet connection) callbackError is called.
 * If no ski region for the specified coordinate is found callbackNoSkiRegionFound is called.
 * If a ski region is loaded successfully callbackSuccess is called.
 */
public class SkiGraphLoader {

    /**
     * Is called when a ski region for the specified coordinate is found.
     * After that the ski region will be loaded.
     */
    private Runnable callbackSkiRegionFound;

    /**
     * Is called when no ski region for the specified coordinate is found.
     */
    private Runnable callbackNoSkiRegionFound;

    /**
     * Is called when the osm graph is loaded, but the elevation data is still missing.
     */
    private Runnable callbackGraph;

    /**
     * Is called when a ski region go loaded successfully.
     */
    private Runnable callbackSuccess;

    /**
     * Is called when an error occurs (e.g no internet connection).
     */
    private Runnable callbackError;

    private double lat;
    private double lon;
    private OSMGraph skiGraph;

    public void load(MapPosition position) {
        load(position.getLatitude(), position.getLongitude());
    }

    public void load(double lat, double lon) {
        this.lat = lat;
        this.lon = lon;
        load(false);
    }

    /**
     * Is called when a ski region for the specified coordinate is found.
     * After that the ski region will be loaded.
     *
     * @param callbackSkiRegionFound the callback object
     */
    public void setCallbackSkiRegionFound(Runnable callbackSkiRegionFound) {
        this.callbackSkiRegionFound = callbackSkiRegionFound;
    }

    /**
     * Is called when no ski region for the specified coordinate is found.
     *
     * @param callbackNoSkiRegionFound the callback object
     */
    public void setCallbackNoSkiRegionFound(Runnable callbackNoSkiRegionFound) {
        this.callbackNoSkiRegionFound = callbackNoSkiRegionFound;
    }

    /**
     * Is called when the osm graph is loaded, but the elevation data is still missing.
     *
     * @param callbackGraph the callback object
     */
    public void setCallbackGraphLoadedButNoElevationYet(Runnable callbackGraph) {
        this.callbackGraph = callbackGraph;
    }

    /**
     * Is called when a ski region go loaded successfully.
     *
     * @param callbackSuccess the callback object
     */
    public void setCallbackSuccess(Runnable callbackSuccess) {
        this.callbackSuccess = callbackSuccess;
    }

    /**
     * Is called when an error occurs (e.g no internet connection).
     *
     * @param callbackError the callback object
     */
    public void setCallbackError(Runnable callbackError) {
        this.callbackError = callbackError;
    }

    private void load(boolean useLargeDelta) {

        double delta = useLargeDelta ? 0.06f : 0.02f;

        String query = new OverpassQueryBuilder()
                .setQueryBounds(lat - delta, lon - delta, lat + delta, lon + delta)
                .createWinterSportsRegionBoundsQuery()
                .build();

        System.out.println(query);

        final OverpassRequest regionBoundsRequest = new OverpassRequest(query, false);
        regionBoundsRequest.setOnCompleteCallback(() -> {
            regionBoundsLoaded(regionBoundsRequest, useLargeDelta);
        });
        regionBoundsRequest.send();
    }

    private void regionBoundsLoaded(OverpassRequest request, boolean largeDelaAlreadyUsed) {
        if (!request.requestLoadedSuccessful()) {
            if (callbackError != null)
                callbackError.run();
            return;
        }

        OSMGraph bounds = OSMGraph.fromOverpassResult(request.getResult(), false);

        // Check if response contains bounds, otherwise reload bounds with larger radius
        if (bounds.getWays().size() == 0) {
            if (largeDelaAlreadyUsed) {
                System.out.println("ABORT");
                if (callbackNoSkiRegionFound != null)
                    callbackNoSkiRegionFound.run();
            } else {
                System.out.println("RETRY");
                load(true);
            }
            return;
        }

        if(callbackSkiRegionFound != null)
            callbackSkiRegionFound.run();

        // Load OSMGraph based on the result of the first request
        OverpassQueryBuilder builder = new OverpassQueryBuilder();
        builder.createSkiRegionQuery();
        builder.usePolygonBounds();

        for (OSMElement way : bounds.getWays()) {
            for (long nodeId : way.nodes) {
                OSMElement node = bounds.getNode(nodeId);
                builder.addPolygonCoordinate(node.lat, node.lon);
            }
        }

        System.out.println(builder.build());

        final OverpassRequest skiGraphRequest = new OverpassRequest(builder.build());
        skiGraphRequest.setOnCompleteCallback(() -> {
            skiGraphLoaded(skiGraphRequest);
        });
        skiGraphRequest.send();
    }

    private void skiGraphLoaded(OverpassRequest request) {
        if (!request.requestLoadedSuccessful()) {
            if (callbackError != null)
                callbackError.run();
            return;
        }

        skiGraph = OSMGraph.fromOverpassResult(request.getResult(), true);

        if(callbackGraph != null)
            callbackGraph.run();

        skiGraph.loadAndAddElevationData(this::elevationLoaded);
    }

    private void elevationLoaded() {
        if (callbackSuccess != null)
            callbackSuccess.run();
    }

    /**
     * Returns the loaded osm graph.
     * <p>
     * Will only return a valid object,
     * after callbackSuccess got called.
     *
     * @return the ski osm graph
     */
    public OSMGraph getSkiGraph() {
        return skiGraph;
    }
}
