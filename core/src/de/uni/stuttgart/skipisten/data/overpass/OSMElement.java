package de.uni.stuttgart.skipisten.data.overpass;

import com.badlogic.gdx.graphics.Color;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.gui.draw.Colors;
import de.uni.stuttgart.skipisten.gui.draw.Polyline;

public class OSMElement {
    //Render options for ways
    private static final boolean RENDER_GENERATED_WAYS = false;
    private static final boolean RENDER_HIGHWAYS = false;

    public static final double PISTE_THICKNESS = 7e-7; //approx. 16m at latitude 47

    @SerializedName("type")
    public String type;

    public int typeInt;

    @SerializedName("id")
    public long id;

    @SerializedName("lat")
    public double lat;

    @SerializedName("lon")
    public double lon;

    @SerializedName("nodes")
    public long[] nodes;

    @SerializedName("tags")
    public Map<String, String> tags = new HashMap<>();

    public double elevation;

    /**
     * Only for nodes.
     * Links to the parent way of the node.
     *
     * If a node is included in multiple ways,
     * only one way is linked here.
     */
    public long parentWayId = -1;

    // --- way fields ---

    public int aerialwayType;
    public int difficulty;

    public boolean isHighway;
    public boolean isPiste;
    public boolean isGenerated;
    public boolean isOneWay;

    public Polyline outerLine;
    public Polyline innerLine;
    public Polyline highlightLine;
    public boolean renderHighlight = false;

    public void initializeStage1() {
        switch (type) {
            case "node":
                typeInt = Type.NODE;
                break;
            case "way":
                typeInt = Type.WAY;
                break;
            case "relation":
                typeInt = Type.RELATION;
                break;
            default:
                typeInt = Type.UNKNOWN;
                break;
        }
        aerialwayTyping();

        if (typeInt == Type.WAY) {
            if (tags.containsKey("piste:type")) {
                isPiste = true;
                switch (getTag("piste:difficulty")) {
                    case "novice":
                        difficulty = Difficulty.NOVICE;
                        break;
                    case "easy":
                        difficulty = Difficulty.EASY;
                        break;
                    case "intermediate":
                        difficulty = Difficulty.INTERMEDIATE;
                        break;
                    case "advanced":
                        difficulty = Difficulty.ADVANCED;
                        break;
                    case "expert":
                        difficulty = Difficulty.EXPERT;
                        break;
                    case "freeride":
                        difficulty = Difficulty.FREERIDE;
                        break;
                    default:
                        difficulty = Difficulty.EXTREME;
                        break;
                }
            } else if (tags.containsKey("highway")) {
                isHighway = true;
            }
        }
    }

    public void initializePolyline(OSMGraph graph) {
        if (!RENDER_GENERATED_WAYS && isGenerated) return;
        if (!RENDER_HIGHWAYS && isHighway) return;

        Color outerWayColor, innerWayColor;
        float outerWayZ = -40;
        float innerWayZ = -41;

        if (isGenerated) {
            outerWayColor = new Color(0x026b02ff);
            innerWayColor = new Color(0x228b22ff);
        } else if (isPiste) {
            outerWayColor = Colors.pisteOutlineColors[difficulty];
            innerWayColor = Colors.pisteInlineColors[difficulty];
            outerWayZ = difficulty - 20;
            innerWayZ = difficulty - 30;
        } else if (isAerialway()) {
            outerWayColor = Colors.AERIALWAY;
            innerWayColor = Colors.LIGHT_AERIALWAY;
        } else {
            outerWayColor = Color.DARK_GRAY;
            innerWayColor = Color.GRAY;
        }

        double[] x = new double[nodes.length];
        double[] y = new double[nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            OSMElement node = graph.getNode(nodes[i]);
            x[i] = TileMath.longitudeToMapX(node.lon);
            y[i] = TileMath.latitudeToMapY(node.lat);
        }
        outerLine = new Polyline(x, y, PISTE_THICKNESS, 10, PISTE_THICKNESS * .7, outerWayColor, outerWayZ);
        innerLine = new Polyline(x, y, PISTE_THICKNESS * .7, 10, innerWayColor, innerWayZ);
        highlightLine = new Polyline(x, y, PISTE_THICKNESS * .5, 10, Color.WHITE, -42);
    }

    private void aerialwayTyping() {
        switch (getTag("aerialway")) {
            case "cable_car":
                aerialwayType = Aerialway.CABLE_CAR;
                break;
            case "gondola":
                aerialwayType = Aerialway.GONDOLA;
                break;
            case "chair_lift":
                aerialwayType = Aerialway.CHAIR_LIFT;
                break;
            case "drag_lift":
            case "t-bar":
            case "j-bar":
                aerialwayType = Aerialway.DRAG_LIFT;
                break;
            case "platter":
                aerialwayType = Aerialway.PLATTER;
                break;
            case "rope_tow":
                aerialwayType = Aerialway.ROPE_TOW;
                break;
            case "magic_carpet":
                aerialwayType = Aerialway.MAGIC_CARPET;
                break;
            case "mixed_lift":
                aerialwayType = Aerialway.MIXED_LIFT;
                break;
            default:
                aerialwayType = Aerialway.NO_AERIALWAY;
                break;
        }
    }

    /**
     * Returns the tag for a specific key.
     * <p>
     * If no tag for this key exists,
     * an empty string is returned.
     */
    public String getTag(String key) {
        if (tags.containsKey(key)) {
            return tags.get(key);
        }

        return "";
    }

    public boolean isAerialway() {
        return aerialwayType != Aerialway.NO_AERIALWAY;
    }

    public boolean isAdjacentTo(OSMElement way) {
        for (long node1 : nodes) {
            for (long node2 : way.nodes) {
                if (node1 == node2) return true;
            }
        }
        return false;
    }

    public static class Type {
        public static final int UNKNOWN = -1;
        public static final int NODE = 0;
        public static final int WAY = 1;
        public static final int RELATION = 2;
    }

    public static class Difficulty {
        public static final int NOVICE = 0;
        public static final int EASY = 1;
        public static final int INTERMEDIATE = 2;
        public static final int ADVANCED = 3;
        public static final int EXPERT = 4;
        public static final int FREERIDE = 5;
        public static final int EXTREME = 6;
    }

    public static class Aerialway {
        public static final int NO_AERIALWAY = -1;
        public static final int CABLE_CAR = 0;
        public static final int GONDOLA = 1;
        public static final int CHAIR_LIFT = 2;
        public static final int DRAG_LIFT = 3; // drag_lift & t_bar & j_bar
        public static final int PLATTER = 4;
        public static final int ROPE_TOW = 5;
        public static final int MAGIC_CARPET = 6;
        public static final int MIXED_LIFT = 7;
    }
}
