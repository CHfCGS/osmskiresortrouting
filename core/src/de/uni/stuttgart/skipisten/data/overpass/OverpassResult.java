package de.uni.stuttgart.skipisten.data.overpass;

import com.google.gson.annotations.SerializedName;

public class OverpassResult {
    @SerializedName("elements")
    public OSMElement[] elements;
}
