package de.uni.stuttgart.skipisten.data.overpass;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Can be used to create Overpass Queries
 */
public class OverpassQueryBuilder {

    private String outputFormat = "json";
    private int timeOut = 180;
    private boolean timeOutSet = false;

    boolean useBBox;
    private double lat1;
    private double lon1;
    private double lat2;
    private double lon2;

    boolean usePolygonBounds;
    private StringBuilder polygon;

    private List<String> innerQuery;

    public OverpassQueryBuilder() {
        innerQuery = new ArrayList<>();
    }

    /**
     * Sets the output format of the overpass request.
     * Default value is json.
     *
     * @param format the output format
     * @return the builder object
     */
    public OverpassQueryBuilder setOutputFormat(String format) {
        this.outputFormat = format;
        return this;
    }

    /**
     * Sets the timeout time for the overpass request.
     * Default value is 180.
     *
     * @param timeOut the time in seconds
     * @return the builder object
     */
    public OverpassQueryBuilder setTimeout(int timeOut) {
        this.timeOut = timeOut;
        timeOutSet = true;
        return this;
    }

    public OverpassQueryBuilder setQueryBounds(double lat1, double lon1, double lat2, double lon2) {
        useBBox = true;

        if (lat1 > lat2) {
            double temp = lat2;
            lat2 = lat1;
            lat1 = temp;
        }

        if (lon1 > lon2) {
            double temp = lon2;
            lon2 = lon1;
            lon1 = temp;
        }

        this.lat1 = lat1;
        this.lon1 = lon1;
        this.lat2 = lat2;
        this.lon2 = lon2;

        return this;
    }

    public OverpassQueryBuilder usePolygonBounds() {
        usePolygonBounds = true;
        polygon = new StringBuilder();
        return this;
    }

    public OverpassQueryBuilder addPolygonCoordinate(double lat, double lon) {
        if (polygon.length() > 0) {
            polygon.append(" ");
        }
        polygon.append(lat);
        polygon.append(" ");
        polygon.append(lon);

        return this;
    }

    public OverpassQueryBuilder createWinterSportsRegionBoundsQuery() {
        innerQuery.add("way[\"landuse\"=\"winter_sports\"]");
        innerQuery.add("relation[\"landuse\"=\"winter_sports\"]");
        return this;
    }

    public OverpassQueryBuilder createSkiRegionQuery() {
        innerQuery.add("way[\"aerialway\"][\"aerialway\"!~\"explosive|goods|proposed|construction|zip_line\"][!\"proposed\"][!\"construction\"][\"building\"!=\"yes\"]");
        innerQuery.add("way[\"railway\"~\"narrow_gauge|funicular\"]");
        innerQuery.add("way[\"highway\"~\"footway|steps|service\"]");
        innerQuery.add("way[\"piste:type\"~\"downhill|connection|yes|playground\"][\"area\"!=\"yes\"]");

        return this;
    }

    /**
     * Creates the query string
     *
     * @return the Overpass Query as String
     */
    public String build() {
        StringBuilder query = new StringBuilder();
        query.append(String.format("[out:%s]", outputFormat));
        if (timeOutSet) {
            query.append(String.format(Locale.US, "[timeout:%d]", timeOut));
        }
        if (useBBox) {
            query.append(String.format(Locale.US, "[bbox:%f,%f,%f,%f]", lat1, lon1, lat2, lon2));
        }
        query.append(";");

        query.append("(");
        for (String queryPart : innerQuery) {
            query.append(queryPart);

            if (usePolygonBounds) {
                query.append(String.format("(poly:\"%s\")", polygon.toString()));
            }

            query.append(";");
            query.append(">;");
        }
        query.append(");");

        query.append(" out;");

        return query.toString();
    }
}
