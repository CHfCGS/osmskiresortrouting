package de.uni.stuttgart.skipisten.data.overpass;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.net.HttpStatus;
import com.google.gson.Gson;

public class OverpassRequest {
    //see https://wiki.openstreetmap.org/wiki/Overpass_API#Public_Overpass_API_instances
    private static final String REQUEST_URL = "https://lz4.overpass-api.de/api/interpreter";
    private static final Gson gson = new Gson();

    private final static long MAX_CACHE_AGE_IN_SECONDS = 3600 * 24 * 30;

    private String requestString;
    private boolean cacheResult;
    private boolean complete = false;
    private OverpassResult result = null;

    private Runnable onCompleteCallback;

    private Net.HttpResponseListener listener = new Net.HttpResponseListener() {
        @Override
        public void handleHttpResponse(Net.HttpResponse httpResponse) {
            if (httpResponse.getStatus().getStatusCode() != HttpStatus.SC_OK) { //reject any non-OK response
                cancelled();
                return;
            }

            String resultAsString = httpResponse.getResultAsString();

            if(cacheResult) {
                // Save result to cache
                getCacheFile().writeString(resultAsString, false);
            }

            result = gson.fromJson(resultAsString, OverpassResult.class);
            complete = true;

            if (onCompleteCallback != null)
                onCompleteCallback.run();
        }

        @Override
        public void failed(Throwable t) {
            t.printStackTrace();
            cancelled();
        }

        @Override
        public void cancelled() {
            complete = true;
            result = null;

            if (onCompleteCallback != null)
                onCompleteCallback.run();
        }
    };

    /**
     * Creates an overpass request with the given query string.
     *
     * @param request Overpass query string, see https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide
     */
    public OverpassRequest(String request) {
        this.requestString = request;
        this.cacheResult = true;
    }

    /**
     *
     * Creates an overpass request with the given query string.
     *
     * @param request Overpass query string, see https://wiki.openstreetmap.org/wiki/Overpass_API/Language_Guide
     * @param cacheResult whether the result of this request should be cached
     */
    public OverpassRequest(String request, boolean cacheResult) {
        this.requestString = request;
        this.cacheResult = cacheResult;
    }

    /***
     * Sets a callback which will be called when the query is complete
     * @param onCompleteCallback the callback
     */
    public void setOnCompleteCallback(Runnable onCompleteCallback) {
        this.onCompleteCallback = onCompleteCallback;
    }

    /**
     * Sends this request to the API server. You can check the status with the `isComplete` and `getResult` queries.
     */
    public void send() {
        FileHandle cacheFile = getCacheFile();
        boolean useCachedFile = false;

        if (cacheFile.exists()) {
            long cacheAgeInS = (System.currentTimeMillis() - cacheFile.lastModified()) / 1000;

            if (cacheAgeInS <= MAX_CACHE_AGE_IN_SECONDS) {
                useCachedFile = true;
            } else {
                cacheFile.delete();
            }
        }

        if (useCachedFile) {
            System.out.println("Loading from Cache");

            result = gson.fromJson(cacheFile.readString(), OverpassResult.class);
            complete = true;

            if (onCompleteCallback != null)
                onCompleteCallback.run();

        } else {
            Net.HttpRequest request = new Net.HttpRequest(Net.HttpMethods.POST);
            request.setUrl(REQUEST_URL);
            request.setContent(requestString);

            complete = false;
            result = null;

            Gdx.net.sendHttpRequest(request, listener);
        }

    }

    private FileHandle getCacheFile() {
        return Gdx.files.local("cache/overpass/" + requestString.hashCode() + ".xml");
    }

    /**
     * Check completion of this request.
     *
     * @return True if the request has been sent and a response has been received, or an error occurred.
     */
    public boolean isComplete() {
        return complete;
    }

    /**
     * Check if request loaded successful
     *
     * @return True if the request has been sent and a response has been received and no error occurred.
     */
    public boolean requestLoadedSuccessful() {
        return isComplete() && result != null;
    }

    /**
     * Get the result of this request.
     *
     * @return Result object returned by this request, `null` if the request is not complete or an error occurred.
     */
    public OverpassResult getResult() {
        return result;
    }

}
