package de.uni.stuttgart.skipisten.data.elevation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import lib.gui.Renderer3D;
import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;
import de.uni.stuttgart.skipisten.gui.osm3d.OSMPanel3D;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import de.uni.stuttgart.skipisten.data.elevation.compression.ElevationDataCompression;

/**
 * Manages the loading of elevation data for OSM tiles
 */
public class ElevationTileLoader {
    /**
     * Elevation server instance url
     */
    private static final String REQUEST_URL = "http://176.9.99.146:37324/elevation";
    private static final int MAX_REQUESTS = 4;

    private static OSMPanel3D panel = null;

    private static BlockingQueue<ElevationTreeTile> immediateRequestQueue = new LinkedBlockingQueue<>();
    private static volatile int sentRequests = 0;

    private static ExecutorService cacheLoader = Executors.newFixedThreadPool(2);

    /**
     * Set the OSMPanel3D that is used for determining the tile position of the camera.
     * @param panel OSMPanel3D to be used
     */
    public static void setPanel(OSMPanel3D panel) {
        ElevationTileLoader.panel = panel;
    }

    /**
     * Signal that an elevation tile request has successfully been processed.
     */
    /*package-private*/ static void onRequestDone() {
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            sentRequests--;
            sendRequest();
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Add a request for the given tile to the immediate request queue.
     * @param tile Tile to be added to queue
     */
    public static void addImmediateRequest(ElevationTreeTile tile) {
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            if (tile == null) return;
            if (tile.getRequestedResolution() > 0) {
                try {
                    immediateRequestQueue.put(tile);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Send the next elevation data request, if possible.
     */
    public static void sendRequest() {
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            if (sentRequests >= MAX_REQUESTS) return; //don't allow more than MAX_REQUESTS simultaneous requests
            ElevationTreeTile queueTile = immediateRequestQueue.poll();
            if (queueTile != null) { //if there are requests in the immediate queue: send those first
                sendRequest(queueTile);
            } else if (panel != null) { //otherwise check for panel
                Camera camera = Renderer3D.getCamera();
                TilePosition.FractionalTilePosition pos = panel.getFractTilePositionAt(camera.position.x, camera.position.y); //get camera tile position
                ElevationTreeTile tile = ElevationProvider.getClosestRequestedTile(pos); //look for closest requested tile
                if (tile != null) sendRequest(tile); //if found: send request
            }
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Sends a request for the given tile.
     */
    private static void sendRequest(ElevationTreeTile tile) {
        if (tile.getRequestedResolution() == 0) return; //check whether tile is still requesting data

        if (loadFromCache(tile)) { //try to load from cache first
            sendRequest();
            return;
        }

        sentRequests++;
        tile.signalRequestStart(); //signal start of load to tile

        byte[] requestData = new byte[10]; //create request, see api-format.txt
        requestData[0] = (byte) tile.getZoomLevel();
        int x = tile.getTileX();
        int y = tile.getTileY();
        requestData[1] = (byte) (x >> 24);
        requestData[2] = (byte) (x >> 16);
        requestData[3] = (byte) (x >> 8);
        requestData[4] = (byte) x;
        requestData[5] = (byte) (y >> 24);
        requestData[6] = (byte) (y >> 16);
        requestData[7] = (byte) (y >> 8);
        requestData[8] = (byte) y;
        requestData[9] = (byte) tile.getLoadingResolution();

        Net.HttpRequest req = new Net.HttpRequest();
        req.setUrl(REQUEST_URL);
        req.setMethod(Net.HttpMethods.POST);
        req.setContent(new ByteArrayInputStream(requestData), requestData.length);
        Gdx.net.sendHttpRequest(req, tile.loadListener); //send request
    }

    /**
     * Get the cache file path for the given tile.
     * @param tile Tile to get the path for
     * @return Cache file path
     */
    /*package-private*/ static String getCachePath(ElevationTreeTile tile) {
        return getCachePath(tile.getZoomLevel(), tile.getTileX(), tile.getTileY());
    }

    /**
     * Get the cache file path for the given data.
     * @param zoom OSM zoom level
     * @param x OSM tile X position
     * @param y OSM tile Y position
     * @return Cache file path
     */
    /*package-private*/ static String getCachePath(int zoom, int x, int y) {
        return "cache/elevation/" + zoom + "/" + x + "-" + y + ".bin";
    }

    /**
     * Attempts to load elevation data for the given tile from cache.
     * @param tile Elevation tile to be loaded
     * @return Whether the tile was successfully loaded from cache
     */
    private static boolean loadFromCache(ElevationTreeTile tile) {
        if (tile.getRequestedResolution() == 0) return true; //check if tile is requesting data
        int requestedRes = tile.getRequestedResolution();
        ElevationTreeTile loadedTile = tile;
        FileHandle cacheFile = Gdx.files.local(getCachePath(loadedTile)); //get cache file handle
        while (!cacheFile.exists()) { //if file doesn't exist: try with parent tiles
            loadedTile = loadedTile.getParent();
            if (loadedTile == null) return false;
            cacheFile = Gdx.files.local(getCachePath(loadedTile));
        }
        if (!cacheFile.exists()) return false; //file not found: load failed
        InputStream str = cacheFile.read();
        try {
            int resolution = str.read(); //read resolution, reject if too low
            if (resolution < requestedRes) {
                str.close();
                return false;
            }
            loadedTile.forceLoadingResolution(resolution);
            ElevationTreeTile finalLoadedTile = loadedTile;
            cacheLoader.submit(() -> cacheLoadAsync(finalLoadedTile, str));
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static void cacheLoadAsync(ElevationTreeTile tile, InputStream str) {
        try {
            short[][] data = ElevationDataCompression.uncompressHeightData(str, (1 << (tile.getLoadingResolution() - tile.getZoomLevel())) + 1 + 2 * (1 << (tile.getLoadingResolution() - ElevationProvider.MIN_RES))); //uncompress data
            tile.loadFromCacheData(data);
            str.close();
        } catch (IOException e) {
            e.printStackTrace();
            tile.loadFromCacheData(null);
        }
    }

    /*package-private*/ static boolean isCached(int zoom, int x, int y, int resolution) {
        FileHandle cacheFile = Gdx.files.local(getCachePath(zoom, x, y)); //get cache file handle
        while (!cacheFile.exists() && zoom > ElevationProvider.MIN_ZOOM) { //if file doesn't exist: try with parent tiles
            zoom--;
            x >>= 1;
            y >>= 1;
            cacheFile = Gdx.files.local(getCachePath(zoom, x, y));
        }
        if (!cacheFile.exists()) return false; //file not found: not cached
        InputStream str = cacheFile.read();
        try {
            int fileRes = str.read(); //read resolution, reject if too low
            if (fileRes < resolution) {
                str.close();
                return false;
            }
            str.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}
