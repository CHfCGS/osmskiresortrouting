package de.uni.stuttgart.skipisten.data.elevation;

import de.uni.stuttgart.skipisten.gui.osm.tiles.TilePosition;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

/**
 * Central interface class for retrieving elevation data for OSM tiles.
 */
public class ElevationProvider {
    /**
     * Minimum OSM zoom level supported by elevation server
     */
    public static final int MIN_ZOOM = 8;
    /**
     * Maximum OSM zoom level supported by elevation server
     */
    public static final int MAX_ZOOM = 13;
    /**
     * Minimum resolution supported by elevation server (sample distance on OSM zoom scale)
     */
    public static final int MIN_RES = 15;
    /**
     * Maximum resolution supported by elevation server (sample distance on OSM zoom scale)
     */
    public static final int MAX_RES = 20;

    /**
     * Lock used to protect the entire elevation data structure from concurrent access.
     */
    private static final ReentrantReadWriteLock ELEVATION_LOCK = new ReentrantReadWriteLock(true);
    /*package-private*/ static final ReentrantReadWriteLock.ReadLock ELEVATION_READ_LOCK = ELEVATION_LOCK.readLock();
    /*package-private*/ static final ReentrantReadWriteLock.WriteLock ELEVATION_WRITE_LOCK = ELEVATION_LOCK.writeLock();

    /**
     * Cache of elevation tiles at zoom level MIN_ZOOM
     * Key consists of the tile coordinates: key = (x << 32) | y
     */
    private static Map<Long, ElevationTreeTile> cache = new HashMap<>();

    /**
     * Gets the closest tile to the given position that is requesting data.
     * @param pos Fractional tile position from which the request should be searched.
     * @return Closest tile to given position that is requesting data, or null if there is no such tile near the position.
     */
    /*package-private*/ static ElevationTreeTile getClosestRequestedTile(TilePosition.FractionalTilePosition pos) {
        long cx = pos.getPosition().getX();
        int cy = pos.getPosition().getY();
        long key = (cx << 32) | cy;
        ElevationTreeTile result = null;
        if (cache.containsKey(key)) result = cache.get(key).getNearestRequested(pos.getXFraction(), pos.getYFraction()); //check center tile first
        if (result != null) return result; //if result found: return it
        for (int d = 1; d <= 5; d++) { //otherwise search in increasing radius, up to 5 MIN_ZOOM tiles away
            int dx = d;
            int dy = 0;
            do { //iterate over tiles with given distance, see https://stackoverflow.com/questions/16571362/how-to-iterate-over-rings-in-a-raster-grid
                key = ((cx + dx) << 32) | (cy + dy);
                if (cache.containsKey(key)) result = cache.get(key).getNearestRequested(dx > 0 ? 0 : 1, dy > 0 ? 0 : 1); //check each tile with the fractional position in the nearest corner
                if (result != null) return result; //return the first result that is found

                int nx = -sgn(dy); //find next tile with same distance
                int ny = sgn(dx);
                if (nx != 0 && dist(dx + nx, dy) == d) dx += nx;
                else if (ny != 0 && dist(dx, dy + ny) == d) dy += ny;
                else {
                    dx += nx;
                    dy += ny;
                }
            } while (dx != d || dy != 0);
        }
        return null;
    }

    private static int dist(int dx, int dy) {
        return (int)Math.sqrt(dx * dx + dy * dy);
    }

    private static int sgn(int n) {
        return Integer.compare(n, 0);
    }

    /**
     * Check whether elevation data is loaded for a given tile and resolution.
     * @param pos OSM Tile position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param lockHandled Whether the read lock has already been handled for this thread
     * @return Whether elevation data for the given tile position and resolution is loaded
     */
    public static boolean isTileLoaded(TilePosition pos, int resolution, boolean lockHandled) {
        return isTileLoaded(pos.getZoom(), pos.getX(), pos.getY(), resolution, lockHandled);
    }

    /**
     * Check whether elevation data is loaded for a given tile and resolution.
     * @param zoom OSM zoom level of tile
     * @param x OSM tile X position
     * @param y OSM tile Y position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param lockHandled Whether the read lock has already been handled for this thread
     * @return Whether elevation data for the given tile position and resolution is loaded
     */
    public static boolean isTileLoaded(int zoom, int x, int y, int resolution, boolean lockHandled) {
        if (zoom < MIN_ZOOM || resolution < zoom || resolution > MAX_RES) return false;
        try {
            if (!lockHandled) ELEVATION_READ_LOCK.lock();
            int tileX = x >> (zoom - MIN_ZOOM); //coordinates of corresponding MIN_ZOOM tile
            int tileY = y >> (zoom - MIN_ZOOM);
            int modulus = 1 << (zoom - MIN_ZOOM); //modulus for determining tile sub-coordinates for desired zoom level
            long key = ((long) tileX << 32) | tileY;
            if (!cache.containsKey(key)) return false;
            ElevationTreeTile tile = cache.get(key);
            if (tile.isNullTile()) return true; //if the MIN_ZOOM tile is a null tile, it means the server doesn't have this data - so we already have all we can get
            return cache.get(key).hasLoadedChild(zoom, x % modulus, y % modulus, resolution, true);
        } finally {
            if (!lockHandled) ELEVATION_READ_LOCK.unlock();
        }
    }

    /**
     * Load elevation data for a given tile and resolution (unless already loaded).
     * @param pos OSM Tile position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param immediate Whether the tile should be loaded immediately and prioritized over location-based loading
     */
    public static void loadTile(TilePosition pos, int resolution, boolean immediate) {
        loadTile(pos.getZoom(), pos.getX(), pos.getY(), resolution, null, immediate);
    }

    /**
     * Load elevation data for a given tile and resolution (unless already loaded).
     * @param pos OSM Tile position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param onComplete Runnable to execute once the request is complete
     * @param immediate Whether the tile should be loaded immediately and prioritized over location-based loading
     */
    public static void loadTile(TilePosition pos, int resolution, Runnable onComplete, boolean immediate) {
        loadTile(pos.getZoom(), pos.getX(), pos.getY(), resolution, onComplete, immediate);
    }

    /**
     * Load elevation data for a given tile and resolution (unless already loaded).
     * @param zoom OSM zoom level of tile
     * @param x OSM tile X position
     * @param y OSM tile Y position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param immediate Whether the tile should be loaded immediately and prioritized over location-based loading
     */
    public static void loadTile(int zoom, int x, int y, int resolution, boolean immediate) {
        loadTile(zoom, x, y, resolution, null, immediate);
    }

    /**
     * Load elevation data for a given tile and resolution (unless already loaded).
     * @param zoom OSM zoom level of tile
     * @param x OSM tile X position
     * @param y OSM tile Y position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param onComplete Runnable to execute once the request is complete
     * @param immediate Whether the tile should be loaded immediately and prioritized over location-based loading
     */
    public static void loadTile(int zoom, int x, int y, int resolution, Runnable onComplete, boolean immediate) {
        if (zoom < MIN_ZOOM || resolution < zoom || resolution > MAX_RES) return; //ignore requests that make no sense
        if (resolution < MIN_RES) resolution = MIN_RES; //load any resolution below minimum as minimum
        if (isTileLoaded(zoom, x, y, resolution, false)) { //don't load data that we already have
            if (onComplete != null) onComplete.run();
            return;
        }
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            if (zoom > MAX_ZOOM) {
                x = x >> (zoom - MAX_ZOOM);
                y = y >> (zoom - MAX_ZOOM);
                zoom = MAX_ZOOM;
            }
            int tileX = x >> (zoom - MIN_ZOOM); //coordinates of corresponding MIN_ZOOM tile
            int tileY = y >> (zoom - MIN_ZOOM);
            int modulus = 1 << (zoom - MIN_ZOOM); //modulus for determining tile sub-coordinates for desired zoom level
            long key = ((long) tileX << 32) | tileY;
            ElevationTreeTile tile;
            if (cache.containsKey(key)) { //if already in cache: get and mark child for loading
                tile = cache.get(key).markChildForLoading(zoom, x % modulus, y % modulus, resolution, onComplete);
            } else { //otherwise: create empty tile, mark child for loading, add to cache
                ElevationTreeTile newTile = new ElevationTreeTile(null, MIN_ZOOM, tileX, tileY, false);
                tile = newTile.markChildForLoading(zoom, x % modulus, y % modulus, resolution, onComplete);
                cache.put(key, newTile);
            }
            if (immediate) ElevationTileLoader.addImmediateRequest(tile);
            ElevationTileLoader.sendRequest();
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Get elevation data for a given tile and resolution.
     * @param pos OSM Tile position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param lockHandled Whether the read lock has already been handled for this thread
     * @return Elevation data for requested tile and resolution, or a null-tile with no data if the elevation data is not available
     */
    public static ElevationTile getTile(TilePosition pos, int resolution, boolean lockHandled) {
        return getTile(pos.getZoom(), pos.getX(), pos.getY(), resolution, lockHandled);
    }

    /**
     * Get elevation data for a given tile and resolution.
     * @param zoom OSM zoom level of tile
     * @param x OSM tile X position
     * @param y OSM tile Y position
     * @param resolution Resolution: sample distance on OSM zoom level scale
     * @param lockHandled Whether the read lock has already been handled for this thread
     * @return Elevation data for requested tile and resolution, or a null-tile with no data if the elevation data is not available
     */
    public static ElevationTile getTile(int zoom, int x, int y, int resolution, boolean lockHandled) {
        if (zoom < MIN_ZOOM || resolution < zoom || resolution > MAX_RES) return new ElevationTreeTile(null, zoom, x, y, true); //zoom too low or invalid resolution: null-tile
        try {
            if (!lockHandled) ELEVATION_READ_LOCK.lock();
            int tileX = x >> (zoom - MIN_ZOOM); //coordinates of corresponding MIN_ZOOM tile
            int tileY = y >> (zoom - MIN_ZOOM);
            int modulus = 1 << (zoom - MIN_ZOOM); //modulus for determining tile sub-coordinates for desired zoom level
            long key = ((long) tileX << 32) | tileY;
            if (!cache.containsKey(key)) return new ElevationTreeTile(null, zoom, x, y, true); //MIN_ZOOM tile not loaded: null-tile
            return cache.get(key).getChild(zoom, x % modulus, y % modulus, resolution, true);
        } finally {
            if (!lockHandled) ELEVATION_READ_LOCK.unlock();
        }
    }

    /**
     * Adds elevation data to all given nodes, requesting it from the server if required.
     * @param nodes Collection of nodes to populate with elevation data.
     */
    public static void addElevationsToNodes(Collection<OSMElement> nodes, OSMGraph.ElevationLoadingParameters params) {
        Map<TilePosition, Set<OSMElement>> elementMap = new HashMap<>();
        for (OSMElement e : nodes) {
            if (e.typeInt != OSMElement.Type.NODE) continue;
            TilePosition pos = new TilePosition(e.lat, e.lon, MAX_ZOOM);
            if (elementMap.containsKey(pos)) elementMap.get(pos).add(e);
            else {
                Set<OSMElement> set = new HashSet<>();
                set.add(e);
                elementMap.put(pos, set);
                if (params != null) params.incrementTotal();
            }
        }

        for (Map.Entry<TilePosition, Set<OSMElement>> e : elementMap.entrySet()) {
            TilePosition pos = e.getKey();
            Set<OSMElement> elements = e.getValue();
            loadTile(pos, MAX_RES, () -> addLoadedElevationsToNodes(pos, elements, params), false);
        }
    }

    private static void addLoadedElevationsToNodes(TilePosition pos, Set<OSMElement> elements, OSMGraph.ElevationLoadingParameters params) {
        ElevationTile tile = getTile(pos, MAX_RES, false);
        for (OSMElement e : elements) {
            e.elevation = tile.getElevation(e.lat, e.lon);
        }
        if (params != null) params.incrementLoaded();
    }

}
