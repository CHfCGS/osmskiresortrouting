package de.uni.stuttgart.skipisten.data.elevation;

import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;

/**
 * Provider of elevation data for a specific OSM tile.
 */
public abstract class ElevationTile {
    private int lastGoodElevation = 0;

    /**
     * @return Whether this tile is a null-tile, i.e. it contains no real elevation data.
     */
    public abstract boolean isNullTile();

    /**
     * @return OSM zoom level of this tile
     */
    public abstract int getZoomLevel();

    /**
     * @return OSM tile X position
     */
    public abstract int getTileX();

    /**
     * @return OSM tile Y position
     */
    public abstract int getTileY();

    /**
     * @return Sample distance on OSM zoom scale
     */
    public abstract int getResolution();

    /**
     * @return Edge length of data array in samples
     */
    public int getEdgeLength() {
        if (isNullTile() || getResolution() == 0) return 0;
        return (1 << (getResolution() - getZoomLevel())) + 1;
    }

    /**
     * @param x Sample X coordinate within this tile
     * @param y Sample Y coordinate within this tile
     * @return Elevation of sample in meters
     */
    public abstract int getElevation(int x, int y);

    /**
     * @param x Sample X coordinate within this tile
     * @param y Sample Y coordinate within this tile
     * @param factor Scale factor for output
     * @return Elevation of sample in meters scaled with the factor
     */
    public float getElevation(int x, int y, float factor) {
        return getElevation(x, y) * factor;
    }

    /**
     * @param xFract Fractional X coordinate within this tile (0-1)
     * @param yFract Fractional Y coordinate within this tile (0-1)
     * @return Interpolated elevation of point in meters
     */
    public double getInterpolatedElevation(double xFract, double yFract) {
        if (isNullTile() || getResolution() == 0) return 0;

        int tileSizeSamples = 1 << (getResolution() - getZoomLevel()); //tile edge length in sample distances
        double sampleX = xFract * tileSizeSamples; //sample coordinates within this tile
        double sampleY = yFract * tileSizeSamples;
        int wholeSampleX = (int)Math.floor(sampleX); //whole sample coordinates
        int wholeSampleY = (int)Math.floor(sampleY);
        double fractSampleX = sampleX - wholeSampleX; //fractional sample coordinates
        double fractSampleY = sampleY - wholeSampleY;
        double invFractSampleX = 1d - fractSampleX; //inverse fractional sample coordinates
        double invFractSampleY = 1d - fractSampleY;

        double nwFactor = invFractSampleX * invFractSampleY; //interpolation factors
        double neFactor = fractSampleX * invFractSampleY;
        double swFactor = invFractSampleX * fractSampleY;
        double seFactor = fractSampleX * fractSampleY;

        int nwSample = getElevation(wholeSampleX, wholeSampleY); //samples surrounding point
        int neSample = getElevation(wholeSampleX + 1, wholeSampleY);
        int swSample = getElevation(wholeSampleX, wholeSampleY + 1);
        int seSample = getElevation(wholeSampleX + 1, wholeSampleY + 1);

        if (nwSample == -32768 || neSample == -32768 || swSample == -32768 || seSample == -32768) {
            return -32768d; //interpolation with voids is impossible, return void value
        }

        return nwFactor * nwSample + neFactor * neSample + swFactor * swSample + seFactor * seSample; //return interpolated value
    }

    /**
     * @param lat Latitude of point
     * @param lon Longitude of point
     * @return Interpolated elevation of point in meters
     */
    public double getElevation(double lat, double lon) {
        if (isNullTile() || getResolution() == 0) return 0;
        double tileSizeMap = 1d / (1 << getZoomLevel()); //tile edge length in map coordinates
        double xFract = (TileMath.longitudeToMapX(lon) - getTileX() * tileSizeMap) / tileSizeMap;
        double yFract = (TileMath.latitudeToMapY(lat) - getTileY() * tileSizeMap) / tileSizeMap;
        return getInterpolatedElevation(xFract, yFract);
    }
}
