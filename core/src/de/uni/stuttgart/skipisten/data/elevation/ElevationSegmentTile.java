package de.uni.stuttgart.skipisten.data.elevation;

/**
 * Pseudo-tile that accesses a section of elevation data from the quadtree in a different zoom level and/or resolution.
 */
public class ElevationSegmentTile extends ElevationTile {
    private int zoomLevel;
    private int tileX;
    private int tileY;
    private int resolution;
    private ElevationTreeTile parent;
    private int xOffset;
    private int yOffset;

    /**
     * Create a pseudo-tile based on the given parameters and parent tile.
     * @param zoomLevel OSM zoom level
     * @param resolution Resolution: sample distance on OSM zoom scale
     * @param parent Parent tile containing the elevation data
     * @param xOffsetFromParent X Offset of this tile's data from the parent tile's data
     * @param yOffsetFromParent Y Offset of this tile's data from the parent tile's data
     */
    public ElevationSegmentTile(int zoomLevel, int tileX, int tileY, int resolution, ElevationTreeTile parent, int xOffsetFromParent, int yOffsetFromParent) {
        if (parent.isNullTile()) throw new IllegalArgumentException("Segment tiles cannot be created from null tiles");
        this.zoomLevel = zoomLevel;
        this.tileX = tileX;
        this.tileY = tileY;
        this.resolution = resolution;
        this.parent = parent;
        this.xOffset = xOffsetFromParent;
        this.yOffset = yOffsetFromParent;
    }

    @Override
    public boolean isNullTile() {
        return false; //never a null-tile
    }

    @Override
    public int getZoomLevel() {
        return zoomLevel;
    }

    @Override
    public int getTileX() {
        return tileX;
    }

    @Override
    public int getTileY() {
        return tileY;
    }

    @Override
    public int getResolution() {
        return resolution;
    }

    @Override
    public int getElevation(int x, int y) {
        short[][] data = parent.data;
        int step = 1 << (parent.getResolution() - resolution); //step size for array iteration, chosen for correct resulting resolution
        int xI = step * x + xOffset + parent.xOffset + parent.edgeWidth;
        int yI = step * y + yOffset + parent.yOffset + parent.edgeWidth;
        if (xI < 0) xI = 0; //clamp indices at actual data array bounds
        if (xI >= data.length) xI = data.length - 1;
        if (yI < 0) yI = 0;
        if (yI >= data.length) yI = data.length - 1;
        return data[xI][yI];
    }
}
