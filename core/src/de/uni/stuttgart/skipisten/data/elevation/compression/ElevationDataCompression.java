package de.uni.stuttgart.skipisten.data.elevation.compression;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ElevationDataCompression {

    /**
     * The left 5 bits are used to encode the total difference.
     * 00001 = 1, ..., 11111 = 31
     */
    private final static byte MAJOR_ADD = (byte) 0b00000_101;

    /**
     * The left 5 bits are used to encode the total difference.
     * 00001 = 1, ..., 11111 = 31
     */
    private final static byte MAJOR_SUB = (byte) 0b00000_001;

    /**
     * The left 6 bits are used to encode the amount of data points.
     * 000001 = 1, ..., 111111 = 63
     */
    private final static byte MINOR_SET = 0b000000_11;

    /**
     * The left 6 bits are used to encode amount how often the data point occurs
     * in the uncompressed array
     * 000001 = 1, ..., 111111 = 63
     */
    private final static byte MINOR_SET_MULTIPLE = 0b000000_10;

    /**
     * Compresses an uncompressed height data array
     */
    public static void compressHeightData(short[][] data, OutputStream str) {
        int length = data.length * data.length * 2;

        int writeIndex = 8; //4 + 4 bytes to store uncompressed & compressed byte array size
        byte[] compressedData = new byte[length * 2 + 100];

        byte lastMajorByte = 0;

        int setMinorStartIndex = -1;
        byte valuesCounter = 0;

        boolean modeMultiple = false;
        byte modeMultipleMinorByte = 0;

        for (int i = 0; i < length; i += 2) {
            int x = (i / 2) / data.length;
            int y = (i / 2) % data.length;
            short dataPoint = data[x][y];
            byte currentMajorByte = (byte)((dataPoint >> 8) & 0xFF);
            byte currentMinorByte = (byte)(dataPoint & 0xFF);

            // Major Byte has changed -> Handle
            if (currentMajorByte != lastMajorByte) {

                // Finish MINOR_SET / MINOR_SET_MULTIPLE
                if (setMinorStartIndex != -1) {
                    compressedData[setMinorStartIndex] |= (valuesCounter << 2);

                    setMinorStartIndex = -1;
                    modeMultiple = false;
                    valuesCounter = 0;
                }

                // Add 31 until difference is smaller than 31
                while (currentMajorByte - lastMajorByte > 31) {
                    compressedData[writeIndex++] = (byte) (MAJOR_ADD | (31 << 3));
                    lastMajorByte += 31;
                }

                // Subtract 31 until difference is smaller than 31
                while (lastMajorByte - currentMajorByte > 31) {
                    compressedData[writeIndex++] = (byte) (MAJOR_SUB | (31 << 3));
                    lastMajorByte -= 31;
                }

                int diff = lastMajorByte - currentMajorByte;

                if (diff < 0) {
                    compressedData[writeIndex++] = (byte) (MAJOR_ADD | ((-diff) << 3));
                }
                if (diff > 0) {
                    compressedData[writeIndex++] = (byte) (MAJOR_SUB | ((diff) << 3));
                }

                lastMajorByte = currentMajorByte;
            }

            // Finish MINOR_SET / MINOR_SET_MULTIPLE
            if ((valuesCounter >= 63 || (modeMultiple && modeMultipleMinorByte != currentMinorByte))
                    && setMinorStartIndex != -1) {

                compressedData[setMinorStartIndex] |= (byte) (valuesCounter << 2);

                setMinorStartIndex = -1;
                modeMultiple = false;
                valuesCounter = 0;
            }

            // Check if MINOR_SET_MULTIPLE should be used
            if (!modeMultiple && i + 5 < length) {
                int x1 = ((i + 2) / 2) / data.length;
                int y1 = ((i + 2) / 2) % data.length;
                short dataPoint1 = data[x1][y1];
                byte nextMajorByte = (byte)((dataPoint1 >> 8) & 0xFF);
                byte nextMinorByte = (byte)(dataPoint1 & 0xFF);

                int x2 = ((i + 4) / 2) / data.length;
                int y2 = ((i + 4) / 2) % data.length;
                short dataPoint2 = data[x2][y2];
                byte nextNextMajorByte = (byte)((dataPoint2 >> 8) & 0xFF);
                byte nextNextMinorByte = (byte)(dataPoint2 & 0xFF);

                if (currentMinorByte == nextMinorByte && currentMinorByte == nextNextMinorByte
                        && currentMajorByte == nextMajorByte && currentMajorByte == nextNextMajorByte) {

                    // Finish MINOR_SET
                    if (setMinorStartIndex != -1) {
                        compressedData[setMinorStartIndex] |= (valuesCounter << 2);
                    }

                    setMinorStartIndex = -1;
                    modeMultiple = true;
                    modeMultipleMinorByte = currentMinorByte;
                }
            }

            if (setMinorStartIndex == -1) {
                valuesCounter = 0;

                setMinorStartIndex = writeIndex;

                if (modeMultiple) {
                    compressedData[writeIndex++] = MINOR_SET_MULTIPLE;
                    compressedData[writeIndex++] = modeMultipleMinorByte;
                } else {
                    compressedData[writeIndex++] = MINOR_SET;
                }
            }

            valuesCounter++;

            if (!modeMultiple) {
                compressedData[writeIndex++] = currentMinorByte;
            }
        }

        // Finish MINOR_SET / MINOR_SET_MULTIPLE
        if (setMinorStartIndex != -1) {
            compressedData[setMinorStartIndex] |= (valuesCounter << 2);
        }

        // Write uncompressed byte array size
        compressedData[0] = (byte) (length >> 24);
        compressedData[1] = (byte) (length >> 16);
        compressedData[2] = (byte) (length >> 8);
        compressedData[3] = (byte) length;

        // Write compressed byte array size
        compressedData[4] = (byte) (writeIndex >> 24);
        compressedData[5] = (byte) (writeIndex >> 16);
        compressedData[6] = (byte) (writeIndex >> 8);
        compressedData[7] = (byte) writeIndex;

        try {
            str.write(compressedData, 0, writeIndex);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Uncompressed a compressed height data array
     */
    public static short[][] uncompressHeightData(InputStream compressedData, int edgeLength) throws IOException {
        // Read byte array sizes
        int uncompressedLength = ((compressedData.read() & 0xFF) << 24) | ((compressedData.read() & 0xFF) << 16) | ((compressedData.read() & 0xFF) << 8) | (compressedData.read() & 0xFF);
        int compressedLength = ((compressedData.read() & 0xFF) << 24) | ((compressedData.read() & 0xFF) << 16) | ((compressedData.read() & 0xFF) << 8) | (compressedData.read() & 0xFF);

        int readCounter = 8;

        int xIndex = 0;
        int yIndex = 0;
        short[][] data = new short[edgeLength][edgeLength];

        byte currentMajorByte = 0;
        byte currentMinorByte;

        while(readCounter < compressedLength) {
            byte readByte = (byte)compressedData.read(); readCounter++;

            if ((readByte & 0b111) == MAJOR_ADD) {

                currentMajorByte += (byte) ((readByte >> 3) & 0b11111); // Amount

            } else if ((readByte & 0b111) == MAJOR_SUB) {

                currentMajorByte -= (byte) ((readByte >> 3) & 0b11111); // Amount

            } else if ((readByte & 0b11) == MINOR_SET_MULTIPLE) {

                byte amountOfValues = (byte) ((readByte >> 2) & 0b111111);

                currentMinorByte = (byte)compressedData.read(); readCounter++;

                for (int j = 0; j < amountOfValues; j++) {
                    data[xIndex][yIndex] = (short)(((currentMajorByte & 0xFF) << 8) | (currentMinorByte & 0xFF));
                    if (++yIndex >= edgeLength) {
                        yIndex = 0;
                        xIndex++;
                    }
                }

            } else if ((readByte & 0b11) == MINOR_SET) {

                byte amountOfValues = (byte) ((readByte >> 2) & 0b111111);

                for (int j = 0; j < amountOfValues; j++) {
                    currentMinorByte = (byte)compressedData.read(); readCounter++;

                    data[xIndex][yIndex] = (short)(((currentMajorByte & 0xFF) << 8) | (currentMinorByte & 0xFF));
                    if (++yIndex >= edgeLength) {
                        yIndex = 0;
                        xIndex++;
                    }
                }
            }
        }

        return data;
    }
}
