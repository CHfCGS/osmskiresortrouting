package de.uni.stuttgart.skipisten.data.elevation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.net.HttpStatus;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;

import de.uni.stuttgart.skipisten.data.elevation.compression.ElevationDataCompression;

/**
 * Node of quadtree structure containing elevation data for OSM tiles.
 */
public class ElevationTreeTile extends ElevationTile {
    private static final int[][] tileOrders = { //index-based tile search orders for finding nearest tile to load, 0 = nw, 1 = ne, 2 = sw, 3 = se
            {0, 1, 2, 3}, //nw first, x > y
            {0, 2, 1, 3}, //nw first, x < y
            {1, 3, 0, 2}, //ne first, x > 1-y
            {1, 0, 3, 2}, //ne first, x < 1-y
            {2, 3, 0, 1}, //sw first, x > 1-y
            {2, 0, 3, 1}, //sw first, x < 1-y
            {3, 1, 2, 0}, //se first, x > y
            {3, 2, 1, 0}  //se first, x < y
    };

    private static final boolean FILL_VOIDS_ON_LOAD = true;

    private int zoomLevel;
    private int tileX;
    private int tileY;
    private int resolution = 0;
    /*package-private*/ int edgeWidth = 0;
    /*package-private*/ int xOffset = 0;
    /*package-private*/ int yOffset = 0;
    /*package-private*/ short[][] data = null;
    private ElevationTreeTile parent = null;
    private ElevationTreeTile nw = null;
    private ElevationTreeTile ne = null;
    private ElevationTreeTile sw = null;
    private ElevationTreeTile se = null;
    private boolean isNullTile = false;
    private volatile int requestedResolution = 0; //resolution this tile would like to request from the server
    private volatile int loadingResolution = 0; //resolution that is currently being requested from the server
    private Set<Runnable> onLoadComplete = new HashSet<>();

    /*package-private*/ Net.HttpResponseListener loadListener = new Net.HttpResponseListener() {
        @Override
        public void handleHttpResponse(Net.HttpResponse httpResponse) {
            if (loadingResolution == 0) {
                cancelled();
                return;
            }

            if (httpResponse.getStatus().getStatusCode() != HttpStatus.SC_OK) { //reject any non-OK response
                failed(new RuntimeException("Received non-OK HTTP response"));
                return;
            }

            InputStream str = httpResponse.getResultAsStream();
            try {
                int size = (str.read() << 8) | str.read();
                if (size == 0) { //size zero means the server doesn't have this data: check for null-tile status
                    try {
                        ElevationProvider.ELEVATION_WRITE_LOCK.lock();
                        checkNullTile();
                        for (Runnable r : onLoadComplete) r.run();
                        onLoadComplete.clear();
                        requestedResolution = 0;
                        loadingResolution = 0;
                    } finally {
                        ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
                    }
                } else { //otherwise read and decompress data into array and add to elevation cache
                    short[][] data = ElevationDataCompression.uncompressHeightData(str, size);
                    try {
                        ElevationProvider.ELEVATION_WRITE_LOCK.lock();
                        init(data, 0, 0, loadingResolution, true, false);
                        if (parent != null) parent.tryCombine();
                        loadingResolution = 0;
                        if (FILL_VOIDS_ON_LOAD) fillVoids();
                        saveToCache();
                    } finally {
                        ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
                    }
                }
                ElevationTileLoader.onRequestDone();
            } catch (IOException e) {
                failed(e);
            }
        }

        @Override
        public void failed(Throwable t) {
            t.printStackTrace();
            cancelled();
        }

        @Override
        public void cancelled() {
            try {
                ElevationProvider.ELEVATION_WRITE_LOCK.lock();
                requestedResolution = 0;
                loadingResolution = 0;
                onLoadComplete.clear();
            } finally {
                ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
            }
            ElevationTileLoader.onRequestDone();
        }
    };

    /**
     * Create an empty tile, optionally marked as a null-tile.
     * @param parent Parent tile
     * @param zoomLevel OSM zoom level
     * @param tileX OSM tile X position
     * @param tileY OSM tile Y position
     * @param nullTile Whether this tile is a null-tile, i.e. there is no data in it.
     */
    public ElevationTreeTile(ElevationTreeTile parent, int zoomLevel, int tileX, int tileY, boolean nullTile) {
        this.parent = parent;
        this.zoomLevel = zoomLevel;
        this.tileX = tileX;
        this.tileY = tileY;
        this.resolution = 0; //resolution is zero for tiles that don't contain immediate data
        this.isNullTile = nullTile;
    }

    /**
     * Create a tile with the given data.
     * @param parent Parent tile
     * @param zoomLevel OSM zoom level
     * @param tileX OSM tile X position
     * @param tileY OSM tile Y position
     * @param resolution Resolution of data: sample distance on OSM zoom scale
     * @param data Array of elevation data, in x-major order
     */
    public ElevationTreeTile(ElevationTreeTile parent, int zoomLevel, int tileX, int tileY,  int resolution, short[][] data) {
        this.parent = parent;
        this.zoomLevel = zoomLevel;
        this.tileX = tileX;
        this.tileY = tileY;
        init(data, 0, 0, resolution, true, false);
    }

    /**
     * Create a tile with the given data and offsets.
     * @param parent Parent tile
     * @param zoomLevel OSM zoom level
     * @param tileX OSM tile X position
     * @param tileY OSM tile Y position
     * @param resolution Resolution of data: sample distance on OSM zoom scale
     * @param data Array of elevation data, in x-major order
     * @param xOffset Array X index where the data of this tile starts
     * @param yOffset Array Y index where the data of this tile starts
     */
    private ElevationTreeTile(ElevationTreeTile parent, int zoomLevel, int tileX, int tileY, int resolution, short[][] data, int xOffset, int yOffset) {
        this.parent = parent;
        this.zoomLevel = zoomLevel;
        this.tileX = tileX;
        this.tileY = tileY;
        init(data, xOffset, yOffset, resolution, false, false);
    }

    /**
     * Load this child from a data array retrieved from cache.
     * @param data Elevation data array
     */
    /*package-private*/ void loadFromCacheData(short[][] data) {
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            if (loadingResolution == 0) return;
            if (data == null) { //when an error is encountered
                resetLoadingResolution(loadingResolution);
                return;
            }
            init(data, 0, 0, loadingResolution, true, false);
            if (parent != null) parent.tryCombine();
            resetLoadingResolution(loadingResolution);
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Initialize this tile with the given data, offsets and resolution.
     * @param data Array of elevation data, in x-major order
     * @param xOffset Array X index where the data of this tile starts
     * @param yOffset Array Y index where the data of this tile starts
     * @param resolution Resolution of data: sample distance on OSM zoom scale
     * @param checkArrayDim Whether the array dimensions should be checked
     * @param force Whether the initialization should be forced even if equal resolution data is already present
     */
    /*package-private*/ void init(short[][] data, int xOffset, int yOffset, int resolution, boolean checkArrayDim, boolean force) {
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            if (this.data != null && (force ? this.resolution > resolution : this.resolution >= resolution)) return; //never overwrite data with lower resolution data, unless forced and equal resolution
            this.resolution = resolution;
            updateEdgeWidth();
            if (checkArrayDim && data.length != getTotalEdgeLength()) { //check for correct data array dimensions
                this.data = null;
                this.resolution = 0;
                edgeWidth = 0;
                throw new IllegalArgumentException("Elevation tile data has incorrect length");
            }
            this.data = data;
            this.xOffset = xOffset;
            this.yOffset = yOffset;
            if (zoomLevel < ElevationProvider.MAX_ZOOM) { //create sub-tiles down to max zoom which reference this tile's data array
                int subX = tileX << 1; //subtile base coordinates
                int subY = tileY << 1;
                int offset = getEdgeLength() / 2;
                if (nw == null) nw = new ElevationTreeTile(this, zoomLevel + 1, subX, subY, resolution, data, xOffset, yOffset);
                else nw.init(data, xOffset, yOffset, resolution, false, force);
                if (ne == null) ne = new ElevationTreeTile(this, zoomLevel + 1, subX + 1, subY, resolution, data, xOffset + offset, yOffset);
                else ne.init(data, xOffset + offset, yOffset, resolution, false, force);
                if (sw == null) sw = new ElevationTreeTile(this, zoomLevel + 1, subX, subY + 1, resolution, data, xOffset, yOffset + offset);
                else sw.init(data, xOffset, yOffset + offset, resolution, false, force);
                if (se == null) se = new ElevationTreeTile(this, zoomLevel + 1, subX + 1, subY + 1, resolution, data, xOffset + offset, yOffset + offset);
                else se.init(data, xOffset + offset, yOffset + offset, resolution, false, force);
            }
            if (resolution >= requestedResolution) { //if load request is fulfilled (directly or indirectly): reset request, run completion callbacks
                requestedResolution = 0;
                loadingResolution = 0;
                for (Runnable r : onLoadComplete) r.run();
                onLoadComplete.clear();
            }
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Fills the voids in this tile's data with the last good value available.
     */
    private void fillVoids() {
        short lastGoodElev = 0;
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data.length; j++) {
                if (data[i][j] < -100) data[i][j] = lastGoodElev;
                else lastGoodElev = data[i][j];
            }
        }
    }

    /**
     * Check whether this tile fulfills the criteria to be a null-tile. If so, propagate the check up to the parent.
     */
    private void checkNullTile() {
        if (data == null) { //no data in this tile: try to check for null-tile and propagate it up
            if ((nw == null || nw.isNullTile) && (ne == null || ne.isNullTile) &&
                    (sw == null || sw.isNullTile) && (se == null || se.isNullTile)) { //all children null or null-tiles: make this null-tile
                isNullTile = true;
                resolution = 0;
                nw = null;
                ne = null;
                sw = null;
                se = null;
                if (parent != null) parent.checkNullTile();
            }
        } else if (nw != null && nw.isNullTile) nw = null; //otherwise simply remove all null-subtiles
        else if (ne != null && ne.isNullTile) ne = null;
        else if (sw != null && sw.isNullTile) sw = null;
        else if (se != null && se.isNullTile) se = null;
    }

    /**
     * Attempt to combine the data arrays of this tile's children into a larger array for this tile. Only works if all four children have data at equal resolution. If successful, propagates up to the parent.
     */
    private void tryCombine() {
        if (nw == null || ne == null || sw == null || se == null) return; //only works if all four children are present
        if (nw.data == null) { //child data null: try to combine into child, let it propagate back up if successful
            nw.tryCombine();
            return;
        }
        if (ne.data == null) {
            ne.tryCombine();
            return;
        }
        if (sw.data == null) {
            sw.tryCombine();
            return;
        }
        if (se.data == null) {
            se.tryCombine();
            return;
        }
        if (resolution >= nw.resolution || ne.resolution != nw.resolution || sw.resolution != nw.resolution || se.resolution != nw.resolution)
            return; //only works if all four tiles have equal resolution that is finer than this tile's resolution
        resolution = nw.resolution; //set resolution
        updateEdgeWidth();
        int length = getTotalEdgeLength();
        int halfLength = length / 2;
        int halfLengthWithoutEdge = getEdgeLength() / 2;
        data = new short[length][length]; //create new larger data array and copy data from children
        for (int i = 0; i < nw.data.length - edgeWidth; i++) System.arraycopy(nw.data[i], 0, data[i], 0, nw.data.length - edgeWidth);
        for (int i = edgeWidth + 1; i < ne.data.length; i++) System.arraycopy(ne.data[i], 0, data[i + halfLengthWithoutEdge], 0, ne.data.length - edgeWidth);
        for (int i = 0; i < sw.data.length - edgeWidth; i++) System.arraycopy(sw.data[i], edgeWidth + 1, data[i], halfLength + 1, sw.data.length - edgeWidth - 1);
        for (int i = edgeWidth + 1; i < se.data.length; i++) System.arraycopy(se.data[i], edgeWidth + 1, data[i + halfLengthWithoutEdge], halfLength + 1, se.data.length - edgeWidth - 1);
        init(data, 0, 0, resolution, false, true); //re-init to update children to reference the new data array instead of their own arrays
        if (parent != null) parent.tryCombine();
    }

    /**
     * Check whether this tile contains loaded data for the given OSM tile coordinates in the given resolution.
     * @param zoom OSM zoom level
     * @param subX X sub-coordinate of requested tile within this tile
     * @param subY Y sub-coordinate of requested tile within this tile
     * @param resolution Required resolution: sample distance on OSM zoom scale
     * @param lockHandled Whether the read lock has already been handled for this thread
     * @return Whether the requested sub-tile has loaded data at the given resolution
     */
    public boolean hasLoadedChild(int zoom, int subX, int subY, int resolution, boolean lockHandled) {
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_READ_LOCK.lock();
            if (zoom < zoomLevel || resolution < zoom || resolution > ElevationProvider.MAX_RES || isNullTile) return false; //never true for tiles bigger than this one, null-tiles, and illegal arguments
            if (data != null && resolution <= this.resolution) return true; //if this tile has data and sufficient resolution: true
            if (zoom == zoomLevel || zoomLevel == ElevationProvider.MAX_ZOOM) return false; //can't be in a child either if we've reached the desired or maximum zoom level
            int mask = 1 << (zoom - zoomLevel - 1); //mask for extracting information about which of this tile's children contains requested sub-tile
            int newSubX = subX % mask; //new sub-tile coordinates within child
            int newSubY = subY % mask;
            if ((subY & mask) > 0) { //check corresponding child
                if ((subX & mask) > 0) return se != null && se.hasLoadedChild(zoom, newSubX, newSubY, resolution, true);
                else return sw != null && sw.hasLoadedChild(zoom, newSubX, newSubY, resolution, true);
            } else {
                if ((subX & mask) > 0) return ne != null && ne.hasLoadedChild(zoom, newSubX, newSubY, resolution, true);
                else return nw != null && nw.hasLoadedChild(zoom, newSubX, newSubY, resolution, true);
            }
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_READ_LOCK.unlock();
        }
    }

    /**
     * Get data for a specific sub-tile of this tile.
     * @param zoom OSM zoom level
     * @param subX X sub-coordinate of requested tile within this tile
     * @param subY Y sub-coordinate of requested tile within this tile
     * @param resolution Requested resolution: sample distance on OSM zoom scale
     * @param lockHandled Whether the read lock has already been handled for this thread
     * @return Elevation data for requested tile and resolution, or a null-tile with no data if the elevation data is not available
     */
    public ElevationTile getChild(int zoom, int subX, int subY, int resolution, boolean lockHandled) {
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_READ_LOCK.lock();
            int targetX = (tileX << (zoom - zoomLevel)) | subX; //target tile coordinates, used for null- and pseudo-tiles
            int targetY = (tileY << (zoom - zoomLevel)) | subY;
            if (zoom < zoomLevel || resolution < zoom || resolution > ElevationProvider.MAX_RES)
                return new ElevationTreeTile(null, zoom, targetX, targetY, true); //null-tile for requested tiles bigger than this one, and illegal arguments
            if (isNullTile) return this; //if this is a null-tile, the zoom level doesn't matter - just return this
            if (zoom == zoomLevel) { //if this tile has the correct zoom level:
                if (data == null || resolution > this.resolution)
                    return new ElevationTreeTile(null, zoom, targetX, targetY, true); //if no data or insufficient resolution: return null tile
                else if (resolution == this.resolution) return this; //if matching resolution: return this
                return new ElevationSegmentTile(zoomLevel, tileX, tileY, resolution, this, 0, 0); //if resolution too high: return pseudo-tile that accesses this tile's data at a lower resolution
            }
            if (zoomLevel < ElevationProvider.MAX_ZOOM) { //if we need a sub-tile and we haven't reached the end of the tree: descend tree
                int mask = 1 << (zoom - zoomLevel - 1); //mask for extracting information about which of this tile's children contains requested sub-tile
                int newSubX = subX % mask; //new sub-tile coordinates within child
                int newSubY = subY % mask;
                if ((subY & mask) > 0) { //check corresponding child, returning a null-tile if the child doesn't exist
                    if ((subX & mask) > 0) {
                        if (se == null) return new ElevationTreeTile(null, zoom, targetX, targetY, true);
                        else return se.getChild(zoom, newSubX, newSubY, resolution, true);
                    } else {
                        if (sw == null) return new ElevationTreeTile(null, zoom, targetX, targetY, true);
                        else return sw.getChild(zoom, newSubX, newSubY, resolution, true);
                    }
                } else {
                    if ((subX & mask) > 0) {
                        if (ne == null) return new ElevationTreeTile(null, zoom, targetX, targetY, true);
                        else return ne.getChild(zoom, newSubX, newSubY, resolution, true);
                    } else {
                        if (nw == null) return new ElevationTreeTile(null, zoom, targetX, targetY, true);
                        else return nw.getChild(zoom, newSubX, newSubY, resolution, true);
                    }
                }
            } else { //if we need a sub-tile and are already at MAX_ZOOM: generate pseudo-tile that accesses part of the elevation data
                if (data == null) return new ElevationTreeTile(null, zoom, targetX, targetY, true); //if this tile has no data: return null-tile
                int mask = 1 << (zoom - zoomLevel); //mask for extracting sub-tile information
                int newSubX = subX % mask; //new sub-tile coordinates
                int newSubY = subY % mask;
                int segWidth = 1 << (this.resolution - zoom); //width of requested tile in samples, in this tile's resolution
                return new ElevationSegmentTile(zoom, targetX, targetY, resolution, this, newSubX * segWidth, newSubY * segWidth);
            }
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_READ_LOCK.unlock();
        }
    }

    /**
     * Marks a specific sub-tile of this tile as requesting data at the given resolution.
     * @param zoom OSM zoom level
     * @param subX X sub-coordinate of requested tile within this tile
     * @param subY Y sub-coordinate of requested tile within this tile
     * @param resolution Requested resolution: sample distance on OSM zoom scale
     * @param onComplete Callback to be called once the tile is successfully loaded. Will not be called if the request fails.
     * @return The tile that is now requesting data, or null if no new data is requested
     */
    public ElevationTreeTile markChildForLoading(int zoom, int subX, int subY, int resolution, Runnable onComplete) {
        boolean lockHandled = ElevationProvider.ELEVATION_WRITE_LOCK.isHeldByCurrentThread();
        try {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.lock();
            if (zoom < zoomLevel || resolution < zoom || resolution > ElevationProvider.MAX_RES || isNullTile) return null; //ignore invalid arguments and attempts to load data into a null-tile
            if (requestedResolution >= resolution) { //don't load data we're already loading
                if (onComplete != null) onLoadComplete.add(onComplete);
                return null;
            }
            if (hasLoadedChild(zoom, subX, subY, resolution, true)) { //don't load data we already have
                if (onComplete != null) onComplete.run();
                return null;
            }
            if (zoom == zoomLevel) { //this tile needs to be loaded
                if (resolution <= loadingResolution) return null; //if this tile already has an open load request for the same resolution: cancel
                requestedResolution = resolution;
                if (onComplete != null) onLoadComplete.add(onComplete);
                return this;
            }
            int mask = 1 << (zoom - zoomLevel - 1); //mask for extracting information about which of this tile's children contains requested sub-tile
            int newSubX = subX % mask; //new sub-tile coordinates within child
            int newSubY = subY % mask;
            int childX = tileX << 1; //child base coordinates, used if new child tiles need to be created
            int childY = tileY << 1;
            ElevationTreeTile result;
            if ((subY & mask) > 0) { //attempt to load corresponding sub-tile, creating new sub-tiles where required
                if ((subX & mask) > 0) {
                    if (se == null) se = new ElevationTreeTile(this, zoomLevel + 1, childX + 1, childY + 1, false);
                    result = se.markChildForLoading(zoom, newSubX, newSubY, resolution, onComplete);
                } else {
                    if (sw == null) sw = new ElevationTreeTile(this, zoomLevel + 1, childX, childY + 1, false);
                    result = sw.markChildForLoading(zoom, newSubX, newSubY, resolution, onComplete);
                }
            } else {
                if ((subX & mask) > 0) {
                    if (ne == null)  ne = new ElevationTreeTile(this, zoomLevel + 1, childX + 1, childY, false);
                    result = ne.markChildForLoading(zoom, newSubX, newSubY, resolution, onComplete);
                } else {
                    if (nw == null) nw = new ElevationTreeTile(this, zoomLevel + 1, childX, childY, false);
                    result = nw.markChildForLoading(zoom, newSubX, newSubY, resolution, onComplete);
                }
            }
            if (nw != null && ne != null && sw != null && se != null && nw.requestedResolution > requestedResolution &&
                    nw.requestedResolution == ne.requestedResolution && nw.requestedResolution == sw.requestedResolution &&
                    nw.requestedResolution == se.requestedResolution) { //try to combine sub-requests into larger request
                requestedResolution = se.requestedResolution;
                nw.requestedResolution = ne.requestedResolution = sw.requestedResolution = se.requestedResolution = 0;
                nw.loadingResolution = ne.loadingResolution = sw.loadingResolution = se.loadingResolution = 0;
                onLoadComplete.addAll(nw.onLoadComplete);
                onLoadComplete.addAll(ne.onLoadComplete);
                onLoadComplete.addAll(sw.onLoadComplete);
                onLoadComplete.addAll(se.onLoadComplete);
                nw.onLoadComplete.clear();
                ne.onLoadComplete.clear();
                sw.onLoadComplete.clear();
                se.onLoadComplete.clear();
                result = this;
            }
            return result;
        } finally {
            if (!lockHandled) ElevationProvider.ELEVATION_WRITE_LOCK.unlock();
        }
    }

    /**
     * Search for the nearest sub-tile of this tile to the given fractional position that has an open data load request.
     * @param fractX Fractional X position within this tile (0-1)
     * @param fractY Fractional Y position within this tile (0-1)
     * @return Nearest sub-tile to the given position with an open load request, or null if there is no such tile
     */
    /*package-private*/ ElevationTreeTile getNearestRequested(double fractX, double fractY) {
        if (fractX < 0d || fractX > 1d || fractY < 0d || fractY > 1d) return null;
        ElevationTreeTile[] subTiles = {nw, ne, sw, se};
        int orderIndex = (fractY > .5 ? 4 : 0) | (fractX > .5 ? 2 : 0) | (fractX > (fractY > .5 ^ fractX > .5 ? 1 - fractY : fractY) ? 1 : 0);
        int[] subTileOrder = tileOrders[orderIndex]; //order that the sub-tiles are searched in, based on subtile containing the point and its closest orthogonal neighbour
        ElevationTreeTile result = null;
        for (int i = 0; i < 4; i++) { //check all sub-tiles
            ElevationTreeTile subTile = subTiles[subTileOrder[i]];
            if (subTile != null) result = subTile.getNearestRequested(fractX * 2d % 1d, fractY * 2d % 1d); //if sub-tile exists: check sub-tile first
            if (result != null) break;
        }
        if (result == null) { //if sub-tiles contain no open request: check this tile for open request
            if (loadingResolution == 0 && requestedResolution > 0) result = this;
        }
        return result;
    }

    /**
     * Force the loading resolution of this tile and its sub-tiles to the given value, unless this tile is already loading a greater resolution.
     * @param res Resolution
     */
    /*package-private*/ void forceLoadingResolution(int res) {
        if (loadingResolution > res) return;
        if (requestedResolution < res) requestedResolution = res;
        loadingResolution = res;
        if (nw != null) nw.forceLoadingResolution(res);
        if (ne != null) ne.forceLoadingResolution(res);
        if (sw != null) sw.forceLoadingResolution(res);
        if (se != null) se.forceLoadingResolution(res);
    }

    /**
     * Reset the loading status for this tile and its sub-tiles up to a given resolution value.
     * @param res Maximum resolution
     */
    private void resetLoadingResolution(int res) {
        if (loadingResolution > res) return;
        if (requestedResolution <= loadingResolution) requestedResolution = 0;
        loadingResolution = 0;
        if (nw != null) nw.resetLoadingResolution(res);
        if (ne != null) ne.resetLoadingResolution(res);
        if (sw != null) sw.resetLoadingResolution(res);
        if (se != null) se.resetLoadingResolution(res);
    }

    /**
     * Signal to this tile that its loading request has started processing.
     */
    /*package-private*/ void signalRequestStart() {
        if (loadingResolution > 0) return;
        loadingResolution = requestedResolution;
    }

    /**
     * Save this tile to the elevation cache, or the parent tile that this tile inherits its data from.
     */
    private void saveToCache() {
        if (isNullTile || resolution == 0 || data == null || ElevationTileLoader.isCached(zoomLevel, tileX, tileY, resolution)) return; //don't save if no data or this data is already cached
        if (data.length != getTotalEdgeLength()) { //if this tile has inherited data: save parent
            if (parent != null) parent.saveToCache();
            return;
        }
        FileHandle cacheFile = Gdx.files.local(ElevationTileLoader.getCachePath(this)); //get cache file
        OutputStream str = cacheFile.write(false); //overwrite file
        try {
            str.write(resolution); //write resolution
            ElevationDataCompression.compressHeightData(data, str); //write compressed tile data
            str.close();
            removeCachedChildren(zoomLevel, tileX, tileY); //remove cached children that are no longer required
        } catch (IOException e) {
            cacheFile.delete();
        }
    }

    /**
     * Recursively remove cached child tiles with worse or equal resolution.
     * @param zoom OSM zoom level
     * @param x OSM tile X position
     * @param y OSM tile Y position
     */
    private void removeCachedChildren(int zoom, int x, int y) {
        if (zoom >= ElevationProvider.MAX_ZOOM) return; //exit condition: max loadable zoom level reached
        int childZoom = zoom + 1;
        int childBaseX = x << 1;
        int childBaseY = y << 1;
        for (int i = 0; i <= 1; i++) {
            for (int j = 0; j <= 1; j++) { //iterate through immediate child positions
                int childX = childBaseX + i;
                int childY = childBaseY + j;
                FileHandle childFile = Gdx.files.local(ElevationTileLoader.getCachePath(childZoom, childX, childY));
                if (childFile.exists()) { //file exists: check if needed
                    InputStream str = childFile.read();
                    try {
                        int childRes = str.read();
                        str.close();
                        if (childRes <= resolution) childFile.delete(); //not needed: delete
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                removeCachedChildren(childZoom, childX, childY); //remove children recursively
            }
        }
    }

    /**
     * @return Parent tile, or null if there is none.
     */
    /*package-private*/ ElevationTreeTile getParent() {
        return parent;
    }

    private void updateEdgeWidth() {
        edgeWidth = 1 << (resolution - ElevationProvider.MIN_RES);
    }

    private int getTotalEdgeLength() {
        return getEdgeLength() + 2 * edgeWidth;
    }

    @Override
    public boolean isNullTile() {
        return isNullTile;
    }

    @Override
    public int getZoomLevel() {
        return zoomLevel;
    }

    @Override
    public int getTileX() {
        return tileX;
    }

    @Override
    public int getTileY() {
        return tileY;
    }

    @Override
    public int getResolution() {
        return resolution;
    }

    /**
     * @return Resolution that this tile wants to load
     */
    public int getRequestedResolution() {
        return requestedResolution;
    }

    /**
     * @return Resolution that this tile is currently loading
     */
    public int getLoadingResolution() {
        return loadingResolution;
    }

    @Override
    public int getElevation(int x, int y) {
        if (isNullTile || resolution == 0 || data == null) return 0;
        int xI = x + xOffset + edgeWidth;
        int yI = y + yOffset + edgeWidth;
        if (xI < 0) xI = 0; //clamp indices at actual data array bounds
        if (xI >= data.length) xI = data.length - 1;
        if (yI < 0) yI = 0;
        if (yI >= data.length) yI = data.length - 1;
        return data[xI][yI];
    }
}
