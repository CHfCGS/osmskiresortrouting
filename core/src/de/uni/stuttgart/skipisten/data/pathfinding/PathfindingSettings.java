package de.uni.stuttgart.skipisten.data.pathfinding;

import com.badlogic.gdx.Gdx;

import java.util.BitSet;
import com.badlogic.gdx.Preferences;

public class PathfindingSettings {
    private static BitSet difficultiesAllowed;
    private static BitSet aerialwayTypesAllowed;

    public static void loadData() {
        if (difficultiesAllowed == null || aerialwayTypesAllowed == null) {
            Preferences prefs = Gdx.app.getPreferences("PathfindingSettings");

            if (prefs.contains("difficultiesAllowed")) {
                long[] array = new long[1];
                array[0] = prefs.getLong("difficultiesAllowed");
                difficultiesAllowed = BitSet.valueOf(array);
            } else {
                difficultiesAllowed = new BitSet(7);
                difficultiesAllowed.flip(0, 7);
            }

            if (prefs.contains("aerialwayTypesAllowed")) {
                long[] array = new long[1];
                array[0] = prefs.getLong("aerialwayTypesAllowed");
                aerialwayTypesAllowed = BitSet.valueOf(array);
            } else {
                aerialwayTypesAllowed = new BitSet(7);
                aerialwayTypesAllowed.flip(0, 7);
            }
        }
    }

    public static void setDifficultyAllowed(int difficultyIndex, boolean isAllowed) {
        loadData();
        difficultiesAllowed.set(difficultyIndex, isAllowed);

        Preferences prefs = Gdx.app.getPreferences("PathfindingSettings");
        if(difficultiesAllowed.toLongArray().length == 0) {
            prefs.putLong("difficultiesAllowed", 0);
        } else {
            prefs.putLong("difficultiesAllowed", difficultiesAllowed.toLongArray()[0]);
        }
        prefs.flush();
    }

    public static void setAerialwayTypeAllowed(int aerialwayTypeIndex, boolean isAllowed) {
        loadData();
        aerialwayTypesAllowed.set(aerialwayTypeIndex, isAllowed);
        Preferences prefs = Gdx.app.getPreferences("PathfindingSettings");
        if(aerialwayTypesAllowed.toLongArray().length == 0) {
            prefs.putLong("aerialwayTypesAllowed", 0);
        } else {
            prefs.putLong("aerialwayTypesAllowed", aerialwayTypesAllowed.toLongArray()[0]);
        }
        prefs.flush();
    }

    public static BitSet getDifficultiesAllowed() {
        loadData();
        return difficultiesAllowed;
    }

    public static boolean getDifficultiesAllowed(int difficultyIndex) {
        loadData();
        return difficultiesAllowed.get(difficultyIndex);
    }

    public static BitSet getAerialwayTypesAllowed() {
        loadData();
        return aerialwayTypesAllowed;
    }

    public static boolean getAerialwayTypesAllowed(int aerialwayTypeIndex) {
        loadData();
        return aerialwayTypesAllowed.get(aerialwayTypeIndex);
    }
}
