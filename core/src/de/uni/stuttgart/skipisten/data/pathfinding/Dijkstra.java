package de.uni.stuttgart.skipisten.data.pathfinding;

import java.util.BitSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.PriorityQueue;

import de.uni.stuttgart.skipisten.data.graph.Edge;
import de.uni.stuttgart.skipisten.data.graph.GraphUtils;
import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class Dijkstra {
    private final OSMGraph graph;
    private final long startNodeID;
    private final BitSet difficulties;
    private final BitSet aerialwayTypes;

    private HashMap<OSMElement, Double> costMap;
    private HashMap<OSMElement, Double> timeMap;
    private HashMap<OSMElement, Double> distanceMap;

    private PriorityQueue<OSMElement> priorityQueue;
    private HashMap<OSMElement, OSMElement> parentMap;

    public Dijkstra(OSMGraph graph, long startNodeID) {
        this(graph, startNodeID, PathfindingSettings.getDifficultiesAllowed(), PathfindingSettings.getAerialwayTypesAllowed());
    }

    public Dijkstra(OSMGraph graph, long startNodeID, BitSet difficulties, BitSet aerialwayTypes) {
        this.graph = graph;
        this.startNodeID = startNodeID;
        this.difficulties = difficulties;
        this.aerialwayTypes = aerialwayTypes;

        costMap = new HashMap<>();
        timeMap = new HashMap<>();
        distanceMap = new HashMap<>();

        priorityQueue = new PriorityQueue<>(500, (e1, e2) -> Double.compare(costMap.get(e1), costMap.get(e2)));
        parentMap = new HashMap<>();
        calculateDijkstra();
    }

    /**
     * Calculates distance to all reachable nodes
     */
    private void calculateDijkstra() {
        for (Long nodeID : graph.getNodeIDs()) {
            if (nodeID == startNodeID) {
                OSMElement startNode = graph.getNode(startNodeID);

                costMap.put(startNode, (double) 0);
                timeMap.put(startNode, (double) 0);
                distanceMap.put(startNode, (double) 0);

                parentMap.put(startNode, startNode);
            } else {
                costMap.put(graph.getNode(nodeID), Double.POSITIVE_INFINITY);
                timeMap.put(graph.getNode(nodeID), Double.POSITIVE_INFINITY);
                distanceMap.put(graph.getNode(nodeID), Double.POSITIVE_INFINITY);
            }
            priorityQueue.add(graph.getNode(nodeID));
        }

        while (!priorityQueue.isEmpty()) {
            OSMElement nextNode = priorityQueue.poll();
            for (Edge edge : graph.getEdges(nextNode.id)) {
                if (checkIfEdgeCanBeUsed(edge)) {
                    double cost = edge.getEstimatedTraversalTime(false);
                    if (costMap.get(graph.getNode(nextNode.id)) + cost < costMap.get(edge.dest)) {

                        double costTime = edge.getEstimatedTraversalTime(true);
                        double costDist = edge.getLengthInMeters();

                        costMap.put(edge.dest, costMap.get(graph.getNode(nextNode.id)) + cost);
                        timeMap.put(edge.dest, timeMap.get(graph.getNode(nextNode.id)) + costTime);
                        distanceMap.put(edge.dest, distanceMap.get(graph.getNode(nextNode.id)) + costDist);

                        parentMap.put(edge.dest, nextNode);
                        priorityQueue.remove(edge.dest);
                        priorityQueue.add(edge.dest);
                    }
                }
            }
        }
    }

    private boolean checkIfEdgeCanBeUsed(Edge edge) {
        if (edge.way.isPiste) {
            return difficulties.get(edge.way.difficulty);
        }

        if (edge.way.isAerialway()) {
            if(edge.way.aerialwayType == OSMElement.Aerialway.MIXED_LIFT) {
                return aerialwayTypes.get(OSMElement.Aerialway.GONDOLA) || aerialwayTypes.get(OSMElement.Aerialway.CHAIR_LIFT);
            }

            return aerialwayTypes.get(edge.way.aerialwayType);
        }

        return true;
    }

    /**
     * Returns shortest path from startNode to endNode
     *
     * @param endNodeID NodeID of destination
     * @return path sequence of nodeIDs or null when no path exists
     */
    public LinkedList<OSMElement> getShortestPath(long endNodeID) {
        LinkedList<OSMElement> pathNodes = new LinkedList<>();

        OSMElement currentNode = graph.getNode(endNodeID);
        while (currentNode.id != startNodeID) {
            pathNodes.addFirst(currentNode);
            currentNode = parentMap.get(currentNode);
            if (currentNode == null) return null;
        }
        pathNodes.addFirst(currentNode);
        return pathNodes;
    }

    /**
     * @return the time in seconds
     */
    public double getTimeTo(OSMElement endNode) {
        return timeMap.get(endNode);
    }

    /**
     * @return the distance in meters
     */
    public double getDistanceTo(OSMElement endNode) {
        return distanceMap.get(endNode);
    }
}
