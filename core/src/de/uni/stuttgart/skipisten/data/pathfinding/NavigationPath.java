package de.uni.stuttgart.skipisten.data.pathfinding;

import com.badlogic.gdx.graphics.Color;
import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;
import de.uni.stuttgart.skipisten.gui.osm.util.TileMath;
import de.uni.stuttgart.skipisten.gui.osm3d.tiles.TileManager3D;

import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;

import de.uni.stuttgart.skipisten.data.graph.GraphManager;
import de.uni.stuttgart.skipisten.data.graph.OSMGraph;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;
import de.uni.stuttgart.skipisten.data.search.SearchResult;
import de.uni.stuttgart.skipisten.gui.draw.Polyline;

/**
 * Represents a navigation path through multiple waypoints.
 */
public class NavigationPath {

    public static NavigationPath currentPath;

    public static void init(OSMGraph graph) {
        currentPath = new NavigationPath(graph);
    }

    public List<OSMElement> pathNodes;
    public Polyline line;
    public List<Waypoint> waypoints;
    private OSMGraph graph;
    private BitSet difficulties;
    private BitSet aerialwayTypes;

    /**
     * Creates a new navigation path.
     *
     * @param graph         OSM Graph to navigate through
     * @param waypointNodes OSM nodes which the path must visit (in the given order)
     */
    public NavigationPath(OSMGraph graph, OSMElement... waypointNodes) {
        this(graph, PathfindingSettings.getDifficultiesAllowed(), PathfindingSettings.getAerialwayTypesAllowed(), waypointNodes);
    }

    /**
     * Creates a new navigation path.
     *
     * @param graph          OSM Graph to navigate through
     * @param difficulties   Allowed piste difficulties
     * @param aerialwayTypes Allowed aerialway types
     * @param waypointNodes  OSM nodes which the path must visit (in the given order)
     */
    public NavigationPath(OSMGraph graph, BitSet difficulties, BitSet aerialwayTypes, OSMElement... waypointNodes) {
        //if (waypointNodes.length == 0) throw new IllegalArgumentException("At least one waypoint must be provided");
        this.graph = graph;
        this.difficulties = difficulties;
        this.aerialwayTypes = aerialwayTypes;
        this.waypoints = new LinkedList<>();
        for (OSMElement node : waypointNodes) {
            this.waypoints.add(new Waypoint(node));
        }
        calculatePath();
    }

    /**
     * @return Whether there exists a valid path through the waypoints
     */
    public boolean pathExists() {
        return pathNodes != null;
    }

    /**
     * (Re)calculates the exact path through the waypoints
     */
    public void calculatePath() {
        pathNodes = null; //erase existing path and line
        line = null;

        if (waypoints.size() < 2) return; //no path possible unless there are at least 2 waypoints

        LinkedList<OSMElement> newList = new LinkedList<>();
        Waypoint prevPoint = waypoints.get(0);
        newList.add(prevPoint.node); //add start node to new list
        prevPoint.pathIndex = 0;
        for (int i = 1; i < waypoints.size(); i++) {
            Waypoint point = waypoints.get(i);
            point.pathIndex = -1;
            LinkedList<OSMElement> segment = prevPoint.dijkstra.getShortestPath(point.node.id);
            if (segment == null)
                return; //no path found from previous to current waypoint: there exists no path
            point.pathIndex = newList.size() - 1;
            newList.addAll(segment.subList(1, segment.size())); //add all but first node of path segment to new list
            prevPoint = point;
        }
        pathNodes = newList; //loop done: path found, save list

        createPolyline();
        TileManager3D.setFramebufferUpdateRequired();
    }

    /**
     * Create the polyline for this path based on the exact nodes
     */
    private void createPolyline() {
        if (pathNodes.size() < 2) {
            line = null;
            return;
        }
        double[] x = new double[pathNodes.size()];
        double[] y = new double[pathNodes.size()];
        for (int i = 0; i < pathNodes.size(); i++) {
            OSMElement node = pathNodes.get(i);
            x[i] = TileMath.longitudeToMapX(node.lon);
            y[i] = TileMath.latitudeToMapY(node.lat);
        }
        line = new Polyline(x, y, OSMElement.PISTE_THICKNESS * .5, 5, Color.BLACK.cpy().mul(1f, 1f, 1f, .7f), -50);
    }

    /**
     * Adds a waypoint with the given node and recalculates the path.
     *
     * @param index   0-based index where the new waypoint should be inserted.
     * @param newNode New OSM node that this waypoint should have
     */
    public void addWaypoint(int index, OSMElement newNode) {
        Waypoint newPoint = new Waypoint(newNode);
        waypoints.add(index, newPoint);
        calculatePath();
    }

    /**
     * Removes a waypoint.
     *
     * @param index 0-based index of the waypoint that should be removed.
     */
    public void removeWaypoint(int index) {
        waypoints.remove(index);
        calculatePath();
    }

    /**
     * Changes a waypoint to a different node and recalculates the corresponding path segments.
     *
     * @param index   0-based index of the waypoint to change
     * @param newNode New OSM node that this waypoint should have
     */
    public void changeWaypoint(int index, OSMElement newNode) {
        Waypoint point = waypoints.get(index);
        point.changeNode(newNode);

        calculatePath();
    }

    /**
     * Call method to calculate dijkstra one more time if settings have changed.
     */
    public void settingsChanged() {
        for (Waypoint point : waypoints) {
            point.changeNode(point.getNode());
        }
    }

    public void reset() {
        waypoints.clear();
        if (pathNodes != null) pathNodes.clear();
        pathNodes = null;
        line = null;
        TileManager3D.setFramebufferUpdateRequired();
    }

    public double getTimeFromTo(int index1, int index2) {
        double totalTime = 0;

        for (int i = index1; i < index2; i++) {
            totalTime += waypoints.get(i).dijkstra.getTimeTo(waypoints.get(i + 1).node);
        }

        return totalTime;
    }

    public double getDistanceFromTo(int index1, int index2) {
        double totalDistance = 0;

        for (int i = index1; i < index2; i++) {
            totalDistance += waypoints.get(i).dijkstra.getDistanceTo(waypoints.get(i + 1).node);
        }

        return totalDistance;
    }

    public String getFormattedTimeFromTo(int index1, int index2) {
        int timeInSecondsInt = (int) getTimeFromTo(index1, index2);

        int h = timeInSecondsInt / 3600;
        int m = (timeInSecondsInt % 3600) / 60;
        int s = timeInSecondsInt % 60;

        if (h > 0 && m == 0) {
            return h + "h";
        } else if (h > 0) {
            return h + "h " + m + "min";
        } else if (m > 0 && s == 0) {
            return m + "min";
        } else if (m > 0) {
            return m + "min " + s + "s";
        } else {
            return s + "s";
        }
    }

    public String getFormattedDistanceFromTo(int index1, int index2) {
        double distanceInMeters = getDistanceFromTo(index1, index2);
        double distanceInKm = distanceInMeters / 1000;

        if (distanceInMeters < 1000) {
            return ((int) distanceInMeters) + " m";
        }

        return (Math.round(distanceInKm * 100) / 100.0) + " km";
    }

    public MapBoundingBox getBoundingBox(int index1, int index2) {
        MapBoundingBox bb = new MapBoundingBox();

        for (int i = index1; i < index2; i++) {
            LinkedList<OSMElement> path = waypoints.get(i).dijkstra.getShortestPath(waypoints.get(i + 1).node.id);
            if (path == null) return null;
            for (OSMElement element : path) {
                bb.addPoint(TileMath.longitudeToMapX(element.lon), TileMath.latitudeToMapY(element.lat));
            }
        }

        return bb;
    }

    public class Waypoint {
        private OSMElement node;
        private Dijkstra dijkstra;
        private int pathIndex = -1; //index of this waypoint's node in the total path

        public Waypoint(OSMElement node) {
            this.node = node;
            dijkstra = new Dijkstra(graph, node.id, difficulties, aerialwayTypes);
        }

        private void changeNode(OSMElement node) {
            this.node = node;
            dijkstra = new Dijkstra(graph, node.id, difficulties, aerialwayTypes);
        }

        public OSMElement getNode() {
            return node;
        }

        public String getName() {
            String name = SearchResult.generatedDisplayedName(GraphManager.getGraph().getWay(node.parentWayId));
            if (name.trim().length() == 0)
                return Math.round(node.lat * 1000) / 1000.0 + " - " + Math.round(node.lon * 1000) / 1000.0;
            return name;
        }
    }

}
