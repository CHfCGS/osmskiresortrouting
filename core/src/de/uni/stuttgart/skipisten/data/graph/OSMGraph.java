package de.uni.stuttgart.skipisten.data.graph;

import de.uni.stuttgart.skipisten.gui.osm.util.MapBoundingBox;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import de.uni.stuttgart.skipisten.data.elevation.ElevationProvider;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;
import de.uni.stuttgart.skipisten.data.overpass.OverpassResult;

public class OSMGraph {

    /**
     * A HashMap of all ways.
     * The element id is used as key.
     */
    private HashMap<Long, OSMElement> ways;

    /**
     * A HashMap of all nodes.
     * The element id is used as key.
     */
    private HashMap<Long, OSMElement> nodes;

    /**
     * A HashMap of all edges for all nodes.
     * The element id is used as key.
     */
    private HashMap<Long, Set<Edge>> edges;

    private MapBoundingBox boundingBox;

    private OSMGraph() {
        ways = new HashMap<>();
        nodes = new HashMap<>();
        edges = new HashMap<>();
        boundingBox = new MapBoundingBox();
    }

    /**
     * @return a set of all node ids.
     */
    public Set<Long> getNodeIDs() {
        return nodes.keySet();
    }

    public OSMElement getNode(long nodeId) {
        return nodes.get(nodeId);
    }

    public Collection<OSMElement> getNodes() {
        return nodes.values();
    }

    /**
     * @return a set of all way ids.
     */
    public Set<Long> getWayIDs() {
        return ways.keySet();
    }

    public OSMElement getWay(long wayId) {
        return ways.get(wayId);
    }

    public Collection<OSMElement> getWays() {
        return ways.values();
    }

    public Set<Edge> getEdges(long nodeId) {
        if (!edges.containsKey(nodeId)) {
            return new HashSet<>();
        }

        return edges.get(nodeId);
    }

    /**
     * Creates an OSMGraph object from an OverpassResult object
     *
     * @param result The OverpassResult used to create the graph
     * @param subdivideWays Subdivide ways, if a distance between to nodes is too large
     * @return an OSMGraph object
     */
    public static OSMGraph fromOverpassResult(OverpassResult result, boolean subdivideWays) {
        OSMGraph osmGraph = new OSMGraph();

        for (OSMElement element : result.elements) {
            element.initializeStage1();
        }

        // First add all nodes
        for (OSMElement element : result.elements) {
            if (element.typeInt == OSMElement.Type.NODE) {
                osmGraph.addNode(element);
            }
        }

        // Then add all ways
        for (OSMElement element : result.elements) {
            if (element.typeInt == OSMElement.Type.WAY) {
                element.initializePolyline(osmGraph);
                if (element.outerLine != null)
                    osmGraph.boundingBox.addBox(element.outerLine.getBoundingBox());

                osmGraph.addWay(element, subdivideWays);
            }
        }

        osmGraph.removeUnusedNodes();

        return osmGraph;
    }

    public void loadAndAddElevationData(Runnable onElevationLoaded) {
        ElevationLoadingParameters params = onElevationLoaded == null ? null : new ElevationLoadingParameters(onElevationLoaded);
        ElevationProvider.addElevationsToNodes(getNodes(), params);
    }

    /**
     * This method removes nodes, which are not included in a way
     */
    private void removeUnusedNodes() {
        for (OSMElement way : getWays()) {
            for (long nodeId : way.nodes) {
                getNode(nodeId).parentWayId = way.id;
            }
        }

        Iterator<OSMElement> iterator = getNodes().iterator();

        while (iterator.hasNext()) {
            OSMElement element = iterator.next();

            if (element.parentWayId == -1) {
                iterator.remove();
            }
        }
    }

    public MapBoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void addNode(OSMElement element) {
        nodes.put(element.id, element);
    }

    public void addWay(OSMElement element, boolean subdivideWays) {
        // Skip unwanted area ways
        if (element.nodes[0] == element.nodes[element.nodes.length - 1]
                && (element.tags.containsKey("piste:type") || element.tags.containsKey("aerialway"))) {
            return;
        }

        // Subdivide way, if a distance between to nodes is too large
        if (subdivideWays) {
            GraphUtils.subdivideWay(element, this);
        }

        ways.put(element.id, element);

        if (!element.isGenerated) {
            element.isOneWay = false;

            if (element.isPiste) {
                element.isOneWay = true;
            } else if (element.getTag("oneway").equals("no")) {
                element.isOneWay = false;
            } else if (element.getTag("oneway").equals("yes")) {
                element.isOneWay = true;
            } else if (GraphUtils.isOneWayAerialway(element.aerialwayType)) {
                element.isOneWay = true;
            }
        }

        for (int i = 0; i < element.nodes.length - 1; i++) {
            addEdge(element.nodes[i], element.nodes[i + 1], element, false);

            if (!element.isOneWay) {
                addEdge(element.nodes[i + 1], element.nodes[i], element, false);
            } else if (element.isPiste) {
                addEdge(element.nodes[i + 1], element.nodes[i], element, true);
            }
        }
    }

    private void addEdge(long fromNodeId, long toNodeId, OSMElement viaWay, boolean isUphill) {
        Set<Edge> neighbors;

        if (edges.containsKey(fromNodeId)) {
            neighbors = edges.get(fromNodeId);
        } else {
            neighbors = new HashSet<>();
            edges.put(fromNodeId, neighbors);
        }

        if(viaWay.isAerialway()) {
            isUphill = getNode(fromNodeId).elevation < getNode(toNodeId).elevation;
        }

        neighbors.add(new Edge(getNode(fromNodeId), getNode(toNodeId), viaWay, isUphill));
    }

    public static class ElevationLoadingParameters {
        private volatile int totalTiles = 0;
        private volatile int loadedTiles = 0;
        private Runnable callback;

        public ElevationLoadingParameters(Runnable callback) {
            this.callback = callback;
        }

        public synchronized void incrementTotal() {
            totalTiles++;
        }

        public synchronized void incrementLoaded() {
            if (++loadedTiles == totalTiles) callback.run();
        }
    }
}
