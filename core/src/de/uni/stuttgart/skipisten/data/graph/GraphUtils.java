package de.uni.stuttgart.skipisten.data.graph;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import de.uni.stuttgart.skipisten.data.elevation.ElevationProvider;
import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class GraphUtils {

    /**
     * Computes and returns the nearest Node from given position
     *
     * @param lat Latitude position
     * @param lon Longtitude position
     * @param includeHelperNodes if helper nodes should be included
     * @return nearest Node from position
     */
    public static OSMElement getNearestPoint(double lat, double lon, boolean includeHelperNodes) {
        OSMGraph graph = GraphManager.getGraph();
        OSMElement nearestPoint = null;
        double distance = Double.MAX_VALUE;
        double tempDistance;

        for (OSMElement node : graph.getNodes()) {
            if(graph.getWay(node.parentWayId).isHighway && !includeHelperNodes) continue;

            tempDistance = Math.sqrt(Math.pow(node.lat - lat, 2) + Math.pow(node.lon - lon, 2));
            if (tempDistance < distance) {
                distance = tempDistance;
                nearestPoint = node;
            }
        }

        for (long way : graph.getWayIDs()) {
            if (!graph.getWay(way).isAerialway()) {
                continue;
            }
            long[] nodes = graph.getWay(way).nodes;
            long firstNode = nodes[0];
            long lastNode = nodes[nodes.length - 1];

            Vector3 start = new Vector3((float) graph.getNode(firstNode).lat, (float) graph.getNode(firstNode).lon, 0);
            Vector3 end = new Vector3((float) graph.getNode(lastNode).lat, (float) graph.getNode(lastNode).lon, 0);
            Vector3 direction = new Vector3(start).sub(end);
            Vector3 position = new Vector3((float) lat, (float) lon, 0);
            Ray ray = new Ray(start, direction);
            float vectorDistance = new Vector3(ray.direction).crs(new Vector3(position).sub(ray.origin)).len();

            /*System.out.println(nearestPoint.id);
            System.out.println(graph.getNode(firstNode).lat + " + " + graph.getNode(firstNode).lon + " + " + graph.getNode(lastNode).lat + " + " + graph.getNode(lastNode).lon);
            System.out.println(distance + " / " + vectorDistance);*/

            if (isPointBetweenLinePoints(start, end, position) && vectorDistance < distance) {
                long nearestPointOnLine = nodes[0];
                double distOnLine = Double.MAX_VALUE;

                for (long node : nodes) {
                    double tempDistOnLine = calculateDistance(graph, lat, lon, node);
                    if (tempDistOnLine < distOnLine) {
                        distOnLine = tempDistOnLine;
                        nearestPointOnLine = node;
                    }
                }

                nearestPoint = graph.getNode(nearestPointOnLine);
                distance = vectorDistance;
            }
        }

        return nearestPoint;
    }

    private static boolean isPointBetweenLinePoints(Vector3 start, Vector3 end, Vector3 point) {
        return ((new Vector3(end).sub(start).nor().dot(new Vector3(point).sub(end).nor())) < 0f) && ((new Vector3(end).sub(start).nor().dot(new Vector3(start).sub(point).nor())) < 0f);
    }

    private static double calculateDistance(OSMGraph graph, double lat, double lon, long nodeID) {
        OSMElement node = graph.getNode(nodeID);
        return Math.sqrt(Math.pow(node.lat - lat, 2) + Math.pow(node.lon - lon, 2));
    }

    public static void subdivideWay(OSMElement way, OSMGraph graph) {
        // 1) Array to List
        ArrayList<Long> nodes = new ArrayList<>();
        for (long node : way.nodes) {
            nodes.add(node);
        }

        // 2) Subdivide way
        final float maxDistanceWithoutSubdivision = 30f; //in meters

        for (int i = nodes.size() - 1; i >= 1; i--) {
            OSMElement node1 = graph.getNode(nodes.get(i - 1));
            OSMElement node2 = graph.getNode(nodes.get(i));

            float distance = getDistanceInMeters(node1, node2);

            if(distance > maxDistanceWithoutSubdivision) {
                int divisions = (int) (distance / maxDistanceWithoutSubdivision);

                double dLat = node2.lat - node1.lat;
                double dLon = node2.lon - node1.lon;

                for(int d = divisions; d >= 1 ; d--) {
                    double latNew = node1.lat + dLat * d / (divisions + 1);
                    double lonNew = node1.lon + dLon * d / (divisions + 1);

                    OSMElement newNode = generateNode(way, latNew, lonNew);

                    nodes.add(i, newNode.id);
                    graph.addNode(newNode);
                }
            }
        }

        // 3) List to Array
        if (way.nodes.length != nodes.size()) {
            way.nodes = new long[nodes.size()];

            for (int i = 0; i < nodes.size(); i++) {
                way.nodes[i] = nodes.get(i);
            }
        }
    }

    private static OSMElement generateNode(OSMElement parentWay, double lat, double lon) {
        OSMElement generatedNode = new OSMElement();
        generatedNode.id = UUID.randomUUID().getLeastSignificantBits();
        generatedNode.type = "node";
        generatedNode.lat = lat;
        generatedNode.lon = lon;
        generatedNode.parentWayId = parentWay.id;
        generatedNode.isGenerated = true;

        return generatedNode;
    }

    /**
     * Returns all nodes from the graph in a given radius around a given node
     * <p>
     * The node aroundNode is not included in the result list
     *
     * @param way Nodes which are included in this way are also skipped.
     */
    public static List<OSMElement> getNodes(OSMGraph graph, OSMElement aroundNode,
                                            OSMElement way, float maxDistanceInMeters) {
        List<OSMElement> nearNodes = new ArrayList<>();

        for (OSMElement element : graph.getNodes()) {
            if (GraphUtils.getDistanceInMeters(aroundNode, element) <= maxDistanceInMeters
                    && aroundNode.id != element.id) {

                // Skip edges from this node to other node in the same way
                boolean elementIsInWay = false;
                for (long wayNode : way.nodes) {
                    if (element.id == wayNode) {
                        elementIsInWay = true;
                        break;
                    }
                }

                if (!elementIsInWay) {
                    nearNodes.add(element);
                }
            }
        }

        return nearNodes;
    }

    /**
     * @param node returns the nearest node to this node
     */
    public static OSMElement getNearestNodeTo(OSMGraph graph, OSMElement node) {
        OSMElement nearestNode = null;
        float distance = Float.MAX_VALUE;

        for (OSMElement element : graph.getNodes()) {
            float tempDistance = getDistanceInMeters(node, element);
            if (tempDistance < distance && node.id != element.id) {
                distance = tempDistance;
                nearestNode = element;
            }
        }

        return nearestNode;
    }

    public static float getDistanceInMeters(OSMGraph graph, long node1Id, long node2Id) {
        return getDistanceInMeters(graph.getNode(node1Id), graph.getNode(node2Id));
    }

    /**
     * See https://www.mkompf.com/gps/distcalc.html#improved_method for information about the calculation
     * <p>
     * This method is not suited for too large distances, because the calculation is approximate.
     * This is not a problem for our graph, because the distances between the nodes are small.
     */
    public static float getDistanceInMeters(OSMElement node1, OSMElement node2) {
        final double metersPerLatitude = 111_300d;
        final double metersPerLongitude = 111_300d * Math.cos((node1.lat + node2.lat) / 2 * MathUtils.degreesToRadians);

        double dX = metersPerLongitude * (node1.lon - node2.lon);
        double dY = metersPerLatitude * (node1.lat - node2.lat);
        double dZ = node2.elevation - node1.elevation;

        return (float) Math.sqrt(dX * dX + dY * dY + dZ * dZ);
    }

    /**
     * See https://www.mkompf.com/gps/distcalc.html#improved_method for information about the calculation
     * <p>
     * This method is not suited for too large distances, because the calculation is approximate.
     * This is not a problem for our graph, because the distances between the nodes are small.
     * <p>
     * Attention: This method does not include elevation into the distance calculation
     */
    public static float getDistanceInMeters(double lat1, double lon1, double lat2, double lon2) {
        final double metersPerLatitude = 111_300d;
        final double metersPerLongitude = 111_300d * Math.cos((lat1 + lat2) / 2 * MathUtils.degreesToRadians);

        double dX = metersPerLongitude * (lon1 - lon2);
        double dY = metersPerLatitude * (lat1 - lat2);

        return (float) Math.sqrt(dX * dX + dY * dY);
    }

    public static boolean isOneWayAerialway(int aerialwayType) {
        return aerialwayType != OSMElement.Aerialway.NO_AERIALWAY
                && aerialwayType != OSMElement.Aerialway.CABLE_CAR
                && aerialwayType != OSMElement.Aerialway.GONDOLA
                && aerialwayType != OSMElement.Aerialway.MIXED_LIFT;
    }
}
