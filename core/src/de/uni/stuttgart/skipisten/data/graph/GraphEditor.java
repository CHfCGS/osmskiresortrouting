package de.uni.stuttgart.skipisten.data.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class GraphEditor {

    private OSMGraph graph;
    private List<OSMElement> generatedWays;

    public GraphEditor(OSMGraph graph) {
        this.graph = graph;
        generatedWays = new ArrayList<>();
    }

    public void improveSkiGraph() {
        for (OSMElement way : graph.getWays()) {
            if (way.isAerialway()) {
                improveAerialwayWay(way);
            } else {
                if (!way.isHighway) {
                    improveWay(way);
                }
            }
        }

        for (OSMElement generatedWay : generatedWays) {
            generatedWay.initializeStage1();
            graph.addWay(generatedWay, false);
            generatedWay.initializePolyline(graph);
        }
    }

    private void improveWay(OSMElement way) {
        if (graph.getEdges(way.nodes[0]).size() == 1) {
            // System.out.println("Problem in way start: " + way.id);
            findConnectionForNode(way, way.nodes[0]);
        }

        if (graph.getEdges(way.nodes[way.nodes.length - 1]).size() == 1) {
            // System.out.println("Problem in way end: " + way.id);
            findConnectionForNode(way, way.nodes[way.nodes.length - 1]);
        }
    }

    private void improveAerialwayWay(OSMElement way) {
        int intersectionNodesCounter = 0;
        int lastIntersectionIndex = 0;

        // Check for intersections at the start of the way
        if (graph.getEdges(way.nodes[0]).size() >= 2) {
            intersectionNodesCounter++;
            lastIntersectionIndex = 0;
        }

        // Check for intersections at the end of the way
        if (graph.getEdges(way.nodes[way.nodes.length - 1]).size() >= (way.isOneWay ? 1 : 2)) {
            intersectionNodesCounter++;
            lastIntersectionIndex = way.nodes.length - 1;
        }

        // Check for intersections in the middle of the way
        for (int i = 1; i < way.nodes.length - 1; i++) {
            if (graph.getEdges(way.nodes[i]).size() >= (way.isOneWay ? 2 : 3)) {
                intersectionNodesCounter++;
                lastIntersectionIndex = i;
            }
        }

        if (intersectionNodesCounter < 2) {
            boolean hasLiftEntrance = false;
            boolean hasLiftExit = false;

            if (intersectionNodesCounter == 1) {
                long intersectionId = way.nodes[lastIntersectionIndex];

                float distanceToEntrance = GraphUtils.getDistanceInMeters(graph, way.nodes[0], intersectionId);
                float distanceToExit = GraphUtils.getDistanceInMeters(graph, way.nodes[way.nodes.length - 1], intersectionId);

                if (distanceToEntrance < distanceToExit) {
                    hasLiftEntrance = true;
                } else {
                    hasLiftExit = true;
                }
            }

            // System.out.println("Problem in way: " + way.id + " (" + hasLiftEntrance + "/" + hasLiftExit + ")");
            if (!hasLiftEntrance) {
                findConnectionForNode(way, way.nodes[0]);
            }
            if (!hasLiftExit) {
                findConnectionForNode(way, way.nodes[way.nodes.length - 1]);
            }
        }
    }

    private void findConnectionForNode(OSMElement way, long nodeId) {
        OSMElement node = graph.getNode(nodeId);

        List<OSMElement> nearNodes = GraphUtils.getNodes(graph, node, way, 50);
        if (nearNodes.size() == 0) nearNodes = GraphUtils.getNodes(graph, node, way, 100);
        if (nearNodes.size() == 0) nearNodes = GraphUtils.getNodes(graph, node, way, 200);

        // System.out.println("NEAR NODES FOUND for Node: " + nodeId);
        for (OSMElement element : nearNodes) {
            // System.out.println(element.id + " -> " + (node.elevation - element.elevation));

            // Ignore nodes which are pylons
            if(element.getTag("aerialway").equals("pylon") ||
                    (element.isGenerated && graph.getWay(element.parentWayId).isAerialway())) {
                continue;
            }

            if (node.elevation - element.elevation > -20) {
                addGeneratedEdge(node, element);
            }
        }
    }

    private void addGeneratedEdge(OSMElement from, OSMElement to) {
        OSMElement generatedWay = new OSMElement();
        generatedWay.id = UUID.randomUUID().getLeastSignificantBits();
        generatedWay.type = "way";

        generatedWay.nodes = new long[2];
        generatedWay.nodes[0] = from.id;
        generatedWay.nodes[1] = to.id;
        generatedWay.isGenerated = true;
        generatedWay.isOneWay = false;

        generatedWays.add(generatedWay);
    }
}
