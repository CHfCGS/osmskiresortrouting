package de.uni.stuttgart.skipisten.data.graph;

import de.uni.stuttgart.skipisten.data.pathfinding.NavigationPath;

public class GraphManager {

    private static OSMGraph currentGraph;

    public static boolean isGraphLoaded() {
        return currentGraph != null;
    }

    public static void setGraph(OSMGraph graph) {
        GraphEditor graphEditor = new GraphEditor(graph);
        graphEditor.improveSkiGraph();
        currentGraph = graph;

        NavigationPath.init(graph);
    }

    public static OSMGraph getGraph() {
        return currentGraph;
    }

}
