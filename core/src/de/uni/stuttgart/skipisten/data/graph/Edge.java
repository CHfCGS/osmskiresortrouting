package de.uni.stuttgart.skipisten.data.graph;

import de.uni.stuttgart.skipisten.data.overpass.OSMElement;

public class Edge {
    public OSMElement src;
    public OSMElement dest;
    public OSMElement way;

    public boolean isUphill;

    public Edge(OSMElement src, OSMElement dest, OSMElement way, boolean isUphill) {
        this.src = src;
        this.dest = dest;
        this.way = way;
        this.isUphill = isUphill;
    }

    @Override
    public int hashCode() {
        return (int) dest.id ^ (int) (dest.id >> 32) ^ (int) way.id ^ (int) (way.id >> 32);
    }

    public boolean isGenerated() {
        return way.isGenerated;
    }

    public float getLengthInMeters() {
        return GraphUtils.getDistanceInMeters(src, dest);
    }

    /**
     * @return the estimated traversal time of this edge in seconds
     */
    public float getEstimatedTraversalTime(boolean realTime) {
        if (way.isPiste) {
            return getEstimatedTraversalTimePiste();
        } else if (way.isAerialway()) {

            float factor = 1f;

            // Increase factor for downhill aerialways, to force that the pathfinding prefers downhill pistes
            if(!isUphill && !realTime) {
                factor = 3f;
            }

            // Lifts are usually a bit slower than their max. speed (or stop sometimes).
            factor *= 1.25f;

            return factor * getEstimatedTraversalTimeAerialway();
        } else if (way.isGenerated || way.isHighway) {

            float length = getLengthInMeters();
            float generatedSpeed = 1f;
            return length / generatedSpeed;

        }  else if (way.tags.containsKey("railway")) {

            float length = getLengthInMeters();
            float generatedSpeed = 5f;
            return length / generatedSpeed;

        }

        return 0;
    }

    /**
     * @see Edge#getEstimatedTraversalTime(boolean)
     */
    private float getEstimatedTraversalTimePiste() {
        float length = getLengthInMeters();
        float heightDiff = (float) (dest.elevation - src.elevation);

        // Check if piste is uphill
        if (isUphill && heightDiff > 0) {
            float slopeAngle = (float) Math.toDegrees(Math.asin(heightDiff / length));

            float uphillPisteSpeed;

            if(slopeAngle < 3) {
                uphillPisteSpeed = .5f; //[m/s]
            } else if(slopeAngle < 20) {
                uphillPisteSpeed = .2f; //[m/s]
            } else {
                uphillPisteSpeed = .1f; //[m/s]
            }

            return length / uphillPisteSpeed;
        } else {
            float downhillPisteSpeed = 9f; //[m/s]
            return length / downhillPisteSpeed;
        }
    }

    /**
     * @see Edge#getEstimatedTraversalTime(boolean)
     */
    private float getEstimatedTraversalTimeAerialway() {
        //Sometimes the duration is with a tag
        if (way.tags.containsKey("aerialway:duration")) {
            String durationStr = way.tags.get("aerialway:duration");
            float minutes = 0;

            try {
                if (durationStr.contains(":")) {
                    int colonIndex = durationStr.indexOf(':');
                    minutes = Integer.parseInt(durationStr.substring(0, colonIndex));
                    minutes += Integer.parseInt(durationStr.substring(colonIndex + 1)) / 60f;
                } else {
                    minutes = Float.parseFloat(durationStr.replace(',', '.'));
                }

                return 60f * minutes / (way.nodes.length - 1);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        //Otherwise estimate the speed of the aerialway
        float aerialwaySpeed; //[m/s]
        switch (way.aerialwayType) {
            case OSMElement.Aerialway.CABLE_CAR:
                aerialwaySpeed = 9f;

                //In average you need to wait half of the distance of the cable car
                aerialwaySpeed /= 1.5f;

                break;
            case OSMElement.Aerialway.GONDOLA:
                aerialwaySpeed = 6f;
                break;
            case OSMElement.Aerialway.MIXED_LIFT:
                aerialwaySpeed = 5f;
                break;
            case OSMElement.Aerialway.CHAIR_LIFT:
                aerialwaySpeed = 5f;

                boolean hasHeating = way.getTag("aerialway:heating").equals("yes");
                boolean hasBubble = way.getTag("aerialway:bubble").equals("yes");
                int occupancy = 0;

                if (way.tags.containsKey("aerialway:occupancy")) {
                    try {
                        occupancy = Integer.parseInt(way.tags.get("aerialway:occupancy"));
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                }

                //Chair lifts without a bubble, heating and less than 6 occupants are usually older and slower.
                if (!hasHeating && !hasBubble && occupancy < 6) {
                    aerialwaySpeed = 2.5f;
                }

                break;
            case OSMElement.Aerialway.MAGIC_CARPET:
                aerialwaySpeed = 1f;
                break;
            default:
                aerialwaySpeed = 3f;
                break;
        }

        return getLengthInMeters() / aerialwaySpeed;
    }
}
