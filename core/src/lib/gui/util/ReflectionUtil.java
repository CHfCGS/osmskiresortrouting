package lib.gui.util;

import lib.gui.components.UIComponent;
import lib.gui.components.dialog.Dialog;
import lib.gui.components.screen.Screen;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ReflectionUtil {

    /**
     * Returns a list of fields from a class,
     * which have a specific type (or subtype of that type)
     * <p>
     * This method also returns arrays with the specific type!
     * <p>
     * The method also turns sets all returned fields to accessible
     *
     * @param javaClass       the class
     * @param type            the type
     * @param onlyFinalFields will only return final fields if this is true
     * @return a list of fields of a class with a specific type
     */
    public static List<Field> getFieldsOfType(Class<?> javaClass, Class<?> type, boolean onlyFinalFields) {

        List<Field> fields = new ArrayList<Field>();

        Field[] allFields = javaClass.getDeclaredFields();

        for (Field field : allFields) {

            final boolean isCorrectType = type.isAssignableFrom(field.getType());

            final boolean isCorrectArrayType =
                    field.getType().isArray() && type.isAssignableFrom(field.getType().getComponentType());

            final boolean finalCondition = !onlyFinalFields || Modifier.isFinal(field.getModifiers());

            if ((isCorrectType || isCorrectArrayType) && finalCondition) {
                field.setAccessible(true);
                fields.add(field);
            }
        }

        return fields;
    }

    public static List<Field> getUIComponentsOfClass(Class<?> javaClass) {

        List<Field> uiComponentFields = new ArrayList<>();

        Class<?> currentClass = javaClass;

        while (currentClass != Object.class && currentClass != UIComponent.class) {
            uiComponentFields.addAll(getFieldsOfType(currentClass, UIComponent.class, true));
            currentClass = currentClass.getSuperclass();
        }

        // Ignore screens and dialogs -> remove them from list
        for (int i = 0; i < uiComponentFields.size(); i++) {

            Field uiComponentField = uiComponentFields.get(i);

            if (Screen.class.isAssignableFrom(uiComponentField.getType())
                    || Dialog.class.isAssignableFrom(uiComponentField.getType())) {
                uiComponentFields.remove(i);
                i--;
            }
        }

        return uiComponentFields;
    }
}
