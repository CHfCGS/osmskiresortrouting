package lib.gui.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.layout.values.DynamicValue;

public class DynamicFrameBuffer {

    private FrameBuffer frameBuffer;

    private Pixmap.Format format;
    private DynamicValue width;
    private DynamicValue height;

    public DynamicFrameBuffer() {
        this(Pixmap.Format.RGB888);
    }

    public DynamicFrameBuffer(Pixmap.Format format) {
        this(format, DynamicValue.SCREEN_W, DynamicValue.SCREEN_H);
    }

    public DynamicFrameBuffer(DynamicValue width, DynamicValue height) {
        this(Pixmap.Format.RGB888, width, height);
    }

    public DynamicFrameBuffer(Pixmap.Format format, DynamicValue width, DynamicValue height) {
        this.format = format;
        this.width = width;
        this.height = height;

        frameBuffer = new FrameBuffer(format, (int) width.value(), (int) height.value(), false);
    }

    public void begin() {
        Renderer2D.getBatch().flush();

        checkSize();

        Matrix4 newProjectionMatrix = new Matrix4();
        newProjectionMatrix.setToOrtho2D(0, getHeight(), getWidth(), -getHeight());
        Renderer2D.getBatch().setProjectionMatrix(newProjectionMatrix);

        frameBuffer.begin();
        Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    private void checkSize() {
        if (frameBuffer.getWidth() != (int) width.value() || frameBuffer.getHeight() != (int) height.value()) {
            dispose();
            frameBuffer = new FrameBuffer(format, (int) width.value(), (int) height.value(), false);
        }
    }

    public void end() {
        Renderer2D.getBatch().flush();
        frameBuffer.end();

        Renderer2D.getBatch().setProjectionMatrix(Renderer2D.instance().getCamera().combined);
    }

    public int getWidth() {
        return frameBuffer.getWidth();
    }

    public int getHeight() {
        return frameBuffer.getHeight();
    }

    public Texture getColorBufferTexture() {
        return frameBuffer.getColorBufferTexture();
    }

    public void drawFrameBufferTexture(float x, float y) {
        drawFrameBufferTexture(x, y, frameBuffer.getWidth(), frameBuffer.getHeight(), 1f);
    }

    public void drawFrameBufferTexture(float x, float y, float scale) {
        drawFrameBufferTexture(x, y, frameBuffer.getWidth(), frameBuffer.getHeight(), scale);
    }

    public void drawFrameBufferTexture(float x, float y, float w, float h, float scale) {
        Draw.image(frameBuffer.getColorBufferTexture(), x, y + h, w, -h, scale);
    }

    public void dispose() {
        frameBuffer.dispose();
    }

    public FrameBuffer getCurrentFrameBufferObject() {
        return frameBuffer;
    }
}
