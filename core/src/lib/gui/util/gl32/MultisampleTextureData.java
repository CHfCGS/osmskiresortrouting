package lib.gui.util.gl32;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MultisampleTextureData implements TextureData {
    /** width and height */
    int width = 0;
    int height = 0;
    boolean isPrepared = false;

    /** properties of opengl texture */
    int samples;
    int internalFormat;

    /** @see "https://www.khronos.org/opengles/sdk/docs/man/xhtml/glTexImage2D.xml"
     * @param internalFormat Specifies the internal format of the texture. Must be one of the following symbolic constants:
     *           {@link GL20#GL_ALPHA}, {@link GL20#GL_LUMINANCE}, {@link GL20#GL_LUMINANCE_ALPHA}, {@link GL20#GL_RGB},
     *           {@link GL20#GL_RGBA}.*/
    public MultisampleTextureData(int width, int height, int samples, int internalFormat) {
        this.width = width;
        this.height = height;
        this.samples = samples;
        this.internalFormat = internalFormat;
    }

    @Override
    public TextureDataType getType () {
        return TextureDataType.Custom;
    }

    @Override
    public boolean isPrepared () {
        return isPrepared;
    }

    @Override
    public void prepare () {
        if (isPrepared) throw new GdxRuntimeException("Already prepared");
        isPrepared = true;
    }

    @Override
    public void consumeCustomData (int target) {
        if (!GL32Manager.gl32Available) return;
        GL32Manager.gl32.glTexImage2DMultisample(target, samples, internalFormat, width, height, false);
    }

    @Override
    public Pixmap consumePixmap () {
        throw new GdxRuntimeException("This TextureData implementation does not return a Pixmap");
    }

    @Override
    public boolean disposePixmap () {
        throw new GdxRuntimeException("This TextureData implementation does not return a Pixmap");
    }

    @Override
    public int getWidth () {
        return width;
    }

    @Override
    public int getHeight () {
        return height;
    }

    @Override
    public Pixmap.Format getFormat () {
        return Pixmap.Format.RGBA8888;
    }

    @Override
    public boolean useMipMaps () {
        return false;
    }

    @Override
    public boolean isManaged () {
        return false;
    }
}
