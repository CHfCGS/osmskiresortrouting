package lib.gui.util.gl32;

public class GL32Manager {
    public static GL32 gl32 = null;
    public static boolean gl32Available = false;

    public static void setGL32(GL32 gl) {
        gl32 = gl;
        gl32Available = gl != null;
    }
}
