package lib.gui.util.gl32;

import com.badlogic.gdx.graphics.Pixmap;

public interface GL32 {
    public static final int GL_TEXTURE_2D_MULTISAMPLE = 0x9100;

    public void glTexImage2DMultisample(
            int target,
            int samples,
            int internalformat,
            int width,
            int height,
            boolean fixedsamplelocations
    );

    public int pixmapFormatToSizedGLFormat(Pixmap.Format format);
}
