package lib.gui.util.gl32;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GLTexture;
import com.badlogic.gdx.utils.GdxRuntimeException;

public class MultisampleTexture extends GLTexture {

    public MultisampleTextureData data;

    public MultisampleTexture(MultisampleTextureData data) {
        super(GL32.GL_TEXTURE_2D_MULTISAMPLE, Gdx.gl.glGenTexture());
        load(data);
    }

    public void load (MultisampleTextureData data) {
        if (this.data != null && data.isManaged() != this.data.isManaged())
            throw new GdxRuntimeException("New data must have the same managed status as the old data");
        this.data = data;

        if (!data.isPrepared()) data.prepare();

        bind();
        uploadImageData(GL32.GL_TEXTURE_2D_MULTISAMPLE, data);
        Gdx.gl.glBindTexture(glTarget, 0);
    }

    @Override
    public int getWidth() {
        return data.getWidth();
    }

    @Override
    public int getHeight() {
        return data.getHeight();
    }

    @Override
    public int getDepth() {
        return 0;
    }

    @Override
    public boolean isManaged() {
        return false;
    }

    @Override
    protected void reload() {
        throw new GdxRuntimeException("Tried to reload unmanaged Texture");
    }
}
