package lib.gui.util.gl32;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.GLFrameBuffer;
import com.badlogic.gdx.utils.Array;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

public class MultisampledFramebuffer extends GLFrameBuffer<MultisampleTexture> {

    int internalFormat;
    int samples;

    boolean gl20Mode = false;

    FrameBuffer blitBuffer;

    protected MultisampledFramebuffer(GLFrameBufferBuilder<? extends GLFrameBuffer<MultisampleTexture>> bufferBuilder) {
        super(bufferBuilder);
    }

    public MultisampledFramebuffer(Pixmap.Format format, int width, int height, int samples) {
        super(null);
        if (Gdx.gl30 != null && GL32Manager.gl32Available) {
            MultisampledFrameBufferBuilder frameBufferBuilder = new MultisampledFrameBufferBuilder(width, height);
            frameBufferBuilder.addBasicColorTextureAttachment(format);
            internalFormat = GL32Manager.gl32.pixmapFormatToSizedGLFormat(format);
            this.samples = samples;
            this.bufferBuilder = frameBufferBuilder;
            build();
        } else gl20Mode = true;
        blitBuffer = new FrameBuffer(format, width, height, false);
    }

    @Override
    protected void build() {
        if (bufferBuilder == null) return;

        GL20 gl = Gdx.gl20;

        // iOS uses a different framebuffer handle! (not necessarily 0)
        if (!defaultFramebufferHandleInitialized) {
            defaultFramebufferHandleInitialized = true;
            if (Gdx.app.getType() == Application.ApplicationType.iOS) {
                IntBuffer intbuf = ByteBuffer.allocateDirect(16 * Integer.SIZE / 8).order(ByteOrder.nativeOrder()).asIntBuffer();
                gl.glGetIntegerv(GL20.GL_FRAMEBUFFER_BINDING, intbuf);
                defaultFramebufferHandle = intbuf.get(0);
            } else {
                defaultFramebufferHandle = 0;
            }
        }

        MultisampledFrameBufferBuilder builder = (MultisampledFrameBufferBuilder)bufferBuilder;

        framebufferHandle = gl.glGenFramebuffer();
        gl.glBindFramebuffer(GL20.GL_FRAMEBUFFER, framebufferHandle);
        MultisampleTextureData data = new MultisampleTextureData(builder.getWidth(), builder.getHeight(), samples, internalFormat);
        MultisampleTexture texture = new MultisampleTexture(data);
        textureAttachments.add(texture);
        gl.glBindTexture(texture.glTarget, texture.getTextureObjectHandle());
        attachFrameBufferColorTexture(textureAttachments.first());

        gl.glBindRenderbuffer(GL20.GL_RENDERBUFFER, 0);
        for (MultisampleTexture tex : textureAttachments) {
            gl.glBindTexture(tex.glTarget, 0);
        }

        int result = gl.glCheckFramebufferStatus(GL20.GL_FRAMEBUFFER);

        gl.glBindFramebuffer(GL20.GL_FRAMEBUFFER, defaultFramebufferHandle);

        if (result != GL20.GL_FRAMEBUFFER_COMPLETE) {
            for (MultisampleTexture tex : textureAttachments) {
                disposeColorTexture(tex);
            }

            gl.glDeleteFramebuffer(framebufferHandle);

            if (result == GL20.GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
                throw new IllegalStateException("Frame buffer couldn't be constructed: incomplete attachment");
            if (result == GL20.GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS)
                throw new IllegalStateException("Frame buffer couldn't be constructed: incomplete dimensions");
            if (result == GL20.GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT)
                throw new IllegalStateException("Frame buffer couldn't be constructed: missing attachment");
            if (result == GL20.GL_FRAMEBUFFER_UNSUPPORTED)
                throw new IllegalStateException("Frame buffer couldn't be constructed: unsupported combination of formats");
            throw new IllegalStateException("Frame buffer couldn't be constructed: unknown error " + result);
        }

        addManagedFrameBuffer(Gdx.app, this);
    }

    @SuppressWarnings("rawtypes")
    private static void addManagedFrameBuffer(Application app, GLFrameBuffer frameBuffer) {
        Array<GLFrameBuffer> managedResources = buffers.get(app);
        if (managedResources == null) managedResources = new Array<>();
        managedResources.add(frameBuffer);
        buffers.put(app, managedResources);
    }

    @Override
    protected MultisampleTexture createTexture(FrameBufferTextureAttachmentSpec attachmentSpec) {
        return null;
    }

    @Override
    public void dispose() {
        try {
            super.dispose();
            blitBuffer.dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void disposeColorTexture(MultisampleTexture colorTexture) {
        colorTexture.dispose();
    }

    @Override
    protected void attachFrameBufferColorTexture(MultisampleTexture texture) {
        Gdx.gl20.glFramebufferTexture2D(GL20.GL_FRAMEBUFFER, GL20.GL_COLOR_ATTACHMENT0, GL32.GL_TEXTURE_2D_MULTISAMPLE, texture.getTextureObjectHandle(), 0);
    }

    @Override
    public void begin() {
        if (gl20Mode) blitBuffer.begin();
        else super.begin();
    }

    @Override
    public void end(int x, int y, int width, int height) {
        if (gl20Mode) blitBuffer.end();
        else {
            unbind();
            Gdx.gl20.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, framebufferHandle);
            Gdx.gl20.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, blitBuffer.getFramebufferHandle());
            Gdx.gl30.glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL20.GL_COLOR_BUFFER_BIT, GL20.GL_NEAREST);
            Gdx.gl20.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, defaultFramebufferHandle);
            Gdx.gl20.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, defaultFramebufferHandle);
            Gdx.gl20.glViewport(x, y, width, height);
        }
    }

    public Texture getResolvedColorBufferTexture() {
        return blitBuffer.getColorBufferTexture();
    }

    public static class MultisampledFrameBufferBuilder extends GLFrameBufferBuilder<MultisampledFramebuffer> {
        public MultisampledFrameBufferBuilder (int width, int height) {
            super(width, height);
        }

        Array<FrameBufferTextureAttachmentSpec> getTextureAttachmentSpecs() {
            return textureAttachmentSpecs;
        }

        int getWidth() {
            return width;
        }

        int getHeight() {
            return height;
        }

        @Override
        public MultisampledFramebuffer build () {
            return new MultisampledFramebuffer(this);
        }
    }
}
