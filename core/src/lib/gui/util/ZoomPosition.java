package lib.gui.util;

public class ZoomPosition {

    // Bounds
    public int boundX, boundY, boundW, boundH;

    // Translation
    private double xTranslation, yTranslation;

    private double minTranslationX = Integer.MIN_VALUE;
    private double maxTranslationX = Integer.MAX_VALUE;
    private double minTranslationY = Integer.MIN_VALUE;
    private double maxTranslationY = Integer.MAX_VALUE;

    private double deltaXSpeed, deltaYSpeed;

    // Zoom
    private double xZoom, yZoom;
    private double initialXZoom, initialYZoom;

    private double minZoomX = Double.MIN_VALUE;
    private double maxZoomX = Double.MAX_VALUE;
    private double minZoomY = Double.MIN_VALUE;
    private double maxZoomY = Double.MAX_VALUE;

    private int zoomPointX, zoomPointY;

    public ZoomPosition(double initialXZoom, double initialYZoom) {
        this.xZoom = initialXZoom;
        this.yZoom = initialYZoom;
        this.initialXZoom = initialXZoom;
        this.initialYZoom = initialYZoom;
    }

    public void reset() {
        xZoom = initialXZoom;
        yZoom = initialYZoom;
        setTranslationX(0);
        setTranslationY(0);
    }

    // Translation
    public void setTranslationX(double x) {
        xTranslation = x;
        xTranslation = Math.max(xTranslation, minTranslationX);
        xTranslation = Math.min(xTranslation, maxTranslationX);
    }

    public void setTranslationY(double y) {
        yTranslation = y;
        yTranslation = Math.max(yTranslation, minTranslationY);
        yTranslation = Math.min(yTranslation, maxTranslationY);
    }

    public float getTranslationX() {
        return (float) xTranslation;
    }

    public float getTranslationY() {
        return (float) yTranslation;
    }

    public void moveX(int value) {
        setTranslationX(xTranslation + value);
        deltaXSpeed += value / 2.0;
    }

    public void moveY(int value) {
        setTranslationY(getTranslationY() + value);
        deltaYSpeed += value / 2.0;
    }

    public void setMinTranslationX(int minTranslationX) {
        this.minTranslationX = minTranslationX;
    }

    public void setMaxTranslationX(int maxTranslationX) {
        this.maxTranslationX = maxTranslationX;
    }

    public void setMinTranslationY(int minTranslationY) {
        this.minTranslationY = minTranslationY;
    }

    public void setMaxTranslationY(int maxTranslationY) {
        this.maxTranslationY = maxTranslationY;
    }

    // Zoom
    public void setZoomPointX(int zoomPointX) {
        this.zoomPointX = zoomPointX;
    }

    public void setZoomPointY(int zoomPointY) {
        this.zoomPointY = zoomPointY;
    }

    public void zoomX(double faktor) {
        double ZoomAlt = xZoom;

        xZoom *= faktor;

        if (xZoom > maxZoomX) xZoom = maxZoomX;
        if (xZoom < minZoomX) xZoom = minZoomX;

        faktor = xZoom / ZoomAlt;

        setTranslationX(xTranslation + zoomPointX);
        setTranslationX(xTranslation * faktor);
        setTranslationX(xTranslation - zoomPointX);
    }

    public void zoomY(double faktor) {
        double ZoomAlt = yZoom;

        yZoom *= faktor;

        if (yZoom > maxZoomY) yZoom = maxZoomY;
        if (yZoom < minZoomY) yZoom = minZoomY;

        faktor = yZoom / ZoomAlt;

        setTranslationY(yTranslation + zoomPointY);
        setTranslationY(yTranslation * faktor);
        setTranslationY(yTranslation - zoomPointY);
    }

    public double getZoomX() {
        return xZoom;
    }

    public double getZoomY() {
        return yZoom;
    }

    public void setMaxZoomX(double max) {
        maxZoomX = max;
    }

    public void setMaxZoomY(double max) {
        maxZoomY = max;
    }

    public void setMinZoomX(double min) {
        minZoomX = min;
    }

    public void setMinZoomY(double min) {
        minZoomY = min;
    }

    public int getXIndex(int XPos) {
        return (int) ((XPos - xTranslation - boundX) / getZoomX());
    }

    public int getYIndex(int YPos) {
        return (int) ((YPos - yTranslation - boundY) / getZoomY());
    }

    public void update(boolean Xpressed, boolean Ypressed) {
        if (deltaXSpeed != 0) {
            deltaXSpeed *= 0.8;

            if (!Xpressed)
                setTranslationX(xTranslation + deltaXSpeed);
        }

        if (deltaYSpeed != 0) {
            deltaYSpeed *= 0.8;

            if (!Ypressed)
                setTranslationY(yTranslation + deltaYSpeed);
        }

        setTranslationX(xTranslation);
        setTranslationY(yTranslation);
    }
}
