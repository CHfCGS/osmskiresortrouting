package lib.gui.util;

public class Stopwatch {

    private long startTimeInNs = 0;
    private long stopTimeInNs = 0;
    private boolean isRunning = false;

    public Stopwatch() {
        start();
    }

    public void start() {
        startTimeInNs = System.nanoTime();
        isRunning = true;
    }

    public void stop() {
        stopTimeInNs = System.nanoTime();
        isRunning = false;
    }

    public long elapsedTimeInNs() {
        if (isRunning) {
            return System.nanoTime() - startTimeInNs;
        }

        return stopTimeInNs - startTimeInNs;
    }

    public float elapsedTimeInMs() {
        return elapsedTimeInNs() / 1_000_000f;
    }

    public float elapsedTimeInS() {
        return elapsedTimeInNs() / 1_000_000_000f;
    }
}
