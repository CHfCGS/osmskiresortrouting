package lib.gui.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.utils.Align;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.Renderer2D;
import lib.gui.resources.Text;

public class TextRenderer {

    private Color textColor = new Color(Color.WHITE);
    private Text text;
    private float width;
    private float height;
    private float scale = 1f;
    private boolean underline;
    private boolean modeMoveText;

    private boolean wrapText;

    private float textX;
    private float textY;
    private float textW;
    private float textH;

    public void setText(String text) {
        if (this.text == null)
            this.text = new Text();
        this.text.setValue(text);
    }

    public void setText(Text text) {
        this.text = text;
    }

    public void wrapText(boolean wrap) {
        this.wrapText = wrap;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public void setTextScale(float scale) {
        this.scale = scale;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public void underlineText(boolean underline) {
        this.underline = underline;
    }

    public void moveTextWhenNecessary(boolean modeMoveText) {
        this.modeMoveText = modeMoveText;
    }

    /**
     * Draws the text at (local) position 0, 0
     */
    public void render() {
        render(0, 0, width, height);
    }

    /**
     * Draws the text at (local) position x, y
     */
    public void render(float x, float y) {
        render(x, y, width, height);
    }

    /**
     * Draws the text at (local) position x, y
     * The text will get centered (using width and height)
     */
    public void render(float x, float y, float w, float h) {
        GlyphLayout layout = getGlyphLayout();

        textW = layout.width;
        textH = layout.height;
        textX = (w - textW) / 2f + x;
        textY = (h - textH) / 2f + y;

        if (modeMoveText) {
            float textXBase = (h - height) / 2f;
            float textXMove = w - textXBase - layout.width;

            textX = Math.min(textXBase, textXMove);
        }

        if (wrapText) {
            Draw.font(textX / 2f, textY, layout);
        } else {
            Draw.font(textX, textY, layout);
        }

        if (underline) {
            float lineWidth = layout.height / 6f;
            Draw.setColor(textColor);
            Draw.line(textX, textY + layout.height + lineWidth / 2f,
                    textX + layout.width, textY + layout.height + lineWidth / 2f,
                    lineWidth);
        }
    }

    public GlyphLayout getGlyphLayout() {
        BitmapFont font = Renderer2D.getTextureManager().getFont();
        font.setColor(textColor.r, textColor.g, textColor.b, textColor.a * Draw.getRenderAlphaValue());

        Draw.setTextSize(GUIConfiguration.fontResolution);
        GlyphLayout textLayout = new GlyphLayout(font, text.toString());

        float textSizeWidth = width * GUIConfiguration.fontResolution / textLayout.width;
        float textSizeHeight = height * GUIConfiguration.fontResolution / textLayout.height;

        if (modeMoveText) {
            textSizeWidth = Float.MAX_VALUE;
        }

        float textSize = Math.min(textSizeWidth, textSizeHeight);

        if (wrapText) {
            textSize = textSizeHeight;
        }

        textSize *= scale;

        Draw.setTextSize(textSize);
        if (wrapText) {
            textLayout = new GlyphLayout(font, text.toString(), font.getColor(), width, Align.center, true);
        } else {
            textLayout = new GlyphLayout(font, text.toString());
        }

        return textLayout;
    }

    public float getTextX() {
        return textX;
    }

    public float getTextY() {
        return textY;
    }

    public float getTextW() {
        return textW;
    }

    public float getTextH() {
        return textH;
    }
}
