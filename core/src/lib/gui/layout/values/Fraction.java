package lib.gui.layout.values;

public class Fraction extends DynamicValue {

    private float fraction;
    private DynamicValue baseValue;

    public Fraction(double fraction, DynamicValue baseValue) {
        this.fraction = (float) fraction;
        this.baseValue = baseValue;
    }

    @Override
    public float value() {
        return baseValue.value() * fraction;
    }
}
