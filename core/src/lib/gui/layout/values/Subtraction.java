package lib.gui.layout.values;

public class Subtraction extends DynamicValue {

    private final DynamicValue value1;
    private final DynamicValue value2;

    public Subtraction(DynamicValue value1, float value2) {
        this(value1, new FixedValue(value2));
    }

    public Subtraction(DynamicValue value1, DynamicValue value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public float value() {
        return value1.value() - value2.value();
    }
}