package lib.gui.layout.values;

import com.badlogic.gdx.Gdx;

public abstract class DynamicValue {

    public static final DynamicValue SCREEN_W = new DynamicValue() {
        @Override
        public float value() {
            return Gdx.graphics.getWidth();
        }
    };

    public static final DynamicValue SCREEN_H = new DynamicValue() {
       @Override
        public float value() {
            return Gdx.graphics.getHeight();
        }
    };

    public abstract float value();

    public void updateValue(float fixedValue) {
        throw new RuntimeException("Not implemented for this type");
    }

    @Override
    public String toString() {
        return Float.toString(value());
    }
}