package lib.gui.layout.values;

import lib.gui.components.UIComponent;

import java.util.LinkedList;

public abstract class TermParser {

    private static final String KEYWORD_SCREEN_W = "screenW";
    private static final String KEYWORD_SCREEN_H = "screenH";

    private static final String KEYWORD_PARENT_W = "w";
    private static final String KEYWORD_PARENT_H = "h";

    private static final String KEYWORD_ADD = "+";
    private static final String KEYWORD_SUBTRACT = "-";
    private static final String KEYWORD_MULTIPLY = "*";

    /**
     * The term does only allow these tokens:
     * screenW, screenH, w, h, +, -, *
     * and numbers (0, 0.1, 1.2, ...)
     * <p>
     * Tokens must be separated by spaces
     */
    public static DynamicValue parseTerm(String term, UIComponent parent) {

        try {

            LinkedList<Object> parseList = generateParseList(term, parent);

            // Multiply and Divide first
            for (int i = 1; i < parseList.size(); i++) {
                Object o = parseList.get(i);

                if (o.equals(KEYWORD_MULTIPLY)) {
                    Object lastValue = parseList.get(i - 1);
                    Object nextValue = parseList.get(i + 1);

                    parseList.remove(i + 1);
                    parseList.remove(i);
                    parseList.remove(i - 1);
                    i--;

                    Object result = null;

                    if (lastValue instanceof Double && nextValue instanceof Double) {
                        result = (Double) lastValue * (Double) nextValue;
                    } else if (lastValue instanceof Double) {
                        result = new Fraction((Double) lastValue, (DynamicValue) nextValue);
                    } else if (nextValue instanceof Double) {
                        result = new Fraction((Double) nextValue, (DynamicValue) lastValue);
                    }

                    parseList.add(i, result);
                }
            }

            // Add and Subtract
            for (int i = 1; i < parseList.size(); i++) {
                Object o = parseList.get(i);

                if (o.equals(KEYWORD_ADD)) {
                    Object lastValue = parseList.get(i - 1);
                    Object nextValue = parseList.get(i + 1);

                    parseList.remove(i + 1);
                    parseList.remove(i);
                    parseList.remove(i - 1);
                    i--;

                    Object result = null;

                    if (lastValue instanceof Double && nextValue instanceof Double) {
                        result = (Double) lastValue + (Double) nextValue;
                    } else if (lastValue instanceof Double) {
                        result = new Addition(new FixedValue((Double) lastValue), (DynamicValue) nextValue);
                    } else if (nextValue instanceof Double) {
                        result = new Addition((DynamicValue) lastValue, new FixedValue((Double) nextValue));
                    } else {
                        result = new Addition((DynamicValue) lastValue, (DynamicValue) nextValue);
                    }

                    parseList.add(i, result);

                } else if (o.equals(KEYWORD_SUBTRACT)) {
                    Object lastValue = parseList.get(i - 1);
                    Object nextValue = parseList.get(i + 1);

                    parseList.remove(i + 1);
                    parseList.remove(i);
                    parseList.remove(i - 1);
                    i--;

                    Object result;

                    if (lastValue instanceof Double && nextValue instanceof Double) {
                        result = (Double) lastValue - (Double) nextValue;
                    } else if (lastValue instanceof Double) {
                        result = new Subtraction(new FixedValue((Double) lastValue), (DynamicValue) nextValue);
                    } else if (nextValue instanceof Double) {
                        result = new Subtraction((DynamicValue) lastValue, new FixedValue((Double) nextValue));
                    } else {
                        result = new Subtraction((DynamicValue) lastValue, (DynamicValue) nextValue);
                    }

                    parseList.add(i, result);
                }
            }

            if (parseList.size() != 1) {
                throw new RuntimeException("An error occurred while parsing term " + term);
            }

            if (parseList.get(0) instanceof Double)
                return new FixedValue((Double) parseList.get(0));

            return (DynamicValue) parseList.get(0);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Term " + term + " cannot be parsed");
        }
    }

    private static LinkedList<Object> generateParseList(String term, UIComponent parent) {

        String[] tokens = term.split("\\s+");

        LinkedList<Object> parseList = new LinkedList<>();

        for (String token : tokens) {

            switch (token) {
                case KEYWORD_SCREEN_W:
                    parseList.add(DynamicValue.SCREEN_W);
                    break;

                case KEYWORD_SCREEN_H:
                    parseList.add(DynamicValue.SCREEN_H);
                    break;

                case KEYWORD_PARENT_W:
                    parseList.add(parent.w);
                    break;

                case KEYWORD_PARENT_H:
                    parseList.add(parent.h);
                    break;

                case KEYWORD_ADD:
                case KEYWORD_SUBTRACT:
                case KEYWORD_MULTIPLY:
                    parseList.add(token);
                    break;

                default:
                    try {
                        parseList.add(Double.parseDouble(token));
                    } catch (Exception e) {
                        throw new RuntimeException("Token " + token + " is invalid in term " + term);
                    }
            }
        }

        return parseList;
    }
}
