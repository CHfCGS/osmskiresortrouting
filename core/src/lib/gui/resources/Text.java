package lib.gui.resources;

public class Text {

    private String key;
    private String value;

    public Text() {
        this.value = "";
    }

    public Text(String key) {
        this.key = key;
    }

    public Text(String keyOrValue, boolean isKey) {
        if(isKey) {
            this.key = keyOrValue;
        } else {
            this.value = keyOrValue;
        }
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setValue(String value) {
        setKey(null);
        this.value = value;
    }

    @Override
    public String toString() {
        if(key != null) {
            return TranslationManager.instance().getValue(key);
        }

        return value;
    }
}
