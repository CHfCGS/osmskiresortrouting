package lib.gui.resources;

import com.badlogic.gdx.Gdx;
import lib.gui.GUIConfiguration;

import java.util.HashMap;

public class TranslationManager {

    private static TranslationManager instance;
    private GUIConfiguration config;

    private String currentLanguage = "";

    private static HashMap<String, String> texts;

    public static void setUp(GUIConfiguration config) {
        instance = new TranslationManager();
        instance.config = config;
        instance.loadTranslationIfNecessary();
    }

    public static TranslationManager instance() {
        return instance;
    }

    private TranslationManager() {
    }

    private void loadTranslationIfNecessary() {
        if (!currentLanguage.equals(config.appLanguage.toLowerCase())) {
            currentLanguage = config.appLanguage.toLowerCase();
            loadTranslation();
        }
    }

    private void loadTranslation() {
        texts = new HashMap<>();

        if (!config.translationDirectory.endsWith("/")) {
            config.translationDirectory += "/";
        }

        String fileName = config.translationDirectory + currentLanguage + config.translationFileEnding;
        String fileContent = Gdx.files.internal(fileName).readString("UTF-8");

        String[] lines = fileContent.replace("\r", "").split("\n");

        for (String line : lines) {

            if (!line.contains("="))
                continue;

            if (line.trim().startsWith("#"))
                continue;

            int firstEqualsSign = line.indexOf("=");

            String key = line.substring(0, firstEqualsSign).trim();
            String value = line.substring(firstEqualsSign + 1).trim();

            texts.put(key, value);
        }
    }

    public String getValue(String key) {
        if (texts.containsKey(key)) {
            return texts.get(key);
        }

        texts.put(key, key);
        System.out.println("Warning: Key " + key + " not found in translation " + currentLanguage);

        return key;
    }

}
