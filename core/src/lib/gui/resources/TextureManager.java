package lib.gui.resources;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.GUIConfiguration;

import java.util.HashMap;
import java.util.Map;

public class TextureManager {

    private static TextureManager instance;

    public static TextureManager instance() {
        return instance;
    }

    private BitmapFont font;
    private TextureAtlas atlas;

    private Map<String, TextureRegion> textureRegions = new HashMap<String, TextureRegion>();

    public boolean isLoading = true;
    private GUIConfiguration config;

    public TextureManager(GUIConfiguration config) {
        instance = this;
        this.config = config;
    }

    public TextureRegion getTextureRegion(String name) {
        return textureRegions.get(name);
    }

    public BitmapFont getFont() {
        return font;
    }

    public void load() {
        loadAtlas();
        loadFont(getTextureRegion(GUIConfiguration.fontTextureRegionName));

        isLoading = false;
    }

    private void loadAtlas() {

        atlas = new TextureAtlas(Gdx.files.internal(config.textureAtlasFilePath));

        for (Texture texture : atlas.getTextures()) {
            texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }

        for (TextureAtlas.AtlasRegion region : atlas.getRegions()) {
            textureRegions.put(region.name, region);
        }
    }

    private void loadFont(TextureRegion fontRegion) {

        BitmapFont.BitmapFontData data = new BitmapFont.BitmapFontData(Gdx.files.internal(GUIConfiguration.fontFntFilePath), true);

        font = new BitmapFont(data, fontRegion, false);

    }

    public void dispose() {
        atlas.dispose();
    }
}
