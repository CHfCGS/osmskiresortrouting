package lib.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.RenderableProvider;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;

public class Renderer3D {

    private Renderer3D() {
    }

    private static Renderer3D instance;

    public static Renderer3D instance() {
        if (instance == null)
            instance = new Renderer3D();

        return instance;
    }

    private ModelBatch modelBatch;
    private PerspectiveCamera camera;
    private Environment environment;

    /**
     * Initializes all variables required for 3d rendering
     */
    void setUp() {
        modelBatch = new ModelBatch();

        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.7f, 0.7f, 0.7f, -1f, 1f, -1f));

        camera = new PerspectiveCamera(70, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        camera.position.set(0, 0, 5_000f);
        camera.near = 0.1f;
        camera.far = 1_000_000f;
    }

    void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
    }

    void dispose() {
        modelBatch.dispose();
    }

    public static ModelBatch getModelBatch() {
        return instance.modelBatch;
    }

    public static Camera getCamera() {
        return instance.camera;
    }

    public static Environment getEnvironment() {
        return instance.environment;
    }

    /**
     * This method can be used to render a 3d object.
     * The environment from Renderer3D will be used.
     */
    public static void render(RenderableProvider renderableProvider) {
        instance.modelBatch.render(renderableProvider, instance.environment);
    }
}
