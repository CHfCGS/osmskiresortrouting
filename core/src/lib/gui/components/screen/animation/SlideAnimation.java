package lib.gui.components.screen.animation;

import com.badlogic.gdx.Gdx;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.GUIManager;
import lib.gui.components.screen.Screen;
import lib.gui.util.DynamicFrameBuffer;

public class SlideAnimation implements ScreenAnimation {

    private int animationValue;
    private final int maxAnimationValue;

    private boolean invertAnimation;

    private static DynamicFrameBuffer frameBuffer;

    public SlideAnimation() {
        this(false);
    }

    public SlideAnimation(boolean invertAnimation) {
        this(invertAnimation, 175);
    }

    public SlideAnimation(boolean invertAnimation, int animationLengthInMs) {
        this.invertAnimation = invertAnimation;

        maxAnimationValue = animationLengthInMs;
        animationValue = 0;

        createNewFrameBufferIfNeeded();
    }

    private static void createNewFrameBufferIfNeeded() {
        if (SlideAnimation.frameBuffer == null) {
            SlideAnimation.frameBuffer = new DynamicFrameBuffer();
        }
    }

    @Override
    public void renderAnimation(Screen oldScreen, Screen currentScreen) {

        if (invertAnimation) {
            currentScreen.renderComplex();
        } else {
            oldScreen.renderComplex();
        }

        double animationProgress = animationValue / (double) maxAnimationValue;

        if (invertAnimation) {
            animationProgress = 1 - animationProgress;
        }

        frameBuffer.begin();

        GUIConfiguration config = GUIManager.instance().getGUIConfig();
        Draw.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), config.backgroundColor);

        if (invertAnimation) {
            oldScreen.renderComplex();
        } else {
            currentScreen.renderComplex();
        }

        frameBuffer.end();

        int yValue = (int) (Math.sin((Math.PI / 2.0) * animationProgress) * Gdx.graphics.getHeight()) - Gdx.graphics.getHeight();

        frameBuffer.drawFrameBufferTexture(0, yValue);
    }

    @Override
    public void finishAnimation() {
        // Reset values
        animationValue = 0;
    }

    @Override
    public void update(long deltaTimeInMs) {

        if (animationValue < maxAnimationValue) {
            animationValue += deltaTimeInMs;

            if (animationValue > maxAnimationValue) {
                animationValue = maxAnimationValue;
            }
        }
    }

    @Override
    public boolean isAnimationActive() {
        return animationValue < maxAnimationValue;
    }

    @Override
    public ScreenAnimation invertAnimation() {
        return new SlideAnimation(!invertAnimation);
    }
}
