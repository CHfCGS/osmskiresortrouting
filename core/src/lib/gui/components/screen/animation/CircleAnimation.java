package lib.gui.components.screen.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.GUIManager;
import lib.gui.Renderer2D;
import lib.gui.components.UIComponent;
import lib.gui.components.screen.Screen;
import lib.gui.layout.values.DynamicValue;

public class CircleAnimation implements ScreenAnimation {

    private final int animationCenterX;
    private final int animationCenterY;
    private final boolean invertAnimation;

    private int animationProgress;
    private final int animationMax;

    public CircleAnimation() {
        this(false);
    }

    public CircleAnimation(boolean invertAnimation) {
        this(DynamicValue.SCREEN_W.value() / 2, DynamicValue.SCREEN_H.value() / 2, invertAnimation);
    }

    public CircleAnimation(float animationCenterX, float animationCenterY) {
        this(animationCenterX, animationCenterY, false);
    }

    public CircleAnimation(float animationCenterX, float animationCenterY, boolean invertAnimation) {
        this(animationCenterX, animationCenterY, invertAnimation, 250);
    }

    public CircleAnimation(UIComponent component, boolean invertAnimation) {
        this(component.getEventScreenX(), component.getEventScreenY(), invertAnimation, 250);
    }

    public CircleAnimation(float animationCenterX, float animationCenterY, boolean invertAnimation, int animationLengthInMs) {
        this.animationCenterX = (int) animationCenterX;
        this.animationCenterY = (int) animationCenterY;
        this.invertAnimation = invertAnimation;

        animationMax = animationLengthInMs;

        animationProgress = animationMax;
    }

    @Override
    public void renderAnimation(Screen oldScreen, Screen currentScreen) {

        Batch batch = Renderer2D.instance().getBatch();
        ShapeRenderer shapeRenderer = Renderer2D.instance().getShapeRenderer();

        batch.end();

        glOptions();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        shapeRenderer.end();

        glOptions2();

        batch.begin();

        if (invertAnimation) {
            currentScreen.renderComplex();
        } else {
            oldScreen.renderComplex();
        }

        batch.end();

        glOptions();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.circle(
                animationCenterX, animationCenterY, (int) (Gdx.graphics.getWidth() * getAnimationRadius()));
        shapeRenderer.end();

        glOptions2();

        batch.begin();

        GUIConfiguration config = GUIManager.instance().getGUIConfig();

        Draw.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), config.backgroundColor);

        if (invertAnimation) {
            oldScreen.renderComplex();
        } else {
            currentScreen.renderComplex();
        }

    }

    @Override
    public void finishAnimation() {

        Batch batch = Renderer2D.instance().getBatch();

        batch.end();

        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);

        batch.begin();

        // Reset values
        animationProgress = animationMax;
    }

    @Override
    public void update(long deltaTimeInMs) {
        animationProgress -= deltaTimeInMs;

        if (animationProgress < 0)
            animationProgress = 0;
    }

    private double getAnimationRadius() {
        if (invertAnimation) {
            return Math.cos((animationProgress) / (double) animationMax * Math.PI / 2.0) * -1 + 1;
        }

        return Math.cos((animationMax - animationProgress) / (double) animationMax * Math.PI / 2.0) * -1 + 1;
    }

    @Override
    public boolean isAnimationActive() {
        return animationProgress > 0;
    }

    @Override
    public ScreenAnimation invertAnimation() {
        return this;
    }

    private void glOptions() {
        Gdx.gl.glClearDepthf(1f);
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glDepthFunc(GL20.GL_LESS);
        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl.glDepthMask(true);
        Gdx.gl.glColorMask(false, false, false, false);
    }

    private void glOptions2() {
        Gdx.gl.glColorMask(true, true, true, true);
        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl.glDepthFunc(GL20.GL_EQUAL);
    }
}
