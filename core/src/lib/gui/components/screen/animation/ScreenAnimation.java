package lib.gui.components.screen.animation;

import lib.gui.components.screen.Screen;

public interface ScreenAnimation {

    void renderAnimation(Screen oldScreen, Screen currentScreen);

    void finishAnimation();

    void update(long deltaTimeInMs);

    boolean isAnimationActive();

    ScreenAnimation invertAnimation();
}