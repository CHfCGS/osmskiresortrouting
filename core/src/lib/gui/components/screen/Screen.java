package lib.gui.components.screen;

import lib.gui.annotations.AlwaysOpen;
import lib.gui.annotations.ZLayer;
import lib.gui.components.ComplexUIComponent;
import lib.gui.components.UIComponent;
import lib.gui.components.panel.PopupPanel;
import lib.gui.layout.values.DynamicValue;
import lib.gui.layout.values.FixedValue;

public abstract class Screen extends ComplexUIComponent {

    boolean alwaysOpen;
    int screenZLayer = 0;

    protected Screen() {

        // Set screen dimensions
        x = FixedValue.ZERO;
        y = FixedValue.ZERO;
        w = DynamicValue.SCREEN_W;
        h = DynamicValue.SCREEN_H;

        // Check for AlwaysOpen annotation
        if (this.getClass().isAnnotationPresent(AlwaysOpen.class)) {
            alwaysOpen = true;
        }

        // Get screenZLayer of screen from ZLayer annotation
        if (this.getClass().isAnnotationPresent(ZLayer.class)) {
            ZLayer zLayerAnnotation = this.getClass().getAnnotation(ZLayer.class);
            screenZLayer = zLayerAnnotation.value();
        }

        if (screenZLayer < 0) {
            throw new RuntimeException("A negative ZLayer for screens is not supported yet");
        }

        // Register screen
        Screens.registerScreen(this);
    }

    public boolean onBackPressed() {
        return false;
    }

    public void screenOpened() {
    }

    public void screenOpenedAfterAnimation() {
    }

    public void screenClosed() {
    }

    boolean onBackPressedScreen() {

        for(UIComponent component : allUiComponents) {
            if(component instanceof PopupPanel) {
                PopupPanel popupPanel = (PopupPanel) component;

                if(popupPanel.isOpen() && popupPanel.onBackPressed()) return true;
            }
        }

        return onBackPressed();
    }
}
