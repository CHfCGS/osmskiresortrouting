package lib.gui.components.screen;

import lib.gui.annotations.StartScreen;
import lib.gui.components.screen.animation.ScreenAnimation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Screens {

    private static List<Screen> alwaysOpenScreens = new ArrayList<>();
    private static Map<Class<? extends Screen>, Screen> screens = new HashMap<>();

    private static Screen currentScreen;
    private static Screen oldScreen;

    private static ScreenAnimation currentAnimation;
    private static ScreenAnimation currentAnimationBackup;

    static void registerScreen(Screen screen) {
        screens.put(screen.getClass(), screen);

        // In case there is no explicit @StartScreen
        if (currentScreen == null) {
            currentScreen = screen;
        }

        // Set screen marked as @StartScreen as current screen
        if (screen.getClass().isAnnotationPresent(StartScreen.class)) {

            if (screen.alwaysOpen) {
                throw new RuntimeException("A screen tagged as AlwaysOpen cannot be a StartScreen");
            }

            currentScreen = screen;
        }

        // Handle case that screen is always open
        if (screen.alwaysOpen) {
            alwaysOpenScreens.add(screen);

            // Sort by screenZLayer ascending
            Collections.sort(alwaysOpenScreens, (s1, s2) -> s1.screenZLayer - s2.screenZLayer);
        }
    }

    public static void open(Class<? extends Screen> screenClass) {
        open(screenClass, null);
    }

    public static void open(Screen screen) {
        open(screen, null);
    }

    public static void open(Class<? extends Screen> screenClass, ScreenAnimation animation) {
        open(get(screenClass), animation);
    }

    public static void open(Screen screen, ScreenAnimation animation) {
        screen.init();

        oldScreen = currentScreen;
        currentAnimation = animation;
        currentAnimationBackup = animation;

        currentScreen = screen;

        if (oldScreen != null) {
            oldScreen.screenClosed();
        }

        currentScreen.screenOpened();

        if (currentAnimation == null) {
            currentScreen.screenOpenedAfterAnimation();
        }
    }

    /**
     * Returns to the last screen.
     * <p>
     * If there is no last screen,
     * nothing happens.
     */
    public static void closeCurrentScreen() {
        if (oldScreen != null) {
            Screen old = oldScreen;
            oldScreen = currentScreen;

            if (currentAnimationBackup == null) {
                open(old, null);
            } else {
                open(old, currentAnimationBackup.invertAnimation());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends Screen> T get(Class<T> screenClass) {

        if (screens.containsKey(screenClass)) {
            return (T) screens.get(screenClass);
        }

        try {
            return screenClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        throw new RuntimeException("Cannot find screen object and unable to create a new one");
    }

    public static boolean isOpen(Class<? extends Screen> screenClass) {
        return currentScreen.getClass() == screenClass;
    }

    public static boolean isOpen(Screen screen) {
        return currentScreen == screen;
    }

    public static void render() {
        if (currentAnimation != null) {

            currentAnimation.renderAnimation(oldScreen, currentScreen);

            if (!currentAnimation.isAnimationActive()) {
                currentAnimation.finishAnimation();
                currentAnimation = null;

                currentScreen.screenOpenedAfterAnimation();
            }
        } else {
            currentScreen.renderComplex();
        }

        for (Screen screen : alwaysOpenScreens) {
            if (screen.isVisible()) {
                screen.renderComplex();
            }
        }
    }

    public static void update(long deltaTimeInMs) {
        currentScreen.init();

        if (currentScreen != null) {
            currentScreen.updateComplex(deltaTimeInMs);
        }

        for (Screen screen : alwaysOpenScreens) {
            if (screen.isVisible()) {
                screen.init();
                screen.updateComplex(deltaTimeInMs);
            }
        }

        if (currentAnimation != null) {
            currentAnimation.update(deltaTimeInMs);
        }
    }

    public static boolean keyTyped(char character) {

        if (currentScreen != null) {
            currentScreen.init();
            currentScreen.keyTypedComplex(character);
            return true;
        }

        return false;
    }

    public static boolean touchDown(int x, int y, int pointer) {
        if (currentScreen != null) {
            currentScreen.init();
            currentScreen.touchDownComplex(x, y, pointer);
            return true;
        }

        return false;
    }

    public static boolean touchUp(int x, int y, int pointer) {
        if (currentScreen != null) {
            currentScreen.touchUpComplex(x, y, pointer);
            return true;
        }

        return false;
    }

    public static boolean touchDragged(int x, int y, int pointer) {
        if (currentScreen != null) {
            currentScreen.touchDraggedComplex(x, y, pointer);
            return true;
        }

        return false;
    }

    public static boolean mouseMoved(int x, int y) {
        if (currentScreen != null) {
            currentScreen.init();
            currentScreen.mouseMovedComplex(x, y);
            return true;
        }

        return false;
    }

    public static boolean scrolled(int amount) {
        if (currentScreen != null) {
            currentScreen.init();
            currentScreen.scrolledComplex(amount);
            return true;
        }

        return false;
    }

    public static boolean onBackPressed() {
        if (currentScreen != null) {
            currentScreen.init();
            if (currentScreen.onBackPressedScreen()) return true;
        }

        return false;
    }
}
