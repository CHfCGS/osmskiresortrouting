package lib.gui.components;

import lib.gui.Draw;
import lib.gui.annotations.ProcessAllInput;
import lib.gui.annotations.Visible;
import lib.gui.annotations.ZLayer;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.AnnotationPosition;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Size;
import lib.gui.annotations.position.Width;
import lib.gui.annotations.text.TextKey;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.panel.Panel;
import lib.gui.layout.values.DynamicValue;
import lib.gui.layout.values.FixedValue;
import lib.gui.resources.Text;
import lib.gui.util.ReflectionUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class ComplexUIComponent extends UIComponent {

    /**
     * A list of all child components and this object
     */
    protected List<UIComponent> allUiComponents;

    private boolean isMouseInComponent;

    /**
     * Must be called before calling any other method like render
     */
    public final void init() {
        if (allUiComponents == null) {
            allUiComponents = new ArrayList<>();

            List<Field> uiComponentFields = ReflectionUtil.getUIComponentsOfClass(this.getClass());

            try {
                for (int i = 0; i < uiComponentFields.size(); i++) {
                    Field uiComponentField = uiComponentFields.get(i);

                    // Get zLayer of ui component from ZLayer annotation
                    int zLayerFromAnnotation = 0;

                    if (uiComponentField.isAnnotationPresent(ZLayer.class)) {
                        ZLayer zLayerAnnotation = uiComponentField.getAnnotation(ZLayer.class);
                        zLayerFromAnnotation = zLayerAnnotation.value();
                    }

                    // Get text for ui component
                    Text text = new Text();

                    if (uiComponentField.isAnnotationPresent(TextValue.class)) {
                        TextValue textValue = uiComponentField.getAnnotation(TextValue.class);
                        text.setValue(textValue.value());
                    }

                    if (uiComponentField.isAnnotationPresent(TextKey.class)) {
                        TextKey textKey = uiComponentField.getAnnotation(TextKey.class);
                        text.setKey(textKey.value());
                    }

                    // Check for isVisible annotation
                    boolean hasIsVisibleAnnotation = false;
                    boolean isVisibleFromAnnotation = true;

                    if (uiComponentField.isAnnotationPresent(Visible.class)) {
                        Visible visibleAnnotation = uiComponentField.getAnnotation(Visible.class);
                        isVisibleFromAnnotation = visibleAnnotation.value();
                        hasIsVisibleAnnotation = true;
                    }

                    boolean processAllInputAnnotationValue = false;

                    if (uiComponentField.isAnnotationPresent(ProcessAllInput.class)) {
                        processAllInputAnnotationValue = true;
                    }

                    // Update positions
                    DynamicValue posX = FixedValue.ZERO;
                    DynamicValue posY = FixedValue.ZERO;
                    DynamicValue width = FixedValue.ZERO;
                    DynamicValue height = FixedValue.ZERO;

                    boolean widthOrHeightSet = false;

                    if (uiComponentField.isAnnotationPresent(Width.class)) {
                        width = AnnotationPosition.getValue(uiComponentField.getAnnotation(Width.class), this);
                        widthOrHeightSet = true;
                    }

                    if (uiComponentField.isAnnotationPresent(Height.class)) {
                        height = AnnotationPosition.getValue(uiComponentField.getAnnotation(Height.class), this);
                        widthOrHeightSet = true;
                    }

                    if (uiComponentField.isAnnotationPresent(Size.class)) {

                        if (widthOrHeightSet) {
                            throw new RuntimeException("An UIComponent with the Size annotation must not have a Width or Height annotation");
                        }

                        width = AnnotationPosition.getValue(uiComponentField.getAnnotation(Size.class), this);
                        height = width;
                    }

                    if (uiComponentField.isAnnotationPresent(PosX.class)) {
                        posX = AnnotationPosition.getValue(uiComponentField.getAnnotation(PosX.class), this, width);
                    }

                    if (uiComponentField.isAnnotationPresent(PosY.class)) {
                        posY = AnnotationPosition.getValue(uiComponentField.getAnnotation(PosY.class), this, height);
                    }

                    // Add ui component(s) to list
                    if (uiComponentField.getType().isArray()) {

                        UIComponent[] uiComponents = (UIComponent[]) uiComponentField.get(this);

                        for (UIComponent uiComponent : uiComponents) {
                            uiComponent.zLayer = zLayerFromAnnotation;
                            uiComponent.processAllInput = processAllInputAnnotationValue;
                            if (uiComponent.componentText == null) {
                                uiComponent.componentText = text;
                            }
                            if (hasIsVisibleAnnotation) {
                                uiComponent.setVisible(isVisibleFromAnnotation);
                            }

                            if (uiComponent.x == null) uiComponent.x = posX;
                            if (uiComponent.y == null) uiComponent.y = posY;
                            if (uiComponent.w == null) uiComponent.w = width;
                            if (uiComponent.h == null) uiComponent.h = height;

                            allUiComponents.add(uiComponent);
                        }

                    } else {

                        UIComponent uiComponent = (UIComponent) uiComponentField.get(this);
                        uiComponent.zLayer = zLayerFromAnnotation;
                        uiComponent.processAllInput = processAllInputAnnotationValue;
                        if (uiComponent.componentText == null) {
                            uiComponent.componentText = text;
                        }
                        if (hasIsVisibleAnnotation) {
                            uiComponent.setVisible(isVisibleFromAnnotation);
                        }

                        if (uiComponent.x == null) uiComponent.x = posX;
                        if (uiComponent.y == null) uiComponent.y = posY;
                        if (uiComponent.w == null) uiComponent.w = width;
                        if (uiComponent.h == null) uiComponent.h = height;

                        allUiComponents.add(uiComponent);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

            // Add this component to execute overridden methods
            final UIComponent thisComponent = this;

            // Check for zLayer on render method
            for (Method method : this.getClass().getMethods()) {
                if (method.isAnnotationPresent(ZLayer.class)) {
                    ZLayer zLayerAnnotation = method.getAnnotation(ZLayer.class);
                    thisComponent.zLayer = zLayerAnnotation.value();
                }
            }

            allUiComponents.add(thisComponent);

            // Sort by zLayer ascending
            Collections.sort(allUiComponents, (c1, c2) -> {
                // overridden render() method will be called first
                if (c1.zLayer == c2.zLayer) {
                    if (c1 == thisComponent) return -1;
                    if (c2 == thisComponent) return 1;
                }

                return c1.zLayer - c2.zLayer;
            });

            for (UIComponent component : allUiComponents) {
                if (component instanceof ComplexUIComponent && component != this) {
                    component.zLayer = 0;
                    ((ComplexUIComponent) component).init();
                }

                if(!(component instanceof ComplexUIComponent) || component == this) {
                    component.componentInitialized();
                }
            }
        }
    }

    public final void renderComplex() {
        for (UIComponent uiComponent : allUiComponents) {
            if (uiComponent.isVisible()) {

                Draw.addComponentTranslation(uiComponent.x.value(), uiComponent.y.value());

                if (uiComponent == this) {

                    Draw.addComponentTranslation(-this.x.value(), -this.y.value());

                    if (uiComponent instanceof Panel) {
                        ((Panel) uiComponent).renderPanel();
                    } else {
                        uiComponent.render();
                    }

                    Draw.addComponentTranslation(this.x.value(), this.y.value());

                } else if (uiComponent instanceof ComplexUIComponent) {
                    ((ComplexUIComponent) uiComponent).renderComplex();
                } else {
                    uiComponent.render();
                }

                Draw.addComponentTranslation(-uiComponent.x.value(), -uiComponent.y.value());
            }
        }
    }

    public final void updateComplex(long deltaTimeInMs) {
        for (UIComponent uiComponent : allUiComponents) {
            if (uiComponent.isVisible()) {
                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    ((ComplexUIComponent) uiComponent).updateComplex(deltaTimeInMs);
                } else {

                    boolean isLongPress = false;

                    if (uiComponent.longPressIsActive && (System.currentTimeMillis() - uiComponent.lastTouchDownTime) >= uiComponent.longPressDuration) {

                        if (uiComponent.distanceFulfillsLongPress(uiComponent.lastTouchDraggedX, uiComponent.lastTouchDraggedY)) {
                            isLongPress = true;
                        }

                        uiComponent.longPressIsActive = false;
                    }

                    if (uiComponent instanceof Panel) {
                        if (isLongPress) {
                            ((Panel) uiComponent).longPressPanel(uiComponent.lastTouchDraggedX, uiComponent.lastTouchDraggedY);
                        }

                        ((Panel) uiComponent).updatePanel(deltaTimeInMs);
                    } else {
                        if (isLongPress) {
                            uiComponent.longPress(uiComponent.lastTouchDraggedX, uiComponent.lastTouchDraggedY);
                        }

                        uiComponent.update(deltaTimeInMs);
                    }
                }
            }
        }
    }

    public final boolean keyTypedComplex(char character) {
        for (int i = allUiComponents.size() - 1; i >= 0; i--) {
            UIComponent uiComponent = allUiComponents.get(i);
            if (uiComponent.isVisible()) {

                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    if (((ComplexUIComponent) uiComponent).keyTypedComplex(character)) return true;
                } else {
                    if (uiComponent.keyTyped(character)) return true;
                }
            }
        }

        return false;
    }

    public final boolean touchDownComplex(int x, int y, int pointer) {

        for (int i = allUiComponents.size() - 1; i >= 0; i--) {
            UIComponent uiComponent = allUiComponents.get(i);
            if (uiComponent.isVisible()) {

                int localX = x;
                int localY = y;

                if (uiComponent != this) {
                    localX -= uiComponent.x.value();
                    localY -= uiComponent.y.value();
                }

                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    if (((ComplexUIComponent) uiComponent).touchDownComplex(localX, localY, pointer))
                        return true;
                } else {
                    if (uiComponent.processAllInput || uiComponent.isWithinComponent(localX, localY)) {

                        uiComponent.lastTouchDownTime = System.currentTimeMillis();
                        uiComponent.lastTouchDownX = localX;
                        uiComponent.lastTouchDownY = localY;
                        uiComponent.lastTouchDraggedX = localX;
                        uiComponent.lastTouchDraggedY = localY;
                        uiComponent.longPressIsActive = true;

                        if (uiComponent instanceof Panel) {
                            if (((Panel) uiComponent).touchDownPanel(localX, localY, pointer))
                                return true;
                        } else {
                            if (uiComponent.touchDown(localX, localY, pointer)) return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public final boolean touchUpComplex(int x, int y, int pointer) {
        for (int i = allUiComponents.size() - 1; i >= 0; i--) {
            UIComponent uiComponent = allUiComponents.get(i);
            if (uiComponent.isVisible()) {

                int localX = x;
                int localY = y;

                if (uiComponent != this) {
                    localX -= uiComponent.x.value();
                    localY -= uiComponent.y.value();
                }

                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    if (((ComplexUIComponent) uiComponent).touchUpComplex(localX, localY, pointer))
                        return false; //return true;
                } else {
                    boolean isDoubleClick = (System.currentTimeMillis() - uiComponent.lastTouchUpTime) <= uiComponent.doubleClickIntervalInMs
                            && uiComponent.distanceFulfillsDoubleClick(localX, localY);

                    uiComponent.lastTouchUpTime = System.currentTimeMillis();
                    uiComponent.lastTouchUpX = localX;
                    uiComponent.lastTouchUpY = localY;
                    uiComponent.longPressIsActive = false;

                    if (uiComponent instanceof Panel) {
                        if (isDoubleClick) {
                            ((Panel) uiComponent).doubleClickPanel(localX, localY, pointer);
                        }

                        /*if(*/
                        ((Panel) uiComponent).touchUpPanel(localX, localY, pointer);/*) return true; */
                    } else {
                        if (isDoubleClick) {
                            uiComponent.doubleClick(localX, localY, pointer);
                        }
                        if (uiComponent.touchUp(localX, localY, pointer))
                            return false; //return true;
                    }
                }
            }
        }

        return false;
    }

    public final boolean touchDraggedComplex(int x, int y, int pointer) {
        for (int i = allUiComponents.size() - 1; i >= 0; i--) {
            UIComponent uiComponent = allUiComponents.get(i);

            if (uiComponent.isVisible()) {

                int localX = x;
                int localY = y;

                if (uiComponent != this) {
                    localX -= uiComponent.x.value();
                    localY -= uiComponent.y.value();
                }

                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    if (((ComplexUIComponent) uiComponent).touchDraggedComplex(localX, localY, pointer))
                        return true;
                } else {
                    uiComponent.lastTouchDraggedX = localX;
                    uiComponent.lastTouchDraggedY = localY;

                    if (uiComponent instanceof Panel) {
                        /*if(*/
                        ((Panel) uiComponent).touchDraggedPanel(localX, localY, pointer);/*) return true; */
                    } else {
                        if (uiComponent.touchDragged(localX, localY, pointer)) return true;
                    }
                }
            }
        }

        return false;
    }

    public final boolean mouseMovedComplex(int x, int y) {

        isMouseInComponent = isWithinComponent(x, y);

        for (int i = allUiComponents.size() - 1; i >= 0; i--) {
            UIComponent uiComponent = allUiComponents.get(i);
            if (uiComponent.isVisible()) {

                int localX = x;
                int localY = y;

                if (uiComponent != this) {
                    localX -= uiComponent.x.value();
                    localY -= uiComponent.y.value();
                }

                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    if (((ComplexUIComponent) uiComponent).mouseMovedComplex(localX, localY))
                        return true;
                } else {
                    if (uiComponent.mouseMoved(localX, localY)) return true;
                }
            }
        }

        return false;
    }

    public final boolean scrolledComplex(int amount) {
        for (int i = allUiComponents.size() - 1; i >= 0; i--) {
            UIComponent uiComponent = allUiComponents.get(i);
            if (uiComponent.isVisible()) {

                if (uiComponent instanceof ComplexUIComponent && uiComponent != this) {
                    if (((ComplexUIComponent) uiComponent).scrolledComplex(amount)) return true;
                } else {

                    if (uiComponent instanceof Panel) {
                        /*if(*/
                        ((Panel) uiComponent).scrolledPanel(amount);/*) return true; */
                    } else {
                        if (uiComponent.scrolled(amount)) return true;
                    }

                }
            }
        }

        return false;
    }

    protected final boolean isMouseInComplexComponent() {
        return isMouseInComponent;
    }
}
