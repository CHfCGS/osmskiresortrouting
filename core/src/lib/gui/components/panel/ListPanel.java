package lib.gui.components.panel;

import lib.gui.Draw;
import lib.gui.Renderer2D;

public abstract class ListPanel extends ScrollablePanel {

    private boolean isVerticalList;
    private boolean alignRightOrBottom;

    private int listSize;
    private float sizePerElement;
    private float padding;

    private final int NO_ITEM_PRESSED = -1;
    private int pressedListItemIndex = NO_ITEM_PRESSED;
    private int touchPosX;
    private int touchPosY;

    protected ListPanel(boolean vertical) {
        this(vertical, false);
    }

    protected ListPanel(boolean vertical, boolean alignRightOrBottom) {
        this.isVerticalList = vertical;
        this.alignRightOrBottom = alignRightOrBottom;
    }

    protected void updateListStatus(int listSize, float sizePerElement) {
        updateListStatus(listSize, sizePerElement, 0);
    }

    protected void updateListStatus(int listSize, float sizePerElement, float padding) {
        this.listSize = listSize;
        this.sizePerElement = sizePerElement;
        this.padding = padding;

        float contentSize = listSize * sizePerElement + (listSize - 1) * padding;

        if (isVerticalList) {
            notifyContentHeight((listSize == 1) ? sizePerElement : contentSize);
        } else {
            notifyContentWidth((listSize == 1) ? sizePerElement : contentSize);
        }
    }

    protected int getListSize() {
        return listSize;
    }

    protected float getSizePerElement() {
        return sizePerElement;
    }

    @Override
    public void renderPanel() {
        Draw.addComponentTranslation(xTranslation, yTranslation);

        renderBeforeClipping();

        Renderer2D.instance().setClipBounds(x.value(), y.value(), w.value(), h.value());

        renderBefore();
        render();
        renderAfter();

        Renderer2D.instance().resetClipBounds();

        renderAfterClipping();

        Draw.addComponentTranslation(-xTranslation, -yTranslation);
    }

    @Override
    protected final void render() {
        for (int i = 0; i < listSize; i++) {
            float itemPos = i * (sizePerElement + padding);

            if (isVerticalList) {
                if (alignRightOrBottom && !heightCanBeScrolled()) {
                    itemPos += h.value() - getContentHeight();
                }

                if (isWithinComponentWithTranslation(0, itemPos)
                        || isWithinComponentWithTranslation(0, itemPos + sizePerElement)) {

                    renderItem(i, 0, itemPos, w.value(), sizePerElement, pressedListItemIndex == i);
                }
            } else {

                if (alignRightOrBottom && !widthCanBeScrolled()) {
                    itemPos += w.value() - getContentWidth();
                }

                if (isWithinComponentWithTranslation(itemPos, 0)
                        || isWithinComponentWithTranslation(itemPos + sizePerElement, 0)) {

                    renderItem(i, itemPos, 0, sizePerElement, h.value(), pressedListItemIndex == i);
                }
            }
        }
    }

    public boolean isListItemPressed() {
        return getPressedListItemIndex() != NO_ITEM_PRESSED;
    }

    public int getPressedListItemIndex() {
        return pressedListItemIndex;
    }

    protected void renderBeforeClipping() {
    }

    protected void renderBefore() {
    }

    protected abstract void renderItem(int index, float iX, float iY, float iW, float iH, boolean isPressed);

    protected void renderAfter() {
    }

    protected void renderAfterClipping() {
    }


    protected void listItemSelected(int index) {
    }

    public boolean touchDownPanel(int x, int y, int pointer) {
        int listTouchPos;

        touchPosX = x;
        touchPosY = y;

        if (isVerticalList) {
            listTouchPos = y - (int) yTranslation;

            if (alignRightOrBottom && !heightCanBeScrolled())
                listTouchPos -= h.value() - getContentHeight();

            if(listTouchPos < 0 || listTouchPos > getContentHeight()) return false;
        } else {
            listTouchPos = x - (int) xTranslation;

            if (alignRightOrBottom && !widthCanBeScrolled())
                listTouchPos -= w.value() - getContentWidth();

            if(listTouchPos < 0 || listTouchPos > getContentWidth()) return false;
        }

        boolean isInPaddingGap = false;

        if (listSize <= 1 || padding == 0) {
            pressedListItemIndex = (int) (listTouchPos / sizePerElement);
        } else {
            pressedListItemIndex = (int) ((listTouchPos + padding) / (sizePerElement + padding));

            // Check for padding gaps
            if (listTouchPos < pressedListItemIndex * (sizePerElement + padding)) {
                isInPaddingGap = true;
            }
        }

        if (pressedListItemIndex >= listSize || pressedListItemIndex < 0) {
            pressedListItemIndex = NO_ITEM_PRESSED;
        }

        boolean isInList = pressedListItemIndex != NO_ITEM_PRESSED;

        if (isInPaddingGap) {
            pressedListItemIndex = NO_ITEM_PRESSED;
        }

        return super.touchDownPanel(x, y, pointer) || isInList /*|| isWithinComponent(x, y)*/;
    }

    public boolean touchUpPanel(int x, int y, int pointer) {
        int pressedListItemIndexNew;

        int listTouchPos;

        if (isVerticalList) {
            listTouchPos = y - (int) yTranslation;

            if (alignRightOrBottom && !heightCanBeScrolled())
                listTouchPos -= h.value() - getContentHeight();
        } else {
            listTouchPos = x - (int) xTranslation;

            if (alignRightOrBottom && !widthCanBeScrolled())
                listTouchPos -= w.value() - getContentWidth();
        }

        if (listSize <= 1 || padding == 0) {
            pressedListItemIndexNew = (int) (listTouchPos / sizePerElement);
        } else {
            pressedListItemIndexNew = (int) ((listTouchPos + padding) / (sizePerElement + padding));

            // Check for padding gaps
            if (listTouchPos < pressedListItemIndexNew * (sizePerElement + padding)) {
                pressedListItemIndexNew = NO_ITEM_PRESSED;
            }
        }

        if (isListItemPressed() && pressedListItemIndex == pressedListItemIndexNew) {
            listItemSelected(pressedListItemIndex);
        }

        boolean result = super.touchUpPanel(x, y, pointer);
        pressedListItemIndex = -1;

        return result /*|| isWithinComponent(x, y)*/;
    }

    public boolean touchDraggedPanel(int x, int y, int pointer) {
        return super.touchDraggedPanel(x, y, pointer) /*|| isWithinComponent(x, y)*/;
    }

    public int getTouchPosX() {
        return touchPosX;
    }

    public int getTouchPosY() {
        return touchPosY;
    }
}
