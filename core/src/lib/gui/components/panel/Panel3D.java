package lib.gui.components.panel;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import lib.gui.Renderer2D;
import lib.gui.Renderer3D;

public abstract class Panel3D extends Panel {

    // --- debug mode ---
    private boolean debugMode;
    private ModelInstance arrow1;
    private ModelInstance arrow2;
    private ModelInstance arrow3;
    private ModelInstance light;

    protected Panel3D() {
        this(false);
    }

    protected Panel3D(boolean debugMode) {
        this.debugMode = debugMode;

        if (debugMode) {
            long attributes = VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal;

            Material matR = new Material(ColorAttribute.createDiffuse(Color.RED));
            Material matG = new Material(ColorAttribute.createDiffuse(Color.GREEN));
            Material matB = new Material(ColorAttribute.createDiffuse(Color.BLUE));
            Material matY = new Material(ColorAttribute.createDiffuse(Color.YELLOW));

            ModelBuilder builder = new ModelBuilder();
            arrow1 = new ModelInstance(builder.createArrow(new Vector3(0, 0, 0), new Vector3(1, 0, 0), matR, attributes));
            arrow2 = new ModelInstance(builder.createArrow(new Vector3(0, 0, 0), new Vector3(0, 1, 0), matG, attributes));
            arrow3 = new ModelInstance(builder.createArrow(new Vector3(0, 0, 0), new Vector3(0, 0, 1), matB, attributes));
            light = new ModelInstance(builder.createArrow(new Vector3(0, 0, 0), new Vector3(-1, 1, -1), matY, attributes));
        }
    }

    public void renderPanel() {
        render2DBefore();
        Renderer2D.getBatch().end();

        ModelBatch modelBatch = Renderer3D.getModelBatch();

        Renderer3D.getCamera().update();
        modelBatch.begin(Renderer3D.getCamera());

        if (debugMode) {
            Environment environment = Renderer3D.getEnvironment();
            modelBatch.render(arrow1, environment);
            modelBatch.render(arrow2, environment);
            modelBatch.render(arrow3, environment);
            modelBatch.render(light, environment);
        }

        render();
        modelBatch.end();

        Renderer2D.getBatch().begin();
        render2DAfter();
    }

    protected void render2DBefore() {
    }

    protected void render2DAfter() {
    }
}
