package lib.gui.components.panel;

import lib.gui.components.ComplexUIComponent;

public abstract class Panel extends ComplexUIComponent {

    public void renderPanel() {
        render();
    }

    public void updatePanel(long deltaTimeInMs) {
        update(deltaTimeInMs);
    }

    public boolean touchDownPanel(int x, int y, int pointer) {
        return touchDown(x, y, pointer);
    }

    public boolean touchUpPanel(int x, int y, int pointer) {
        return touchUp(x, y, pointer);
    }

    public boolean touchDraggedPanel(int x, int y, int pointer) {
        return touchDragged(x, y, pointer);
    }

    public boolean doubleClickPanel(int x, int y, int pointer) {
        return doubleClick(x, y, pointer);
    }

    public boolean longPressPanel(int x, int y) {
        return longPress(x, y);
    }

    public boolean scrolledPanel(int amount) {
        return scrolled(amount);
    }
}
