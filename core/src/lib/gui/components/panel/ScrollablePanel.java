package lib.gui.components.panel;

import com.badlogic.gdx.Gdx;
import lib.gui.Draw;
import lib.gui.Renderer2D;
import lib.gui.layout.values.DynamicValue;
import lib.gui.layout.values.Fraction;
import lib.gui.signals.UISignal;

public abstract class ScrollablePanel extends Panel {

    public DynamicValue dragUntilTouchUp = new Fraction(0.02, DynamicValue.SCREEN_H);

    private float contentWidth, contentHeight;
    private boolean scrollingEnabledX = true, scrollingEnabledY = true;

    float xTranslation, yTranslation;

    private float dragSpeedX, dragSpeedY;
    private float dragXSinceClick, dragYSinceClick;

    private int dragFingerPointer = -1;

    protected ScrollablePanel() {
        subscribeToSignal(UISignal.RESET_UI);
    }

    public void notifyContentSize(float width, float height) {
        this.contentWidth = width;
        this.contentHeight = height;
    }

    public void notifyContentWidth(float width) {
        this.contentWidth = width;
    }

    public void notifyContentHeight(float height) {
        this.contentHeight = height;
    }

    public void setScollingEnabledX(boolean scrollingEnabled) {
        this.scrollingEnabledX = scrollingEnabled;
    }

    public void setScollingEnabledY(boolean scrollingEnabled) {
        this.scrollingEnabledY = scrollingEnabled;
    }

    protected boolean isWithinComponentWithTranslation(float localX, float localY) {
        localX += xTranslation;
        localY += yTranslation;

        return isWithinComponent(localX, localY);
    }

    protected boolean widthCanBeScrolled() {
        return scrollingEnabledX && contentWidth > w.value();
    }

    protected boolean heightCanBeScrolled() {
        return scrollingEnabledY && contentHeight > h.value();
    }

    protected float getContentWidth() {
        return contentWidth;
    }

    protected float getContentHeight() {
        return contentHeight;
    }

    @Override
    protected void signalReceived(UISignal signal) {
        if (signal == UISignal.RESET_UI) {
            xTranslation = 0;
            yTranslation = 0;
            dragSpeedX = 0;
            dragSpeedY = 0;
        }
    }

    @Override
    public void renderPanel() {
        Draw.addComponentTranslation(xTranslation, yTranslation);

        Renderer2D.instance().setClipBounds(x.value(), y.value(), w.value(), h.value());

        render();

        Renderer2D.instance().resetClipBounds();

        Draw.addComponentTranslation(-xTranslation, -yTranslation);
    }

    @Override
    public void updatePanel(long deltaTimeInMs) {

        update(deltaTimeInMs);

        if (dragFingerPointer != -1) return;

        if (widthCanBeScrolled()) {
            dragSpeedX /= Math.pow(1.015f, deltaTimeInMs);
            xTranslation += dragSpeedX;

            float maxX = 0;
            float minX = w.value() - contentWidth;

            if (xTranslation > maxX) {
                float difference = xTranslation - maxX;

                xTranslation -= difference / 5 + DynamicValue.SCREEN_W.value() / 300f;

                if (xTranslation < maxX)
                    xTranslation = maxX;
            }

            if (xTranslation < minX) {
                float difference = minX - xTranslation;

                xTranslation += difference / 5 + DynamicValue.SCREEN_W.value() / 300f;

                if (xTranslation > minX)
                    xTranslation = minX;
            }

        } else {
            if (xTranslation != 0)
                xTranslation /= 1.2f;
        }

        if (heightCanBeScrolled()) {
            dragSpeedY /= Math.pow(1.015f, deltaTimeInMs);
            yTranslation += dragSpeedY;

            float maxY = 0;
            float minY = h.value() - contentHeight;

            if (yTranslation > maxY) {
                float difference = yTranslation - maxY;

                yTranslation -= difference / 5 + DynamicValue.SCREEN_H.value() / 300f;

                if (yTranslation < maxY)
                    yTranslation = maxY;
            }

            if (yTranslation < minY) {
                float difference = minY - yTranslation;

                yTranslation += difference / 5 + DynamicValue.SCREEN_H.value() / 300f;

                if (yTranslation > minY)
                    yTranslation = minY;
            }

        } else {
            if (yTranslation != 0)
                yTranslation /= 1.2f;
        }
    }

    @Override
    public boolean touchDownPanel(int x, int y, int pointer) {
        if (dragFingerPointer == -1) {
            dragFingerPointer = pointer;
            dragXSinceClick = 0;
            dragYSinceClick = 0;
        }

        return touchDown(x - (int) xTranslation, y - (int) yTranslation, pointer);
    }

    @Override
    public boolean touchUpPanel(int x, int y, int pointer) {

        if (isWithinComponent(x, y) || pointer == dragFingerPointer) {
            dragFingerPointer = -1;
            dragXSinceClick = 0;
            dragYSinceClick = 0;
        }

        return touchUp(x - (int) xTranslation, y - (int) yTranslation, pointer);
    }

    @Override
    public boolean doubleClickPanel(int x, int y, int pointer) {
        return doubleClick(x - (int) xTranslation, y - (int) yTranslation, pointer);
    }

    @Override
    public boolean longPressPanel(int x, int y) {
        return longPress(x - (int) xTranslation, y - (int) yTranslation);
    }

    @Override
    public boolean touchDraggedPanel(int x, int y, int pointer) {

        if (pointer != dragFingerPointer) return false;

        int deltaX = Gdx.input.getDeltaX(dragFingerPointer);
        int deltaY = Gdx.input.getDeltaY(dragFingerPointer);

        if (widthCanBeScrolled()) {
            xTranslation += deltaX;

            dragSpeedX += deltaX / 2f;
            dragXSinceClick += Math.abs(deltaX);

            if (dragXSinceClick > dragUntilTouchUp.value()) {
                touchUpComplex(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
            }
        }

        if (heightCanBeScrolled()) {
            yTranslation += deltaY;

            dragSpeedY += deltaY / 2;
            dragYSinceClick += Math.abs(deltaY);

            if (dragYSinceClick > dragUntilTouchUp.value()) {
                touchUpComplex(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE);
            }
        }

        return touchDragged(x - (int) xTranslation, y - (int) yTranslation, pointer);
    }

    @Override
    public boolean scrolledPanel(int amount) {
        return scrolled(amount);
    }
}
