package lib.gui.components.panel;

public class PopupPanel extends Panel {

    private boolean isOpen;

    public boolean onBackPressed() {
        return false;
    }

    public void open() {
        isOpen = true;
        setVisible(true);
    }

    public void close() {
        isOpen = false;
        setVisible(false);
    }

    public boolean isOpen() {
        return isOpen;
    }
}
