package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.Renderer2D;
import lib.gui.resources.Text;
import lib.gui.util.TextRenderer;

public class TextButton extends Button {

    private TextureRegion buttonTexture;
    private Color buttonColor = Color.BLUE;
    private Color pressedColor = Color.BLUE;

    public static final Color COLOR_BLUE = new Color(0f, 0.463f, 0.788f, 1f);
    public static final Color COLOR_GREEN = new Color(0f, 0.62f, 0.173f, 1f);
    public static final Color COLOR_RED = new Color(0.749f, 0f, 0f, 1f);

    private TextRenderer textRenderer;
    private float textScale = 1f;

    public TextButton() {
        setAnimation(ButtonAnimation.SCALE_ON_CLICK);
        textRenderer = new TextRenderer();
    }

    public TextButton(Color buttonColor) {
        this(buttonColor, new Color(buttonColor).mul(0.8f, 0.8f, 0.8f, 1f));
    }

    public TextButton(Color buttonColor, Color pressedColor) {
        setAnimation(ButtonAnimation.COLOR_ON_CLICK);
        this.buttonColor = buttonColor;
        this.pressedColor = pressedColor;
        buttonTexture = Renderer2D.getTextureManager().getTextureRegion(GUIConfiguration.simpleButtonTextureRegionName);

        textRenderer = new TextRenderer();
    }

    @Override
    protected void componentInitialized() {
        super.componentInitialized();
        textRenderer.setText(componentText);
    }

    public void setText(String value) {
        if (this.componentText == null) {
            this.componentText = new Text();
        }
        this.componentText.setValue(value);
    }

    public void setText(Text text) {
        this.componentText = text;
    }

    public void setTextColor(Color color) {
        textRenderer.setTextColor(color);
    }

    public void setTextScale(float scale) {
        this.textScale = scale;
    }

    public void underlineText(boolean underline) {
        textRenderer.underlineText(underline);
    }

    @Override
    public void render() {
        if (buttonTexture != null) {
            Draw.setColor((animation == ButtonAnimation.COLOR_ON_CLICK) && (isPressed || !isActive) ? pressedColor : buttonColor);

            Draw.imageNinePatch(buttonTexture, 0, 0, w.value(), h.value(), GUIConfiguration.simpleButtonNinePatchBorder,
                    h.value() * GUIConfiguration.simpleButtonNinePatchBorderSize);
            Draw.resetColor();
        }

        if (animation == ButtonAnimation.SCALE_ON_CLICK) {
            textRenderer.setTextScale(textScale * clickScale);
        } else {
            textRenderer.setTextScale(textScale);
        }

        textRenderer.setTextScale(textScale);
        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(h.value() * 0.4f);
        textRenderer.render(0, 0, w.value(), h.value());
    }
}
