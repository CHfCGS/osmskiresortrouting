package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import lib.gui.resources.Text;
import lib.gui.util.TextRenderer;

public class Label extends Button {

    private TextRenderer textRenderer;

    /**
     * In ratio of the label width
     */
    private float maxTextSizeW = 0.85f;

    /**
     * In ratio of the label height
     */
    private float maxTextSizeH = 0.4f;

    public Label() {
        textRenderer = new TextRenderer();
        consumeInput = false;
    }

    public Label(String text) {
        this();
        setText(text);
    }

    public Label(String text, Color textColor) {
        this();
        setText(text);
        setTextColor(textColor);
    }


    @Override
    protected void componentInitialized() {
        textRenderer.setText(componentText);
    }

    public void setText(String value) {
        if (this.componentText == null) {
            this.componentText = new Text();
        }
        this.componentText.setValue(value);
    }

    public void setText(Text text) {
        this.componentText = text;
    }

    public Text getText() {
        return this.componentText;
    }

    public void setTextColor(Color color) {
        textRenderer.setTextColor(color);
    }

    public void setTextScale(float scale) {
        textRenderer.setTextScale(scale);
    }

    /**
     * In ratio of the label width
     * <p>
     * textScale is applied after this limit
     * and ignores this value
     */
    public void setMaxTextSizeW(float maxTextSizeW) {
        this.maxTextSizeW = maxTextSizeW;
    }

    /**
     * In ratio of the label height
     * <p>
     * textScale is applied after this limit
     * and ignores this value
     */
    public void setMaxTextSizeH(float maxTextSizeH) {
        this.maxTextSizeH = maxTextSizeH;
    }

    @Override
    protected void render() {
        textRenderer.setWidth(w.value() * maxTextSizeW);
        textRenderer.setHeight(h.value() * maxTextSizeH);
        textRenderer.render(0, 0, w.value(), h.value());
    }
}