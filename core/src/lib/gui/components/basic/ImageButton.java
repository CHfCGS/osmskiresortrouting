package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.GUIManager;
import lib.gui.Renderer2D;

public class ImageButton extends Button {

    private TextureRegion textureRegion;
    private Color buttonColor = Color.WHITE;

    protected String textureRegionFolder;
    private String textureName;

    private float buttonRenderScale = 1f;

    public ImageButton(TextureRegion textureRegion) {
        this.textureRegion = textureRegion;
        this.animation = ButtonAnimation.SCALE_ON_CLICK;
    }

    public ImageButton(String textureName) {
        this(textureName, 1f, ButtonAnimation.SCALE_ON_CLICK);
    }

    public ImageButton(String textureName, float buttonImageScale) {
        this(textureName, buttonImageScale, ButtonAnimation.SCALE_ON_CLICK);
    }

    public ImageButton(String textureName, float buttonRenderScale, ButtonAnimation animation) {
        this.textureName = textureName;
        this.buttonRenderScale = buttonRenderScale;
        this.animation = animation;
    }

    public void updateTexture(String textureName){
        this.textureName = textureName;
        textureRegion = null;
    }

    public void setButtonColor(Color buttonColor) {
        this.buttonColor = buttonColor;
    }

    public void setRenderScale(float buttonRenderScale) {
        this.buttonRenderScale = buttonRenderScale;
    }

    @Override
    protected void render() {
        if (textureRegion == null) {
            if(textureRegionFolder == null) {
                textureRegionFolder = GUIManager.instance().getGUIConfig().imageButtonTextureFolder;
            }

            textureRegion = Renderer2D.getTextureManager().getTextureRegion(textureRegionFolder + "/" + textureName);
        }

        Draw.setColor(buttonColor);

        switch (animation) {
            case SCALE_ON_CLICK:
                Draw.image(textureRegion,0, 0, w.value(), h.value(), buttonRenderScale * clickScale);
                break;
            case COLOR_ON_CLICK:
            default:
                Draw.image(textureRegion,0, 0, w.value(), h.value(), buttonRenderScale);

        }

        Draw.setColor(Color.WHITE);
    }
}
