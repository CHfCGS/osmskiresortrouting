package lib.gui.components.basic;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import lib.gui.Draw;
import lib.gui.components.UIComponent;
import lib.gui.util.TextRenderer;

public class TextBox extends UIComponent {

    private static final int KEY_DELETE = 8;
    private static final int KEY_ENTER = 10;
    private static final int KEY_ENTER_DESKTOP = 13;

    private TextRenderer textRenderer;

    private StringBuilder input;
    private boolean isInputActive;

    private boolean isPressed;
    private Runnable keyTypedEvent;

    private String characterWhitelist;
    private int characterLimit = Integer.MAX_VALUE;

    private long totalDeltaTime;

    public TextBox() {
        input = new StringBuilder();
        textRenderer = new TextRenderer();
    }

    @Override
    protected void componentInitialized() {
        textRenderer.setText(this.componentText);
        textRenderer.moveTextWhenNecessary(true);
    }

    /**
     * @param characterWhitelist the white list encoded as string.
     *                           A valid input would be "0123456789".
     */
    public void setCharacterWhitelist(String characterWhitelist) {
        this.characterWhitelist = characterWhitelist;
    }

    public void setCharacterLimit(int characterLimit) {
        this.characterLimit = characterLimit;
    }

    public void setInput(String input) {
        clearInput();
        this.input.append(input);
        inputHasChanged();
    }

    public void clearInput() {
        this.input.setLength(0);
        inputHasChanged();
    }

    public String getInput() {
        return input.toString();
    }

    public void setKeyTypedEvent(Runnable keyTypedEvent) {
        this.keyTypedEvent = keyTypedEvent;
    }

    private void inputHasChanged() {
        this.componentText.setValue(input.toString());
    }

    @Override
    protected void render() {
        Draw.setColor(0.4f, 0.5f, 0.5f, 0.5f);
        Draw.roundedRect(0, 0, w.value(), h.value(), h.value() * 0.15f, 1f);

        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(h.value() * 0.4f);
        textRenderer.render(0, 0, w.value(), h.value());

        // Cursor
        if (isInputActive && showCursor()) {
            Draw.setColor(Color.WHITE);
            Draw.rect(textRenderer.getTextX() + textRenderer.getTextW() + textRenderer.getTextH() / 10f,
                    textRenderer.getTextY() - textRenderer.getTextH() * 0.05f,
                    textRenderer.getTextH() / 5f,
                    textRenderer.getTextH() * 1.1f);
        }
    }

    @Override
    protected void update(long deltaTimeInMs) {
        totalDeltaTime += deltaTimeInMs;
    }

    private boolean showCursor() {
        return MathUtils.sin(totalDeltaTime / 200f) > 0;
    }

    @Override
    protected boolean keyTyped(char character) {
        if (!isInputActive) return false;
        if(((int) character) == 0) return true;

        if (character == KEY_DELETE) {
            if (input.length() > 0) {
                input.deleteCharAt(input.length() - 1);
                inputHasChanged();
            }
        } else if (character == KEY_ENTER || character == KEY_ENTER_DESKTOP) {
            callEvent(0, 0);
            Gdx.input.setOnscreenKeyboardVisible(false);
            isInputActive = false;
        } else {
            characterTyped(character);
        }

        if (keyTypedEvent != null)
            keyTypedEvent.run();
        return true;
    }

    private void characterTyped(char character) {
        if (characterWhitelist != null) {
            // Character is not in whitelist
            if (characterWhitelist.indexOf(character) == -1) return;
        }

        if (input.length() >= characterLimit) {
            return;
        }

        if ((int) character != 0) {
            input.append(character);
            inputHasChanged();
        }
    }

    @Override
    protected boolean touchDown(int x, int y, int pointer) {
        if (isWithinComponent(x, y)) {
            isPressed = true;
            return true;
        }

        return false;
    }

    @Override
    protected boolean touchUp(int x, int y, int pointer) {
        if (isPressed && isWithinComponent(x, y)) {
            Gdx.input.setOnscreenKeyboardVisible(true);
            isInputActive = true;

            isPressed = false;
            return true;
        }

        if (!isPressed) {
            isInputActive = false;
            Gdx.input.setOnscreenKeyboardVisible(false);
        }

        isPressed = false;

        return false;
    }
}
