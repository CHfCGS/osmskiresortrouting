package lib.gui.components.basic;

import com.badlogic.gdx.Gdx;
import lib.gui.components.UIComponent;

public abstract class Button extends UIComponent {

    protected boolean consumeInput = true;
    protected boolean isActive = true;
    protected boolean isPressed = false;
    float clickScale = 1f;

    protected ButtonAnimation animation;

    public void setAnimation(ButtonAnimation animation) {
        this.animation = animation;
    }

    @Override
    protected boolean touchDown(int x, int y, int pointer) {
        if (isWithinComponent(x, y) && isActive) {
            isPressed = true;
            return consumeInput;
        }

        return false;
    }

    @Override
    protected boolean touchUp(int x, int y, int pointer) {
        if (isPressed && isActive && isWithinComponent(x, y)) {
            callEvent(Gdx.input.getX(pointer), Gdx.input.getY(pointer));

            isPressed = false;
            return consumeInput;
        }

        isPressed = false;
        return false;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public float getClickAnimationScale() {
        return clickScale;
    }

    public void setClickAnimationScale(float clickScale) {
        this.clickScale = clickScale;
    }

    @Override
    protected void update(long deltaTimeInMs) {
        final float minScale = 1f;
        final float maxScale = 1.12f;

        for (int i = 0; i < deltaTimeInMs; i++) {

            if (!isPressed && clickScale > minScale) {
                clickScale -= (clickScale - minScale) / 50f;
            } else if (clickScale < maxScale) {
                clickScale += (maxScale - clickScale) / 85f + 0.001f;
            }

        }
    }
}
