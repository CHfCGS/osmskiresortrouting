package lib.gui.components.basic;

import com.badlogic.gdx.graphics.Color;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.Size;
import lib.gui.annotations.position.Width;
import lib.gui.components.panel.Panel;

public class CheckBox extends Panel {

    @Size(h = 1)
    private final ImageButton imageButton;

    @PosX(h = 1)
    @Width(term = "w - h")
    @Height(h = 1)
    private final TextButton textButton;

    private boolean isChecked = true;

    public CheckBox(String text) {
        this();
        setText(text);
    }

    public CheckBox(String text, Color color) {
        this();
        setText(text);
        imageButton.setButtonColor(color);
        textButton.setTextColor(color);
    }

    public CheckBox() {
        imageButton = new ImageButton("checked");
        imageButton.setEvent(this::changeChecked);
        textButton = new TextButton();
        textButton.setEvent(this::changeChecked);
        imageButton.setButtonColor(Color.BLACK);
        textButton.setTextColor(Color.BLACK);
    }

    public void setText(String text) {
        textButton.setText(text);
    }

    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;

        if (isChecked) {
            imageButton.updateTexture("checked");
        } else {
            imageButton.updateTexture("unchecked");
        }
    }

    public boolean isChecked() {
        return isChecked;
    }

    private void changeChecked() {
        isChecked = !isChecked;
        setChecked(isChecked);
        callEvent(0,0);
    }

    @Override
    protected void update(long deltaTimeInMs) {
        if(textButton.isPressed) {
            imageButton.setClickAnimationScale(textButton.getClickAnimationScale());
        }
    }

    @Override
    protected void render() {
    }
}
