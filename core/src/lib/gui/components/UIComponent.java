package lib.gui.components;

import lib.gui.layout.values.DynamicValue;
import lib.gui.resources.Text;
import lib.gui.signals.UISignal;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class UIComponent {

    private static List<UIComponent> allUIComponents = new ArrayList<UIComponent>();

    public UIComponent() {
        allUIComponents.add(this);
    }

    int zLayer = 0;

    // --- Touch Interaction ---
    long lastTouchUpTime;
    int lastTouchUpX;
    int lastTouchUpY;
    protected int doubleClickIntervalInMs = 300;

    boolean longPressIsActive;

    long lastTouchDownTime;
    int lastTouchDownX;
    int lastTouchDownY;

    int lastTouchDraggedX;
    int lastTouchDraggedY;

    protected long longPressDuration = 1000;

    boolean distanceFulfillsDoubleClick (int localX, int localY) {
        float distance = (float) Math.sqrt(Math.pow(lastTouchUpX - localX, 2) + Math.pow(lastTouchUpY - localY, 2));
        return distance < DynamicValue.SCREEN_H.value() * 0.03f;
    }

    boolean distanceFulfillsLongPress (int localX, int localY) {
        float distance = (float) Math.sqrt(Math.pow(lastTouchDownX - localX, 2) + Math.pow(lastTouchDownY - localY, 2));
        return distance < DynamicValue.SCREEN_H.value() * 0.015f;
    }

    private boolean isVisible = true;

    /**
     * Can be used by components if needed
     * Use @TextKey("key") or @TextValue("value")
     * to set this text
     */
    protected Text componentText;

    protected boolean processAllInput;

    public DynamicValue x;
    public DynamicValue y;
    public DynamicValue w;
    public DynamicValue h;

    private Set<UISignal> subscribedSignalEvents = new HashSet<UISignal>();

    protected final void subscribeToSignal(UISignal signal) {
        subscribedSignalEvents.add(signal);
    }

    public static void broadcastSignal(UISignal signal) {
        for (UIComponent uiComponent : allUIComponents) {
            if (uiComponent.subscribedSignalEvents.contains(signal)) {
                uiComponent.signalReceived(signal);
            }
        }
    }

    public static void pauseComponents() {
        for (UIComponent uiComponent : allUIComponents) {
            uiComponent.pause();
        }
    }

    public static void resumeComponents() {
        for (UIComponent uiComponent : allUIComponents) {
            uiComponent.resume();
        }
    }

    public static void disposeComponents() {
        for (UIComponent uiComponent : allUIComponents) {
            uiComponent.dispose();
        }

        allUIComponents.clear();
    }

    protected boolean isWithinComponent(float localX, float localY) {
        return (localX >= 0 && localX <= w.value()
                && localY >= 0 && localY <= h.value());
    }


    public final boolean isVisible() {
        return isVisible;
    }

    public final void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    // --- Event ---
    private int eventScreenX;
    private int eventScreenY;
    private Runnable event;

    public final void setEvent(Runnable event) {
        this.event = event;
    }

    protected final void callEvent(int eventScreenX, int eventScreenY) {
        this.eventScreenX = eventScreenX;
        this.eventScreenY = eventScreenY;

        if(event != null) {
            event.run();
        }
    }

    public final int getEventScreenX() {
        return eventScreenX;
    }

    public final int getEventScreenY() {
        return eventScreenY;
    }

    protected void componentInitialized() {
    }

    protected void render() {
    }

    protected void update(long deltaTimeInMs) {
    }

    protected boolean keyTyped(char character) {
        return false;
    }

    protected boolean touchDown(int x, int y, int pointer) {
        return false;
    }

    protected boolean touchUp(int x, int y, int pointer) {
        return false;
    }

    protected boolean touchDragged(int x, int y, int pointer) {
        return false;
    }

    protected boolean doubleClick(int x, int y, int pointer) {
        return true;
    }

    protected boolean longPress(int x, int y) {
        return true;
    }

    protected boolean mouseMoved(int x, int y) {
        return false;
    }

    protected boolean scrolled(int amount) {
        return false;
    }

    protected void signalReceived(UISignal signal) {
    }

    protected void pause() {
    }

    protected void resume() {
    }

    protected void dispose() {
    }
}
