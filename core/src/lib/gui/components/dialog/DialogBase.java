package lib.gui.components.dialog;

import lib.gui.GUIConfiguration;
import lib.gui.annotations.dialog.DialogRatio;
import lib.gui.annotations.dialog.HideCloseButton;
import lib.gui.annotations.dialog.Scale;
import lib.gui.annotations.text.TextKey;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.panel.Panel;
import lib.gui.layout.values.DynamicValue;
import lib.gui.layout.values.FixedValue;
import lib.gui.resources.Text;

public abstract class DialogBase extends Panel {

    protected Text title;

    protected boolean hideCloseButton;
    protected float dialogRatio = 1f;
    private float dialogScale = 1f;

    private boolean isOpen;

    private int animationProgressInMs;

    // Dialog configs
    protected int animationDurationMs = 200;
    protected float baseDialogScale = 1f;
    protected float topBorderFraction = 0.1f;
    protected float leftBorderFraction = 0.1f;
    protected float rightBorderFraction = 0.1f;
    protected float bottomBorderFraction = 0.1f;

    // Total dialog (with border) bounds and position
    protected float topBorderSize;
    protected float leftBorderSize;
    protected float rightBorderSize;
    protected float bottomBorderSize;
    protected float xDialog;
    protected float yDialog;
    protected float wDialog;
    protected float hDialog;

    protected DialogBase() {

        // Enable processAllInput
        processAllInput = true;

        // Create instances for dialog bounds
        x = new FixedValue(0);
        y = new FixedValue(0);
        w = new FixedValue(0);
        h = new FixedValue(0);

        // Get dialog title
        title = new Text();
        if (this.getClass().isAnnotationPresent(TextValue.class)) {
            TextValue textValue = this.getClass().getAnnotation(TextValue.class);
            title.setValue(textValue.value());
        }
        if (this.getClass().isAnnotationPresent(TextKey.class)) {
            TextKey textKey = this.getClass().getAnnotation(TextKey.class);
            title.setKey(textKey.value());
        }

        if (getClass().isAnnotationPresent(DialogRatio.class)) {
            DialogRatio ratioAnnotation = this.getClass().getAnnotation(DialogRatio.class);
            dialogRatio = (float) ratioAnnotation.value();
        }

        if (getClass().isAnnotationPresent(Scale.class)) {
            Scale scaleAnnotation = this.getClass().getAnnotation(Scale.class);
            dialogScale = (float) scaleAnnotation.value();

            if(GUIConfiguration.landscapeMode) {
                dialogScale = 1f / dialogScale;
            }
        }

        if (getClass().isAnnotationPresent(HideCloseButton.class)) {
            hideCloseButton = true;
        }

        // Register dialog
        Dialogs.registerDialog(this);
    }

    void setIsOpen(boolean isOpen) {
        this.isOpen = isOpen;
    }

    boolean isOpen() {
        return isOpen;
    }

    boolean shouldBeRendered() {
        return isOpen() || animationProgressInMs > 0;
    }

    @Override
    public void updatePanel(long deltaTimeInMs) {

        if (isOpen && animationProgressInMs < animationDurationMs) {
            animationProgressInMs += deltaTimeInMs;

            if (animationProgressInMs > animationDurationMs)
                animationProgressInMs = animationDurationMs;

        } else if (!isOpen && animationProgressInMs > 0) {
            animationProgressInMs -= deltaTimeInMs;

            if (animationProgressInMs < 0)
                animationProgressInMs = 0;
        }

        update(deltaTimeInMs);
    }

    /**
     * These values are pre-scale, dialogScale and getAnimationScale
     * are used to calculate the final dialog size
     *
     * The dialog will be centered.
     */
    protected void setDialogSize(float panelWidth, float panelHeight) {
        panelWidth *= dialogScale * getAnimationScale();
        panelHeight *= dialogScale * getAnimationScale();

        float fractionBase = GUIConfiguration.landscapeMode ? panelHeight : panelWidth;

        topBorderSize = fractionBase * topBorderFraction;
        leftBorderSize = fractionBase * leftBorderFraction;
        rightBorderSize = fractionBase * rightBorderFraction;
        bottomBorderSize = fractionBase * bottomBorderFraction;

        wDialog = panelWidth + leftBorderSize + rightBorderSize;
        hDialog = panelHeight + topBorderSize + bottomBorderSize;
        xDialog = (DynamicValue.SCREEN_W.value() - wDialog) / 2f;
        yDialog = (DynamicValue.SCREEN_H.value() - hDialog) / 2f;

        w.updateValue(panelWidth);
        h.updateValue(panelHeight);
        x.updateValue(xDialog + leftBorderSize);
        y.updateValue(yDialog + topBorderSize);
    }

    protected boolean isWithinDialog(float localX, float localY) {
        return (localX >= (xDialog - x.value()) && localX <= (xDialog - x.value()) + wDialog
                && localY >= (yDialog - y.value()) && localY <= (yDialog - y.value()) + hDialog);
    }

    protected float getAnimationProgress() {
        return animationProgressInMs / (float) animationDurationMs;
    }

    protected abstract float getAnimationScale();
    protected abstract float getAnimationAlpha();

    public Text getTitle() {
        return title;
    }

    /**
     * Return if the dialog should be closed
     * when the back key is pressed
     */
    protected boolean onBackPressed() {
        return true;
    }

    protected void dialogOpened() {
    }

    protected void dialogClosed() {
    }
}
