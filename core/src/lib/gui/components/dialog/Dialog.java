package lib.gui.components.dialog;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lib.gui.Draw;
import lib.gui.GUIConfiguration;
import lib.gui.Renderer2D;
import lib.gui.components.basic.ImageButton;
import lib.gui.layout.values.DynamicValue;
import lib.gui.layout.values.FixedValue;
import lib.gui.resources.TextureManager;
import lib.gui.util.TextRenderer;

public abstract class Dialog extends DialogBase {

    private static TextureRegion dialogBackground;
    private static TextureRegion dialogCloseButton;
    private TextRenderer titleText;

    private boolean touchDownOutsideOfDialog;

    private final ImageButton closeButton;

    /**
     * Setting this value to true, limits the dialog size
     * depending on the GUIConfiguration.landscapeMode value
     */
    protected boolean limitDialogSize = false;

    protected Dialog() {
        TextureManager textureManager = Renderer2D.getTextureManager();

        if (dialogBackground == null || dialogCloseButton == null) {
            dialogBackground = textureManager.getTextureRegion(GUIConfiguration.dialogBackgroundTextureRegionName);
            dialogCloseButton = textureManager.getTextureRegion(GUIConfiguration.dialogCloseButtonTextureRegionName);
        }

        titleText = new TextRenderer();
        titleText.setTextColor(Color.WHITE);

        closeButton = new ImageButton(dialogCloseButton);
        closeButton.setVisible(!hideCloseButton);
        closeButton.setEvent(() -> Dialogs.close(this));
        closeButton.x = new FixedValue(0);
        closeButton.y = new FixedValue(0);
        closeButton.w = new FixedValue(0);
        closeButton.h = new FixedValue(0);

        super.baseDialogScale = 0.85f;
        super.topBorderFraction = 0.1f;
        super.leftBorderFraction = 0.019f;
        super.rightBorderFraction = 0.027f;
        super.bottomBorderFraction = 0.028f;
    }

    @Override
    public void updatePanel(long deltaTimeInMs) {
        super.updatePanel(deltaTimeInMs);

        float screenRatio = DynamicValue.SCREEN_H.value() / DynamicValue.SCREEN_W.value();

        float borderWidthMultiplier = 1.01f;
        float borderHeightMultiplier = 1.1f;

        float totalRatio = dialogRatio * borderHeightMultiplier / borderWidthMultiplier;

        float panelWidth;
        float panelHeight;

        if (totalRatio > screenRatio) {
            panelHeight = DynamicValue.SCREEN_H.value();
            panelWidth = panelHeight / dialogRatio;
        } else {
            panelWidth = DynamicValue.SCREEN_W.value();
            panelHeight = panelWidth * dialogRatio;
        }

        if(limitDialogSize) {
            if(GUIConfiguration.landscapeMode) {
                panelHeight = Math.min(DynamicValue.SCREEN_W.value() / 1.7f, DynamicValue.SCREEN_H.value());
                panelWidth = panelHeight / dialogRatio;
            } else {
                panelWidth = Math.min(DynamicValue.SCREEN_H.value() / 1.7f, DynamicValue.SCREEN_W.value());
                panelHeight = panelWidth * dialogRatio;
            }
        }

        panelHeight *= baseDialogScale;
        panelWidth *= baseDialogScale;

        setDialogSize(panelWidth, panelHeight);
    }

    @Override
    public final void renderPanel() {
        Draw.disableComponentTranslation();
        // "Shadow" in Background
        Draw.setColor(0, 0, 0, 0.5f);
        Draw.rect(0, 0, DynamicValue.SCREEN_W.value(), DynamicValue.SCREEN_H.value());
        Draw.resetColor();

        // Dialog Background
        Draw.imageNinePatch(dialogBackground, xDialog, yDialog, wDialog, hDialog,
                GUIConfiguration.dialogBackgroundNinePatchBorder, topBorderSize);

        if (!hideCloseButton) {
            closeButton.w.updateValue(topBorderSize);
            closeButton.h.updateValue(topBorderSize);
            closeButton.x.updateValue(w.value() + leftBorderSize - closeButton.w.value() * 1.1f);
            closeButton.y.updateValue(-closeButton.h.value());
            closeButton.setRenderScale(0.8f);
        }

        // Title
        titleText.setText(title);
        titleText.setWidth(wDialog * 0.7f);
        titleText.setHeight(topBorderSize * 0.35f);
        titleText.render(xDialog, yDialog, wDialog, topBorderSize);

        Draw.enableComponentTranslation();

        render();
    }

    @Override
    protected final float getAnimationScale() {
        double baseScale = Math.sin(getAnimationProgress() * Math.PI * 0.715) * 1.27;

        final double animationScaleDivisor = 20;

        return (float) (baseScale / animationScaleDivisor + (animationScaleDivisor - 1) / animationScaleDivisor);
    }

    @Override
    protected float getAnimationAlpha() {
        return Math.min(getAnimationProgress() * 2f, 1f);
    }

    @Override
    public final boolean touchDownPanel(int x, int y, int pointer) {

        if (!isWithinDialog(x, y)) {
            touchDownOutsideOfDialog = true;
        }

        return touchDown(x, y, pointer);
    }

    @Override
    public final boolean touchUpPanel(int x, int y, int pointer) {

        if (touchDownOutsideOfDialog && !isWithinDialog(x, y)) {
            Dialogs.close(this);
        }
        touchDownOutsideOfDialog = false;

        return touchUp(x, y, pointer);
    }
}
