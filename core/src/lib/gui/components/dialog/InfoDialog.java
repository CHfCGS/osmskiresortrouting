package lib.gui.components.dialog;

import com.badlogic.gdx.graphics.Color;
import lib.gui.annotations.position.Alignment;
import lib.gui.annotations.position.Height;
import lib.gui.annotations.position.PosX;
import lib.gui.annotations.position.PosY;
import lib.gui.annotations.position.Width;
import lib.gui.annotations.text.TextValue;
import lib.gui.components.basic.TextButton;
import lib.gui.util.TextRenderer;

class InfoDialog extends Dialog {

    private final TextRenderer textRenderer;

    @PosX(align = Alignment.CENTER)
    @PosY(w = 0.05, align = Alignment.BOTTOM)
    @Width(w = 0.3)
    @Height(w = 0.12)
    @TextValue("Ok")
    private final TextButton ok;

    InfoDialog() {
        textRenderer = new TextRenderer();
        textRenderer.setTextColor(Color.DARK_GRAY);
        limitDialogSize = true;

        ok = new TextButton(TextButton.COLOR_BLUE);
        ok.setEvent(() -> Dialogs.close(this));
    }

    public void updateContent(String title, String text) {
        super.title.setValue(title);
        textRenderer.setText(text);
        textRenderer.wrapText(true);
    }

    @Override
    protected void render() {
        textRenderer.setWidth(w.value() * 0.85f);
        textRenderer.setHeight(w.value() * 0.035f);
        textRenderer.render(0, 0, w.value(), h.value() - 0.25f * w.value());

        dialogRatio = (textRenderer.getTextH() / w.value()) + 0.4f;
    }
}
