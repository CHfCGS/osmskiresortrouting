package lib.gui.annotations.dialog;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * If a dialog has this annotation the dialog size will be
 * set automatically by the DialogRenderer
 *
 * With this annotation the ratio of the dialog can be set.
 *
 *  If the value is 2, the _height_ of the dialog
 *  will be twice as large as the _width_.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DialogRatio {
    double value() default 1;
}