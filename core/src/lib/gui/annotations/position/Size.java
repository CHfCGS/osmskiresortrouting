package lib.gui.annotations.position;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Instead of writing
 * \@Width(w = 0.5)
 * \@Height(w = 0.5)
 * <p>
 * \@Size(w = 0.5)
 * can be used.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Size {
    double w() default 0;

    double h() default 0;

    double screenW() default 0;

    double screenH() default 0;

    String term() default "";
}