package lib.gui.annotations.position;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface PosY {
    double w() default 0;

    double h() default 0;

    double screenW() default 0;

    double screenH() default 0;

    String term() default "";

    Alignment align() default Alignment.TOP;
}