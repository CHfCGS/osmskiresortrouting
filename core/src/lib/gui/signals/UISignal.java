package lib.gui.signals;

public class UISignal {

    // Predefined Signals
    public static final UISignal RESET_UI = new UISignal("reset_ui");

    private String id;

    public UISignal (String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return (this.hashCode() == o.hashCode());
    }

    public String getId() {
        return id;
    }
}
