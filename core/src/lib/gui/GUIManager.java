package lib.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.LifecycleListener;

import java.util.ArrayList;
import java.util.List;

import lib.gui.components.UIComponent;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screens;
import lib.gui.resources.TranslationManager;
import lib.gui.signals.UISignal;

public class GUIManager implements InputProcessor, LifecycleListener {

    private static GUIManager instance;

    private GUIConfiguration config;
    private Renderer2D renderer2D;
    private Renderer3D renderer3D;

    private int[] inputButton;
    private List<Integer> currentTouches;

    private GUIManager(GUIConfiguration config) {
        this.config = config;

        renderer2D = Renderer2D.instance();
        renderer2D.setUp(config);

        if(config.useRenderer3D) {
            renderer3D = Renderer3D.instance();
            renderer3D.setUp();
        }

        TranslationManager.setUp(config);

        inputButton = new int[Math.min(Gdx.input.getMaxPointers(), 100)];
        currentTouches = new ArrayList<>(Math.min(Gdx.input.getMaxPointers(), 100));

        Gdx.input.setInputProcessor(this);
        Gdx.app.addLifecycleListener(this);
    }

    /**
     * Can only be called after setUp
     */
    public static GUIManager instance() {
        return instance;
    }

    public static void setUp() {
        setUp(GUIConfiguration.createDefault());
    }

    public static void setUp(GUIConfiguration config) {
        instance = new GUIManager(config);
    }

    public void render() {
        update();
        renderer2D.render();
    }

    private long lastUpdateTime = 0;

    private void update() {

        if (lastUpdateTime == 0) {
            lastUpdateTime = System.currentTimeMillis();
        }

        long deltaTimeInMs = System.currentTimeMillis() - lastUpdateTime;

        Screens.update(deltaTimeInMs);
        Dialogs.update(deltaTimeInMs);

        lastUpdateTime = System.currentTimeMillis();
    }

    public static void broadcastSignal(UISignal signal) {
        UIComponent.broadcastSignal(signal);
    }

    /**
     * Must be called in overridden resize method of ApplicationAdapter
     */
    public void resize(int width, int height) {
        renderer2D.resize(width, height);

        if(config.useRenderer3D) {
            renderer3D.resize(width, height);
        }
    }

    @Override
    public void pause() {
        UIComponent.pauseComponents();
    }

    @Override
    public void resume() {
        UIComponent.resumeComponents();
    }

    @Override
    public void dispose() {
        renderer2D.dispose();

        if(config.useRenderer3D) {
            renderer3D.dispose();
        }

        UIComponent.disposeComponents();
    }

    /**
     * @return false, if onBackPressed should be handled by the android activity.
     * This will probably finish the current activity
     */
    public boolean onBackPressed() {

        if (renderer2D.isLoadingResources()) return true;

        if (Dialogs.onBackPressed()) return true;
        return Screens.onBackPressed();
    }

    public GUIConfiguration getGUIConfig() {
        return config;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if (Dialogs.keyTyped(character))
            return true;

        return Screens.keyTyped(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        inputButton[pointer] = button;
        currentTouches.add(pointer);

        if (Dialogs.touchDown(screenX, screenY, pointer))
            return true;

        return Screens.touchDown(screenX, screenY, pointer);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        inputButton[pointer] = button;
        currentTouches.remove(Integer.valueOf(pointer));

        if (Dialogs.touchUp(screenX, screenY, pointer))
            return true;

        return Screens.touchUp(screenX, screenY, pointer);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (Dialogs.touchDragged(screenX, screenY, pointer))
            return true;

        return Screens.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (Dialogs.mouseMoved(screenX, screenY))
            return true;

        return Screens.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        if (Dialogs.scrolled((int) amountY))
            return true;

        return Screens.scrolled((int) amountY);
    }

    public int getInputButton(int pointer) {
        return inputButton[pointer];
    }

    public int getPointerCount() {
        return currentTouches.size();
    }

    public int getTouchPointer(int index) {
        return currentTouches.get(index);
    }
}
