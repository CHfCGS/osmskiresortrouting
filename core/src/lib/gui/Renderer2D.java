package lib.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonBatch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;
import com.badlogic.gdx.utils.BufferUtils;
import lib.gui.components.dialog.Dialogs;
import lib.gui.components.screen.Screens;
import lib.gui.resources.TextureManager;

import java.nio.IntBuffer;

/**
 * Use for 2d rendering
 */
public class Renderer2D {

    private Renderer2D() {
    }

    private static Renderer2D instance;

    public static Renderer2D instance() {
        if (instance == null)
            instance = new Renderer2D();

        return instance;
    }

    private GUIConfiguration config;

    private OrthographicCamera camera;

    private ModelBatch modelBatch;

    private PolygonSpriteBatch batch;
    private ShapeRenderer shapeRenderer;

    private TextureManager textureManager;

    /**
     * Initializes all variables required for 2d rendering
     */
    void setUp(GUIConfiguration config) {
        this.config = config;
        batch = new PolygonSpriteBatch(20000);

        modelBatch = new ModelBatch();

        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        batch.setProjectionMatrix(camera.combined);

        textureManager = new TextureManager(config);
        textureManager.load();

        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);

        Draw.init(batch, textureManager);
    }

    /**
     * Must be called in overridden render method of ApplicationAdapter
     */
    void render() {
        if (textureManager.isLoading) return;

        Gdx.gl.glClearColor(config.backgroundColor.r, config.backgroundColor.g, config.backgroundColor.b, 1);

        if(config.useRenderer3D) {
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        } else {
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }

        // Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        // Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        // Gdx.gl.glDepthMask(true);
        // Gdx.gl.glDepthFunc(GL20.GL_LESS);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);

        batch.begin();

        Screens.render();
        Dialogs.render();

        batch.end();
    }

    void resize(int width, int height) {
        camera.setToOrtho(true, width, height);
    }

    void dispose() {
        batch.dispose();
        shapeRenderer.dispose();
        textureManager.dispose();
    }

    boolean isLoadingResources() {
        return textureManager.isLoading;
    }

    public static PolygonBatch getBatch() {
        return instance.batch;
    }

    public static ModelBatch getModelBatch() {
        return instance.modelBatch;
    }

    /**
     * Only use it if necessary.
     * Does not work by default
     * (begin() and end() needs to be called)
     */
    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }

    public static TextureManager getTextureManager() {
        return instance.textureManager;
    }

    public Camera getCamera() {
        return camera;
    }

    /**
     * Limits the area where things are drawn
     * <p>
     * Attention: Causes a render call
     * (Don't call this method too often during rendering)
     */
    public void setClipBounds(float x, float y, float w, float h) {
        batch.flush();

        Rectangle scissors = new Rectangle();
        Rectangle clipBounds = new Rectangle(x, y, w, h);
        ScissorStack.calculateScissors(camera, batch.getTransformMatrix(), clipBounds, scissors);
        ScissorStack.pushScissors(scissors);
    }

    /**
     * Resets the clip bound limit
     */
    public void resetClipBounds() {
        batch.flush();
        ScissorStack.popScissors();
    }

    public int getGlMaxTextureSize() {
        IntBuffer intBuffer = BufferUtils.newIntBuffer(16);
        Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, intBuffer);
        return intBuffer.get();
    }
}
