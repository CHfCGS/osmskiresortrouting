package lib.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.PolygonBatch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import lib.gui.components.UIComponent;
import lib.gui.layout.values.DynamicValue;
import lib.gui.resources.TextureManager;

public class Draw {

    private static PolygonBatch batch;

    private static BitmapFont font;
    private static TextureRegion white_pixels;
    private static TextureRegion shadow_horizontal;
    private static TextureRegion shadow_vertical;
    private static TextureRegion rounded_rect;

    private static final boolean flipX = false;
    private static final boolean flipY = true;

    public static void init(PolygonSpriteBatch batch, TextureManager textureManager) {
        Draw.batch = batch;

        white_pixels = textureManager.getTextureRegion(GUIConfiguration.whitePixelsTextureRegionName);
        shadow_horizontal = textureManager.getTextureRegion(GUIConfiguration.shadowHorizontalTextureRegionName);
        shadow_vertical = textureManager.getTextureRegion(GUIConfiguration.shadowVerticalTextureRegionName);
        rounded_rect = Renderer2D.getTextureManager().getTextureRegion(GUIConfiguration.roundedRectTextureRegionName);
        font = textureManager.getFont();
    }

    private static Color tempColor = new Color(Color.WHITE);

    private static float renderAlphaValue = 1f;

    private static float customAlphaValue = 1f;
    private static float dialogAlphaValue = 1f;


    /**
     * Do not call
     */
    public static void setDialogAlpha(float dialogAlphaValue) {
        Draw.dialogAlphaValue = dialogAlphaValue;
        updateAlphaColor();
    }

    public static void setAlpha(float customAlphaValue) {
        Draw.customAlphaValue = customAlphaValue;
        updateAlphaColor();
    }

    private static void updateAlphaColor() {
        Draw.renderAlphaValue = Draw.dialogAlphaValue * Draw.customAlphaValue;
        setColor(batch.getColor().r, batch.getColor().g, batch.getColor().b, 1f);
    }

    public static float getRenderAlphaValue() {
        return renderAlphaValue;
    }

    /**
     * This color is used by e.g method Draw.rect
     */
    public static void setColor(Color color) {
        Draw.tempColor.set(color);
        Draw.tempColor.a *= Draw.renderAlphaValue;
        batch.setColor(tempColor);
    }

    public static void setColor(float r, float g, float b, float a) {
        Draw.tempColor.set(r, g, b, a * Draw.renderAlphaValue);
        batch.setColor(tempColor);
    }

    public static void resetColor() {
        setColor(Color.WHITE);
    }

    private static float xTranslation = 0;
    private static float yTranslation = 0;

    private static float xTranslationComponent = 0;
    private static float yTranslationComponent = 0;

    private static float xTotalTranslation = 0;
    private static float yTotalTranslation = 0;


    public static void setCameraTranslation(float x, float y) {
        xTranslation = x;
        yTranslation = y;
        enableComponentTranslation();
    }

    public static void setComponentTranslation(float x, float y) {
        xTranslationComponent = x;
        yTranslationComponent = y;
        enableComponentTranslation();
    }

    public static void addComponentTranslation(float x, float y) {
        xTranslationComponent += x;
        yTranslationComponent += y;
        enableComponentTranslation();
    }

    public static void disableComponentTranslation() {
        xTotalTranslation = xTranslation;
        yTotalTranslation = yTranslation;
    }

    public static void enableComponentTranslation() {
        xTotalTranslation = xTranslation + xTranslationComponent;
        yTotalTranslation = yTranslation + yTranslationComponent;
    }

    // Rectangles

    /**
     * Fills the parent component with the current color
     */
    public static void rect(UIComponent c) {
        rect(0, 0, c.w.value(), c.h.value());
    }

    /**
     * Fills the parent component with the color
     */
    public static void rect(UIComponent c, Color color) {
        rect(0, 0, c.w.value(), c.h.value(), color);
    }

    public static void rect(float x, float y, float w, float h, Color color) {
        setColor(color);
        rect(x, y, w, h);
        resetColor();
    }

    public static void rect(float x, float y, float w, float h) {
        image(white_pixels, x, y, w, h, 2, 2, 1, 1);
    }

    public static void roundedRect(float x, float y, float w, float h, float edgeSize, Color color) {
        setColor(color);
        roundedRect(x, y, w, h, edgeSize, 1f);
        resetColor();
    }

    public static void roundedRect(float x, float y, float w, float h, float edgeSize, float scale) {
        Draw.imageNinePatch(rounded_rect,
                x + w * (1f - scale) * 0.5f,
                y + h * (1f - scale) * 0.5f,
                w * scale, h * scale, GUIConfiguration.roundedRectNinePatchBorder, edgeSize * scale);
    }

    // Lines

    public static void line(Vector2 pos1, Vector2 pos2, float thickness, Color color) {
        setColor(color);
        line(pos1, pos2, thickness);
        resetColor();
    }

    public static void line(Vector2 pos1, Vector2 pos2, float thickness) {
        line(pos1.x, pos1.y, pos2.x, pos2.y, thickness);
    }

    public static void line(float x1, float y1, float x2, float y2, float thickness, Color color) {
        setColor(color);
        line(x1, y1, x2, y2, thickness);
        resetColor();
    }


    public static void line(float x1, float y1, float x2, float y2, float thickness) {

        float length = calculateDistance(x1, y1, x2, y2);

        float dx = x1;
        float dy = y1;
        dx = dx - x2;
        dy = dy - y2;

        //float angle = MathUtils.radiansToDegrees * MathUtils.atan2(dy, dx);
        float angle = (float) (MathUtils.radiansToDegrees * Math.atan2(dy, dx));
        angle = angle - 180;

        x1 += xTotalTranslation;
        y1 += yTotalTranslation;

        TextureRegion t = white_pixels;
        batch.draw(t.getTexture(), x1, y1, 0f, thickness * 0.5f, length /*+ thickness / 2*/, thickness,
                1f, 1f, angle,
                t.getRegionX() + 2, t.getRegionY() + 2, 1, 1,
                false, false);
    }

    private static float calculateDistance(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }

    // Images

    public static void image(TextureRegion t, float x, float y, float w, float h) {
        image(t, x, y, w, h, 0, 0, t.getRegionWidth(), t.getRegionHeight(), 0f, 1f);
    }

    public static void image(TextureRegion t, float x, float y, float w, float h, int srcX, int srcY, int srcWidth, int srcHeight) {
        image(t, x, y, w, h, srcX, srcY, srcWidth, srcHeight, 0f, 1f);
    }

    public static void image(TextureRegion t, float x, float y, float w, float h, float scale) {
        image(t, x, y, w, h, 0, 0, t.getRegionWidth(), t.getRegionHeight(), 0f, scale);
    }

    public static void image(TextureRegion t, float x, float y, float w, float h, int srcX, int srcY, int srcWidth, int srcHeight, float rotation, float scale) {
        x += xTotalTranslation;
        y += yTotalTranslation;

        batch.draw(t.getTexture(), x, y, w / 2, h / 2, w, h, scale, scale, rotation,
                t.getRegionX() + srcX, t.getRegionY() + srcY, srcWidth, srcHeight, flipX, flipY);
    }

    public static void image(Texture t, float x, float y, float w, float h, float scale) {
        image(t, x, y, w, h, 0, 0, t.getWidth(), t.getHeight(), 0f, scale);
    }

    public static void image(Texture t, float x, float y, float w, float h, int srcX, int srcY, int srcWidth, int srcHeight, float rotation, float scale) {
        x += xTotalTranslation;
        y += yTotalTranslation;

        batch.draw(t, x, y, w / 2, h / 2, w, h, scale, scale, rotation, srcX, srcY, srcWidth, srcHeight, flipX, flipY);
    }

    public static void imageNinePatch(TextureRegion t, float x, float y, float w, float h, int border, float borderSize) {
        final int srcX1 = 0;
        final int srcX2 = border;
        final int srcX3 = t.getRegionWidth() - border;
        final int srcY1 = 0;
        final int srcY2 = border;
        final int srcY3 = t.getRegionHeight() - border;

        final int srcWEdge = border;
        final int srcWMiddle = t.getRegionWidth() - border * 2;
        final int srcHEdge = border;
        final int srcHMiddle = t.getRegionHeight() - border * 2;

        // Top Left
        image(t, x, y, borderSize, borderSize, srcX1, srcY1, srcWEdge, srcHEdge);
        // Top Center
        image(t, x + borderSize, y, w - borderSize * 2, borderSize, srcX2, srcY1, srcWMiddle, srcHEdge);
        // Top Right
        image(t, x + w - borderSize, y, borderSize, borderSize, srcX3, srcY1, srcWEdge, srcHEdge);
        // Middle Left
        image(t, x, y + borderSize, borderSize, h - borderSize * 2, srcX1, srcY2, srcWEdge, srcHMiddle);
        // Middle Center
        image(t, x + borderSize, y + borderSize, w - borderSize * 2, h - borderSize * 2, srcX2, srcY2, srcWMiddle, srcHMiddle);
        // Middle Right
        image(t, x + w - borderSize, y + borderSize, borderSize, h - borderSize * 2, srcX3, srcY2, srcWEdge, srcHMiddle);
        // Bottom Left
        image(t, x, y + h - borderSize, borderSize, borderSize, srcX1, srcY3, srcWEdge, srcHEdge);
        // Bottom Center
        image(t, x + borderSize, y + h - borderSize, w - borderSize * 2, borderSize, srcX2, srcY3, srcWMiddle, srcHEdge);
        // Bottom Right
        image(t, x + w - borderSize, y + h - borderSize, borderSize, borderSize, srcX3, srcY3, srcWEdge, srcHEdge);
    }

    // Shadow

    private static DynamicValue shadowWidth;

    public static void setShadowWidth(DynamicValue shadowWidth) {
        Draw.shadowWidth = shadowWidth;
    }

    public static void shadow(float x, float y, float length, boolean vertical) {
        shadow(x, y, length, vertical, false);
    }

    public static void shadow(float x, float y, float length, boolean vertical, boolean invert) {
        if (vertical) {
            if (invert)
                image(shadow_vertical, x, y, -shadowWidth.value(), length);
            else
                image(shadow_vertical, x, y, shadowWidth.value(), length);
        } else {
            if (invert)
                image(shadow_horizontal, x, y, length, -shadowWidth.value());
            else
                image(shadow_horizontal, x, y, length, shadowWidth.value());
        }
    }

    public static void shadow(float x, float y, float w, float h, boolean vertical) {
        if (vertical) {
            image(shadow_vertical, x, y, w, h);
        } else {
            image(shadow_horizontal, x, y, w, h);
        }
    }

    // Font

    private static int currentAlignment;

    public static void setTextAlignment(int alignment) {
        currentAlignment = alignment;
    }

    public static void setTextSize(float size) {
        float scale = size / GUIConfiguration.fontResolution;

        if (scale <= 0) scale = 0.0001f;

        font.getData().setScale(scale, scale);
    }

    public static GlyphLayout font(float x, float y, float w, String text, Color color) {
        setColor(color);
        GlyphLayout glyphLayout = font(x, y, w, text);
        resetColor();

        return glyphLayout;
    }

    public static GlyphLayout font(float x, float y, float w, String text) {
        x += xTotalTranslation;
        y += yTotalTranslation;

        font.setColor(batch.getColor());
        return font.draw(batch, text, x, y, w, currentAlignment, true);
    }

    public static void font(float x, float y, GlyphLayout layout) {
        x += xTotalTranslation;
        y += yTotalTranslation;

        font.setColor(batch.getColor());
        font.draw(batch, layout, x, y);
    }
}
