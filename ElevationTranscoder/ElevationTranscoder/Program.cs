﻿using ElevationServer;
using System;
using System.IO;
using System.Threading.Tasks;

namespace ElevationTranscoder {
    public class Program {
        private const string DATA_OUT_DIR = @"F:\Programme\bda\Bulk Order srtm-sweu\converted";
        private const string DATA_OUT_FORMAT = "{0}-{1}-{2}.bin";

        private const int ZOOM_LEVEL = 8;
        private const int MIN_X = 127;
        private const int MAX_X = 140; //exclusive
        private const int MIN_Y = 87;
        private const int MAX_Y = 94; //exclusive

        private const int SAMPLE_ZOOM = 20; //angular sample distance is 360° / 2^n
        private const int MIN_ZOOM = 15;

        static void Main(string[] args) {
            Directory.CreateDirectory(DATA_OUT_DIR);
            int numTiles = (MAX_X - MIN_X) * (MAX_Y - MIN_Y);
            int tileCounter = 1;
            for (int i = MIN_X; i < MAX_X; i++) {
                for (int j = MIN_Y; j < MAX_Y; j++) {
                    Console.WriteLine("Processing " + (tileCounter++) + "/" + numTiles + " (" + i + ", " + j + ")");
                    ProcessTile(ZOOM_LEVEL, i, j);
                }
            }
        }

        private static void ProcessTile(int zoom, int x, int y) {
            var rawEdgeLength = 1 << (SAMPLE_ZOOM - zoom); //edge length of raw output data in samples
            var additionalSamples = 1 << (SAMPLE_ZOOM - MIN_ZOOM); //additional samples to add around the edges for the purpose of gradient preservation
            var edgeLength = rawEdgeLength + 1 + 2 * additionalSamples; //total edge length
            var tileWidth = 1d / (1 << zoom); //tile width in map coordinates
            var additionalEdgeWidth = additionalSamples * tileWidth / rawEdgeLength; //additional sample edge width in map coordinates
            var sampleDivisor = (double)(1 << SAMPLE_ZOOM); //angular sample distance is 360° / this
            var xOffset = x * tileWidth - additionalEdgeWidth; //X position of this tile's NW corner in map coordinates
            var yOffset = y * tileWidth - additionalEdgeWidth; //Y position of this tile's NW corner in map coordinates
            var path = Path.Combine(DATA_OUT_DIR, string.Format(DATA_OUT_FORMAT, zoom, x, y));
            var voidCount = 0;

            using (var fs = File.OpenWrite(path)) {
                for (int i = 0; i < edgeLength; i++) {
                    var sx = xOffset + i / sampleDivisor; //sample X position in map coordinates
                    for (int j = 0; j < edgeLength; j++) {
                        var sy = yOffset + j / sampleDivisor; //sample Y position in map coordinates
                        var elev = DTEDCache.GetElevation(MapYToLat(sy), MapXToLon(sx)); //get interpolated elevation at corresponding lat/lon
                        var elevInt = (short)Math.Round(elev); //round to integer
                        if (elevInt == -32768) voidCount++; //detect void
                        fs.WriteByte((byte)(elevInt >> 8)); //write to file
                        fs.WriteByte((byte)elevInt);
                    }
                }
            }
            if (voidCount > 0) Console.WriteLine("Tile " + x + ", " + y +" contains " + voidCount + " void samples");
        }

        private static double MapXToLon(double x) {
            return 360d * x - 180d;
        }

        private static double MapYToLat(double y) {
            return 180d * Math.Atan(Math.Sinh(Math.PI - (2d * Math.PI * y))) / Math.PI;
        }
    }
}
