﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Windows.Markup;

namespace ElevationServer {
    public class DTEDReader {
        private const int DATA_START_OFFSET = 0xD64;
        private const string FILENAME_FORMAT = "{0}{1:D2}_{2}{3:D3}_1arc_v3.dt2";
        private const string FOLDER_PATH = @"F:\Programme\bda\Bulk Order srtm-sweu\SRTM 1 Arc-Second Global";
        private const string VF_FILENAME_FORMAT = "{0}{1:D2}{2}{3:D3}.hgt";
        private const string VF_FOLDER_PATH = @"F:\Programme\bda\Bulk Order srtm-sweu\voidfill";

        public static byte[][] LoadData(int lat, int lon) {
            var path = Path.Combine(FOLDER_PATH, string.Format(FILENAME_FORMAT, lat < 0 ? 's' : 'n', Math.Abs(lat), lon < 0 ? 'w' : 'e', Math.Abs(lon)));
            if (!File.Exists(path)) return null; //no data: return null

            var vfpath = Path.Combine(VF_FOLDER_PATH, string.Format(VF_FILENAME_FORMAT, lat < 0 ? 'S' : 'N', Math.Abs(lat), lon < 0 ? 'W' : 'E', Math.Abs(lon)));
            var vf_present = File.Exists(vfpath); //voidfill data is present?

            byte[][] data = new byte[3601][];
            using (var f = File.OpenRead(path)) { //read data from DTED format into array
                var textReader = new StreamReader(f);
                f.Seek(DATA_START_OFFSET, SeekOrigin.Begin);
                while (f.ReadByte() == 0xAA) {
                    f.Seek(3, SeekOrigin.Current);
                    var lonIndex = (f.ReadByte() << 8) | f.ReadByte();
                    f.Seek(2, SeekOrigin.Current);
                    data[lonIndex] = new byte[7202];
                    f.Read(data[lonIndex], 0, 7202);
                    f.Seek(4, SeekOrigin.Current);
                }
            }

            if (vf_present) {
                using (var vf = File.OpenRead(vfpath)) { //if voidfill data is present: search for voids in array and replace them with voidfill data
                    for (int i = 0; i < 3601; i++) {
                        for (int j = 0; j < 7202; j += 2) {
                            if ((data[i][j] == 0xFF && data[i][j + 1] == 0xFF) || (data[i][j] == 0x80 && data[i][j + 1] == 0x00)) {
                                var offset = (3600 - (j >> 1)) * 7202 + 2 * i;
                                vf.Seek(offset, SeekOrigin.Begin);
                                vf.Read(data[i], j, 2);
                            }
                        }
                    }
                }
            }

            return data;
        }
    }
}
