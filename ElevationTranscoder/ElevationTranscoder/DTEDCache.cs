﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;

namespace ElevationServer {
    public class DTEDCache {
        private const int MAX_CACHE_SIZE = 40; //tiles ~25MB each

        private static readonly object _lock = new object();

        private static uint requestCounter = 0;

        //(lat, long) -> (points, lastUsed)
        private static readonly Dictionary<(int, int), (byte[][], uint)> cache = new Dictionary<(int, int), (byte[][], uint)>();

        /// <summary>
        /// Gets a 1x1 degree chunk of elevation data points.
        /// </summary>
        /// <param name="lat">Latitude of south-west corner in degrees</param>
        /// <param name="lon">Longitude of south-west corner in degrees</param>
        /// <returns></returns>
        public static byte[][] GetData(int lat, int lon) {
            var key = (lat, lon);
            byte[][] data;
            lock (_lock) {
                if (cache.ContainsKey(key)) { //tile already present in cache:
                    var pair = cache[key]; //get from cache
                    pair.Item2 = requestCounter++; //last used: now
                    return pair.Item1;
                }
                data = DTEDReader.LoadData(lat, lon); //otherwise: load from disk
                cache[key] = (data, requestCounter++); //add to cache
                if (cache.Count > MAX_CACHE_SIZE) RemoveOldestFromCache(); //remove oldest data from cache if required
            }
            return data;
        }

        public static double GetElevation(double lat, double lon) {
            var wLat = (int)Math.Floor(lat); //latitude, whole degrees
            var wLon = (int)Math.Floor(lon); //longitude, whole degrees
            var latSample = (lat - wLat) * 3600d; //latitude position within 1°x1° tile in arcseconds
            var lonSample = (lon - wLon) * 3600d; //longitude position within 1°x1° tile in arcseconds
            var wLatSample = (int)Math.Floor(latSample); //Nearest latitude sample index south-west of requested position
            var wLonSample = (int)Math.Floor(lonSample); //Nearest longitude sample index south-west of requested position
            var fLatSample = latSample - wLatSample; //latitude distance of requested position from nearest south-west sample, in arcseconds
            var fLonSample = lonSample - wLonSample; //longitude distance of requested position from nearest south-west sample, in arcseconds
            var invFLatSample = 1d - fLatSample; //latitude distance of requested position from nearest north-east sample, in arcseconds
            var invFLonSample = 1d - fLonSample; //longitude distance of requested position from nearest north-east sample, in arcseconds

            var swFactor = invFLatSample * invFLonSample; //interpolation factor for nearest south-west sample
            var seFactor = invFLatSample * fLonSample; //interpolation factor for nearest south-east sample
            var nwFactor = fLatSample * invFLonSample; //interpolation factor for nearest north-west sample
            var neFactor = fLatSample * fLonSample; //interpolation factor for nearest north-east sample

            var data = GetData(wLat, wLon); //get 1°x1° tile
            if (data == null) return -32768d; //if no data: interpolation impossible, return void value
            var swSample = (short)((data[wLonSample][wLatSample * 2] << 8) | data[wLonSample][wLatSample * 2 + 1]); //retrieve nearest south-west sample elevation
            var seSample = (short)((data[wLonSample + 1][wLatSample * 2] << 8) | data[wLonSample + 1][wLatSample * 2 + 1]); //retrieve nearest south-east sample elevation
            var nwSample = (short)((data[wLonSample][wLatSample * 2 + 2] << 8) | data[wLonSample][wLatSample * 2 + 3]); //retrieve nearest north-west sample elevation
            var neSample = (short)((data[wLonSample + 1][wLatSample * 2 + 2] << 8) | data[wLonSample + 1][wLatSample * 2 + 3]); //retrieve nearest north-east sample elevation

            //check for data void
            if (swSample == -32768 || seSample == -32768 || nwSample == -32768 || neSample == -32768 || swSample == -1 || seSample == -1 || nwSample == -1 || neSample == -1) {
                return -32768d; //interpolation with voids is impossible: return void value
            }

            return swFactor * swSample + seFactor * seSample + nwFactor * nwSample + neFactor * neSample; //return interpolated value
        }

        private static void RemoveOldestFromCache() {
            (int, int)? oldestKey = null;
            uint oldest = uint.MaxValue;
            foreach (var (key, val) in cache) { //find cached tile that has not been used for the longest time
                if (val.Item2 < oldest) {
                    oldest = val.Item2;
                    oldestKey = key;
                }
            }
            if (oldestKey.HasValue) cache.Remove(oldestKey.Value); //remove tile from cache
        }
    }
}
